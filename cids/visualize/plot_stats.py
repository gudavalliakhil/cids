# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Visualization tolbox."""
import json
import os

import matplotlib
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc


def _mean(metric_vec, y, y_predicted, subplot=(6, 3), title=None, save_dir=None):
    assert subplot[0] * subplot[1] == np.size(y, 2)
    y_mean = np.mean(y, 0)
    y_std = np.std(y, 0)
    y_pred_mean = np.mean(y_predicted, 0)
    y_pred_std = np.std(y_predicted, 0)
    fig = plt.figure()
    if title is not None:
        plt.suptitle(title)
    for i in range(np.size(y, 2)):
        plt.subplot(subplot[0], subplot[1], i + 1)
        plt.plot(range(y_mean.shape[0]), y_mean[:, i], "g", label="Real")
        plt.plot(range(y_pred_mean.shape[0]), y_pred_mean[:, i], "r", label="Predicted")
        plt.fill_between(
            range(y_mean.shape[0]),
            y_mean[:, i] - y_std[:, i],
            y_mean[:, i] + y_std[:, i],
            color="g",
            alpha=0.3,
        )
        plt.fill_between(
            range(y_pred_mean.shape[0]),
            y_pred_mean[:, i] - y_pred_std[:, i],
            y_pred_mean[:, i] + y_pred_std[:, i],
            color="r",
            alpha=0.3,
        )
    if save_dir is not None:
        plt.savefig(save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)
        plt.close(fig)


def _violin(
    x,
    save_dir=None,
    fig_size=(12, 9),
    y_limits=None,
    title=None,
    x_color=(0, 0, 1.0),
    means=True,
    medians=False,
    extrema=False,
    y=None,
    y_predicted=None,
    subplot=(3, 3),
):
    # TODO: filetype - extensions
    assert np.ndim(x) <= 2
    fig = plt.figure(figsize=fig_size)
    ax = fig.add_subplot(111)
    if y_limits is not None:
        plt.ylim(y_limits)
    x_plt = plt.violinplot(
        x,
        positions=range(np.size(x, 1)),
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )

    for pc in x_plt["bodies"]:
        pc.set_facecolor(x_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    if means:
        x_plt["cmeans"].set_edgecolor(x_color)
    if medians:
        x_plt["cmedians"].set_edgecolor(x_color)
    if extrema:
        x_plt["cmaxes"].set_edgecolor(x_color)
        x_plt["cbars"].set_edgecolor(x_color)
        x_plt["cmins"].set_edgecolor(x_color)

    limits = ax.get_ylim()
    if save_dir is not None:
        _, file_extension = os.path.splitext(save_dir)
        # plt.axis("off")
        plt.savefig(save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)
        print("Saved figure: ", save_dir)
        print("Y-limits: ", limits)
        json_path = save_dir.replace(file_extension, ".json")
        print("Saving means and standard deviations to: ", json_path)
        with open(json_path, "w+", encoding="utf8") as file:
            out_dict = {
                "overall_mean": np.float64(np.mean(x)),
                "overall_std": np.float64(np.std(x)),
                "features_mean": np.mean(x, axis=0).tolist(),
                "features_std": np.std(x, axis=0).tolist(),
                "shape": x.shape,
                "Y_limits": limits,
            }
            file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )
        plt.close(fig)
    else:
        ax.set_xticks(range(np.size(x, 1)))
        ax.set_xticklabels(["S", "F", "T"] * (np.size(x, 1) // 3))
        ax.tick_params(axis="both", labelsize="large")
        if title is not None:
            plt.title(title)
        plt.ylim(limits)


def _best_worst(metric_vec, y, y_predicted, title=None, save_dir=None, subplot=(6, 3)):
    mi, me, ma = _get_min_median_max_idx(metric_vec, per_sample=True)
    if save_dir is not None:
        _, file_extension = os.path.splitext(save_dir)

        # min
        new_save_dir = save_dir.replace(file_extension, "_min" + file_extension)
        _plot_features(
            y[mi], y_predicted[mi], save_dir=new_save_dir, title=None, subplot=subplot
        )
        plt.savefig(new_save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)

        # median
        new_save_dir = save_dir.replace(file_extension, "_median" + file_extension)
        _plot_features(
            y[me], y_predicted[me], save_dir=new_save_dir, title=None, subplot=subplot
        )
        plt.savefig(new_save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)

        # max
        new_save_dir = save_dir.replace(file_extension, "_max" + file_extension)
        _plot_features(
            y[ma], y_predicted[ma], save_dir=new_save_dir, title=None, subplot=subplot
        )
        plt.savefig(new_save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)
        print("Saved figures: ", save_dir)
        # print("Y-limits: ", limits)

        # Saving json file with stats
        json_path = save_dir.replace(file_extension, ".json")
        print("Saving means and feature-wise metrics to: ", json_path)
        with open(json_path, "w+", encoding="utf8") as file:
            out_dict = {
                "min": np.float64(np.mean(metric_vec[mi])),
                "min_featurewise": metric_vec[mi].tolist(),
                "min_idx": np.int(mi),
                "median": np.float64(np.mean(metric_vec[me])),
                "median_featurewise": metric_vec[me].tolist(),
                "median_idx": np.int(me),
                "max": np.float64(np.mean(metric_vec[ma])),
                "max_featurewise": metric_vec[ma].tolist(),
                "max_idx": np.int(ma),
                "metric_shape": metric_vec.shape,
            }
            # "Y_limits": limits}
            file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )
        plt.close("all")
    else:
        _plot_features(
            y[mi],
            y_predicted[mi],
            title=f"minimum {title}: {np.mean(metric_vec[mi])}",
            subplot=subplot,
        )
        _plot_features(
            y[me],
            y_predicted[me],
            title=f"median {title}: {np.mean(metric_vec[me])}",
            subplot=subplot,
        )
        _plot_features(
            y[ma],
            y_predicted[ma],
            title=f"maximum {title}: {np.mean(metric_vec[ma])}",
            subplot=subplot,
        )


def _best_worst_featurewise(
    metric_vec, y, y_predicted, title=None, save_dir=None, subplot=(6, 3)
):
    mi, me, ma = _get_min_median_max_idx(metric_vec, per_sample=False)
    if save_dir is not None:
        _, file_extension = os.path.splitext(save_dir)

        # min
        new_save_dir = save_dir.replace(file_extension, "_min" + file_extension)

        _plot_features(
            y[mi],
            y_predicted[mi],
            save_dir=new_save_dir,
            title=None,
            featurewise_examples=True,
            subplot=subplot,
        )
        # plt.axis("off")
        plt.savefig(new_save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)

        # median
        new_save_dir = save_dir.replace(file_extension, "_median" + file_extension)
        _plot_features(
            y[me],
            y_predicted[me],
            save_dir=new_save_dir,
            title=None,
            featurewise_examples=True,
            subplot=subplot,
        )
        # plt.axis("off")
        plt.savefig(new_save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)

        # max
        new_save_dir = save_dir.replace(file_extension, "_max" + file_extension)
        _plot_features(
            y[ma],
            y_predicted[ma],
            save_dir=new_save_dir,
            title=None,
            featurewise_examples=True,
            subplot=subplot,
        )
        # plt.axis("off")
        plt.savefig(new_save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)
        print("Saved figures: ", save_dir)
        # print("Y-limits: ", limits)

        # Saving json file with stats
        json_path = save_dir.replace(file_extension, ".json")
        print("Saving means and feature-wise metrics to: ", json_path)
        with open(json_path, "w+", encoding="utf8") as file:
            out_dict = {
                "min": np.float64(
                    np.mean([metric_vec[idx, i] for i, idx in enumerate(mi)])
                ),
                "min_featurewise": [metric_vec[idx, i] for i, idx in enumerate(mi)],
                "min_idx": mi.tolist(),
                "median": np.float64(
                    np.mean([metric_vec[idx, i] for i, idx in enumerate(me)])
                ),
                "median_featurewise": [metric_vec[idx, i] for i, idx in enumerate(me)],
                "median_idx": me.tolist(),
                "max": np.float64(
                    np.mean([metric_vec[idx, i] for i, idx in enumerate(ma)])
                ),
                "max_featurewise": [metric_vec[idx, i] for i, idx in enumerate(ma)],
                "max_idx": ma.tolist(),
                "metric_shape": metric_vec.shape,
            }
            # "Y_limits": limits}
            file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )

    else:
        mean_mi = np.mean([metric_vec[idx, i] for i, idx in enumerate(mi)])
        mean_me = np.mean([metric_vec[idx, i] for i, idx in enumerate(me)])
        mean_ma = np.mean([metric_vec[idx, i] for i, idx in enumerate(ma)])
        _plot_features(
            y[mi],
            y_predicted[mi],
            title=f"minimum {title} feature-wise: {mean_mi}",
            featurewise_examples=True,
        )
        _plot_features(
            y[me],
            y_predicted[me],
            title=f"median {title} feature-wise: {mean_me}",
            featurewise_examples=True,
        )
        _plot_features(
            y[ma],
            y_predicted[ma],
            title=f"maximum {title} feature-wise: {mean_ma}",
            featurewise_examples=True,
        )


def violin(
    x,
    save_dir=None,
    fig_size=(12, 9),
    y_limits=None,
    title=None,
    x_color=(0, 0, 1.0),
    means=True,
    medians=False,
    extrema=False,
):
    # TODO: filetype - extensions
    """
    Creates a violin plot of array x for a variable amount of features.
    If save_dir=None: shows the plot. Otherwise the figure will be saved
    in save_dir without axes
    :param x: np.array first input
    dimensions: (samples, features)
    :param save_dir: Directory and filename for saving the plot. Default: None
    if None: plot is shown and not saved
    :param fig_size: Size of figure
    :param y_limits: Sets the y-limits of violin plot. Default automatically chooses.
    :param title: Optional, title string for plt plot if save_dir=None
    :param x_color: Color of violin plots, default = blue
    :param means: Show means with horizontal lines?
    :param medians: Show medians with horizontal lines?
    :param extrema: Show extrema with horizontal lines + range with vertical?
    :return: True if successful, False if error occured
    """
    assert np.ndim(x) <= 2
    fig = plt.figure(figsize=fig_size)
    ax = fig.add_subplot(111)
    if y_limits is not None:
        plt.ylim(y_limits)
    x_plt = plt.violinplot(
        x,
        positions=range(np.size(x, 1)),
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )

    for pc in x_plt["bodies"]:
        pc.set_facecolor(x_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    if means:
        x_plt["cmeans"].set_edgecolor(x_color)
    if medians:
        x_plt["cmedians"].set_edgecolor(x_color)
    if extrema:
        x_plt["cmaxes"].set_edgecolor(x_color)
        x_plt["cbars"].set_edgecolor(x_color)
        x_plt["cmins"].set_edgecolor(x_color)

    limits = ax.get_ylim()
    print("Y-limits: ", limits)
    if save_dir is not None:
        _, file_extension = os.path.splitext(save_dir)
        plt.axis("off")
        plt.savefig(save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)
        print("Saved figure: ", save_dir)
        json_path = save_dir.replace(file_extension, ".json")
        print("Saving means and standard deviations to: ", json_path)
        with open(json_path, "w+", encoding="utf8") as file:
            out_dict = {
                "overall_mean": np.float64(np.mean(x)),
                "overall_std": np.float64(np.std(x)),
                "features_mean": np.mean(x, axis=0).tolist(),
                "features_std": np.std(x, axis=0).tolist(),
                "shape": x.shape,
            }
            file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )
    else:
        ax.set_xticks(range(np.size(x, 1)))
        ax.set_xticklabels(["S", "F", "T"] * (np.size(x, 1) // 3))
        ax.tick_params(axis="both", labelsize="large")
        if title is not None:
            plt.title(title)
        plt.ylim(limits)
        # plt.show()


def violin_2(
    x,
    y,
    save_dir=None,
    fig_size=(12, 9),
    y_limits=None,
    title=None,
    legend=(
        "LSTM",
        "FFNN",
    ),
    x_color=(0, 0, 1.0),
    y_color=(1.0, 0, 0),
    x_y_distance=0.7,
    means=True,
    medians=False,
    extrema=False,
):
    """
    Creates a violin plot of 2 arrays x and y for a variable amount of features.
    If save_dir=None: shows the plot. Otherwise the figure will be saved
    in save_dir without axes and means + stds saved in json file
    :param x: np.array first input
    dimensions: (samples, features)
    :param y: np.array second input
    dimensions: (samples, features)
    :param save_dir: Directory and filename for saving the plot. Default: None
    if None: plot is shown and not saved
    :param fig_size: Size of figure
    :param y_limits: Sets the y-limits of plot. Default automatically chooses limits
    :param title: Optional, title string for plt plot if save_dir=None
    :param legend: Optional, string tuple of legend names. If save_dir legend is used
        for naming the json results
    :param x_color: Color of first violin plots, default = blue
    :param y_color: Color of second violin plots, default = red
    :param x_y_distance: Distance between x and y plots
    :param means: Show means with horizontal lines?
    :param medians: Show medians with horizontal lines?
    :param extrema: Show extrema with horizontal lines + range with vertical?
    :return: True if successful, False if error occured
    """
    assert np.size(x, 1) == np.size(y, 1)
    assert np.ndim(x) == np.ndim(y)
    assert np.ndim(x) <= 2
    fig = plt.figure(figsize=fig_size)
    ax = fig.add_subplot(111)
    if y_limits is not None:
        plt.ylim(y_limits)
    x_plt = plt.violinplot(
        x,
        positions=[i * 2 for i in range(np.size(x, 1))],
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )

    y_plt = plt.violinplot(
        y,
        positions=[i * 2 + x_y_distance for i in range(np.size(y, 1))],
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )

    for pc in x_plt["bodies"]:
        pc.set_facecolor(x_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    for pc in y_plt["bodies"]:
        pc.set_facecolor(y_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    if means:
        x_plt["cmeans"].set_edgecolor(x_color)
        y_plt["cmeans"].set_edgecolor(y_color)
    if medians:
        x_plt["cmedians"].set_edgecolor(x_color)
        y_plt["cmedians"].set_edgecolor(y_color)
    if extrema:
        x_plt["cmaxes"].set_edgecolor(x_color)
        y_plt["cmaxes"].set_edgecolor(y_color)
        x_plt["cbars"].set_edgecolor(x_color)
        y_plt["cbars"].set_edgecolor(y_color)
        x_plt["cmins"].set_edgecolor(x_color)
        y_plt["cmins"].set_edgecolor(y_color)

    limits = ax.get_ylim()
    print("Y-limits: ", limits)
    if save_dir is not None:
        ax.set_xticks([i * 2 + x_y_distance / 2 for i in range(np.size(x, 1))])
        ax.set_xticklabels(["X", "Y", "Z"] * (np.size(x, 1) // 3))
        ax.tick_params(axis="both", labelsize="large")
        (tmp_1,) = plt.plot([limits[0] - 1, limits[0] - 1], color=x_color + (0.4,))
        (tmp_2,) = plt.plot([limits[0] - 1, limits[0] - 1], color=y_color + (0.4,))
        plt.ylim(limits)
        if title is not None:
            plt.title(title)
        if legend is not None:
            fig.legend(
                (tmp_1, tmp_2),
                legend,
                loc="upper left",
                bbox_to_anchor=(0.13, 0.5, 0.15, 0.25),
            )
        ax.set_ylabel("NRMSE [%]")

        plt.savefig(save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)
        print("Saved figure: ", save_dir)
        json_path = save_dir.replace(".pdf", "_means.json")
        print("Saving means and standard deviations to: ", json_path)
        # with open(json_path, "w+", encoding="utf8") as file:
        #     out_dict = {
        #         "{}_overall_mean".format(legend[0]): np.float64(np.mean(x)),
        #         "{}_overall_std".format(legend[0]): np.float64(np.std(x)),
        #         "{}_features_mean".format(legend[0]): np.mean(x, axis=0).tolist(),
        #         "{}_features_std".format(legend[0]): np.std(x, axis=0).tolist(),
        #         "{}_shape".format(legend[0]): x.shape,
        #         "{}_overall_mean".format(legend[1]): np.float64(np.mean(y)),
        #         "{}_overall_std".format(legend[1]): np.float64(np.std(y)),
        #         "{}_features_mean".format(legend[1]): np.mean(y, axis=0).tolist(),
        #         "{}_features_std".format(legend[1]): np.std(y, axis=0).tolist(),
        #         "{}_shape".format(legend[1]): y.shape,
        #     }
        #     file.write(
        #         json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
        #     )
    else:
        ax.set_xticks([i * 2 + x_y_distance / 2 for i in range(np.size(x, 1))])
        ax.set_xticklabels(["X", "Y", "Z"] * (np.size(x, 1) // 3))
        ax.tick_params(axis="both", labelsize="large")
        (tmp_1,) = plt.plot([limits[0] - 1, limits[0] - 1], color=x_color + (0.4,))
        (tmp_2,) = plt.plot([limits[0] - 1, limits[0] - 1], color=y_color + (0.4,))
        plt.ylim(limits)
        if title is not None:
            plt.title(title)
        if legend is not None:
            fig.legend(
                (tmp_1, tmp_2),
                legend,
                loc="upper left",
                bbox_to_anchor=(0.15, 0.5, 0.35, 0.35),
            )
        # plt.show()


def violin_4_overlay(
    x_0,
    x,
    y_0,
    y,
    save_dir=None,
    fig_size=(12, 9),
    y_limits=None,
    title=None,
    use_tex=False,
    legend=(
        "Baseline LSTM",
        "Baseline FFNN",
        "LSTM final model",
        "FFNN final model",
    ),
    x_0_color=(0.3, 0.3, 0.6),
    y_0_color=(0.6, 0.3, 0.3),
    x_color=(0, 0, 1.0),
    y_color=(1.0, 0, 0),
    x_y_distance=0.7,
    no_axis=False,
    means=True,
    medians=False,
    extrema=False,
):
    """
    Creates a violin plots of 4 arrays x and y and their baselines x_0 and y_0.
    If save_dir=None: shows the plot. Otherwise the figure will be saved
    in save_dir without axes and means + stds saved in json file
    :param x_0: np.array first input - baseline
    dimensions: (samples, features)
    :param x: np.array first input
    dimensions: (samples, features)
    :param y_0: np.array second input - baseline
    dimensions: (samples, features)
    :param y: np.array second input
    dimensions: (samples, features)
    :param save_dir: Directory and filename for saving the plot. Default: None
    if None: plot is shown and not saved
    :param fig_size: Size of figure
    :param y_limits: Sets the y-limits of plot. Default automatically chooses limits
    :param title: Optional, title string for plt plot if save_dir=None
    :param use_tex: [bool] if true, renders the text in latex font
    :param legend: Optional, string tuple of legend names. If save_dir legend is used
        for naming the json results
    :param x_0_color: Color of first plot background (baseline), default = blue-grey
    :param y_0_color: Color of second violin plots, default = red-grey
    :param x_color: Color of first violin plots, default = blue
    :param y_color: Color of second violin plots, default = red
    :param x_y_distance: Distance between x and y plots
    :param means: Show means with horizontal lines?
    :param medians: Show medians with horizontal lines?
    :param extrema: Show extrema with horizontal lines + range with vertical?
    :return: True if successful, False if error occured
    """
    assert np.size(x, 1) == np.size(y, 1)
    assert np.size(x, 1) == np.size(x_0, 1)
    assert np.size(x_0, 1) == np.size(y_0, 1)
    assert np.ndim(x) == np.ndim(y)
    assert np.ndim(x_0) == np.ndim(y_0)
    assert np.ndim(x) <= 2

    grey = (0.6, 0.6, 0.6)

    if use_tex:
        rc("font", **{"family": "serif", "serif": ["Computer Modern"], "size": "10"})
        rc("text", usetex=True)
    fig = plt.figure(figsize=fig_size)
    ax = fig.add_subplot(111)
    if y_limits is not None:
        plt.ylim(y_limits)
    x_0_plt = plt.violinplot(
        x_0,
        positions=[i * 2 for i in range(np.size(x, 1))],
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )
    x_plt = plt.violinplot(
        x,
        positions=[i * 2 for i in range(np.size(x, 1))],
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )

    y_0_plt = plt.violinplot(
        y_0,
        positions=[i * 2 + x_y_distance for i in range(np.size(y, 1))],
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )
    y_plt = plt.violinplot(
        y,
        positions=[i * 2 + x_y_distance for i in range(np.size(y, 1))],
        showmeans=means,
        showmedians=medians,
        showextrema=extrema,
    )

    for pc in x_0_plt["bodies"]:
        pc.set_facecolor(x_0_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    for pc in x_plt["bodies"]:
        pc.set_facecolor(x_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    for pc in y_0_plt["bodies"]:
        pc.set_facecolor(y_0_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    for pc in y_plt["bodies"]:
        pc.set_facecolor(y_color)
        pc.set_edgecolor("black")
        pc.set_alpha(0.4)
    if means:
        x_0_plt["cmeans"].set_edgecolor(grey)
        y_0_plt["cmeans"].set_edgecolor(grey)
        x_plt["cmeans"].set_edgecolor(x_color)
        y_plt["cmeans"].set_edgecolor(y_color)
    if medians:
        x_0_plt["cmedians"].set_edgecolor(grey)
        y_0_plt["cmedians"].set_edgecolor(grey)
        x_plt["cmedians"].set_edgecolor(x_color)
        y_plt["cmedians"].set_edgecolor(y_color)
    if extrema:
        x_0_plt["cmaxes"].set_edgecolor(grey)
        y_0_plt["cmaxes"].set_edgecolor(grey)
        x_0_plt["cbars"].set_edgecolor(grey)
        y_0_plt["cbars"].set_edgecolor(grey)
        x_0_plt["cmins"].set_edgecolor(grey)
        y_0_plt["cmins"].set_edgecolor(grey)
        x_plt["cmaxes"].set_edgecolor(x_color)
        y_plt["cmaxes"].set_edgecolor(y_color)
        x_plt["cbars"].set_edgecolor(x_color)
        y_plt["cbars"].set_edgecolor(y_color)
        x_plt["cmins"].set_edgecolor(x_color)
        y_plt["cmins"].set_edgecolor(y_color)

    limits = ax.get_ylim()
    print("Y-limits: ", limits)
    if save_dir is not None:
        ax.set_xticks([i * 2 + x_y_distance / 2 for i in range(np.size(x, 1))])
        ax.set_xticklabels(["X", "Y", "Z"] * (np.size(x, 1) // 3))
        ax.tick_params(axis="both")
        if no_axis:
            ax.set_axis_off()

        (tmp_1_0,) = plt.plot([limits[0] - 1, limits[0] - 1], color=x_0_color + (0.4,))
        (tmp_1,) = plt.plot([limits[0] - 1, limits[0] - 1], color=x_color + (0.4,))
        (tmp_2_0,) = plt.plot([limits[0] - 1, limits[0] - 1], color=y_0_color + (0.4,))
        (tmp_2,) = plt.plot([limits[0] - 1, limits[0] - 1], color=y_color + (0.4,))
        plt.ylim(limits)
        if title is not None:
            plt.title(title)
        if legend is not None:
            fig.legend(
                (tmp_1_0, tmp_2_0, tmp_1, tmp_2),
                legend,
                loc="upper left",
                bbox_to_anchor=(0.2, 0.5, 0.45, 0.25),
            )
        if use_tex:
            ax.set_ylabel(r"NRMSE [\%]")
        else:
            ax.set_ylabel("NRMSE [%]")

        plt.savefig(save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)
        print("Saved figure: ", save_dir)
        json_path = save_dir.replace(".pdf", "_means.json")
        print("Saving means and standard deviations to: ", json_path)

    else:
        ax.set_xticks([i * 2 + x_y_distance / 2 for i in range(np.size(x, 1))])
        ax.set_xticklabels(["X", "Y", "Z"] * (np.size(x, 1) // 3))
        ax.tick_params(axis="both", labelsize="large")
        (tmp_1,) = plt.plot([limits[0] - 1, limits[0] - 1], color=x_color + (0.4,))
        (tmp_2,) = plt.plot([limits[0] - 1, limits[0] - 1], color=y_color + (0.4,))
        plt.ylim(limits)
        if title is not None:
            plt.title(title)
        if legend is not None:
            fig.legend(
                (tmp_1, tmp_2),
                legend,
                loc="upper left",
                bbox_to_anchor=(0.15, 0.5, 0.35, 0.35),
            )


def _plot_features(
    x, y, subplot=(6, 3), title=None, featurewise_examples=False, save_dir=None
):
    assert np.shape(x) == np.shape(y)

    if featurewise_examples:
        assert subplot[0] * subplot[1] == np.size(x, 0)
        assert np.size(x, 2) == np.size(x, 0)
        plt.figure()
        if title is not None:
            plt.suptitle(title)
        for i in range(np.size(x, 0)):
            plt.subplot(subplot[0], subplot[1], i + 1)
            plt.plot(x[i, :, i], "g", label="Real")
            plt.plot(y[i, :, i], "r", label="Approximated")
        if save_dir is not None:
            _, file_extension = os.path.splitext(save_dir)
            # new_save_dir = save_dir.replace(file_extension, "_min" + file_extension)
            # plt.axis("off")
            plt.savefig(save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)

            time = list(range(x.shape[1]))
            real_array = []
            real_array.append(time)
            predicted_array = []
            predicted_array.append(time)
            for i in range(np.size(x, 0)):
                real_array.append(x[i, :, i])
                predicted_array.append(y[i, :, i])
            new_save_dir_real = save_dir.replace(file_extension, "_real.csv")
            new_save_dir_pred = save_dir.replace(file_extension, "_pred.csv")

            np.savetxt(
                new_save_dir_real, np.transpose(real_array, (1, 0)), delimiter=","
            )
            np.savetxt(
                new_save_dir_pred, np.transpose(predicted_array, (1, 0)), delimiter=","
            )
    else:
        assert subplot[0] * subplot[1] == np.size(x, 1)
        plt.figure()
        if title is not None:
            plt.suptitle(title)
        for i in range(np.size(x, 1)):
            plt.subplot(subplot[0], subplot[1], i + 1)
            plt.plot(x[:, i], "g", label="Real")
            plt.plot(y[:, i], "r", label="Approximated")

        if save_dir is not None:
            _, file_extension = os.path.splitext(save_dir)
            # new_save_dir = save_dir.replace(file_extension, "_min" + file_extension)
            # plt.axis("off")
            plt.savefig(save_dir, transparent=True, bbox_inches="tight", pad_inches=0.0)

            time = list(range(x.shape[0]))
            real_array = []
            real_array.append(time)
            predicted_array = []
            predicted_array.append(time)
            for i in range(np.size(x, 1)):
                real_array.append(x[:, i])
                predicted_array.append(y[:, i])
            new_save_dir_real = save_dir.replace(file_extension, "_real.csv")
            new_save_dir_pred = save_dir.replace(file_extension, "_pred.csv")
            # final_array = final_array.transpose((1, 0))
            np.savetxt(
                new_save_dir_real, np.transpose(real_array, (1, 0)), delimiter=","
            )
            np.savetxt(
                new_save_dir_pred, np.transpose(predicted_array, (1, 0)), delimiter=","
            )


def _get_min_median_max_idx(x, per_sample=False):
    if per_sample:
        x = np.mean(x, axis=1)
    min_val = np.argmin(x, 0)
    tmp = np.argsort(x)
    median_val = tmp[int(np.size(x, 0) / 2)]
    max_val = np.argmax(x, 0)

    return min_val, median_val, max_val


def compare_feature_distributions(
    *args,
    colors=None,
    alpha=0.4,
    labels=None,
    offsets=0.0,
    showmeans=True,
    showmedians=True,
    showextrema=True,
    featurelabels=None,
    featureaxis=-1,
    featureoffset=1.0,
    normalize=None,
    plottype="violin",
):
    """Draw violin plots of the given arrays, split along the feature axis.

    Args:
        *args:          numpy ndarrays for the data distributions
        colors:         list containing a color for each array
        alpha:          opacity of the violin plots
        labels:         list containing a label for each array
        offsets:         offset of the violin plots among each feature
        showmeans:      draw mean values?
        showmedians:    draw median values?
        showextrema:    draw extrema values?
        featurelabels:  labels for each feature
        featureaxis:    the feature axis in the arrays
        featureoffset:  offset between features
        normalize:      normalize features? (None, "range", "std")
        plottype:       type of plot ("violin" or "box"

    Returns:
        ax:             current axes handle
    """
    if colors is None:
        colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
    if featurelabels is None:
        featurelabels = list(range(args[0].shape))
    if isinstance(offsets, (float, int)):
        offsets = [0.0] + [offsets] * (len(args) - 1)
    else:
        if len(offsets) == (len(args) - 1):
            offsets = [0.0] + list(offsets)
    offsets = np.asarray(offsets)
    vp_pos = np.arange(0.0, featureoffset * args[0].shape[featureaxis], featureoffset)
    # Normalize
    if normalize is not None:
        try:
            ii = int(normalize[-1])
            tmp = np.reshape(args[ii], [-1, args[ii].shape[featureaxis]], order="F")
        except ValueError:
            tmp = np.reshape(
                np.concatenate(args, axis=0),
                [-1, args[0].shape[featureaxis]],
                order="F",
            )
        shift = np.mean(tmp, axis=0)

        if normalize[:3] == "std":
            width = np.std(tmp, axis=0)
        elif normalize[:5] == "range":
            width = np.max(tmp, axis=0) - np.min(tmp, axis=0)
        elif normalize[:4] == "mean":
            width = shift
        elif normalize[:5] == "quant":
            width = np.abs(np.quantile(tmp, 0.75) - np.quantile(tmp, 0.25))
        else:
            raise ValueError(f"Unknown normalize mode: {normalize:s}")
    else:
        shift = 0.0
        width = 1.0
    # Loop over all arrays
    for i, a in enumerate(args):
        if normalize:
            aa = (a - shift) / width
        else:
            aa = a
        # Plots
        if plottype == "box":
            # Boxplot
            box_width = np.min(offsets[offsets > 0.0]) * 0.8
            bpy = plt.boxplot(
                np.reshape(aa, [-1, aa.shape[featureaxis]], order="F"),
                positions=vp_pos + sum(offsets[: (i + 1)]),
                showmeans=False,
                widths=box_width,
                patch_artist=True,
                showfliers=False,
                medianprops={"color": "k"},
            )
            # Set colors
            for pc in bpy["boxes"]:
                face_color = list(matplotlib.colors.to_rgba(colors[i]))
                face_color[-1] = alpha
                pc.set_facecolor(face_color)
                pc.set_edgecolor("black")
            if showmeans:
                for pc in bpy["means"]:
                    pc.set_color(colors[i])
        else:
            # Violinplot
            vpy = plt.violinplot(
                np.reshape(aa, [-1, aa.shape[featureaxis]], order="F"),
                positions=vp_pos + sum(offsets[: (i + 1)]),
                showmeans=showmeans,
                showmedians=showmedians,
                showextrema=showextrema,
            )
            # Set colors
            for pc in vpy["bodies"]:
                pc.set_facecolor(colors[i])
                pc.set_edgecolor("black")
                pc.set_alpha(alpha)
            if showmeans:
                vpy["cmeans"].set_edgecolor(colors[i])
            if showmedians:
                vpy["cmedians"].set_edgecolor(colors[i])
            if showextrema:
                vpy["cmaxes"].set_edgecolor(colors[i])
                vpy["cbars"].set_edgecolor(colors[i])
                vpy["cmins"].set_edgecolor(colors[i])
    # Axes ticks
    ax = plt.gca()
    ax.set_xticks(vp_pos + 0.5 * sum(offsets))
    ax.set_xticklabels(featurelabels)
    plt.xlim(
        left=(vp_pos[0] - featureoffset / 2.0 + sum(offsets) / 2.0),
        right=(vp_pos[-1] + featureoffset / 2.0 + sum(offsets) / 2.0),
    )
    # Legend
    dummy_patches = (mpatches.Patch(color=c, alpha=alpha) for c in colors)
    return dummy_patches


def setup_plot(axis_labels=("x", "y"), title="Figure"):
    fig, ax = plt.subplots(1, 1)
    fig.suptitle(title)
    ax.set_xlabel(axis_labels[0])
    ax.set_ylabel(axis_labels[1])
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    return fig, ax


def update_plot(xx, yy, colors, labels, fig, ax, file=""):
    plt.figure(fig.number)
    # Update axes
    xmin = min(min(x) for x in xx)
    xmax = max(max(x) for x in xx)
    if xmin == xmax:
        xmin = 0.0
        xmax = 1.0
    ymin = min(min(y) for y in yy)
    ymax = max(max(y) for y in yy)
    if ymin == ymax:
        ymin = 0.0
        ymax = 1.0
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    # Update data plot
    if ax.lines:
        for x, y, line in zip(xx, yy, ax.lines):
            line.set_xdata(x)
            line.set_ydata(y)
    else:
        handles = []
        for x, y, color, label in zip(xx, yy, colors, labels):
            (h,) = ax.plot(x, y, color, label=label)
            handles.append(h)
        ax.legend(handles, labels)
    # Redraw
    fig.canvas.draw()
    # Save
    if file:
        plt.savefig(file)

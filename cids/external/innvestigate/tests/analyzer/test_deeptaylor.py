# Get Python six functionality:
###############################################################################
###############################################################################
###############################################################################
import pytest

from ...analyzer import BoundedDeepTaylor
from ...analyzer import DeepTaylor
from ...utils.tests import cases
from ...utils.tests import dryrun


###############################################################################
###############################################################################
###############################################################################


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST)
def test_fast__DeepTaylor(case_id):
    def create_analyzer_f(model):
        return DeepTaylor(model)

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.PRECOMMIT)
def test_precommit__DeepTaylor(case_id):
    def create_analyzer_f(model):
        return DeepTaylor(model)

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST)
def test_fast__BoundedDeepTaylor(case_id):
    def create_analyzer_f(model):
        return BoundedDeepTaylor(model, low=-1, high=1)

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.PRECOMMIT)
def test_precommit__BoundedDeepTaylor(case_id):
    def create_analyzer_f(model):
        return BoundedDeepTaylor(model, low=-1, high=1)

    dryrun.test_analyzer(case_id, create_analyzer_f)

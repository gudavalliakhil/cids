# Get Python six functionality:
###############################################################################
###############################################################################
###############################################################################
import pytest

from ...utils.tests import cases
from ...utils.tests import dryrun


###############################################################################
###############################################################################
###############################################################################


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST + cases.PRECOMMIT)
def test_fast__DryRunAnalyzerTestCase(case_id):
    """
    Sanity test for the TestCase.
    """

    def create_analyzer_f(output_layer):
        class TestAnalyzer:
            def fit(self, X):
                pass

            def analyze(self, X):
                return X

        return TestAnalyzer()

    dryrun.test_analyzer(case_id, create_analyzer_f)

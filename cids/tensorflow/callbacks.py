# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tensorflow callbacks for CIDS. Part of the CIDS toolbox.

    Classes:
        CIDSCheckpoint: Saves CIDSModel at regular intervals during training
        FreezeControl: Freezes online normalization parameters after defined epoch
        StepProgressCallback:   Print training progress for each batch
        EpochProgressCallback:  Print training progress for each epoch
"""
import os
import sys
from collections import OrderedDict

import numpy as np
from tensorflow.keras.callbacks import Callback
from tqdm.auto import trange


class CIDSCheckpoint(Callback):

    DEBUG = False

    def __init__(
        self,
        cids_model,
        monitor="val_loss",
        verbose=1,
        save_best_only=False,
        save_weights_only=True,
        mode="auto",
        save_freq=100,
    ):
        self.cids_model = cids_model
        super().__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.save_best_only = save_best_only
        self.save_weights_only = save_weights_only
        self.save_freq = save_freq
        self.epochs_since_last_save = 0

        if mode not in ["auto", "min", "max"]:
            self.cids_model.warn(
                f"Model checkpoint mode {mode:s} is unknown. Falling back to auto mode."
            )
            mode = "auto"

        if mode == "min":
            self.monitor_op = np.less
            if self.best is None:
                self.best = np.Inf
        elif mode == "max":
            self.monitor_op = np.greater
            if self.best is None:
                self.best = -np.Inf
        else:
            if "acc" in self.monitor or self.monitor.startswith("fmeasure"):
                self.monitor_op = np.greater
                if self.best is None:
                    self.best = -np.Inf
            else:
                self.monitor_op = np.less
                if self.best is None:
                    self.best = np.Inf
        self.set_model(getattr(self.cids_model, "core_model"))

    @property
    def best(self):
        return self.cids_model._best_monitor

    @best.setter
    def best(self, value):
        self.cids_model._best_monitor = value

    def on_epoch_end(self, epoch, logs=None):
        # TODO: switch back once automatic keras saving works again
        # if self.epochs_since_last_save >= self.save_freq - 1:
        #     current = logs.get(self.monitor)
        #     if (self.save_best_only
        #             and current is not None
        #             and self.monitor_op(current, self.best)):
        #         filepath = self.filepath.format(epoch=epoch + 1, **logs)
        #         dirname = os.path.dirname(filepath)
        #         self.cids_model.create_dir(dirname)
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.save_freq:
            self.epochs_since_last_save = 0
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    self.cids_model.warn(
                        f"Can save best model only with {self.monitor:s} available. "
                        + "Skipping.",
                    )
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            self.cids_model.log(
                                f"Epoch {epoch + 1:05d}: {self.monitor:s} improved "
                                + f"from {self.best:0.5f} to {current:0.5f}. "
                                + f" Saving model to {self.cids_model.count:d}."
                            )
                        self.best = current
                        self.cids_model.save(self.cids_model.count)
                    else:
                        if self.verbose > 0:
                            self.cids_model.log(
                                f"Epoch {epoch + 1:05d}: {self.monitor:s} did not "
                                + f"improve from {self.best:0.5f}. "
                            )
            else:
                if self.verbose > 0:
                    self.cids_model.log(
                        f"Epoch {epoch + 1:05d}: saving model "
                        + f"to {self.cids_model.count:s}."
                    )
                self.cids_model.save(self.cids_model.count)
        #
        # super(CIDSCheckpoint, self).on_epoch_end(epoch, logs=logs)


class FreezeControl(Callback):

    DEBUG = False

    def __init__(self, cids_model):
        self.cids_model = cids_model
        self._old_freeze = False
        super().__init__()

    def on_test_begin(self, logs=None):
        try:
            self._old_freeze = self.cids_model.freeze.numpy()
        except AttributeError:
            self._old_freeze = self.cids_model.freeze
        self.cids_model.freeze = True

    def on_test_end(self, logs=None):
        self.cids_model.freeze = self._old_freeze

    def on_predict_begin(self, logs=None):
        try:
            self._old_freeze = self.cids_model.freeze.numpy()
        except AttributeError:
            self._old_freeze = self.cids_model.freeze
        self.cids_model.freeze = True

    def on_predict_end(self, logs=None):
        self.cids_model.freeze = self._old_freeze


class StepProgressCallback(Callback):

    DEBUG = False

    def __init__(self, cids_model, num_steps, phase):
        self.cids_model = cids_model
        self.num_steps = num_steps - self.cids_model.count
        self.phase = phase
        self.progbar = None
        self.postfix = OrderedDict()
        self._step_format_str = "{:07d}"
        self._loss_format_str = "{:.3e}"
        # self._time_format_str = "{:d}h {:02d}m {:02d}s "
        super().__init__()

    def on_train_begin(self, logs=None):
        self.postfix = OrderedDict()
        self.postfix["step"] = self._step_format_str.format(0)
        if self.cids_model.VERBOSITY:
            self.tqdm_target = self.cids_model.stream_to_logger()
        else:
            self.tqdm_target = open(  # pylint: disable=consider-using-with
                os.devnull, "w", encoding="utf8"
            )
        self.progbar = trange(
            self.num_steps,
            miniters=self.cids_model.report_freq,
            file=self.tqdm_target,
            dynamic_ncols=True,
            desc=self.cids_model.INDENT
            + self.cids_model.base_name
            + f": Training phase {self.phase:d}",
        )

    def on_train_batch_end(self, batch, logs=None):
        # Increase step
        self.cids_model.count += 1
        # Get data
        step = self.cids_model.count
        # Set postfix
        if step % self.cids_model.report_freq == 0:
            update_dict = OrderedDict(
                [
                    (k, logs[k])
                    for k in sorted(logs.keys())
                    if k not in ["size", "val_batch", "batch", "val_batch"]
                ]
            )
            self.postfix.update(update_dict)

    def on_test_batch_end(self, batch, logs=None):
        # Get data
        step = self.cids_model.count
        initial_step = self.cids_model.initial_count
        # time = timeit.default_timer() - self.cids_nn.init_time
        # seconds = int(time)
        # milliseconds = int((time - seconds) * 1000)
        # minutes, seconds = divmod(seconds, 60)
        # hours, minutes = divmod(minutes, 60)
        if step % self.cids_model.report_freq == 0:
            # Set postfix
            self.postfix["step"] = self._step_format_str.format(step)
            sorted_logs_keys = sorted(logs.keys())
            update_dict = OrderedDict(
                [
                    ("val_" + k, logs[k])
                    for k in sorted_logs_keys
                    if k not in ["size", "val_batch", "batch", "val_batch"]
                ]
            )
            self.postfix.update(update_dict)
            # self.postfix["time"] = self._time_format_str.format(
            #     hours, minutes, seconds)
            # Update progress bar
            self.progbar.set_postfix(self.postfix, refresh=False)
            self.progbar.update(step - initial_step - self.progbar.n)

    def on_train_end(self, logs=None):
        if self.progbar is not None:
            self.progbar.close()
            self.progbar = None
        if self.tqdm_target is not sys.stdout:
            self.tqdm_target.close()


class EpochProgressCallback(Callback):

    DEBUG = False

    def __init__(self, cids_model, num_epochs, phase):
        self.cids_model = cids_model
        self.num_epochs = num_epochs - self.cids_model.count
        self.phase = phase
        self.progbar = None
        self.postfix = OrderedDict()
        self._epoch_format_str = "{:04d}"
        self._loss_format_str = "{:.3e}"
        # self._time_format_str = "{:d}h {:02d}m {:02d}s "
        super().__init__()

    def on_train_begin(self, logs=None):
        self.postfix = OrderedDict()
        self.postfix["epoch"] = self._epoch_format_str.format(0)
        if self.cids_model.VERBOSITY:
            self.tqdm_target = self.cids_model.stream_to_logger()
        else:
            self.tqdm_target = open(  # pylint: disable=consider-using-with
                os.devnull, "w", encoding="utf8"
            )
        self.progbar = trange(
            self.num_epochs,
            miniters=self.cids_model.report_freq,
            file=self.tqdm_target,
            dynamic_ncols=True,
            desc=self.cids_model.INDENT
            + self.cids_model.base_name
            + f": Training phase {self.phase:d}",
        )

    def on_train_batch_end(self, batch, logs=None):
        pass

    def on_epoch_end(self, epoch, logs=None):
        # Increase step
        self.cids_model.count += 1
        # Get data
        epoch = self.cids_model.count
        initial_epoch = self.cids_model.initial_count
        # time = timeit.default_timer() - self.cids_nn.init_time
        # seconds = int(time)
        # milliseconds = int((time - seconds) * 1000)
        # minutes, seconds = divmod(seconds, 60)
        # hours, minutes = divmod(minutes, 60)
        if epoch % self.cids_model.report_freq == 0:
            # Set postfix
            self.postfix["epoch"] = self._epoch_format_str.format(epoch)
            update_dict = OrderedDict(
                [
                    (k, logs[k])
                    for k in sorted(logs.keys())
                    if k not in ["size", "val_batch", "batch", "val_batch"]
                ]
            )
            self.postfix.update(update_dict)
            # self.postfix["time"] = self._time_format_str.format(
            #     hours, minutes, seconds)
            # Update progress bar
            self.progbar.set_postfix(self.postfix, refresh=False)
            self.progbar.update(epoch - initial_epoch - self.progbar.n)

    def on_train_end(self, logs=None):
        if self.progbar is not None:
            self.progbar.close()
            self.progbar = None
            if self.tqdm_target is not sys.stdout:
                self.tqdm_target.close()


# class StagingCallback(Callback):
#     """
#     It allows to prefetch input batches to GPU using TensorFlow StagingArea,
#     making a simple asynchronous pipeline.
#     The classic mechanism of copying input data to GPU in Keras with TensorFlow
#     is `feed_dict`: a numpy array is synchronously copied from Python to TF memory
#     and then using a host-to-device memcpy to GPU memory. The computation,
#     however has to wait, which is wasteful.
#     This class makes the HtoD memcpy asynchronous using a GPU-resident queue
#     of size two (implemented by StaginArea). The mechanism is as follows:
#     - at the beginning of an epoch one batch is `put()` into the queue
#     - during each training step another is is `put()` into the queue and in
#       parallel the batch already present at the GPU is `get()` from the queue
#       at provide as tesnor input to the Keras model (this runs within a single
#       `tf.Session.run()`)
#     The input numpy arrays (features and targets) are provided via this
#     callback and sliced into batches inside it. The last batch might be of
#     smaller size without any problem (the StagingArea supports variable-sized
#     batches and allows to enforce constant data sample shape). In the last
#     batch zero-length slice is still put into the queue to keep the get+put
#     operation uniform across all batches.
#     Since it"s hard to modify Keras to add more data to `feed_dict`, the data
#     from numpy is fed into StagingArea in another `tf.Session.run()` before each
#     training step via an intermediate `tf.Variable` and `feed_dict`. It is still
#     synchronous. A better, though more complicated way would be to use TF queues
#     (depracated) or Dataset API.
#     In order to provide extra put() operation to `fetches`, we depend on a fork
#     of Keras (https://github.com/bzamecnik/keras/tree/tf-function-session-run-args).
#     A pull request to upstream will be made soon.
#     Example usage:
#     ```
#     staging_area_callback = StagingAreaCallback(x_train, y_train, batch_size)
#     image = Input(tensor=staging_area_callback.input_tensor)
#     x = Dense(512, activation="relu")(image)
#     digit = Dense(num_classes, activation="softmax")(x)
#     model = Model(inputs=image, outputs=digit)
#     model.compile(optimizer="sgd", loss="categorical_crossentropy",
#         target_tensors=[staging_area_callback.target_tensor],
#         fetches=staging_area_callback.extra_ops)
#     model.fit(steps_per_epoch=steps_per_epoch, epochs=2,
#         callbacks=[staging_area_callback])
#
#     """
#     def __init__(self, dataset, prefetch_count=1):
#
#         self.dataset = dataset
#         self.prefetch_count = prefetch_count
#
#         # Create compute stage
#         self.compute_stage = StagingArea(
#             dtypes=self.dataset.output_dtypes,
#             shapes=self.dataset.output_shapes)
#
#         self.area_put = self.compute_stage.put(self.dataset)
#         area_get_features, area_get_labels = self.compute_stage.get()
#         self.area_size = self.compute_stage.size()
#         self.area_clear = self.compute_stage.clear()
#
#         self.input_tensor = area_get_features
#         self.target_tensor = area_get_labels
#         self.extra_ops = [self.area_put]
#
#     def _slice_batch(self, i):
#         start = i * self.batch_size
#         end = start + self.batch_size
#         return self.x[start:end], self.y[start:end]
#
#     def _assign_batch(self, session, data):
#         x_batch, y_batch = data
#         session.run(self.assign_next_batch, feed_dict={
#             self.features_batch_next_value: x_batch,
#             self.labels_batch_next_value: y_batch})
#
#     def on_epoch_begin(self, epoch, logs=None):
#         sess = tf.keras.backend.get_session()
#         for i in range(self.prefetch_count):
#             self._assign_batch(sess, self._slice_batch(i))
#             sess.run(self.area_put)
#
#     def on_batch_begin(self, batch, logs=None):
#
#         sess = tf.keras.backend.get_session()
#         # Slice for `prefetch_count` last batches is empty.
#         # It serves as a dummy value which is put into StagingArea
#         # but never read.
#         data = self._slice_batch(batch + self.prefetch_count)
#         self._assign_batch(sess, data)
#
#     def on_train_begin(self, logs=None):
#         for i in range(self.prefetch_count):
#             self.compute_stage.put(next(self.dataset))
#
#     def on_train_end(self, logs=None):
#         self.compute_stage.clear()

# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Contains the common base model for all CIDSModels. Part of the CIDS toolbox.

    Classes:
        BaseModel: common base model with shared functionality for the CIDS models
"""
import json
import os
from pathlib import Path
from threading import Thread

import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from kerastuner import HyperModel
from matplotlib import pyplot as plt
from sklearn import metrics as skmetrics
from tensorflow.keras import backend as K

from ..data import DataReader
from .logging import BaseLoggerMixin
from kadi_ai.projects.base import BaseProject


class BaseModel(HyperModel, BaseLoggerMixin):  # pylint: disable=abstract-method

    DEBUG = False
    VERBOSITY = 2
    INDENT = ""

    def __init__(
        self, data_definition, model, name=None, result_dir=None, **kwargs
    ):  # pylint: disable=super-init-not-called
        """Base class for CIDS models.

        Args:
            data_definition (DataDefinition or BaseProject): Data definition that
                defines features or project with data definition and result directory
            model:           a (function returning a) model
            name (str, optional): Base name of the model. Defaults to None.
            result_dir (PathLike, optional): Path to results. Defaults to None.
            identifier (str, optional): Model instance name to be added to the base
                name. Defaults to "".
        """
        # Maybe unpack project
        if isinstance(data_definition, BaseProject):
            project = data_definition
            data_definition = project.data_definition
            if result_dir is not None:
                raise ValueError(
                    "Cannot manually set result_dir in model while using a "
                    + "KadiAIProject. Set result_dir in the project instead."
                )
            result_dir = project.result_dir
        else:
            result_dir = result_dir or "RESULTS/"
        # Logging and directories
        self._start_logging()
        self.progress_callback = None
        self.base_name = name
        self.suppress_architecture_string = False
        self.identifier = kwargs.get("identifier", "")
        self.result_dir = result_dir
        self._checkpoint_format_str = "{epoch:07d}"
        self.meta_folder = ""
        # Data definition and reader
        self.data_definition = data_definition
        self.data_reader = kwargs.get("data_reader", DataReader(self.data_definition))
        # Task
        self.task = "regression"  # Set by class methods
        self.num_classes = None  # Set by class methods
        # Placeholder for model
        self._core_model = None
        # Data preprocessing switches
        self._online_normalize = True
        self._encode_categorical = False
        self._collapse_repeated_features = False

    def __getattr__(self, attr):
        # Ensure core model was built
        if self._core_model is None:
            raise RuntimeError(
                "Core model has not been created. Use `cids_model.build` to "
                + "initialize a core model."
            )
        # Try to find attribute in core model instead
        try:
            return getattr(self._core_model, attr)
        except AttributeError as e:
            raise AttributeError(
                "Neither the CIDS model nor the core model has the attribute: ", attr
            ) from e

    @property
    def name(self):
        return self.assemble_model_name(self.base_name, self.identifier, [])

    @property
    def base_model_dir(self):
        base_model_dir = os.path.join(self.result_dir, self.base_name)
        self.create_dir(base_model_dir)
        return base_model_dir

    @property
    def model_dir(self):
        model_dir = self._model_dir
        self.create_dir(model_dir)
        return model_dir

    @property
    def _model_dir(self):
        if self.meta_folder:
            model_dir = os.path.join(self.base_model_dir, self.meta_folder, self.name)
        else:
            model_dir = os.path.join(self.base_model_dir, self.name)
        return model_dir

    @property
    def summary_dir(self):
        summary_dir = os.path.join(self.model_dir, "summary")
        self.create_dir(summary_dir)
        return summary_dir

    @property
    def _checkpoint_dir(self):
        return os.path.join(self._model_dir, "checkpoint")

    @property
    def checkpoint_dir(self):
        checkpoint_dir = self._checkpoint_dir
        self.create_dir(checkpoint_dir)
        return checkpoint_dir

    @property
    def log_dir(self):
        log_dir = os.path.join(self.model_dir, "log")
        self.create_dir(log_dir)
        return log_dir

    @property
    def profile_dir(self):
        profile_dir = os.path.join(self.model_dir, "profile")
        self.create_dir(profile_dir)
        return profile_dir

    @property
    def tmp_dir(self):
        tmp_dir = os.path.join(self.model_dir, "tmp")
        self.create_dir(tmp_dir)
        return tmp_dir

    @property
    def plot_dir(self):
        plot_dir = os.path.join(self.model_dir, "plot")
        self.create_dir(plot_dir)
        return plot_dir

    @property
    def analyze_dir(self):
        analyze_dir = os.path.join(self.model_dir, "analysis")
        self.create_dir(analyze_dir)
        return analyze_dir

    @property
    def input_format(self):
        return self.data_definition.data_format["X"]

    @property
    def output_format(self):
        return self.data_definition.data_format["Y"]

    @property
    def input_shape(self):
        return self.data_definition.input_shape

    @property
    def output_shape(self):
        return self.data_definition.output_shape

    @property
    def batch_axis(self):
        return self.data_definition.batch_axis

    @property
    def feature_axis(self):
        return self.data_definition.feature_axis

    @property
    def sequence_axis(self):
        return self.data_definition.sequence_axis

    @property
    def spatial_axes(self):
        return self.data_definition.spatial_axes

    @property
    def iter_axis(self):
        return self.data_definition.iter_axis

    @property
    def online_normalize(self):
        return self._online_normalize

    @online_normalize.setter
    def online_normalize(self, value):
        allowed_values = [True, False, "both", "inputs", "input", "outputs", "output"]
        if value not in allowed_values:
            raise ValueError(f"Value must be one of {str(allowed_values)}")
        self._online_normalize = value

    @property
    def encode_categorical(self):
        return self._encode_categorical

    @encode_categorical.setter
    def encode_categorical(self, value):
        allowed_values = [True, False, "both", "inputs", "input", "outputs", "output"]
        if value not in allowed_values:
            raise ValueError(f"Value must be one of {str(allowed_values)}")
        self._encode_categorical = value

    @property
    def collapse_repeated_features(self):
        return self._collapse_repeated_features

    @collapse_repeated_features.setter
    def collapse_repeated_features(self, value):
        allowed_values = [True, False, "both", "inputs", "input", "outputs", "output"]
        if value not in allowed_values:
            raise ValueError(f"Value must be one of {str(allowed_values)}")
        self._collapse_repeated_features = value

    @staticmethod
    def _recursive_chmod(path, folderperms, fileperms):
        for dirpath, _, filenames in os.walk(path):
            os.chmod(dirpath, folderperms)
            for filename in filenames:
                os.chmod(os.path.join(dirpath, filename), fileperms)

    @staticmethod
    def create_dir(path):
        os.makedirs(path, exist_ok=True)
        # try:
        #     BaseModel._recursive_chmod(path, 0o777, 0o666)
        # except PermissionError:
        #     pass

    @staticmethod
    def _save_weights(model, checkpoint_path):
        model.save_weights(checkpoint_path)
        # weights = model.get_weights()
        # weights_names = ["_".join(w._unique_id.split("_")[:-1])
        #                  for w in model.weights]
        # with h5py.File(checkpoint_path, "w") as file:
        #     for wn, w in zip(weights_names, weights):
        #         ds = file.create_dataset(wn, data=w)
        os.chmod(checkpoint_path, 0o666)

    @staticmethod
    def _load_weights(model, checkpoint_path):
        model.load_weights(checkpoint_path, by_name=True)
        # weights_names = ["_".join(w._unique_id.split("_")[:-1])
        #                  for w in model.weights]
        # with h5py.File(checkpoint_path, "r") as file:
        #     weights = [np.asarray(file[wn]) for wn in weights_names]
        #     model.set_weights(weights)

    @staticmethod
    def _save_model(model, checkpoint_path):
        model.save(checkpoint_path)
        os.chmod(checkpoint_path, 0o666)

    @staticmethod
    def _load_model(checkpoint_path):
        return tf.keras.models.load_model(checkpoint_path)

    @staticmethod
    def assemble_model_name(name, identifier, layers):
        """Assemble the unique name of a model base on its layers."""
        # TODO: refactor tensorflow part out and implement human readable hashing
        # layer_strings = [l[1] if isinstance(l[1], int)
        #                  else l[1][0] if (isinstance(l[1], (list, tuple))
        #                                   and len(l[1]) > 0
        #                                   and isinstance(l[1][0], int))
        #                  else l[2]["num_units"] if (len(l) > 1 and
        #                                             "num_units" in l[2].keys())
        #                  # else l[0].__name__ if callable(l[0])
        #                  else None
        #                  for l in layers]
        layer_strings = []
        for layer in layers:
            if hasattr(layer, "units"):
                if hasattr(layer, "cell"):
                    # Recurrent layer
                    recurrent_layer_name = (
                        f"{layer.units:d}R{layer.__class__.__name__[0]:s}"
                    )
                    layer_strings.append(recurrent_layer_name)
                else:
                    # Typical dense layer
                    layer_strings.append(f"{layer.units:d}")
            elif hasattr(layer, "layer") and hasattr(layer.layer, "units"):
                # Typical wrapped layer (e.g. time-distributed)
                layer_strings.append(f"{layer.layer.units:d}")
            elif hasattr(layer, "filters") and hasattr(layer, "kernel_size"):
                # Convolutional layer
                short_kernel_size = set(layer.kernel_size)
                if len(short_kernel_size) == 1:
                    short_kernel_size = str(list(short_kernel_size)[0])
                else:
                    short_kernel_size = "x".join(layer.kernel_size)
                conv_layer_name = f"{layer.filters:d}C{short_kernel_size:s}"
                layer_strings.append(conv_layer_name)
            elif hasattr(layer, "pool_size"):
                # Convolutional layer
                short_pool_size = set(layer.pool_size)
                if len(short_pool_size) == 1:
                    short_pool_size = str(list(short_pool_size)[0])
                else:
                    short_pool_size = "x".join(layer.pool_size)
                pool_layer_name = f"{layer.__class__.__name__[0]:s}P{short_pool_size:s}"
                layer_strings.append(pool_layer_name)
            else:
                layer_strings.append(None)
        layer_strings = [ls for ls in layer_strings if ls not in (None, 0, -1)]
        if identifier:
            name = "-".join([name, identifier])
        return "-".join([name] + list(map(str, layer_strings)))

    @staticmethod
    def _calc_model_memory_usage(batch_size, model):
        shapes_mem_count = 0
        internal_model_mem_count = 0
        for layer in model.layers:
            layer_type = layer.__class__.__name__
            if layer_type == "Model":
                internal_model_mem_count += BaseModel._calc_model_memory_usage(
                    batch_size, layer
                )
            single_layer_mem = 1
            out_shape = layer.get_output_shape_at(-1)
            if isinstance(out_shape, list):
                out_shape = out_shape[0]
            for s in out_shape:
                if s is None:
                    continue
                single_layer_mem *= s
            shapes_mem_count += single_layer_mem

        trainable_count = np.sum([K.count_params(p) for p in model.trainable_weights])
        non_trainable_count = np.sum(
            [K.count_params(p) for p in model.non_trainable_weights]
        )

        number_size = 4.0
        if K.floatx() == "float16":
            number_size = 2.0
        if K.floatx() == "float64":
            number_size = 8.0

        total_memory = number_size * (
            batch_size * shapes_mem_count + trainable_count + non_trainable_count
        )
        gbytes = np.round(total_memory / (1024.0**3), 3) + internal_model_mem_count
        return gbytes

    @staticmethod
    def _clean_model_dict(d):
        if isinstance(d, dict):
            return {k: BaseModel._clean_model_dict(v) for k, v in d.items()}
        if isinstance(d, (tuple, list)):
            return [BaseModel._clean_model_dict(v) for v in d]
        if isinstance(d, np.ndarray):
            return d.tolist()
        if callable(d):
            # Replace functions with their name
            if hasattr(d, "__name__"):
                return d.__name__
            if hasattr(d, "name"):
                return d.name
            if hasattr(d, "__repr__"):
                return d.__repr__()
            return "(unknown)"
        try:
            json.dumps(d)
        except TypeError:
            try:
                return d.__name__
            except AttributeError:
                try:
                    return d.__repr__()
                except AttributeError:
                    return "(not serializable)"
        return d

    def _collect_model_function_args(self, model_fun, hp):
        # Maybe pass data definition
        num_all_args = model_fun.__code__.co_argcount
        if model_fun.__defaults__ is not None:
            num_kwargs = len(model_fun.__defaults__)
        else:
            num_kwargs = 0
        # signature = inspect.signature(model_fun)
        # num_positional = sum(
        #     1 for param in signature.parameters.values()
        #     if param.kind == param.POSITIONAL_ONLY)
        num_positional = num_all_args - num_kwargs
        data_definition = self.data_definition
        if num_positional == 2:
            return (hp, data_definition), {}
        if num_positional == 1:
            # Used data definition may be defined outside the scope
            return (hp,), {}
        if num_positional == 0:
            # Used data definition may be defined outside the scope
            return (), {}
        raise ValueError("Invalid core model function.")

    def read_tfrecords(self, tfr_files, disable_feature_merge=False):
        """Reads tfrecord files and return their contents.

        Args:
            tfr_files:              a list or tuple of tfrecord files
            disable_feature_merge:  keep features in data separate or merge

        Returns:
            samples:                the samples after reading
        """
        if isinstance(tfr_files, str):
            tfr_files = [tfr_files]
        return self.data_reader.read_tfrecords(
            tfr_files, disable_feature_merge=disable_feature_merge
        )

    @staticmethod
    def plot_feature_distribution(samples, path, subdir=None):
        """Plot the distribution of features in a sample.

        Args:
            samples (dict): A dictionary with feature tensors over multiple samples
            path (PathLike): The path to a file or directory to save the plot
            subdir (str, optional): Name of a subdirectory to create if given directory
        """
        # TODO: split multi-feature entries to single feature entries
        # TODO: move to DataReader and add direct link to BaseModel
        path = Path(path)
        cleaned_samples = {
            k: np.mean(v, axis=tuple(range(1, np.ndim(v))))
            for k, v in samples.items()
            if np.issubdtype(v.dtype, np.number)
        }
        sample_df = pd.DataFrame.from_dict(cleaned_samples)
        sns.set_theme(style="ticks")
        g = sns.PairGrid(sample_df, diag_sharey=False)
        g.map_upper(sns.scatterplot, s=15)
        g.map_diag(sns.kdeplot)
        g.map_lower(sns.kdeplot, fill=True)
        plt.draw()
        if path.is_dir():
            if subdir is not None:
                path = path / subdir
            path.mkdir(exist_ok=True)
            path = path / "feature_distributions"
        plt.savefig(os.fspath(path))

    def check_feature_distribution(self, samples, path):
        self.warn(
            "Method check_feature_distribution(...) has been renamed to"
            + " plot_feature_distribution(...)"
        )
        self.plot_feature_distribution(samples, path, subdir="plot")

    def build(self, *args, **kwargs):
        raise NotImplementedError

    def clear(self, *args, **kwargs):
        raise NotImplementedError

    def save(self, *args, **kwargs):
        raise NotImplementedError

    def load(self, *args, **kwargs):
        raise NotImplementedError

    def train(self, *args, **kwargs):
        raise NotImplementedError

    def fit(self, *args, **kwargs):
        """Alias for CIDSModel.train(...) for SKlearn compatibility."""
        return self.train(*args, **kwargs)

    def predict(self, *args, **kwargs):
        raise NotImplementedError

    def infer_data(self, *args, **kwargs):
        raise NotImplementedError

    def infer(self, *args, **kwargs):
        """Alias for CIDSModel.infer_data(...) for Keras compatibility."""
        return self.infer_data(*args, **kwargs)

    def _get_keras_metric(self, metric_string):
        # Fallback for keras using a different accuracy metric in eval and train
        if metric_string.lower() == "accuracy" and self.encode_categorical in [
            True,
            "output",
            "outputs",
        ]:
            metric_string = "categorical_accuracy"
        # Cycle through options
        metric_options = [
            metric_string,
            metric_string.capitalize(),
            metric_string.upper(),
            metric_string.lower(),
        ]
        for m in metric_options:
            try:
                return tf.keras.metrics.get(m)
            except ValueError:
                continue
        raise ValueError(
            f"No compatible keras metric found: {metric_string}. "
            + f"Tried: {metric_options}."
        )

    def _filter_metrics(self, metrics, prioritize="keras"):
        keras_metrics = []
        functional_metrics = []
        for m in metrics:
            if isinstance(m, tf.keras.metrics.Metric):
                keras_metrics.append(m)
            elif isinstance(m, tuple(v.__class__ for v in skmetrics.SCORERS.values())):
                functional_metrics.append(m._score_func)
            elif callable(m):
                functional_metrics.append(m)
            elif isinstance(m, str):
                try:
                    if prioritize == "keras":
                        keras_metrics.append(self._get_keras_metric(m))
                    else:
                        functional_metrics.append(skmetrics.SCORERS[m]._score_func)
                except ValueError:
                    try:
                        functional_metrics.append(skmetrics.SCORERS[m]._score_func)
                    except KeyError as e:
                        raise ValueError(
                            f'Metric "{m}" could not be identified.'
                        ) from e
            else:
                raise ValueError(f'Metric "{repr(type(m))}" could not be identified.')
        return keras_metrics, functional_metrics

    def eval_data(self, *args, **kwargs):
        raise NotImplementedError

    def score(self, *args, **kwargs):
        """Alias for CIDSModel.eval_data(...) for SKlearn compatibility."""
        return self.eval_data(*args, **kwargs)

    def evaluate(self, *args, **kwargs):
        """Alias for CIDSModel.eval_data(...) for Keras compatibility."""
        return self.eval_data(*args, **kwargs)

    def eval(self, *args, **kwargs):
        """Alias for CIDSModel.eval_data(...) for Keras compatibility."""
        return self.eval_data(*args, **kwargs)

    def _threaded_to_json(self, out_dict, filename):
        thread = Thread(
            target=self.to_json,
            args=(filename,),
            kwargs=out_dict,
        )
        thread.start()
        thread.join()

    def to_json(self, file, **kwargs):
        """Serialize model to human-readable json file.

        Args:
            file:       a json file
            **kwargs:   a dictionary of additional data to store in the json
        """
        out_dict = dict(**kwargs)
        # Model and test results json
        if "/" in file or "\\" in file:
            json_path = file
        else:
            json_path = os.path.join(self.model_dir, file)
        with open(json_path, "w+", encoding="utf8") as json_file:
            # Collect, clean, and output
            out_dict = self._clean_model_dict(out_dict)
            json_file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )
        os.chmod(json_path, 0o777)

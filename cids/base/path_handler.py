# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Methods for filesystem path handling. Part of the CIDS toolbox.

    Classes:
        BasePathHandler:    Handles files and directories in the file system

"""
import copy
import fnmatch
import os
from pathlib import Path
from pathlib import PurePosixPath

from .logging import BaseLoggerMixin


class BasePathHandler(BaseLoggerMixin):
    """Base class for file and directory handling classes."""

    @staticmethod
    def _branch_search(start_dir, branch_globs):
        assert isinstance(
            branch_globs, (list, tuple, str)
        ), "Parameter branch_globs must be list, tuple, or string. Received: " + str(
            type(branch_globs)
        )
        if isinstance(branch_globs, str):
            branch_globs = [branch_globs]
        branches = [
            f
            for branch_glob in branch_globs
            for f in BasePathHandler._find_paths(
                start_dir, branch_glob, allow_missing=True
            )
        ]
        return branches

    def _check_consist_file_hierarchy(self, trees, strict=False):
        max_num_leaves = max(len(v) for v in trees.values())
        incomplete_trees = {
            branch: leaves
            for branch, leaves in trees.items()
            if len(leaves) < max_num_leaves
        }
        if len(incomplete_trees):
            msg = (
                "Some branch files have fewer leaves, maybe due to "
                + f"incomplete execution: {repr(incomplete_trees)}"
            )
            if strict:
                raise ValueError(msg)
            self.warn(msg)
            # Filter out incomplete trees
            trees = {
                branch: leaves
                for branch, leaves in trees.items()
                if branch not in incomplete_trees.keys()
            }
        return trees

    def scan_file_hierarchy(
        self, src_dir, branch_globs, leaf_globs, strict_check=False
    ):
        """Search files matching the globs and associate branches with leaves.

        Args:
            src_dir:        source directory to search
            branch_globs:    list of glob patterns for branch files
            leaf_globs:     list of glob patterns for leaf files for the branches
            strict_check:   raise (True) / warn (False) for inconsistent leaf numbers

        Returns:
            Dictionary of trees {branch_1: [leaf_1, leaf_2, ...], branch2: ...}

        """
        trees = {}
        branches = self._branch_search(src_dir, branch_globs)
        assert len(branches), "No branch files found. Check the glob pattern."
        max_depth = max(len(t.parents) for t in branches)
        branches = [t for t in branches if len(t.parents) >= max_depth]
        for branch in branches:
            branch_dir = branch.parent
            leaves = self._branch_search(branch_dir, leaf_globs)
            if leaf_globs and not leaves:
                self.warn(
                    f"No leaf files found for branch file {os.fspath(branch)}."
                    " Check the glob pattern."
                )
            parallel_branches = self._branch_search(branch_dir, branch_globs)
            if len(parallel_branches) <= 1 or not leaves:
                # Only a single branch file found. Associate leaves with unique branch
                trees[branch] = leaves
            else:
                # Multiple branch files found. Filter leaves to separate.
                tree = {}
                tmp_leaves = copy.deepcopy(leaves)
                for pbranch in parallel_branches:
                    match_string = pbranch.stem.split(".", maxsplit=1)[0]
                    # TODO: remove parallel branches if they match string
                    pbranch_leaves = [
                        leave
                        for leave in tmp_leaves
                        if match_string in os.fspath(leave)
                    ]
                    if pbranch_leaves:
                        tmp_leaves = list(set(tmp_leaves) - set(pbranch_leaves))
                        tree[pbranch] = pbranch_leaves
                    else:
                        self.warn(
                            f"Found branch file without any leaves: {pbranch}."
                            + " This may mean that a simulation did not complete,"
                            + " that the leave files are difficult to find,"
                            + " or file names are ambiguous."
                        )
                trees[branch_dir] = tree
        self._check_consist_file_hierarchy(trees, strict=strict_check)
        return trees

    @staticmethod
    def _find_paths(start_dir, pattern, allow_missing=False):
        """Walk from starting directory and find files and parent directories."""
        paths = Path(start_dir).absolute().glob(pattern)
        files = [p for p in paths if p.is_file()]
        if not (allow_missing or files):
            raise FileNotFoundError(
                f"No file matching pattern {pattern} found in: {os.fspath(start_dir)}"
            )
        # Sort files alphabetically
        files = sorted(files)
        return files

    @staticmethod
    def _exclude_paths(paths, exclude_patterns):
        for e in exclude_patterns:
            paths = [
                path
                for path in paths
                if not any(fnmatch.fnmatch(p, e) for p in path.parts)
            ]
        return paths

    @staticmethod
    def _check_if_child_of(child, parent):
        check = Path(child).absolute().is_relative_to(Path(parent).absolute())
        if not check:
            check = Path(child).absolute().is_relative_to(Path(parent).resolve())
        if not check:
            check = Path(child).resolve().is_relative_to(Path(parent).absolute())
        if not check:
            check = Path(child).resolve().is_relative_to(Path(parent).resolve())
        return check

    @staticmethod
    def _to_relative_paths(paths, root_dir):
        """Extract relative paths from a root directory and list of absolute paths.

        Args:
            paths (list): A list of PathLike objects in root directory.
            root_dir (PathLike): The path of the root directory

        Returns:
            [type]: [description]
        """
        root_dir = Path(root_dir).absolute()
        # Single path
        if isinstance(paths, (os.PathLike, str)):
            p = Path(paths)
            if not p.is_absolute():
                return p
            try:
                return p.absolute().relative_to(root_dir)
            except ValueError:
                return p.resolve().relative_to(root_dir.resolve())

        # Recursion for all iterables
        return [BasePathHandler._to_relative_paths(p, root_dir=root_dir) for p in paths]

    @staticmethod
    def _to_absolute_paths(paths, root_dir):
        """Join a root directory to list of paths relative to root directory.

        Args:
            paths (list): A list of PathLike objects relative to root directory.
            root_dir (PathLike): The path of the root directory
        """
        root_dir = Path(root_dir).absolute()
        # Single path
        if isinstance(paths, (os.PathLike, str)):
            p = Path(paths)
            if p.is_absolute():
                return p
            return root_dir.joinpath(p)
        # Recursion for all iterables
        return [BasePathHandler._to_absolute_paths(p, root_dir=root_dir) for p in paths]

    @staticmethod
    def _to_generic_paths(paths):
        if isinstance(paths, (os.PathLike, str)):
            # Native path on native system
            return str(PurePosixPath(BasePathHandler._to_pathlib_paths(paths)))
        # Recursion for all iterables
        return [BasePathHandler._to_generic_paths(p) for p in paths]

    @staticmethod
    def _to_filesystem_paths(paths):
        # Single path
        if isinstance(paths, (os.PathLike, str)):
            return os.fspath(paths)
        # Recursion for all iterables
        return [BasePathHandler._to_filesystem_paths(p) for p in paths]

    @staticmethod
    def _to_pathlib_paths(paths):
        # Single path
        if isinstance(paths, str):
            # Fix paths not native to current system
            return Path(paths.replace("\\", os.sep).replace("/", os.sep))
        if isinstance(paths, os.PathLike):
            # Native path on native system
            return Path(paths)
        # Recursion for all iterables
        return [BasePathHandler._to_pathlib_paths(p) for p in paths]

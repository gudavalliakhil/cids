# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Contains the common base functionality of CIDS. Part of the CIDS toolbox.


.. autosummary::
    :toctree: _autosummary

    BaseLoggerMixin
    StreamToLogger
    BaseModel
    BasePathHandler
    logging
    model
    path_handler
"""
from .logging import BaseLoggerMixin
from .logging import StreamToLogger
from .model import BaseModel
from .path_handler import BasePathHandler

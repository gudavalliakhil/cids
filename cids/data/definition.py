# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data definition and description functionality. Part of the CIDS toolbox.

    Classes:
        BaseData:       Common base class for all data classes.
        Feature:        Data class for a single (usually numerical) tensor
                        defined by name, shape and format
        DataDefinition: Data class for multiple features.

"""
import json
import os
from collections import OrderedDict
from copy import deepcopy
from pathlib import Path

import pandas as pd
import tensorflow as tf

from ..base.path_handler import BasePathHandler


class BaseData:
    def __init__(self, dtype=None, decode_str_to=None):
        """Abstract base class for Feature and DataDefinition."""
        self.dtype = dtype
        self.decode_str_to = decode_str_to

    @property
    def dtype(self):  # pylint: disable=missing-function-docstring
        return self._dtype

    @staticmethod
    def _tf_dtype(value):
        try:
            return tf.as_dtype(value)
        except TypeError:
            return tf.string

    @dtype.setter
    def dtype(self, value):  # pylint: disable=missing-function-docstring
        self._dtype = self._tf_dtype(value)

    @property
    def decode_str_to(self):  # pylint: disable=missing-function-docstring
        return self._decode_str_to

    @decode_str_to.setter
    def decode_str_to(self, value):  # pylint: disable=missing-function-docstring
        if value is None:
            self._decode_str_to = value
        else:
            self._decode_str_to = self._tf_dtype(value)

    @property
    def batch_axis(self):  # pylint: disable=missing-function-docstring
        return self.data_format.index("N")

    @property
    def feature_axis(self):  # pylint: disable=missing-function-docstring
        # Always counted from the back
        if "F" in self.data_format:
            return self.data_format.index("F") - len(self.data_format)
        if "C" in self.data_format:
            return self.data_format.index("C") - len(self.data_format)
        raise ValueError("No feature axis: F or C")

    @property
    def sequence_axis(self):  # pylint: disable=missing-function-docstring
        try:
            return self.data_format.index("S")
        except ValueError:
            return None

    @property
    def sequence_length(self):  # pylint: disable=missing-function-docstring
        if self.sequence_axis is not None:
            return self.data_shape[self.sequence_axis]
        return None

    @sequence_length.setter
    def sequence_length(self, value):  # pylint: disable=missing-function-docstring
        if self.sequence_axis is not None:
            self.data_shape[self.sequence_axis] = value
        else:
            raise AttributeError(
                f"No sequence axis in data_format ({self.data_format}) "
                + f"of feature: {self.name}"
            )

    @property
    def spatial_axes(self):  # pylint: disable=missing-function-docstring
        return [i for i, a in enumerate(self.data_format) if a in "DWHXYZ"]

    @property
    def iter_axis(self):  # pylint: disable=missing-function-docstring
        try:
            return self.data_format.index("I")
        except ValueError:
            return None

    # def clean_cache(self):  # pylint: disable=missing-function-docstring
    #     delete_lazy_property_cache(self)


class Feature(BaseData):
    def __init__(
        self,
        name,
        data_shape,
        data_format="NF",
        dtype=tf.string,
        decode_str_to=tf.float64,
        categories=None,
    ):
        """A feature that defines part of a sample.

        A feature is a named subset of the data of each sample, usually a
        tensor, that is present in all samples of a dataset. The data format,
        i.e. the order of the axes of a feature tensor is fixed, but the shape
        of the data can be variable along some axes. Some features require
        decoding before they are turned into a tensor.

        Args:
            name:           a unique name that identifies the feature
            data_shape:     the shape of the data (use None for unknown)
            data_format:    a data format string that defines order of the axes
                                N: batch axis, F: feature axis,
                                S: sequence axis, X,Y,Z: spatial axes
                                (e.g. "NF", "NSF" or "NXYF", N and F required)
            dtype:          data type the tensor is stored in
                                (defaults to bytestring)
            decode_str_to:  data type to decode bytestrings to
            categories:     Names for categories of encoded categorical feature
        """
        super().__init__(dtype=dtype, decode_str_to=decode_str_to)
        for _ in range(2 - len(data_shape)):
            data_shape = [1] + list(data_shape)
        assert "N" in data_format, "Requires batch axis: N"
        assert "F" in data_format or "C" in data_format, "Requires feature axis: F or C"
        assert len(data_shape) == len(data_format)
        self.name = name
        self.data_shape = list(data_shape)
        self.data_format = data_format
        self.categories = categories or []

    def to_json(self):
        """Gather feature definition in a dictionary for serialization."""
        if self.decode_str_to is not None:
            decode_str_to = self.decode_str_to.name
        else:
            decode_str_to = self.decode_str_to
        json_dict = {
            "name": self.name,
            "data_shape": self.data_shape,
            "data_format": self.data_format,
            "dtype": self.dtype.name,
            "decode_str_to": decode_str_to,
            "categories": self.categories,
        }
        return json_dict

    def slice(self, slice_string):
        """Slice the feature along the feature axis.

        Args:
            slice_string:   a string that defines slices of the feature
                                (e.g. "[1, 3, 4]")
        """
        slice_string = slice_string.replace(" ", "").replace("[", "").replace("]", "")
        # Create new instance by deepcopying
        inst = deepcopy(self)
        # Update name and shape
        inst.name += f"[{slice_string}]"
        indices = [int(i) for i in slice_string.split(",")]
        for i in indices:
            num_features = self.data_shape[self.feature_axis]
            if abs(i) >= num_features:
                raise ValueError(
                    f"Invalid slice string: [{slice_string}]. "
                    + f"Feature {self.name} only has {num_features} feature dimensions"
                )
        new_num_features = len(indices)
        data_shape = list(deepcopy(inst.data_shape))
        data_shape[inst.feature_axis] = new_num_features
        inst.data_shape = tuple(data_shape)
        return inst

    @property
    def tfr_feature(self):  # pylint: disable=missing-function-docstring
        # # TODO: Variable sequence axis position
        # if "S" in self.data_format:
        #     if self.dtype == tf.string:
        #         return tf.io.FixedLenSequenceFeature(
        #             self.data_shape[2:], self.dtype, default_value=""
        #         )  # TODO: check this to work
        #     return tf.io.FixedLenSequenceFeature(
        #         self.data_shape[2:], self.dtype, default_value=tf.cast(0, self.dtype)
        #     )
        if self.dtype == tf.string:
            return tf.io.FixedLenFeature((), tf.string, default_value="")
        return tf.io.FixedLenFeature(
            self.data_shape[1:], self.dtype, default_value=tf.cast(0, self.dtype)
        )

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise ValueError(
                f"Can only compare two instanced of {self.__class__.__name__}"
            )
        return (
            self.name == other.name
            and self.data_shape == other.data_shape
            and self.data_format == other.data_format
            and self.decode_str_to == other.decode_str_to
            and self.dtype == other.dtype
        )


class LegacyFeature(Feature):
    def __init__(  # pylint: disable=super-init-not-called
        self,
        name,
        data_shape,
        data_format="NF",
        dtype=tf.string,
        decode_str_to=tf.float64,
    ):
        """A feature that defines part of a sample.

        A feature is a named subset of the data of each sample, usually a
        tensor, that is present in all samples of a dataset. The data format,
        i.e. the order of the axes of a feature tensor is fixed, but the shape
        of the data can be variable along some axes. Some features require
        decoding before they are turned into a tensor.

        Args:
            name:           a unique name that identifies the feature
            data_shape:     the shape of the data (use None for unknown)
            data_format:    a data format string that defines order of the axes
                                N: batch axis, F: feature axis,
                                S: sequence axis, X,Y,Z: spatial axes
                                (e.g. "NF", "NSF" or "NXYF", N and F required)
            dtype:          data type the tensor is stored in
                                (defaults to bytestring)
            decode_str_to:  data type to decode bytestrings to
        """
        self.name = name
        self.data_shape = data_shape
        self.data_format = data_format
        self._dtype = dtype
        self.decode_str_to = decode_str_to

    @property
    def tfr_feature(self):  # pylint: disable=missing-function-docstring
        if self.dtype == tf.string and self.decode_str_to is not None:
            return tf.io.FixedLenFeature((), tf.string, default_value="")
        return tf.io.FixedLenFeature(
            [], self.dtype, default_value=tf.cast(0, self.dtype)
        )


class DataDefinition(BasePathHandler, BaseData):
    def __init__(
        self,
        *features,
        input_features=None,
        output_features=None,
        dtype=tf.float64,
        merge_strategy="all",
    ):
        """Provides a definition for all data in a dataset.

        Contains a list of features and defines how they are merged together
        into inputs and outputs for CIDSModels. Provides access to shared axes
        and tfrecord feature instructions for the reading of tfrecord files.
        Can be serialized to and instantiated from a json file.

        Args:
            *features:          Feature objects that define data features
            input_features:     list of input feature names with optionally
                                appended slice strings for partial features
                                    (e.g. "[1, 3, 4]")
            output_features:    list of output feature names with optionally
                                appended slice strings for partial features
                                    (e.g. "[1, 3, 4]")
            dtype:              shared data type that all features are cast to
                                during merging
            merge_strategy:     How to collect multiple input and output features
        """
        self._start_logging()
        super().__init__(dtype=dtype, decode_str_to=None)
        # Features maintain order, to ensure tensor indexing remains constant
        self.features = OrderedDict((feature.name, feature) for feature in features)
        for feature in features:
            assert isinstance(
                feature, Feature
            ), "Data features must be of class Feature."
        # Feature merge strategy
        self.merge_strategy = merge_strategy
        # Input and output features
        self._input_features = []
        if input_features is None:
            self.input_features = []
        else:
            self.input_features = input_features
        self._output_features = []
        if output_features is None:
            self.output_features = []
        else:
            self.output_features = output_features
        # Samples and datasets
        self._samples = None
        # File
        self._file = None

    @classmethod
    def from_json(
        cls, src, input_features=None, output_features=None, dtype=tf.float64
    ):
        """Instantiates a data definition by reading a json file.

        Contains a list of features and defines how they are merged together
        into inputs and outputs for CIDSModels. Provides access to shared axes
        and tfrecord feature instructions for the reading of tfrecord files.
        Can be serialized to and instantiated from a json file.

        Args:
            src:                a json file or json dict with serialized Features
            input_features:     list of input feature names with optionally
                                appended slice strings for partial features
                                    (e.g. "[1, 3, 4]")
            output_features:    list of output feature names with optionally
                                appended slice strings for partial features
                                    (e.g. "[1, 3, 4]")
            dtype:              shared data type that all features are cast to
                                during merging
        """
        if isinstance(src, dict):
            # File already loaded
            json_dict = src
            path = None
        elif isinstance(src, (str, Path)):
            path = Path(src)
            assert path.exists(), f"File not found: {os.fspath(path)}"
            with path.open(encoding="utf8") as ofile:
                json_dict = json.load(ofile)
        else:
            raise ValueError(f"Invalid json type: {type(src)}")
        if "features" in json_dict.keys():
            features_dict = dict(json_dict["features"])
            if input_features is None and "input_features" in json_dict.keys():
                input_features = json_dict["input_features"]
            if output_features is None and "output_features" in json_dict.keys():
                output_features = json_dict["output_features"]
        else:
            # Fallback to old format
            features_dict = json_dict
        merge_strategy = json_dict.get("merge_strategy", "all")
        features = [Feature(**f) for f in features_dict.values()]
        # Initialize data definition
        inst = cls(
            *features,
            input_features=input_features,
            output_features=output_features,
            merge_strategy=merge_strategy,
            dtype=dtype,
        )
        # Link to data definition file
        if path is not None:
            inst.file = path
        # Restore samples
        if "samples" in json_dict.keys():
            samples = json_dict["samples"]
            if path is not None:
                samples = list(inst._to_pathlib_paths(samples))
                samples = list(inst._to_absolute_paths(samples, root_dir=path.parent))
            inst.samples = samples
        return inst

    @classmethod
    def from_dataframe(cls, data_frame):
        records = data_frame.to_dict(orient="records")
        # TODO: better pandas dataframe handling
        features = []
        for row in records:
            try:
                # Read
                name = row["name"]
                decode_str_to = row["decode_str_to"]
                dtype = row["dtype"]
                data_format = row["data_format"]
                data_shape = row["data_shape"]
                # Process
                name = name.strip()
                decode_str_to = cls._replace_null_strings(decode_str_to.strip())
                if decode_str_to is not None:
                    decode_str_to = cls._tf_dtype(decode_str_to)
                data_format = data_format.strip()
                data_shape = [
                    int(d.strip())
                    if d.strip().isnumeric()
                    else cls._replace_null_strings(d.strip())
                    for d in data_shape.split(",")
                ]
                # Instantiate feature
                features.append(
                    Feature(
                        name,
                        data_shape,
                        data_format=data_format,
                        dtype=dtype,
                        decode_str_to=decode_str_to,
                    )
                )
            except KeyError:
                cls.warn("Feature definition incomplete: " + name)
                continue
        return cls(*features)

    @classmethod
    def from_table(cls, table):
        return cls.from_dataframe(pd.DataFrame.from_records(table))

    @classmethod
    def placeholder(cls):
        empty_feature = Feature("empty", [None, 1, 1, 1, 1, 1, 1], "NSXYZF")
        return cls(empty_feature, input_features=["empty"], output_features=["empty"])

    @staticmethod
    def _replace_null_strings(string):
        null_strings = ["None", "none", "Null", "null"]
        if string in null_strings:
            return None
        return string

    def to_json(
        self,
        path=None,
        serialize_selected_features=False,
        serialize_samples=True,
        write=True,
    ):
        """Serialize all features into a json file.

        Args:
            path:                           a root path to serialize as json
            serialize_selected_features:    write input and output features to json?
            serialize_samples:              write samples to json?
            write:                          whether to write to a ffile
        """
        if path is not None:
            path = Path(path)
            if path.suffix == ".json":
                directory = path.parent
                file = path
            else:
                directory = path
                file = path / "data_definition.json"
        elif self.file is not None:
            directory = self.file.parent
            file = self.file
        else:
            directory = None
            file = None
        json_dict = {}
        # Serialize features
        features_dict = {k: v.to_json() for k, v in self.features.items()}
        json_dict["features"] = features_dict
        # Serialize samples
        if serialize_samples:
            if directory is not None:
                # Store relative paths to tfrecord directory if possible
                samples = list(
                    self._to_relative_paths(self.samples, root_dir=file.parent)
                )
            else:
                samples = self.samples
            samples = list(self._to_generic_paths(samples))
            json_dict["samples"] = samples
        # Serialize input and output features for project
        if serialize_selected_features:
            json_dict["input_features"] = self.input_features
            json_dict["output_features"] = self.output_features
        json_dict["merge_strategy"] = self.merge_strategy
        if file is not None and write:
            # Write json to file
            file.parent.mkdir(parents=True, exist_ok=True)
            with file.open("w+", encoding="utf8") as f:
                f.write(
                    json.dumps(
                        json_dict, sort_keys=True, indent=4, separators=(",", ": ")
                    )
                )
        return json_dict

    def to_dataframe(self):
        # Serialize data definition to dictionary
        data_definition_dict = self.to_json(
            write=False, serialize_samples=False, serialize_selected_features=False
        )
        # Fallbacks for old style data_definitions
        features = data_definition_dict.get("features", data_definition_dict)
        # Gather data definition
        records = []
        for _, feature_dict in features.items():
            # List to string sequence
            feature_dict["data_shape"] = ",".join(
                str(d) for d in feature_dict["data_shape"]
            )
            # Everything else to string
            for key, val in feature_dict.items():
                if not isinstance(val, str):
                    feature_dict[key] = str(val)
            records.append(feature_dict)
        # TODO: better pandas dataframe handling
        if records:
            return pd.DataFrame.from_records(records)
        return pd.DataFrame(
            columns=["name", "data_shape", "data_format", "decode_str_to", "dtype"]
        )

    def to_table(self):
        return self.to_dataframe().to_dict(orient="records")

    def __getitem__(self, item):
        if item in self.features.keys():
            return self.features[item]
        if item == "X":
            return self.input
        if item == "Y":
            return self.output
        raise KeyError("Unknown feature: " + str(item))

    def _merge_formats(self, *data_formats):
        data_formats = set(data_formats)
        if len(data_formats) == 1:
            return list(data_formats)[0]
        if len(data_formats) == 2 and "NF" in data_formats:
            return list(data_formats - {"NF"})[0]
        raise ValueError("Data formats cannot be merged: " + str(data_formats))

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, newpath):
        # # Update relative paths of all read samples
        # if self._samples and self._file:
        #     tfrecord_dir = self._file.parent
        #     tmp_samples = [
        #         self._to_relative_paths(s, tfrecord_dir) for s in self._samples
        #     ]
        #     tmp_samples =
        # Reset samples to be rediscovered
        self._samples = None
        # Set new file path
        self._file = Path(newpath)

    @property
    def samples(self):
        if self._samples is None:
            try:
                if self.file is None:
                    self.file = Path.cwd() / "data_definition.json"
                search_dir = self.file.parent
                self._samples = self.find_samples(search_dir)
            except FileNotFoundError:
                self._samples = []
        return self._samples

    @samples.setter
    def samples(self, newlist):
        if self._samples:
            self.warn(f"Overwriting predefined samples list: {repr(newlist)}")
        self._samples = list(self._to_pathlib_paths(list(newlist)))

    # TODO(arnd): generic nested apply of any function/method

    def _get_data_format(self, data_definition_or_dict):
        if isinstance(data_definition_or_dict, DataDefinition):
            return data_definition_or_dict.data_format
        return [self._get_data_format(v) for v in data_definition_or_dict.values()]

    def _get_data_shape(self, data_definition_or_dict):
        if isinstance(data_definition_or_dict, DataDefinition):
            return data_definition_or_dict.data_shape
        return {k: self._get_data_shape(v) for k, v in data_definition_or_dict.items()}

    @property
    def data_format(self):
        """A dictionary with input/output formats or a shared format."""
        if self.input_features and self.output_features:
            # Return both input and output data format
            return {
                "X": self._get_data_format(self.input),
                "Y": self._get_data_format(self.output),
            }
        if not self.input_features and not self.output_features:
            # Either an entire input or an entire output tensor
            return self._merge_formats(
                *(feature.data_format for feature in self.features.values())
            )
        raise ValueError("Requires neither or both input_features and output_features")

    def _merge_shapes(self, *features):
        axis_elements = [
            [
                feature.data_shape[feature.data_format.index(df)]
                for feature in features
                if df in feature.data_format
            ]
            for df in self.data_format
        ]

        def raiser(ex):
            raise ex

        data_shape = [
            None
            if None in ae  # Unspecified axis
            else sum(ae)
            if at == "F"  # Append axis
            else raiser(ValueError("Incompatible tensor shapes for axis " + at))
            if len(set(ae)) > 1 and at != "F"
            else ae[0]  # Shared axis (must be equal)
            for at, ae in zip(self.data_format, axis_elements)
        ]
        return data_shape

    @property
    def feature_shape(self):
        return {k: v.data_shape for k, v in self.features.items()}

    def get_input_feature_indices(self, feature_name):
        assert self.input_features, "DataDefinition.input_features must be set."
        feature_position = self.input_features.index(feature_name)
        start_index = 0
        stop_index = 0
        for p in range(feature_position + 1):
            feature = self.features[feature_name]
            if p < feature_position:
                start_index += feature.data_shape[feature.feature_axis]
            stop_index += feature.data_shape[feature.feature_axis]
        return tuple(range(start_index, stop_index))

    def get_output_feature_indices(self, feature_name):
        assert self.output_features, "DataDefinition.output_features must be set."
        feature_position = self.output_features.index(feature_name)
        start_index = 0
        stop_index = 0
        for p in range(feature_position + 1):
            feature = self.features[feature_name]
            if p < feature_position:
                start_index += feature.data_shape[feature.feature_axis]
            stop_index += feature.data_shape[feature.feature_axis]
        return tuple(range(start_index, stop_index))

    def _create_nested_data_definition(self, *feature_names):
        return DataDefinition(
            *(
                self.features[f.split("[")[0]].slice("[" + f.split("[")[1])
                if "[" in f
                else self.features[f]
                for f in feature_names
            ),
            input_features=[],
            output_features=[],
        )

    def _collate_features(self, feature_names, merge_strategy):
        if merge_strategy == "all":
            collated = self._create_nested_data_definition(*feature_names)
        else:
            collated = {}
            for f in feature_names:
                feature = self.features[f]
                data_format = feature.data_format
                try:
                    collated[data_format].add_feature(feature)
                except KeyError:
                    collated[data_format] = self._create_nested_data_definition(f)
        return collated

    @property
    def input_features(self):
        return self._input_features

    @input_features.setter
    def input_features(self, value):
        self._input_features = value
        for f in self._input_features:
            base_feature = f.split("[")[0]
            assert (
                base_feature in self.features.keys()
            ), f"Input feature {f:s} not a feature."
        # Create nested data definitions to collate featues based on merge strategy
        if self._input_features:
            self.input = self._collate_features(
                self._input_features, self.merge_strategy
            )

    @property
    def output_features(self):
        return self._output_features

    @output_features.setter
    def output_features(self, value):
        self._output_features = value
        for f in self._output_features:
            base_feature = f.split("[")[0]
            assert (
                base_feature in self.features.keys()
            ), f"Output feature {f:s} not a feature."
        if self._output_features:
            self.output = self._collate_features(
                self._output_features, self.merge_strategy
            )

    @property
    def data_shape(self):  # pylint: disable=missing-function-docstring
        """A dictionary with input/output shapes or a shared shape."""
        if self.input_features and self.output_features:
            # Return both input and output data format
            return {
                "X": self._get_data_shape(self.input),
                "Y": self._get_data_shape(self.output),
            }
        if not self.input_features and not self.output_features:
            # CHECK: should this exist? Does this make sense?
            # Either an entire input or an entire output tensor
            return self._merge_shapes(*(feature for feature in self.features.values()))
        raise ValueError("Requires neither or both input_features and output_features")

    @property
    def input_shape(self):  # pylint: disable=missing-function-docstring
        if self.input_features:
            return self.data_shape["X"]
        raise AttributeError(
            "No input_features set. Set in constructor "
            + "DataDefinition(..., input_features=['featurename']), "
            + "DataDefinition.from_json(..., input_features=['featurename']), "
            + "or through the property "
            + "data_definition.input_features=['featurename']"
        )

    @property
    def output_shape(self):  # pylint: disable=missing-function-docstring
        if self.output_features:
            return self.data_shape["Y"]
        raise AttributeError(
            "No output_features set. Set in constructor "
            + "DataDefinition(..., output_features=['featurename']), "
            + "DataDefinition.from_json(..., output_features=['featurename']), "
            + "or through the property "
            + "data_definition.output_features=['featurename']"
        )

    @property
    def batch_axis(self):  # pylint: disable=missing-function-docstring
        if isinstance(self.data_format, str):
            return super().batch_axis
        batch_axes = [v.index("N") for k, v in self.data_format.items()]
        assert len(set(batch_axes)) == 1, "Batch axis must be the same for all features"
        return batch_axes[0]

    @property
    def feature_axis(self):  # pylint: disable=missing-function-docstring
        if isinstance(self.data_format, str):
            return super().feature_axis
        feature_axes = [
            v.index("F") - len(v)
            if "F" in v
            else v.index("C") - len(v)
            if "C" in v
            else None
            for k, v in self.data_format.items()
        ]
        assert (
            len(set(feature_axes)) == 1
        ), "Feature axis (negative index) must be the same for all features"
        return feature_axes[0]

    @property
    def sequence_axis(self):  # pylint: disable=missing-function-docstring
        if isinstance(self.data_format, str):
            return super().sequence_axis
        sequence_axes = {
            k: v.index("S") if "S" in v else None for k, v in self.data_format.items()
        }
        return sequence_axes

    @property
    def sequence_length(self):  # pylint: disable=missing-function-docstring
        sequence_lengths = [
            feature.sequence_length
            for feature in self.features.values()
            if feature.sequence_length is not None
        ]
        if len(set(sequence_lengths)) > 1:
            self.warn(
                "Feature sequence lengths inconsistent:" + f" {repr(sequence_lengths)}"
            )
        return max(sequence_lengths, default=0)

    @property
    def spatial_axes(self):  # pylint: disable=missing-function-docstring
        if isinstance(self.data_format, str):
            return super().spatial_axes
        spatial_axes = {
            k: [i for i, a in enumerate(v) if a in "DWHXYZ"]
            for k, v in self.data_format.items()
        }
        return spatial_axes

    @property
    def iter_axis(self):  # pylint: disable=missing-function-docstring
        if isinstance(self.data_format, str):
            return super().iter_axis
        iter_axes = {
            k: v.index("I") if "I" in v else None for k, v in self.data_format.items()
        }
        return iter_axes

    @property
    def num_input_features(self):  # pylint: disable=missing-function-docstring
        return self.data_shape["X"][self.feature_axis]

    @property
    def num_output_features(self):  # pylint: disable=missing-function-docstring
        return self.data_shape["Y"][self.feature_axis]

    @property
    def tfr_features(self):  # pylint: disable=missing-function-docstring
        # All features with a shape are stored as bytestring of a numpy array object
        # The corresponding data shape is stored separately
        # Parsing the bytestring to data type and reshaping in the the data reader
        # All features with shape () may be stored as arbitrary dtype
        features = {k: v.tfr_feature for k, v in self.features.items()}
        feature_shapes = {
            k + "_shape": tf.io.FixedLenFeature((len(v.data_shape),), tf.int64)
            for k, v in self.features.items()
            if v.dtype == tf.string and v.decode_str_to is not None
        }
        features.update(feature_shapes)
        return features

    @property
    def tfr_features_context(self):  # pylint: disable=missing-function-docstring
        return {
            k: v
            for k, v in self.tfr_features.items()
            if not isinstance(v, tf.io.FixedLenSequenceFeature)
        }

    @property
    def tfr_features_sequential(self):  # pylint: disable=missing-function-docstring
        return {
            k: v
            for k, v in self.tfr_features.items()
            if isinstance(v, tf.io.FixedLenSequenceFeature)
        }

    def check_sequence_lengths(self, strict=True):
        max_sequence_length = self.sequence_length
        short_features = {
            feature_name: feature
            for feature_name, feature in self.features.items()
            if (
                feature.sequence_axis is not None
                and feature.sequence_length < max_sequence_length
            )
        }
        msg = (
            "Features found with sequence length smaller than maximum:"
            + f" {repr(short_features.keys())}. May be caused by selective"
            + " result writing or incomplete execution."
        )
        if short_features:
            if strict:
                raise ValueError(msg)
            self.warn(msg)
        return short_features

    def add_feature(self, feature: Feature):
        """Add a Feature to the DataDefinition."""
        # Update existing or add new feature
        if feature.name in self.features.keys():
            if feature.sequence_axis is not None:
                if (
                    feature.sequence_length
                    > self.features[feature.name].sequence_length
                ):
                    self.features[
                        feature.name
                    ].sequence_length = feature.sequence_length
        else:
            self.features[feature.name] = feature

    def clean_samples(self):
        self._samples = None

    def add_sample(self, path, parent=None):
        """Adds a sample to the DataDefinition for provenance tracking

        Args:
            path (Pathlike): file path
            parent (Pathlike, optional): parent directory. Defaults to None.
        """
        path = Path(path).resolve()
        if parent is None:
            parent = path.parent
        else:
            parent = Path(parent)
        if self._samples is None:
            self._samples = [path]
        else:
            self._samples.append(path)

    def find_samples(
        self, search_dir, pattern="**/*.tfrecord", return_directories=False
    ):
        """Find all tfrecords in the tfrecord_dir or input_dir."""
        # Walk through starting directory and find files and parent directories
        files = self._find_paths(search_dir, pattern)
        # Extract directories
        directories = sorted({f.parent for f in files})
        if len(directories) != 1:
            self.warn(
                f"Found samples matching pattern {pattern} in more than one "
                + f"subdirectory: {str(directories)}"
            )
        # Store samples
        self.samples = files
        if return_directories:
            return files, directories
        return files

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise ValueError(
                f"Can only compare two instanced of {self.__class__.__name__}"
            )
        if len(self.features) != len(other.features):
            return False
        feature_equal = [
            other[feature_name] == feature
            for feature_name, feature in self.features.items()
        ]
        return all(feature_equal)

# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion, reading and processing. Part of the CIDS toolbox.

.. autosummary::
    :toctree: _autosummary

    read_file
    read_file_branch
    read_file_hierarchy
    read_jpg
    read_csv
    read_root_summary
    split_samples
    split_samples_by_directory
    content
    content_image
    content_text
    content_pace
    files
"""
from .content import read_file
from .content import read_file_branch
from .content import read_file_hierarchy
from .content_image import read_jpg
from .content_text import read_csv
from .content_text import read_root_summary
from .files import extract_subjects  # Legacy
from .files import get_list_of_subject_numbers  # Legacy
from .files import get_max_sequence_length  # Legacy
from .files import leave_one_out_split  # Legacy
from .files import split_samples
from .files import split_samples_by_directory
from .files import split_samples_by_subjects  # Legacy
from .files import subject_wise_cross_validation  # Legacy

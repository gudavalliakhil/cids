# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion and processing for text file formats.  Part of the CIDS toolbox."""
import numpy as np
import tensorflow as tf
from PIL import Image

from ..definition import Feature


def read_jpg(file):
    """Process a jpg image file.

    Args:
        file (PathLike): path to a jpg file

    Returns:
        dict: a sample dictionary of numerical tensors in file.
        list: a list of Features in the sample dictionary
    """
    # Read image from file
    image = Image.open(file)
    # Extract tensor and feature information
    ndarray = np.asarray(image)
    dtype = tf.as_dtype(ndarray.dtype)
    image_feature = Feature(
        "image",
        [None] + list(ndarray.shape),
        data_format="NXYF",
        dtype=tf.string,
        decode_str_to=dtype,
    )
    # Assemble sample dictionary
    sample_dict = {"image": ndarray}
    features = [image_feature]
    return sample_dict, features

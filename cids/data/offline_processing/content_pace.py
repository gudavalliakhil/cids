# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion, reading and processing for PACE3D data. Part of the CIDS toolbox."""
import os
import warnings

import numpy as np
import pandas as pd
import tensorflow as tf

try:
    from pypace.lib.fields.scalardata import ScalarDataReader
    from pypace.lib.fields.simgeo import Simgeo
except ImportError:
    warnings.warn("Could not import pypace. PACE conversion will not work!")

from ..definition import Feature


def read_p3s(file):
    field_reader = ScalarDataReader(os.fspath(file))
    field = np.asarray(
        [
            field_reader.read(i)[..., np.newaxis]
            for i in range(field_reader.frame_count)
        ],
        dtype=np.float32,
    )
    field = np.expand_dims(field, axis=0)  # Add batch axis
    # Sample
    field_name = file.stem.split(".")[-1]
    sample_dict = {field_name: field}
    # Feature
    data_shape = [None] + list(field.shape[1:])
    if len(data_shape) == 6:  # 3D
        data_format = "NSXYZF"
    elif len(data_shape) == 5:  # 2D
        data_format = "NSXYF"
    elif len(data_shape) == 4:  # 1D
        raise NotImplementedError("1D pace simulations not yet supported.")
    else:
        raise ValueError(f"Invalid field shape: {repr(field.shape)}")
    features = [
        Feature(
            field_name,
            data_shape=data_shape,
            data_format=data_format,
            dtype=tf.string,
            decode_str_to=tf.float32,
        )
    ]
    return sample_dict, features


def read_p3simgeo(file):
    params = Simgeo.fromfile(os.fspath(file))
    sample_dict = {
        "file_name": np.asarray(file.split(".")[0]),
        "component_count": np.asarray(params.component_count),
        "component_names": np.asarray(params.component_names),
        "domain_sizes": np.asarray(params.domain_sizes),
        "grid_space": np.asarray(params.grid_space),
        "phase_count": np.asarray(params.phase_count),
        "phase_names": np.asarray(params.phase_names),
        "si_scaling_factor": np.asarray(params.si_scaling_factors),
    }

    # Features
    features = []

    for feature_name, ndarray in sample_dict.items():

        dtype = tf.as_dtype(ndarray.dtype)
        try:
            if len(ndarray) > 1:
                # Multiple 1d slices: assume time sequence
                data_shape = [None, ndarray.shape[0], 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NSF",
                        dtype=tf.string,
                        decode_str_to=dtype,
                    )
                )

        except TypeError:
            if dtype == tf.string:
                data_shape = [None, 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NF",
                        dtype=tf.string,
                        decode_str_to=None,
                    )
                )
            else:
                data_shape = [None, 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NF",
                        dtype=tf.string,
                        decode_str_to=dtype,
                    )
                )

    return sample_dict, features


def read_infile(file):
    # TODO: Parse pace infiles

    # Detect the first rows with params
    with file.open("r", encoding="utf8") as content:
        lines = content.readlines()
    params = [s for s in lines if "=" in s]
    params_start = lines.index(params[0])

    # Read the file starting from first rows detected above
    params_fields = pd.read_table(file, sep="=", skiprows=params_start, header=None)
    params_fields = params_fields.T
    cols = params_fields.loc[0]
    params_fields = params_fields[1:]
    params_fields.columns = cols

    # Change values of params into processable dtype (need changes)
    for col in cols:
        try:
            params_fields[col] = params_fields[col].astype(float)
        except TypeError:
            params_fields.drop(columns=col, inplace=True)

    # Making sample and features similar to read_csv
    numeric_df = params_fields.select_dtypes(include=np.number)
    sample_dict = {k: np.asarray(numeric_df[k]) for k in numeric_df.keys()}

    # Features
    features = []
    for feature_name, ndarray in sample_dict.items():
        dtype = tf.as_dtype(ndarray.dtype)
        try:
            if len(ndarray) > 1:
                # Multiple 1d slices: assume time sequence
                data_shape = [None, ndarray.shape[0], 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NSF",
                        dtype=tf.string,
                        decode_str_to=dtype,
                    )
                )

        except TypeError:
            if dtype == tf.string:
                data_shape = [None, 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NF",
                        dtype=tf.string,
                        decode_str_to=None,
                    )
                )
            else:
                data_shape = [None, 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NF",
                        dtype=tf.string,
                        decode_str_to=dtype,
                    )
                )

    return sample_dict, features


def read_h(file):
    # TODO: Parse pace header files
    return {}, []

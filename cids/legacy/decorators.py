# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""CIDS computational intelligence library"""
# TODO: Add class namescope
# TODO: lazy loading decorator
import time
from functools import wraps

import tensorflow as tf
from tensorflow.python.util import tf_decorator

DEBUG = False


def doublewraps(fn):
    """
    The following function is from Danijar Hafner (https://danijar.com/)
        https://danijar.com/structuring-your-tensorflow-models/
        https://gist.github.com/danijar/8663d3bbfd586bffecf6a0094cd116f2

    A decorator decorator, allowing to use the decorator to be used without
    parentheses if not arguments are provided. All arguments must be optional.
    """

    @wraps(fn)
    def decorator(*args, **kwargs):
        if len(args) == 1 and len(kwargs) == 0 and callable(args[0]):
            return fn(args[0])
        return lambda wrapee: fn(wrapee, *args, **kwargs)

    return decorator


@doublewraps
def variable_scoped_method(fn, *args, fn_scope=None, **kwargs):
    """Gives each function the tensorflow namescope of its own function name."""

    @wraps(fn)
    def decorator(*args, **kwargs):
        fn_name = fn_scope or fn.__name__
        with tf.variable_scope(fn_name):
            return fn(*args, **kwargs)

    return tf_decorator.make_decorator(fn, decorator)


@doublewraps
def name_scoped_method(fn, *args, fn_scope=None, **kwargs):
    """Gives each function the tensorflow namescope of its own function name."""

    @wraps(fn)
    def decorator(*args, **kwargs):
        fn_name = fn_scope or fn.__name__
        with tf.name_scope(fn_name):
            return fn(*args, **kwargs)

    return tf_decorator.make_decorator(fn, decorator)


# @doublewraps
# def scoped_lazy_method(fn,
#                        fn_scope=None,
#                        parent_scope=None,
#                        *scope_args,
#                        **scope_kwargs):
#     """Gives each function the tensorflow namescope of its own function name."""
#     base_attribute = "_cache_" + fn.__name__
#     @wraps(fn)
#     def decorator(*args, **kwargs):
#         attribute = base_attribute
#         # print(md5(args[1].name.encode("utf8")).digest())
#         # try:
#         #     attribute += md5(args[1].name.encode("utf8")).digest()
#         # except Exception:
#         #     pass
#         #attribute = attribute.replace(":", "-")
#         #attribute = attribute.replace("/", "-")
#         # get names
#         fn_name = fn_scope or fn.__name__
#         self = args[0]
#         # print(self.__class__.__name__)
#         # if parent_scope is None:
#         #     try:
#         #         cls_scope = self.scope
#         #     except Exception:
#         #         cls_scope = self.__class__.__name__
#         # lazy loading
#         if not hasattr(self, attribute):
#             # scoping
#             # with tf.variable_scope(self.scope):
#             #     print(self.scope)
#             #     print(tf.get_variable_scope())
#             with tf.variable_scope(fn_name, *scope_args, **scope_kwargs):
#                 setattr(self, attribute, fn(*args, **kwargs))
#                 # print("set: " + attribute)
#         # print("get: " + attribute)
#         return getattr(self, attribute)
#     return tf_decorator.make_decorator(fn, decorator)


def delete_lazy_property_cache(inst, attributes=None):
    if attributes is None:
        keys = list(inst.__dict__.keys())
        for attr in keys:
            if attr.startswith("_cache_"):
                delattr(inst, attr)


def lazy_property(fn):
    """
    The following function is from Danijar Hafner (https://danijar.com/)
        https://danijar.com/structuring-your-tensorflow-models/
        https://gist.github.com/danijar/8663d3bbfd586bffecf6a0094cd116f2
    """
    attribute = "_cache_" + fn.__name__

    @property
    @wraps(fn)
    def decorator(self):
        if not hasattr(self, attribute):
            if DEBUG:
                print(attribute + " set.")
            setattr(self, attribute, fn(self))
        else:
            if DEBUG:
                print(attribute + " get.")
        return getattr(self, attribute)

    return decorator


def method_catch_interrupts(fn):
    name = fn.__name__

    def decorator(self, *args, **kwargs):
        try:
            return fn(self, *args, **kwargs)
        except KeyboardInterrupt:
            if hasattr(self, "INDENT"):
                indent = self.INDENT
            else:
                indent = ""
            if self.VERBOSITY:
                print(indent + name + " interrupted by user.")
                print(self.INDENT + "... waiting for further interrupts.")
            time.sleep(3.0)

    return decorator


@doublewraps
def variable_scoped_lazy_property(fn, *args, fn_scope=None, **kwargs):
    """
    The following function is from Danijar Hafner (https://danijar.com/)
        https://danijar.com/structuring-your-tensorflow-models/
        https://gist.github.com/danijar/8663d3bbfd586bffecf6a0094cd116f2

    A decorator for functions that define TensorFlow operations. The wrapped
    fn will only be executed once. Subsequent calls to it will directly
    return the result so that operations are added to the graph only once.
    The operations added by the fn live within a tf.variable_scope(). If
    this decorator is used with arguments, they will be forwarded to the
    variable fn_scope. The fn_scope name defaults to the name of the wrapped
    fn.
    """
    attribute = "_cache_" + fn.__name__
    name = fn_scope or fn.__name__

    @property
    @wraps(fn)
    def decorator(self):
        if not hasattr(self, attribute):
            with tf.variable_scope(name, *args, **kwargs):
                if DEBUG:
                    print(attribute + " set.")
                setattr(self, attribute, fn(self))
        else:
            if DEBUG:
                print(attribute + " get.")
        return getattr(self, attribute)

    return decorator  # tf_decorator.make_decorator(fn, decorator)


@doublewraps
def name_scoped_lazy_property(fn, *args, fn_scope=None, **kwargs):
    """
    The following function is from Danijar Hafner (https://danijar.com/)
        https://danijar.com/structuring-your-tensorflow-models/
        https://gist.github.com/danijar/8663d3bbfd586bffecf6a0094cd116f2

    A decorator for functions that define TensorFlow operations. The wrapped
    fn will only be executed once. Subsequent calls to it will directly
    return the result so that operations are added to the graph only once.
    The operations added by the fn live within a tf.variable_scope(). If
    this decorator is used with arguments, they will be forwarded to the
    variable fn_scope. The fn_scope name defaults to the name of the wrapped
    fn.
    """
    attribute = "_cache_" + fn.__name__
    name = fn_scope or fn.__name__

    @property
    @wraps(fn)
    def decorator(self):
        if not hasattr(self, attribute):
            with tf.variable_scope(name, *args, **kwargs):
                if DEBUG:
                    print(attribute + " set.")
                setattr(self, attribute, fn(self))
        else:
            if DEBUG:
                print(attribute + " get.")
        return getattr(self, attribute)

    return decorator  # tf_decorator.make_decorator(fn, decorator)

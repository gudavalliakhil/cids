#!/bin/bash
#USER=ak584521
#SERVER=cluster-copy.rz.rwth-aachen.de
USER=koeppe
SERVER=tesla
LOCAL_PATH=./INPUT/brittle_fracture/
#REMOTE_PATH=/work/$USER/intelligent_analysis/INPUT/elastoplastic/
REMOTE_PATH=/work/$SERVER/$USER/DATA/INPUT/brittle_fracture/
rsync -aq --rsync-path="mkdir -p $REMOTE_PATH && rsync" $LOCAL_PATH $USER@$SERVER:$REMOTE_PATH

#!/bin/bash
USER=ak584521
SERVER=cluster-copy.rz.rwth-aachen.de
LOCAL_PATH=./
REMOTE_BASE=/work/$USER/intelligent_analysis
rsync -avK \
  $USER@$SERVER:$REMOTE_BASE/CHECKPOINT \
    :$REMOTE_BASE/LOG \
    :$REMOTE_BASE/PROFILE \
    :$REMOTE_BASE/RESULT \
    :$REMOTE_BASE/SUMMARY \
  $LOCAL_PATH

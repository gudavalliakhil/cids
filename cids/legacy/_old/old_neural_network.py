"""IAM computational intelligence library"""
import os
import time

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from scipy.linalg.decomp_schur import eps

from cids._old.dataprocessor_numpy import Dataprocessor


class NeuralNetwork:
    """Fully connected neural network"""

    summary_dir = "./summary/"
    checkpoint_dir = "./checkpoint/"

    def __init__(
        self,
        n_nodes,
        n_steps,
        init_learning_rate,
        decay_step=1,
        decay_rate=1.0,
        reg=0.0,
        keep_prob=1.0,
        display_step=100,
        target_loss=0.0,
        checkpoint_path="",
        verbose=False,
    ):
        """Build the neural network model.

        Args:
            n_nodes:              vector of the sizes of the different layers
            n_steps:               number of training epochs
            init_learning_rate:  initial learning rate
            decay_step:             iterations between learning rate updates
            decay_rate:             factor by which the learning rate is changed
            reg:                    regularization factor
            keep_prob:              probability to keep activation during dropout
            display_step:           number of iterations between print output
        """

        # create directories if they do not exist
        if not os.path.exists(self.summary_dir):
            os.makedirs(self.summary_dir)
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)

        # model parameters
        self.init_learning_rate = init_learning_rate
        self.n_steps = n_steps
        self.decay_step = decay_step
        self.decay_rate = decay_rate
        self.reg = reg
        self.keep_prob = keep_prob
        self.display_step = display_step
        self.n_nodes = n_nodes
        self.n_layers = len(self.n_nodes)
        self.target_loss = target_loss
        self.verbose = verbose

        # pre- and postprocessor
        self.dp = Dataprocessor()

        # graph
        self.setup_graph()

        # saving and restoring
        self.saver = tf.train.Saver()
        self.checkpoint_path = checkpoint_path

    def preprocess(self, x_data, y_data, test_split, valid_split=0.0):
        """Preprocess the data.

        Args:
            x_data:     input data
            y_data:     output data
            test_split:  percentage of data used for testing
            valid_split: percentage of data used for validation

        Returns:
            feed_train:    training set data feed
            feed_valid:    validation set data feed
            feed_test:     test set data feed
        """

        # preprocess data
        x_norm, y_norm = self.dp.mean_normalize(x_data, y_data, overwrite=True)
        x_shuffled, y_shuffled = self.dp.shuffle(x_norm, y_norm)
        train_list, valid_list, test_list = self.dp.split(
            [x_shuffled, y_shuffled], test_split=test_split, valid_split=valid_split
        )

        # setup feeds
        feed_train = {self.x: train_list[0], self.y: train_list[1]}
        feed_valid = {self.x: valid_list[0], self.y: valid_list[1]}
        feed_test = {self.x: test_list[0], self.y: test_list[1]}

        return feed_train, feed_valid, feed_test

    def preprocess_eval(self, x_eval, y_eval):
        """Preprocess the data for evaluation.

        Args:
            x_eval:     input data
            y_eval:     output data

        Returns:
            feed_eval:  feed for evaluation
        """

        # preprocess data
        x_norm, y_norm = self.dp.mean_normalize(x_eval, y_eval, overwrite=False)

        # setup feeds
        feed_eval = {self.x: x_norm, self.y: y_norm}

        return feed_eval

    def postprocess(self, feed_data):
        """Postprocess the data.

        Args:
            feed_data:  data feed with input and output data

        Returns:
            x_scaled:    rescaled input data
            y_scaled:    rescaled output data

        """

        # extract data from feed
        x_data = feed_data[self.x]
        y_data = feed_data[self.y]

        # rescale data
        x_scaled, y_scaled = self.dp.rescale(x_data, y_data)

        return x_scaled, y_scaled

    def setup_eval(self, x):
        """Sets up evaluation operation for the neural network.

        Args:
            x:    input tensor

        Returns:
            y_:   predicted output tensor
        """

        # input layer
        activation = x

        # hidden layers
        for n_nodes_next in self.n_nodes[1:-1]:
            activation = self.hidden_layer(
                activation, n_nodes_next, keep_prob=self.keep_prob, reg=self.reg
            )

        # output layer
        y_ = self.output_layer(activation, self.n_nodes[-1])

        return y_

    def setup_loss(self, y, y_):
        """Sets up the the L2 loss operation.

        Args:
            y:   target output tensor
            y_:  predicted output tensor

        Returns:
            loss: loss tensor
        """
        n_batch = tf.shape(y)[0]
        # rel error squared independent of batch size
        loss = tf.truediv(
            tf.reduce_sum(tf.pow(y - y_, 2)), tf.cast(tf.pow(n_batch, 2), "float32")
        )

        return loss

    def setup_train(self, loss):
        """Sets up the training operation.

        Args:
            loss:        loss tensor

        Returns:
            train_op:    training operation
        """

        # global iteration
        self.global_step = tf.Variable(0, trainable=False)

        # learning rate decay
        self.learning_rate = tf.train.exponential_decay(
            self.init_learning_rate,
            self.global_step,
            decay_steps=self.decay_step,
            decay_rate=self.decay_rate,
            staircase=False,
        )

        # create optimizer
        optimizer = tf.train.AdamOptimizer(self.learning_rate)

        # train op
        train_op = optimizer.minimize(loss, global_step=self.global_step)

        return train_op

    def setup_graph(self):
        """Sets up the tensorflow graph for the neural network."""

        # inputs
        self.x = tf.placeholder(tf.float32, [None, self.n_nodes[0]], name="input")
        self.y = tf.placeholder(tf.float32, [None, self.n_nodes[-1]], name="target")

        self.y_ = self.setup_eval(self.x)

        self.loss = self.setup_loss(self.y, self.y_)

        self.train = self.setup_train(self.loss)

    def preload_variables(self, sess):

        if self.checkpoint_path:
            if self.checkpoint_path == "last":
                print("Trying to load last checkpoint...")
                checkpoint = tf.train.get_checkpoint_state(self.checkpoint_dir)
                self.checkpoint_path = checkpoint.model_checkpoint_path
            else:
                print("Trying to load checkpoint at %s..." % self.checkpoint_path)
            self.saver.restore(sess, self.checkpoint_path)
            print("Checkpoint %s loaded successfully." % self.checkpoint_path)
            print("Initializing remaining variables...")
            sess.run(tf.initialize_all_variables())
            print("Variables initialized successfully.")
        else:
            print("Initializing all variables as new...")
            sess.run(tf.initialize_all_variables())
            print("Variables initialized successfully.")

    def hidden_layer(self, x, n_hidden, keep_prob=1.0, reg=0.0):
        def activation_fn(x):
            return tf.nn.dropout(tf.nn.relu(x), keep_prob=keep_prob)

        y = tf.contrib.layers.fully_connected(
            x,
            n_hidden,
            activation_fn=activation_fn,
            weights_initializer=tf.contrib.layers.xavier_initializer(),
            weights_regularizer=tf.contrib.layers.l2_regularizer(reg),
        )
        return y

    def output_layer(self, x, n_hidden):
        y = tf.contrib.layers.fully_connected(
            x,
            n_hidden,
            activation_fn=None,
            weights_initializer=tf.contrib.layers.xavier_initializer(),
        )
        return y

    def run_train(self, feed_train, feed_valid, plot_train=False):
        """Launches the graph for training."""

        # setup plot
        if plot_train:
            plt.ion()
            plt.xlabel("iteration")
            plt.ylabel("loss")
            plt.pause(0.0001)

        tic = time.clock()

        with tf.Session() as sess:

            self.preload_variables(sess)
            loss_list = []

            try:

                for step in range(self.n_steps):

                    _, loss_train = sess.run([self.train, self.loss], feed_train)
                    loss_valid = self.loss.eval(feed_valid, sess)
                    current_learning_rate = self.learning_rate.eval(feed_train, sess)

                    if step % self.display_step == 0:

                        # store loss array in list
                        loss_list.append(np.asarray((step, loss_train, loss_valid)))

                        if self.verbose:
                            # commandline output
                            t = time.clock() - tic
                            print(
                                "Step:",
                                f"{step:06d}",
                                "Time:",
                                f"{t:.9g}",
                                "s",
                                flush=True,
                            )
                            print(
                                "- Training loss =",
                                f"{loss_train:.9g}",
                                flush=True,
                            )
                            print(
                                "- Validation loss =",
                                f"{loss_valid:.9g}",
                                flush=True,
                            )
                            if self.decay_rate != 1.0:
                                print(
                                    "- Current learning rate =",
                                    f"{current_learning_rate:.5g}",
                                    flush=True,
                                )

                        # plot training loss
                        if plot_train:
                            steps, L2_train, L2_valid = zip(*loss_list)
                            L2_std = np.std(L2_train)
                            plt.axis([0, max(step, 1), 0, max(10 * eps, L2_std)])
                            handle_train = plt.plot(
                                steps, L2_train, "b", label="training loss"
                            )
                            handle_valid = plt.plot(
                                steps, L2_valid, "r", label="validation loss"
                            )
                            plt.legend(handles=[handle_train[0], handle_valid[0]])
                            plt.pause(0.0001)

                        if (loss_train < self.target_loss) and (
                            loss_valid < self.target_loss
                        ):
                            print(
                                "Training and validation loss below target loss. Training finished."
                            )
                            break

                # save checkpoint
                self.saver.save(
                    sess,
                    self.checkpoint_dir + "model.ckpt",
                    global_step=self.global_step,
                )

            except KeyboardInterrupt:
                # save checkpoint
                self.saver.save(
                    sess,
                    self.checkpoint_dir + "model.ckpt",
                    global_step=self.global_step,
                )
                pass

            return loss_list

    def run_eval(self, feed_test):
        """Launches the graph for evaluation."""

        with tf.Session() as sess:
            self.preload_variables(sess)
            y_pred = self.y_.eval(feed_test, sess)
            loss_test = self.loss.eval(feed_test, sess)

        return y_pred, loss_test

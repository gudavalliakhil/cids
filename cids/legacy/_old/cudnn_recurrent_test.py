"""IAM computational intelligence library"""
import unittest

import numpy as np
from _old.recurrent_cudnn import *
from tensorflow.python.platform import test

from cids.test.src.create_test_dataset import _3D_dataset


DEBUG = True


class CudnnRecurrentTest(test.TestCase):
    """Tester for the cudnn_recurrent module."""

    @unittest.skipUnless(
        test.is_built_with_cuda(), "Test only applicable when running on GPUs"
    )
    def test_CudnnLSTM(self):
        """Tester for CudnnLSTM."""
        train_paths = tf.gfile.Glob("cids/tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = ["cids/tests/src/tfrecord/3D_dataset_sample_00.tfrecord"]
        dataset, data_shape, data_format = _3D_dataset()
        if DEBUG:
            print("dataset", dataset.shape)  # , "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        hidden_layers = [5, 10]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        data = tf.placeholder(tf.string, name="data")
        nn = CudnnLSTM(
            data_shape,
            input_idx,
            output_idx,
            hidden_layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(sess, feed_dict={nn.train_data: train_paths})
            # Initial loss
            l0 = nn.sess_loss(sess, feed_dict={nn.train_data: train_paths})
            # Training
            for i in range(0, num_steps, 1):
                l, lv = nn.sess_train(
                    sess,
                    summarize=(i % report_steps == 0 or i == num_steps - 1),
                    feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths},
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(
                sess,
                feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths},
                options=run_options,
                run_metadata=run_metadata,
            )
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(
                sess, feed_dict={nn.train_data: train_paths}
            )
            lv = nn.sess_loss(
                sess,
                feed_dict={
                    nn.valid_data: valid_paths,
                    nn.TRAIN: False,
                    nn.idp.freeze: True,
                    nn.odp.freeze: True,
                },
            )
            Xb, Yb, Yb_ = nn.sess_infer(
                sess,
                feed_dict={
                    nn.valid_data: valid_paths,
                    nn.idp.freeze: True,
                    nn.odp.freeze: True,
                    nn.BYPASS: True,
                    nn.TRAIN: False,
                    nn.data_bypass: np.ones_like(dataset[0:1, :]),
                },
            )
            # Save and load
            nn.sess_save(sess, global_step=i)
            nn.sess_load(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )  # "\n", X)
                print(
                    "Y",
                    Y.shape,
                )  # "\n", Y)
                print(
                    "Y_",
                    Y_.shape,
                )  # "\n", Y_)
                print(
                    "Xb",
                    Xb.shape,
                )  # "\n", Xb)
                print(
                    "Yb",
                    Yb.shape,
                )  # "\n", Yb)
                print(
                    "Yb_",
                    Yb_.shape,
                )  # "\n", Yb_)
                print("validation_error", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)

    # @unittest.skipUnless(test.is_built_with_cuda(),
    #                      "Test only applicable when running on GPUs")
    # def test_MultiGPUCudnnLSTM(self):
    #     """Tester for MultiGPUCudnnLSTM."""
    #     train_paths = tf.gfile.Glob(
    #         "iam/test/src/tfrecord/3D_dataset_sample*.tfr*")
    #     train_paths = sorted(train_paths)
    #     valid_paths = ["iam/test/src/tfrecord/3D_dataset_sample_00.tfrecord"]
    #     dataset, data_shape, data_format = _3D_dataset()
    #     if DEBUG:
    #         print("dataset", dataset.shape)#, "\n", dataset)
    #     input_idx = [0, 1]
    #     output_idx = [i for i in range(data_shape[-1])
    #                   if i not in input_idx]
    #     batch_size = 100
    #     layers = [5, 10]
    #     learning_rate = 0.01
    #     num_steps = 100
    #     report_steps = 10
    #     # Graph
    #     data = tf.placeholder(tf.string, name="data")
    #     nn = MultiGPUCudnnLSTM(
    #         data_shape,
    #         input_idx,
    #         output_idx,
    #         layers,
    #         batch_size=batch_size,
    #         init_learning_rate=learning_rate)
    #     config = tf.ConfigProto(allow_soft_placement=True)
    #     with self.test_session(use_gpu=True, config=config) as sess:
    #         nn.sess_initialize(sess, feed_dict={nn.train_data: train_paths})
    #         # Initial loss
    #         l0 = nn.sess_loss(sess,
    #                           feed_dict={nn.train_data: train_paths})
    #         # Training
    #         for i in range(0, num_steps, 1):
    #             l, lv = nn.sess_train(
    #                 sess,
    #                 summarize=(i % report_steps == 0
    #                            or i == num_steps - 1),
    #                 feed_dict={nn.train_data: train_paths,
    #                            nn.valid_data: valid_paths})
    #         # Do a run for a full trace
    #         i = num_steps
    #         run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    #         run_metadata = tf.RunMetadata()
    #         l, lv = nn.sess_train(sess,
    #                           feed_dict={nn.train_data: train_paths,
    #                                      nn.valid_data: valid_paths},
    #                           options=run_options,
    #                           run_metadata=run_metadata)
    #         # Check quality of results
    #         X, Y, Y_ = nn.sess_forward_pass(sess,
    #                                         feed_dict={nn.train_data: train_paths})
    #         lv = nn.sess_loss(sess, feed_dict={nn.valid_data: valid_paths,
    #                                            nn.TRAIN: False,
    #                                            nn.idp.freeze: True,
    #                                            nn.odp.freeze: True})
    #         Xb, Yb, Yb_ = nn.sess_infer(
    #             sess,
    #             feed_dict={nn.valid_data: valid_paths,
    #                        nn.idp.freeze: True,
    #                        nn.odp.freeze: True,
    #                        nn.BYPASS: True,
    #                        nn.TRAIN: False,
    #                        nn.data_bypass: np.ones_like(dataset[0:1, :])})
    #         # Save and load
    #         nn.sess_save(sess, global_step=i)
    #         nn.sess_load(sess)
    #         # Outputs
    #         if DEBUG:
    #             print("X", X.shape, )#"\n", X)
    #             print("Y", Y.shape, )# "\n", Y)
    #             print("Y_", Y_.shape, )# "\n", Y_)
    #             print("Xb", Xb.shape, )  # "\n", Xb)
    #             print("Yb", Yb.shape, )  # "\n", Yb)
    #             print("Yb_", Yb_.shape, ) # "\n", Yb_)
    #             print("validation_error", lv)
    #         # Assertions
    #         self.assertAllEqual(Y.shape, Y_.shape)
    #         self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
    #         self.assertEqual(Xb.shape[0], 1)
    #         self.assertEqual(Yb.shape[0], 1)
    #         self.assertEqual(Yb_.shape[0], 1)
    #         self.assertLess(l, l0)


if __name__ == "__main__":
    test.main()

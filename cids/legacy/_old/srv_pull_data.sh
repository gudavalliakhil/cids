#!/bin/bash
USER=koeppe
SERVER=tesla
LOCAL_PATH=./INPUT/
REMOTE_PATH=/work/$SERVER/$USER/DATA/INPUT/brittle*
rsync -avK --progress $USER@$SERVER:$REMOTE_PATH $LOCAL_PATH

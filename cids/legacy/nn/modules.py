import os
import timeit
from warnings import warn

import tensorflow as tf

import cids


RNN_LAYERS = ["lstm", "gru", "cudnn_lstm", "cudnn_gru"]


class BaseModule(tf.keras.layers.Layer):
    @property
    def architecture_string(self):
        module_name = self.__class__.name
        if "activation" in self.__dict__.keys():
            if isinstance(self.activation, str):
                module_name += self.activation
            elif hasattr(self.activation, "__name__"):
                module_name += self.activation.__name__
        if "num_units" in self.__dict__.keys():
            module_name += str(self.num_units)
        return module_name

    def call(self, inputs, **kwargs):
        raise NotImplementedError


class FC(BaseModule):
    def __init__(
        self,
        num_units,
        activaction=None,
        keep_prob=1.0,
        use_batch_norm=False,
        is_training=True,
        **kwargs,
    ):
        super().__init__(
            num_units=num_units,
            activaction=activaction,
            keep_prob=keep_prob,
            use_batch_norm=use_batch_norm,
            is_training=is_training,
            **kwargs,
        )
        self.num_units = num_units
        self.activation = activaction
        self.keep_prob = keep_prob
        self.use_batch_norm = use_batch_norm
        if not isinstance(is_training, tf.Tensor):
            warn("Layer parameter is_training should be a tensorflow Tensor")
        self.is_training = is_training
        self.y_ = None

    def call(self, inputs, **kwargs):
        y_ = tf.layers.dense(
            inputs,
            self.num_units,
            activation=self.activation,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
        )
        if self.use_batch_norm:
            y_ = tf.layers.batch_normalization(
                y_, fused=True, training=self.is_training
            )
        # Apply dropout during training
        if self.keep_prob < 1.0:
            self.y_ = tf.layers.dropout(
                y_, rate=(1.0 - self.keep_prob), training=self.is_training
            )
        return self.y_


class FCout(FC):
    def __init__(self, num_units, activaction=None, **kwargs):
        keep_prob = 1.0
        use_batch_norm = False
        is_training = True
        super().__init__(
            num_units=num_units,
            activaction=activaction,
            keep_prob=keep_prob,
            use_batch_norm=use_batch_norm,
            is_training=is_training,
            **kwargs,
        )


# Layer functions
def fc_relu(
    inputs, num_units, keep_prob=1.0, use_batch_norm=False, is_training=True, **kwargs
):
    """A fully-connected feed-forward layer with rectified linear activation."""
    use_bias = not use_batch_norm
    y_ = tf.layers.dense(
        inputs,
        num_units,
        activation=tf.nn.relu,
        kernel_initializer=tf.initializers.he_normal(),
        use_bias=use_bias,
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.layers.dropout(y_, rate=(1.0 - keep_prob), training=is_training)
    return y_


def fc_sigmoid(
    inputs, num_units, keep_prob=1.0, use_batch_norm=False, is_training=True, **kwargs
):
    """A fully-connected feed-forward layer with rectified linear activation."""
    use_bias = not use_batch_norm
    y_ = tf.layers.dense(
        inputs,
        num_units,
        activation=tf.nn.sigmoid,
        kernel_initializer=tf.contrib.layers.xavier_initializer(),
        use_bias=use_bias,
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.contrib.layers.dropout(y_, keep_prob=keep_prob, is_training=is_training)
    return y_


def fc_tanh(
    inputs, num_units, keep_prob=1.0, use_batch_norm=False, is_training=True, **kwargs
):
    """A fully-connected feed-forward layer with rectified linear activation."""
    use_bias = not use_batch_norm
    y_ = tf.layers.dense(
        inputs,
        num_units,
        activation=tf.nn.tanh,
        kernel_initializer=tf.contrib.layers.xavier_initializer(),
        use_bias=use_bias,
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.contrib.layers.dropout(y_, keep_prob=keep_prob, is_training=is_training)
    return y_


def fc_relu_residual(
    inputs, num_units, keep_prob=1.0, use_batch_norm=False, is_training=True, **kwargs
):
    """A fully-connected feed-forward layer with rectified linear activation."""
    assert (
        inputs.shape[-1] == num_units
    ), "residual layer size must equal number of input features"
    y_ = inputs + tf.layers.dense(
        inputs,
        num_units,
        activation=tf.nn.relu,
        weights_initializer=tf.contrib.layers.xavier_initializer(),
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.layers.dropout(y_, rate=(1.0 - keep_prob), training=is_training)
    return y_


def fc_out(inputs, num_units, **kwargs):
    """A fully-connected feed-forward layer with linear activation."""
    y_ = tf.layers.dense(
        inputs,
        num_units,
        activation=None,
        kernel_initializer=tf.contrib.layers.xavier_initializer(),
    )
    return y_


def legacy_fc_relu(
    inputs, num_units, keep_prob=1.0, use_batch_norm=False, is_training=True, **kwargs
):
    """A fully-connected feed-forward layer with rectified linear activation."""
    y_ = tf.contrib.layers.fully_connected(
        inputs,
        num_units,
        activation_fn=tf.nn.relu,
        weights_initializer=tf.contrib.layers.xavier_initializer(),
    )
    if use_batch_norm:
        y_ = tf.contrib.layers.batch_norm(y_, fused=True, is_training=is_training)
    # Apply dropout during training
    y_ = tf.contrib.layers.dropout(y_, keep_prob=keep_prob, is_training=is_training)
    return y_


def legacy_fc_out(inputs, num_units, **kwargs):
    """A fully-connected feed-forward layer with linear activation."""
    y_ = tf.contrib.layers.fully_connected(
        inputs,
        num_units,
        activation_fn=None,
        weights_initializer=tf.contrib.layers.xavier_initializer(),
    )
    return y_


def lstm_cell(
    inputs,
    num_units,
    sequence_lengths,
    index,
    is_training=True,
    dtype=tf.float32,
    keep_prob=1.0,
    **kwargs,
):
    """A Long Short-Term Memory cell."""
    with tf.variable_scope(f"cell_{str(index):s}"):
        # Base cell
        cell = tf.contrib.rnn.LSTMBlockFusedCell(num_units, use_peephole=True)
        # define pass
        y_, c = cell(inputs, dtype=dtype, sequence_length=sequence_lengths)
        # Apply dropout during training
        y_ = tf.contrib.layers.dropout(y_, keep_prob=keep_prob, is_training=is_training)
    return y_


def cudnn_lstm_cell(
    inputs,
    num_units,
    sequence_lengths,
    index,
    is_training=True,
    dtype=tf.float32,
    **kwargs,
):
    """A Long Short-Term Memory cell using nvidia cudnn."""
    with tf.variable_scope(f"cell_{str(index):s}"):
        # Base cell
        num_layers = 1
        # if is_training:
        #     dropout = 1.0 - self.keep_prob
        # else:
        dropout = 0.0
        cell = tf.contrib.cudnn_rnn.CudnnLSTM(num_layers, num_units, dropout=dropout)
        y_, c = cell(inputs, training=is_training)
        # TODO: sequence lengths
        # TODO: saveable
    return y_


def gru_cell(inputs, num_units, sequence_lengths, index, dtype=tf.float32, **kwargs):
    """A Gated Recurrent Unit cell."""
    with tf.variable_scope(f"cell_{str(index):s}"):
        # Base cell
        cell = tf.contrib.rnn.GRUBlockCell(num_units)
        cell = tf.contrib.rnn.FusedRNNCellAdaptor(cell, use_dynamic_rnn=True)
        # define pass
        y_, c = cell(inputs, dtype=dtype, sequence_length=sequence_lengths)
        # Apply dropout during training
        # TODO: Dropout?
        # y_ = dropout(y_, keep_prob=self.keep_prob, is_training=self.TRAIN)
    return y_


def cudnn_gru_cell(
    inputs, num_units, sequence_lengths, batch_size, index, dtype=tf.float32, **kwargs
):
    """A Gated Recurrent Unit cell for nvidia cudnn."""
    raise NotImplementedError
    with tf.variable_scope(f"cell_{str(index):s}"):
        # Base cell
        num_layers = 1
        dropout = 0.0
        cell = CudnnGRU(num_layers, num_units, num_units)
        # define pass
        y_, c = cell(inputs)
        # TODO: sequence length
        # TODO: dropout
        # TODO: saveable
    return y_


def conv2(
    inputs,
    num_kernels,
    kernel_shape,
    strides=(1, 1),
    keep_prob=1.0,
    padding="same",
    use_batch_norm=False,
    is_training=True,
    **kwargs,
):
    """A 2D convolutional layer."""
    use_bias = not use_batch_norm
    y_ = tf.layers.conv2d(
        inputs,
        num_kernels,
        kernel_shape,
        strides=strides,
        data_format="channels_last",
        padding=padding,
        activation=tf.nn.relu,
        kernel_initializer=tf.initializers.he_normal(),
        use_bias=use_bias,
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.layers.dropout(y_, rate=(1.0 - keep_prob), training=is_training)
    return y_


def conv2_out(
    inputs, num_kernels, kernel_shape, strides=(1, 1), padding="valid", **kwargs
):
    """A 2D convolutional layer."""
    y_ = tf.layers.conv2d(
        inputs,
        num_kernels,
        kernel_shape,
        strides=strides,
        data_format="channels_last",
        padding=padding,
        activation=None,
        kernel_initializer=tf.initializers.he_normal(),
    )
    return y_


def conv2T(
    inputs,
    num_kernels,
    kernel_shape,
    strides=(1, 1),
    keep_prob=1.0,
    padding="same",
    use_batch_norm=False,
    is_training=True,
    **kwargs,
):
    """A transpoed 2D convolutional layer."""
    use_bias = not use_batch_norm
    y_ = tf.layers.conv2d_transpose(
        inputs,
        num_kernels,
        kernel_shape,
        strides=strides,
        data_format="channels_last",
        padding=padding,
        activation=tf.nn.relu,
        kernel_initializer=tf.initializers.he_normal(),
        use_bias=use_bias,
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.layers.dropout(y_, rate=(1.0 - keep_prob), training=is_training)
    return y_


def mpool2(inputs, pool_shape, strides=None, **kwargs):
    """A 2D max pooling layer."""
    if strides is None:
        strides = pool_shape
    y_ = tf.layers.max_pooling2d(
        inputs, pool_shape, strides, data_format="channels_last"
    )
    return y_


def conv3(
    inputs,
    num_kernels,
    kernel_shape,
    strides=(1, 1, 1),
    padding="same",
    keep_prob=1.0,
    use_batch_norm=False,
    is_training=True,
    **kwargs,
):
    """A 3D convolutional layer."""
    use_bias = not use_batch_norm
    y_ = tf.layers.conv3d(
        inputs,
        num_kernels,
        kernel_shape,
        strides=strides,
        padding=padding,
        data_format="channels_last",
        activation=tf.nn.relu,
        kernel_initializer=tf.initializers.he_normal(),
        use_bias=use_bias,
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.layers.dropout(y_, rate=(1.0 - keep_prob), training=is_training)
    return y_


def conv3T(
    inputs,
    num_kernels,
    kernel_shape,
    strides=(1, 1, 1),
    keep_prob=1.0,
    padding="same",
    use_batch_norm=False,
    is_training=True,
    **kwargs,
):
    """A 3D convolutional layer."""
    use_bias = not use_batch_norm
    y_ = tf.layers.conv3d_transpose(
        inputs,
        num_kernels,
        kernel_shape,
        strides=strides,
        padding=padding,
        data_format="channels_last",
        activation=tf.nn.relu,
        kernel_initializer=tf.initializers.he_normal(),
        use_bias=use_bias,
    )
    if use_batch_norm:
        y_ = tf.layers.batch_normalization(y_, fused=True, training=is_training)
    # Apply dropout during training
    y_ = tf.layers.dropout(y_, rate=(1.0 - keep_prob), training=is_training)
    return y_


def mpool3(inputs, pool_shape, strides=None, **kwargs):
    """A 3D max pooling layer."""
    if strides is None:
        strides = pool_shape
    y_ = tf.layers.max_pooling3d(
        inputs, pool_shape, strides=strides, data_format="channels_last"
    )
    return y_


def unet(
    inputs,
    num_kernels,
    num_levels=3,
    kernel_size=3,
    conv_per_level=3,
    bypass_mode="residual",
    data_format="NXYF",
    keep_prob=1.0,
    use_batch_norm=False,
    is_training=True,
    **kwargs,
):
    if use_batch_norm:
        assert keep_prob == 1.0, "Batch normalization after dropout is a bad idea!"
    # Select correct convolution operation for spatial dimensionality
    num_spatial_dims = (
        data_format.count("X") + data_format.count("Y") + data_format.count("Z")
    )
    assert num_spatial_dims in [2, 3]
    if num_spatial_dims == 2:
        mpool = mpool2
        conv = conv2
        convT = conv2T
    else:
        mpool = mpool3
        conv = conv3
        convT = conv3T

    bypasses = []
    y_ = inputs

    # Down: level 0 to num_levels - 1
    for il in range(num_levels):
        if il > 0:
            # Reduce spatial size by 2
            y_ = mpool(y_, [2] * num_spatial_dims)
        # Convolutions
        for ic in range(conv_per_level):
            y_ = conv(
                y_,
                num_kernels,
                [kernel_size] * num_spatial_dims,
                padding="same",
                use_batch_norm=use_batch_norm,
                keep_prob=keep_prob,
                is_training=is_training,
            )
        # Store bypass
        bypasses.append(y_)

    # Drop-Out
    y_ = tf.layers.dropout(y_, rate=(1.0 - keep_prob), training=is_training)

    # Up: level num_levels - 1 to 0
    for il in reversed(range(num_levels - 1)):
        # Increase spatial size by 2
        y_ = convT(
            y_,
            num_kernels,
            [2] * num_spatial_dims,
            strides=[2] * num_spatial_dims,
            padding="valid",
            is_training=is_training,
        )
        # Pad to spatial size of downward branch
        paddings = [[0, 0]]  # batch axis N
        for d in range(1, 1 + num_spatial_dims):
            if bypasses[il].shape[d] - y_.shape[d] == 1:
                paddings += [[0, 1]]  # spatial axes XYZ
            elif bypasses[il].shape[d] - y_.shape[d] == 0:
                paddings += [[0, 0]]  # spatial axes XYZ
            else:
                raise ValueError("U-net bypass shape not recovered.")
        paddings += [[0, 0]]  # feature axis F
        y_ = tf.pad(y_, paddings, mode="SYMMETRIC")
        # Merge with bypass
        if bypass_mode == "residual":
            y_ = tf.add(y_, bypasses[il])
        elif bypass_mode == "concat":
            y_ = tf.concat([y_, bypasses[il]], -1)
        # Convolutions
        for ic in range(conv_per_level):
            y_ = conv(
                y_,
                num_kernels,
                [kernel_size] * num_spatial_dims,
                padding="same",
                use_batch_norm=use_batch_norm,
                keep_prob=keep_prob,
                is_training=is_training,
            )
    return y_


def legacy_unet(inputs, num_kernels, spatial_dims=3, mode="residual", **kwargs):
    assert spatial_dims in [2, 3]
    if spatial_dims == 2:
        mpool = mpool2
        conv = conv2
        convT = conv2T
    else:
        mpool = mpool3
        conv = conv3
        convT = conv3T
    y_ = inputs
    if mode in ["residual", "concat"]:
        if spatial_dims == 2:
            bypass1 = y_[:, :, :, :2]
        elif spatial_dims == 3:
            bypass1 = y_[:, :, :, :, :2]
    y_ = conv(y_, num_kernels // 2, [3] * spatial_dims)
    y_ = conv(y_, num_kernels // 2, [3] * spatial_dims)
    y_ = conv(y_, num_kernels // 2, [3] * spatial_dims)
    if mode in ["residual", "concat"]:
        bypass2 = y_
    y_ = mpool(y_, [2] * spatial_dims)
    y_ = conv(y_, num_kernels, [3] * spatial_dims)
    y_ = conv(y_, num_kernels, [3] * spatial_dims)
    y_ = conv(y_, num_kernels, [3] * spatial_dims)
    if mode in ["residual", "concat"]:
        bypass3 = y_
    y_ = mpool(y_, [2] * spatial_dims)
    y_ = convT(
        y_, num_kernels, [2] * spatial_dims, strides=[2] * spatial_dims, padding="valid"
    )
    if mode == "residual":
        y_ = tf.add(y_, bypass3)
    elif mode == "concat":
        y_ = tf.concat([y_, bypass3], -1)
    y_ = conv(y_, num_kernels, [3] * spatial_dims)
    y_ = conv(y_, num_kernels, [3] * spatial_dims)
    y_ = conv(y_, num_kernels, [3] * spatial_dims)
    y_ = convT(
        y_,
        num_kernels // 2,
        [3] * spatial_dims,
        strides=[2] * spatial_dims,
        padding="valid",
    )
    if mode == "residual":
        y_ = tf.add(y_, bypass2)
    elif mode == "concat":
        y_ = tf.concat([y_, bypass2], -1)
    y_ = conv(y_, num_kernels // 2, [3] * spatial_dims)
    y_ = conv(y_, num_kernels // 2, [3] * spatial_dims)
    y_ = conv(y_, num_kernels // 2, [3] * spatial_dims)
    if mode == "residual":
        # Pad bypass1 to output shape
        bypass1_paddings = [[0, 0]] * (spatial_dims + 1)
        bypass1_paddings += [[0, y_.shape[-1] - bypass1.shape[-1]]]
        y_ = tf.add(y_, tf.pad(bypass1, bypass1_paddings))
    elif mode == "concat":
        y_ = tf.concat([y_, bypass1], -1)
    return y_


def local_response_normalization(inputs, **kwargs):
    return tf.nn.local_response_normalization(
        inputs, depth_radius=5, bias=1, alpha=1, beta=0.5, name=None
    )


def reshape(inputs, shape, order="C", **kwargs):
    with tf.variable_scope("Reshape"):
        dynamic_shape = [
            tf.shape(inputs)[int(i)] if isinstance(s, str) else s
            for i, s in enumerate(shape)
        ]
        if order == "F":
            dynamic_shape.reverse()
        x = tf.reshape(inputs, dynamic_shape)
        if order == "F":
            perm = list(range(len(shape)))
            perm.reverse()
            x = tf.transpose(x, perm)
        return x


def flatten(inputs, **kwargs):
    with tf.variable_scope("Flatten"):
        outputs = tf.contrib.layers.flatten(inputs)
        return outputs


def resnet_identity_block(input_tensor, kernel_size, filters, stage, block, **kwargs):
    """The identity block is the block that has no conv layer at shortcut.
    # Arguments
        input_tensor: input tensor
        kernel_size: default 3, the kernel size of
            middle conv layer at main path
        filters: list of integers, the filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: "a","b"..., current block label, used for generating layer names
    # Returns
        Output tensor for the block.
    """
    filters1, filters2, filters3 = filters
    conv_name_base = "res" + str(stage) + block + "_branch"
    bn_name_base = "bn" + str(stage) + block + "_branch"

    x = tf.keras.layers.Conv2D(
        filters1, (1, 1), kernel_initializer="he_normal", name=conv_name_base + "2a"
    )(input_tensor)
    x = tf.keras.layers.BatchNormalization(axis=-1, name=bn_name_base + "2a")(x)
    x = tf.keras.layers.Activation("relu")(x)

    x = tf.keras.layers.Conv2D(
        filters2,
        kernel_size,
        padding="same",
        kernel_initializer="he_normal",
        name=conv_name_base + "2b",
    )(x)
    x = tf.keras.layers.BatchNormalization(axis=-1, name=bn_name_base + "2b")(x)
    x = tf.keras.layers.Activation("relu")(x)

    x = tf.keras.layers.Conv2D(
        filters3, (1, 1), kernel_initializer="he_normal", name=conv_name_base + "2c"
    )(x)
    x = tf.keras.layers.BatchNormalization(axis=-1, name=bn_name_base + "2c")(x)

    x = tf.keras.layers.add([x, input_tensor])
    x = tf.keras.layers.Activation("relu")(x)
    return x


def resnet_conv_block(
    input_tensor, kernel_size, filters, stage, block, strides=(2, 2), **kwargs
):
    """A block that has a conv layer at shortcut.
    # Arguments
        input_tensor: input tensor
        kernel_size: default 3, the kernel size of
            middle conv layer at main path
        filters: list of integers, the filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: "a","b"..., current block label, used for generating layer names
        strides: Strides for the first conv layer in the block.
    # Returns
        Output tensor for the block.
    Note that from stage 3,
    the first conv layer at main path is with strides=(2, 2)
    And the shortcut should have strides=(2, 2) as well
    """
    filters1, filters2, filters3 = filters
    conv_name_base = "res" + str(stage) + block + "_branch"
    bn_name_base = "bn" + str(stage) + block + "_branch"

    x = tf.keras.layers.Conv2D(
        filters1,
        (1, 1),
        strides=strides,
        kernel_initializer="he_normal",
        name=conv_name_base + "2a",
    )(input_tensor)
    x = tf.keras.layers.BatchNormalization(axis=-1, name=bn_name_base + "2a")(x)
    x = tf.keras.layers.Activation("relu")(x)

    x = tf.keras.layers.Conv2D(
        filters2,
        kernel_size,
        padding="same",
        kernel_initializer="he_normal",
        name=conv_name_base + "2b",
    )(x)
    x = tf.keras.layers.BatchNormalization(axis=-1, name=bn_name_base + "2b")(x)
    x = tf.keras.layers.Activation("relu")(x)

    x = tf.keras.layers.Conv2D(
        filters3, (1, 1), kernel_initializer="he_normal", name=conv_name_base + "2c"
    )(x)
    x = tf.keras.layers.BatchNormalization(axis=-1, name=bn_name_base + "2c")(x)

    shortcut = tf.keras.layers.Conv2D(
        filters3,
        (1, 1),
        strides=strides,
        kernel_initializer="he_normal",
        name=conv_name_base + "1",
    )(input_tensor)
    shortcut = tf.keras.layers.BatchNormalization(axis=-1, name=bn_name_base + "1")(
        shortcut
    )

    x = tf.keras.layers.add([x, shortcut])
    x = tf.keras.layers.Activation("relu")(x)
    return x

"""CIDS computational intelligence library"""
from copy import deepcopy
from warnings import warn

import numpy as np
import tensorflow as tf

import cids
from cids.legacy.decorators import *
from cids.tensorflow.utility import read_axes


class DataReader:

    # TODO: casting to tf.float64 fails during categorical classification

    def __init__(
        self,
        data_shape,
        data_format,
        input_features=None,
        output_features=None,
        src_type="file",
        sample_dtype=tf.float64,
        cast_dtype=tf.float32,
        cutoff=True,
        slice_tensors=False,
        file_type="tfr",
        tfr_features=None,
        csv_skip=0,
        csv_comment="#",
        prefetch=True,
        buffer_size=10000,
        seed=None,
    ):
        """A class that loads and batches data into tensorflow tensors.

        DataReader uses the new tensorflow dataset API to load files into
        tf.Dataset objects. The class currently supports reading of csv and
        tfrecord files. When processing tfrecords and no tfr_features is present
        it defaults to the cids default tfrecord format (see below).

        Args:
            data_shape:     Shape of the input data
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:             batch axis,
                                 S:             sequence axis
                                 H/W/D/X/Y/Z:   spatial axes
                                 F/C:           feature or channel axis
            src_type:       The source of data ("file" or "placeholder")
            sample_dtype:   Data type in the data file
            cast_dtype:     Data type to cast to
            slice_tensors:  Boolean: Slice dataset into multiple samples?
            tfr_features:   Dictionary of protobuf keys and tf.Features()
                            (Defaults to cids default format) for tfrecords
            csv_skip:       Number of lines to skip for csv
            csv_comment:    Lines to skip starting with this string for csv
            shuffle:        Boolean: Shuffle dataset using buffer_size?
            prefetch:       Boolean: Preload dataset of buffer_size?
            buffer_size:    Number of samples to collect for shuffling
            seed:           Set a seed for shuffling. (Default None: disabled)
        """
        if tfr_features is None:
            # Use default cids tfrecord format
            self.tfr_features = {
                "data": tf.io.FixedLenFeature((), tf.string, default_value=""),
                "data_shape": tf.io.FixedLenFeature((len(data_shape),), tf.int64),
            }
        else:
            self.tfr_features = tfr_features
        self.input_features = input_features
        self.output_features = output_features
        for fi in self.input_features:
            assert isinstance(fi, int) or fi in self.tfr_features.keys() or fi is None
        for fo in self.output_features:
            assert isinstance(fo, int) or fo in self.tfr_features.keys() or fo is None
        self.data_shape = data_shape
        self.src_type = src_type
        self.split_batch = True
        self.data_format = data_format
        axes = read_axes(data_format)
        self.batch_axis = axes[0]
        self.feature_axis = axes[1]
        self.sequence_axis = axes[2]
        self.spatial_axes = axes[3]
        self.sample_dtype = sample_dtype
        self.cast_dtype = cast_dtype
        self.cutoff = cutoff
        self.slice_tensors = slice_tensors
        self.samples_per_file = 1
        self.skip = csv_skip
        self.comment = csv_comment
        self.file_type = file_type
        self.seed = seed
        self.prefetch = prefetch
        self.buffer_size = buffer_size
        self.VERBOSITY = 2

    # def _parse_tfrecord2(self, tfr):
    #     parsed = tf.io.parse_single_example(tfr, self.tfr_features)
    #     feature_keys = [fk for fk in self.tfr_features.keys()
    #                     if "_shape" not in fk]
    #     for fk in feature_keys:
    #         if fk + "_shape" in self.tfr_features.keys():
    #             # Tensor parameter
    #             feature_raw = parsed[fk]
    #             feature = tf.io.decode_raw(feature_raw, self.sample_dtype)
    #             # Get tensor dynamic shape
    #             dynamic_data_shape = parsed[fk + "_shape"]
    #             dynamic_data_shape = tf.cast(dynamic_data_shape, tf.int32)
    #             # Reshape tensor
    #             feature_tensor = tf.reshape(feature, dynamic_data_shape)
    #         else:
    #             # Scalar parameter
    #             param_data = [tf.cast(d, self.cast_dtype) for d in param_data]

    def _parse_tfrecord(self, tfr):
        parsed = tf.io.parse_single_example(tfr, self.tfr_features)
        data = parsed["data"]
        dynamic_data_shape = parsed["data_shape"]
        # Decode and cast
        data = tf.io.decode_raw(data, self.sample_dtype)
        data = tf.cast(data, self.cast_dtype)
        dynamic_data_shape = tf.cast(dynamic_data_shape, tf.int32)
        # Restore data
        data = tf.reshape(data, dynamic_data_shape)
        data = tf.cond(
            tf.equal(dynamic_data_shape[self.batch_axis], 1),
            lambda: tf.squeeze(data, axis=0),
            lambda: data,
        )
        # Tile parameter data
        param_keys = sorted(k for k in self.tfr_features.keys() if "data" not in k)
        if self.VERBOSITY > 2:
            if param_keys:
                print(
                    "  Expanding data with parameter features ",
                    "(sorted: alphanumerically):",
                )
                print("    ", param_keys)
        param_data = [parsed[k] for k in param_keys]
        # Cast
        param_data = [tf.cast(d, self.cast_dtype) for d in param_data]
        # Expand and tile
        param_data = [tf.squeeze(d) for d in param_data]
        for p in param_data:
            assert (
                len(p.shape) == 0
            ), 'All parameters without "data" in their key must be scalars.'
        active_feature_axis = self.feature_axis
        active_dims = list(range(len(self.data_shape)))
        if not self.slice_tensors:
            active_dims.remove(self.batch_axis)
        for _ in active_dims:
            param_data = [tf.expand_dims(d, 0) for d in param_data]
        # Tile (replace feature size with 1)
        tile_shape = tf.gather(dynamic_data_shape, active_dims)
        tile_shape = tf.unstack(tile_shape)
        tile_shape[active_feature_axis] = tf.ones([], dtype=tf.int32)
        tile_shape = tf.stack(tile_shape)
        param_data = [tf.tile(d, tile_shape) for d in param_data]
        # Concat
        data = tf.concat([data] + param_data, active_feature_axis)
        return data

    def _parse_csv_line(self, line):
        if self.cast_dtype == tf.float32 or self.cast_dtype == tf.float64:
            defaults = [[0.0]] * self.data_shape[-1]
        elif self.cast_dtype == tf.int64 or self.cast_dtype == tf.int32:
            defaults = [[0]] * self.data_shape[-1]
        else:
            raise ValueError("Unknown self.cast_dtype = ", self.cast_dtype)
        parsed = tf.io.decode_csv(line, record_defaults=defaults)
        parsed = tf.stack(parsed, axis=0)
        return parsed

    def _cutoff_sample(self, x, sample_shape):
        dynamic_sample_shape = tf.shape(x)
        slice_shape = tf.minimum(dynamic_sample_shape, sample_shape)
        return tf.slice(x, [0] * len(sample_shape), slice_shape)

    def generate_batch_dataset(
        self,
        *data,
        batch_size,
        chunk_size=None,
        mode="train",
        shuffle=True,
        repeats=None,
    ):
        """Read data as specified by src_type during object initialisation.

        Args:
            data:       Data source tensors
            mode:       Which mode to use (defines batch sized used)

        Returns:
            dataset:    A tensorflow Dataset (for iterator)

        """
        if isinstance(batch_size, int):
            batch_size = batch_size
            valid_batch_size = batch_size
        elif hasattr(batch_size, "keys"):
            batch_size = batch_size["train"]
            valid_batch_size = batch_size["valid"]
        else:
            raise ValueError("Variable batch_size must be type int or dict.")
        if self.src_type[:5].lower() == "file":
            dataset = self.generate_dataset_from_files(data)
        elif self.src_type[:5].lower() == "place":
            dataset = self.generate_dataset_from_placeholders(*data)
        else:
            raise ValueError(
                'Unknown src_type: must be either "file" or "placeholder".'
            )
        # Define final shape of sample
        sample_shape = list(deepcopy(self.data_shape))
        del sample_shape[self.batch_axis]
        # Cutoff if sample shape exceeds data shape
        if self.cutoff:
            # Slice to sample shape
            dataset = dataset.map(lambda x: self._cutoff_sample(x, sample_shape))
        # Chunk sequence dataset
        if self.sequence_axis is not None and chunk_size:
            assert (
                self.data_shape[self.sequence_axis] is not None
            ), "Data shape along sequence axis must be defined when chunk_size is set."
            assert chunk_size < self.data_shape[self.sequence_axis]
            sample_sequence_axis = self.sequence_axis - 1
            sequence_length = sample_shape[sample_sequence_axis]
            # Shuffle dataset before synthesizing more samples
            if shuffle:
                dataset = dataset.shuffle(
                    buffer_size=min(len(data[0]), self.buffer_size), seed=self.seed
                )
            # Ensure that all sequences are padded with zeros to max length
            dataset = dataset.padded_batch(batch_size=1, padded_shapes=sample_shape)
            dataset = dataset.map(tf.squeeze)  # remove added axis
            # Create multiple sequence chunks from chunkable sequences
            if sequence_length > chunk_size:
                sample_shape[sample_sequence_axis] = chunk_size
                num_chunks = sequence_length - chunk_size
                chunk_idx = [list(range(i, i + chunk_size)) for i in range(num_chunks)]
                dataset = dataset.flat_map(
                    lambda sample: tf.data.Dataset.from_tensor_slices(
                        [
                            tf.gather(sample, c, axis=sample_sequence_axis)
                            for c in chunk_idx
                        ]
                    )
                )
                # remove blank samples
                dataset = dataset.filter(
                    lambda sample: tf.reduce_any(
                        tf.cast(tf.sign(tf.abs(sample)), dtype=tf.bool)
                    )
                )
        # # Cache
        # if self.prefetch:
        #     dataset = dataset.prefetch(buffer_size=self.buffer_size)
        # Shuffle dataset after synthesizing samples
        if shuffle:
            dataset = dataset.shuffle(
                min(len(data[0]), self.buffer_size), seed=self.seed
            )
        # Repeat dataset
        if repeats is None:
            dataset = dataset.repeat()
        elif repeats > 0:
            dataset = dataset.repeat(repeats)

        # Create batches
        if mode == "valid":
            batch_size = valid_batch_size
        else:
            batch_size = batch_size
        # Split inputs and outputs
        if self.split_batch and self.input_features is not None:
            # Padding necessary when dataset comes from multiple files / sources
            dataset = dataset.padded_batch(
                batch_size=batch_size, padded_shapes=sample_shape
            )
            # Split
            dataset = dataset.map(
                lambda batch: (
                    tf.gather(batch, self.input_features, axis=self.feature_axis),
                    tf.gather(batch, self.output_features, axis=self.feature_axis),
                )
            )
        else:
            # Padding only necessary when input did not come from a
            #   pre-split tensor (already padded by default)
            dataset = dataset.batch(batch_size=batch_size)
        return dataset

    def generate_full_dataset(self, *data, disable_feature_split=False):
        """Create a dataset of tensors from various data sources.

        Args:
            data:          List of data
        Returns:
            dataset:        A dataset of tensors
        """
        if self.src_type[:5].lower() == "file":
            dataset = self.generate_dataset_from_files(data)
        elif self.src_type[:5].lower() == "place":
            dataset = self.generate_dataset_from_placeholders(*data)
        else:
            raise ValueError(
                'Unknown src_type: must be either "file" or "placeholder".'
            )
        # Define final shape of sample
        sample_shape = list(deepcopy(self.data_shape))
        del sample_shape[self.batch_axis]
        # Cutoff if sample shape exceeds data shape
        if self.cutoff:
            # Slice to sample shape
            dataset = dataset.map(lambda x: self._cutoff_sample(x, sample_shape))
        # Create batch of the entire dataset                                    # TODO: Check if this makes sense with keras
        num_samples = tf.cast(tf.shape(data[0])[self.batch_axis], dtype=tf.int64)
        if self.slice_tensors:
            num_samples *= self.samples_per_file
            # Padding only necessary when input did not come from a
            #   pre-split tensor (already padded by default)
            dataset = dataset.batch(batch_size=num_samples)
        else:
            # Padding necessary when dataset comes from multiple files / sources
            dataset = dataset.padded_batch(
                batch_size=num_samples, padded_shapes=sample_shape
            )
        # Split inputs and outputs
        if self.input_features is not None and not disable_feature_split:
            # Split
            dataset = dataset.map(
                lambda batch: (
                    tf.gather(batch, self.input_features, axis=self.feature_axis),
                    tf.gather(batch, self.output_features, axis=self.feature_axis),
                )
            )
        return dataset

    def generate_dataset_from_files(self, files):
        """Create a dataset of tensors from files.

        Args:
            files:          List of filenames
        Returns:
            dataset:    A dataset of tensors
        """
        # Parse file
        if self.file_type[:3] == "tfr":
            dataset = tf.data.TFRecordDataset(files)
            dataset = dataset.map(self._parse_tfrecord)
            if self.slice_tensors:
                dataset = dataset.flat_map(
                    lambda d: tf.data.Dataset.from_tensor_slices(d)
                )
        elif self.file_type[:3] == "csv":
            dataset = tf.data.Dataset.from_tensor_slices(files)
            # Skip line and filter out comments
            dataset = dataset.flat_map(
                lambda filename: (
                    tf.data.TextLineDataset(filename)
                    .skip(self.skip)
                    .filter(
                        lambda line: tf.not_equal(tf.substr(line, 0, 1), self.comment)
                    )
                )
            )
            dataset = dataset.map(self._parse_csv_line)
            if not self.slice_tensors:
                # Stack sequence
                if self.sequence_axis:
                    dataset = dataset.batch(
                        batch_size=self.data_shape[self.sequence_axis]
                    )
        else:
            raise ValueError(f'file_type "{self.file_type:s}" unknown.')
        return dataset

    def generate_dataset_from_placeholders(self, *placeholders):
        """Create a dataset of tensors from placeholders.

        Args:
            placeholders:   A tensorflow placeholder for feeding
        Returns:
            dataset:        A dataset of tensors
        """
        datasets = tuple(tf.data.Dataset.from_tensors(p) for p in placeholders)
        # Slice multiple sample tensors into single sample tensors
        if self.slice_tensors:
            datasets = tuple(
                d.flat_map(lambda d: tf.data.Dataset.from_tensor_slices(d))
                for d in datasets
            )
        # Cast
        datasets = tuple(
            d.map(lambda sample: tf.cast(sample, self.cast_dtype)) for d in datasets
        )
        # Zip datasets from multiple source tensors (inputs, targets) together
        if len(datasets) > 1:
            dataset = tf.data.Dataset.zip(datasets)
        else:
            dataset = datasets[0]
        return dataset

    def read_tfrecords(self, tfr_files):
        """Reads a single tfrecord file to check its content."""
        dataset = self.generate_full_dataset(tfr_files, disable_feature_split=True)
        samples = [s.numpy() for s in dataset]
        if len(tfr_files) == 1:
            samples = np.squeeze(samples, axis=self.batch_axis)
        else:
            samples = np.concatenate(samples, axis=0)
        return samples

"""CIDS computational intelligence library"""
# TODO: tf.svd input dimensionality reduction (e.g. with moving mean)
import tensorflow as tf

import cids
from cids.legacy.decorators import *
from cids.tensorflow.utility import read_axes


class DataProcessor:
    def __init__(
        self,
        data_shape,
        data_format="NF",
        reduction_axes=None,
        shuffle=True,
        shuffle_seed=None,
        dtype=tf.float64,
        normalize_mode="stddev",
        init_stats=None,
        join_features=None,
        scope="DataProcessor",
    ):
        """A class that pre- and postprocesses tensors using tensorflow.

        Args:
            data_shape:     shape of input data
            data_format:    data format used ("NF", NSF", or "NCHW")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            reduction_axes: axes for statistics to be calculated over
            shuffle:        boolean
            shuffle_seed:   optional seed for shuffling
            dtype:          data type used for computations, variables and
                            output (tf.float64 recommended)
            normalize_mode: normalization mode ("stddev", "minmax" or "fixed")
            init_stats:     dictionary to overwrite the initial stats
            join_features:  nested list of lists of indices to normalize jointly
            scope:          variable scope for DataProcessor

        """
        assert len(data_shape) > 1, "Data_shape must be larger than 1"
        assert len(data_shape) == len(
            data_format
        ), f"Invalid data_format {data_format:s} for data_shape."
        self.data_shape = data_shape
        self.data_format = data_format
        axes = read_axes(data_format)
        self.batch_axis = axes[0]
        self.feature_axis = axes[1]
        self.sequence_axis = axes[2]
        self.spatial_axes = axes[3]
        self.reduction_axes = reduction_axes or tuple(
            i for i, a in enumerate(self.data_format) if not a == "F"
        )
        self.shuffle = shuffle
        self.shuffle_seed = shuffle_seed
        self.dtype = dtype
        self.smoothing_const = tf.constant(1e-12, dtype=self.dtype)
        self.normalize_mode = normalize_mode
        # statistics shape
        self.stat_shape = tuple(
            data_shape[i] if i not in self.reduction_axes else 1
            for i in range(len(data_shape))
        )
        with tf.variable_scope(scope), tf.device("/device:cpu:0") as varscope:
            with tf.device("/device:cpu:0"):
                self.scope = varscope
                # Define tensorflow population statistics initializers
                count_init = tf.constant_initializer(0)
                count_shape = []
                mean_init = tf.constant_initializer(0.0)
                mean_shape = self.stat_shape
                std_init = tf.constant_initializer(0.0)
                std_shape = self.stat_shape
                m2_init = tf.constant_initializer(0.0)
                m2_shape = self.stat_shape
                min_init = tf.constant_initializer(self.dtype.max)
                min_shape = self.stat_shape
                max_init = tf.constant_initializer(self.dtype.min)
                max_shape = self.stat_shape
                if init_stats is not None:
                    if "count" in init_stats.keys():
                        count_init = tf.constant(init_stats["count"], dtype=tf.int32)
                        count_shape = None  # get from constant
                    if "mean" in init_stats.keys():
                        mean_init = tf.constant(init_stats["mean"], dtype=self.dtype)
                        mean_shape = None  # get from constant
                    if "std" in init_stats.keys():
                        std_init = tf.constant(init_stats["std"], dtype=self.dtype)
                        std_shape = None  # get from constant
                    if "m2" in init_stats.keys():
                        m2_init = tf.constant(init_stats["m2"], dtype=self.dtype)
                        m2_shape = None  # get from constant
                    if "min" in init_stats.keys():
                        min_init = tf.constant(init_stats["min"], dtype=self.dtype)
                        min_shape = None  # get from constant
                    if "max" in init_stats.keys():
                        max_init = tf.constant(init_stats["max"], dtype=self.dtype)
                        max_shape = None  # get from constant
                # Define tensorflow population statistics
                with tf.variable_scope("stats"):
                    self.count = tf.get_variable(
                        "count",
                        shape=count_shape,
                        initializer=count_init,
                        dtype=tf.int32,
                        trainable=False,
                    )
                    self.mean = tf.get_variable(
                        "mean",
                        shape=mean_shape,
                        initializer=mean_init,
                        dtype=self.dtype,
                        trainable=False,
                    )
                    self.std = tf.get_variable(
                        "std",
                        shape=std_shape,
                        initializer=std_init,
                        dtype=self.dtype,
                        trainable=False,
                    )
                    self.m2 = tf.get_variable(
                        "m2",
                        shape=m2_shape,
                        initializer=m2_init,
                        dtype=self.dtype,
                        trainable=False,
                    )
                    self.min = tf.get_variable(
                        "min",
                        shape=min_shape,
                        initializer=min_init,
                        dtype=self.dtype,
                        trainable=False,
                    )
                    self.max = tf.get_variable(
                        "max",
                        shape=max_shape,
                        initializer=max_init,
                        dtype=self.dtype,
                        trainable=False,
                    )

                with tf.variable_scope("FLAGS"):
                    self.freeze = tf.placeholder_with_default(
                        tf.constant(False, dtype=tf.bool), [], name="freeze"
                    )
                    # Keep same as BaseNeuralNetwork.train_freeze default

    @variable_scoped_method
    def sequence_mask(self, x, axis=None):
        """Calculate the sequence mask of tensor batch.

        The sequence mask has the same shape as batch, with ones at each value
        that is part of sequence length and zeros otherwise.

        Args:
            x:      batch tensor
            axis:   sequence axis (defaults to self.sequence_axis

        Returns:
            sequence_mask:  batch-sized tensor with ones for each sequence entry
                            of batch and zero at other indices

        """
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if axis is None:
            axis = self.sequence_axis
        axes = [i for i in range(x.shape.ndims) if i > axis]
        # Find frames with non-zero features, reduce all axes larger than axis
        sequence_mask = tf.sign(tf.reduce_max(tf.abs(x), axis=axes, keepdims=True))
        # Convert to boolean
        sequence_mask = tf.cast(sequence_mask, tf.bool)
        # Blow sequence mask up
        multiples = [1] * len(x.shape)
        for i in axes:
            multiples[i] = tf.shape(x)[i]
        multiples = tf.stack(multiples)
        sequence_mask = tf.tile(sequence_mask, multiples=multiples)
        return sequence_mask

    @variable_scoped_method
    def sequence_lengths(self, x, axis=None):
        """Calculate the lengths of each sequence in a batch.

        Args:
            x:      batch tensor
            axis:   sequence axis (defaults to self.sequence_axis

        Returns:
            sequence_length:    1D-tensor of the number of frames for each
                                sample in the batch.

        Raises:
            AssertionError:     if shape of batch or self.batch shape does not
                                have enough dimensions for reduction

        """
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if axis is None:
            axis = self.sequence_axis
        axes = tuple(i for i in range(x.shape.ndims) if i > axis)
        # Find frames with non-zero features
        sequence_mask = tf.sign(tf.reduce_max(tf.abs(x), axis=axes))
        # count numbers of non-zero frames, sequence axis is now last axis
        sequence_length = tf.reduce_sum(sequence_mask, axis=axis)
        sequence_length = tf.cast(
            sequence_length, tf.int32
        )  # ensure we return an int32
        return sequence_length

    @variable_scoped_method
    def _calc_online_moments(self, x):
        """Successively calculate and store moments of the batch tensors.

        The moments are calculated along the batch (0) axis only and sequences
        are masked.

        The used online one-pass algorithm is based on:
            Chan, Tony F.; Golub, Gene H.; LeVeque, Randall J. (1979),
            "Updating Formulae and a Pairwise Algorithm for Computing sample
             Variances."

        Args:
            x:  batch tensor (batch_size, ..., num_features)

        Returns:
            Tuple of
                pop_count:  Number of samples in pupulation so far
                pop_mean:   Estimated population mean tensor
                pop_std:    Estimated population standard deviation tensor
                pop_m2:     second moment

        """
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        # Recall memory variables (perform deep copy)
        #  TODO: tensorflow 1.2: identity op is not threadsafe!
        #   The following workaround enforces a deep copy for reliability
        pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        if self.sequence_axis is not None:
            # Calculate sequence mask
            sequence_mask = self.sequence_mask(x)
            # Calculate input moments
            x_mean, x_var = tf.nn.weighted_moments(
                x,
                axes=self.reduction_axes,
                frequency_weights=sequence_mask,
                keep_dims=True,
            )
            # Sequence steps that are zero for all samples (e.g. when padding globally)
            # divide zero by zero in tf.nn.weighted_moments(). We overwrite these nans.
            stat_sequence_mask = tf.reduce_any(
                sequence_mask, axis=self.reduction_axes, keepdims=True
            )
            x_mean = tf.where(stat_sequence_mask, x_mean, pop_mean)
            x_var = tf.where(stat_sequence_mask, x_var, tf.zeros_like(x_var))
        else:
            # Calculate input moments
            x_mean, x_var = tf.nn.moments(x, axes=self.reduction_axes, keep_dims=True)
        x_count = tf.shape(x)[0]
        x_m2 = x_var * tf.cast(x_count - 1, self.dtype)
        # Update mean value
        delta_mean = x_mean - pop_mean
        # Update second moment
        delta_m2 = tf.cond(
            tf.less(pop_count + x_count, 2),
            lambda: tf.zeros(self.stat_shape, dtype=self.dtype),
            lambda: x_m2
            + delta_mean**2
            * tf.cast(pop_count, dtype=self.dtype)
            * tf.cast(x_count, dtype=self.dtype)
            / tf.cast(pop_count + x_count, dtype=self.dtype),
        )
        delta_mean *= tf.cast(x_count, self.dtype) / tf.cast(
            pop_count + x_count, self.dtype
        )
        delta_count, delta_mean, delta_m2 = tf.tuple([x_count, delta_mean, delta_m2])
        return delta_count, delta_mean, delta_m2

    @variable_scoped_method
    def _calc_online_minmax(self, x):
        """Successively calculate and store the mean, min, max of batch tensors.

        The statistics are calculated along the batch (0) axis only and the
        sequences are masked.

        Args:
            x:  batch tensor (batch_size, ..., num_features)

        Returns:
            pop_mean:   Estimated population mean tensor (..., num_features)
            pop_min:    Estimated population min tensor (..., num_features)
            pop_max:    Estimated population max tensor (..., num_features)

        """
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        # Recall memory variables
        old_pop_count = self.count
        pop_mean = self.mean
        pop_min = self.min
        pop_max = self.max
        if self.sequence_axis is not None:
            # Calculate sequence mask
            sequence_mask = self.sequence_mask(x)
            # Mask and reduce mean
            x_mean, _ = tf.nn.weighted_moments(
                x,
                axes=self.reduction_axes,
                frequency_weights=sequence_mask,
                keep_dims=True,
            )
            # Sequence steps that are zero for all samples (e.g. when padding globally)
            # divide zero by zero in tf.nn.weighted_moments(). We overwrite these nans.
            stat_sequence_mask = tf.reduce_any(
                sequence_mask, axis=self.reduction_axes, keepdims=True
            )
            x_mean = tf.where(stat_sequence_mask, x_mean, pop_mean)
            # Mask and reduce min and max
            x_min = tf.reduce_min(
                tf.where(sequence_mask, x, tf.fill(tf.shape(x), self.dtype.max)),
                axis=self.reduction_axes,
                keepdims=True,
            )
            x_max = tf.reduce_max(
                tf.where(sequence_mask, x, tf.fill(tf.shape(x), self.dtype.min)),
                axis=self.reduction_axes,
                keepdims=True,
            )
        else:
            # Calculate shift and variance
            x_mean = tf.reduce_mean(x, axis=self.reduction_axes, keepdims=True)
            x_min = tf.reduce_min(x, axis=self.reduction_axes, keepdims=True)
            x_max = tf.reduce_max(x, axis=self.reduction_axes, keepdims=True)
        x_count = tf.shape(x)[self.batch_axis]
        # Update counter
        pop_count = old_pop_count + x_count
        # Update mean value
        delta = x_mean - pop_mean
        # TODO: Numerically unstable if counts and self._pop_count close and large
        pop_mean = tf.assign_add(
            pop_mean,
            delta * (tf.cast(x_count, self.dtype) / tf.cast(pop_count, self.dtype)),
        )
        # Update min and max
        pop_min = tf.minimum(pop_min, x_min)
        pop_max = tf.maximum(pop_max, x_max)
        return (pop_count, pop_mean, pop_min, pop_max)

    @variable_scoped_method
    def _get_online_moments(self):
        """Recall the online mean and standard deviation.

        Returns:
            pop_count:  Number of samples in pupulation so far
            pop_mean:   Estimated population mean tensor
            pop_std:    Estimated population standard deviation tensor
            pop_m2:     second moment

        """
        # Recall memory variables
        pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        pop_m2 = tf.add(self.m2, 0.0)
        pop_std = tf.add(self.std, 0.0)
        return pop_count, pop_mean, pop_std, pop_m2

    @variable_scoped_method
    def _get_online_minmax(self):
        """Recall the online mean, min and max tensors.

        Returns:
            pop_count:  Number of samples in pupulation so far
            pop_mean:   Estimated population mean tensor
            pop_min:    Estimated population minimum tensors
            pop_max:    Estimated population maximum tensors

        """
        # Recall memory variables
        pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        pop_min = tf.add(self.min, 0.0)
        pop_max = tf.add(self.max, 0.0)
        return pop_count, pop_mean, pop_min, pop_max

    @variable_scoped_method
    def _assign_online_moments(self, delta_count, delta_mean, delta_m2):
        """Set online population count, mean std and m2.

        Args:
            delta_count:  population count update
            delta_mean:   population mean update
            delta_m2:     population second moment update

        Returns:
            pop_count:  assigned population count
            pop_mean:   assigned population mean
            pop_m2:     assigned population second moment
            pop_std:    assigned standard deviation
        """
        delta_count = tf.convert_to_tensor(delta_count)
        delta_mean = tf.convert_to_tensor(delta_mean)
        delta_m2 = tf.convert_to_tensor(delta_m2)
        assign_count = tf.assign_add(self.count, delta_count)
        assign_mean = tf.assign_add(self.mean, delta_mean)
        assign_m2 = tf.assign_add(self.m2, delta_m2)
        with tf.control_dependencies([assign_count, assign_mean, assign_m2]):
            div = tf.cond(
                tf.less_equal(self.count, 1),
                lambda: tf.constant(1.0, dtype=self.dtype),
                lambda: tf.cast(self.count - 1, self.dtype),
            )
            pop_std = tf.sqrt(self.m2 / div)
            assign_std = tf.assign(self.std, pop_std)
        # group all together
        pop_count, pop_mean, pop_m2, pop_std = tf.tuple(
            [assign_count, assign_mean, assign_m2, assign_std]
        )
        return pop_count, pop_mean, pop_m2, pop_std

    @variable_scoped_method
    def _assign_online_minmax(self, pop_count, pop_mean, pop_min, pop_max):
        """Set online population count, mean, min and max.

        Args:
            pop_count:  population count
            pop_mean:   population mean value
            pop_min:    population min value
            pop_max:    population max value

        Returns:
            pop_count:  assigned population count
            pop_mean:   assigned population mean value
            pop_min:    assigned population min value
            pop_max:    assigned population max value

        """
        pop_count = tf.convert_to_tensor(pop_count)
        pop_mean = tf.convert_to_tensor(pop_mean)
        pop_min = tf.convert_to_tensor(pop_min)
        pop_max = tf.convert_to_tensor(pop_max)
        assign_count = tf.assign(self.count, pop_count)
        assign_mean = tf.assign(self.mean, pop_mean)
        assign_min = tf.assign(self.min, pop_min)
        assign_max = tf.assign(self.max, pop_max)
        # group all together
        pop_count, pop_mean, pop_min, pop_max = tf.tuple(
            [assign_count, assign_mean, assign_min, assign_max]
        )
        return pop_count, pop_mean, pop_min, pop_max

    @variable_scoped_method
    def _reduce_online_moments(self, shift, width):
        """Reduce axes other than the batch axis.

        This function combines shift (e.g. mean) and width (e.g. std_dev)
        entries of arrays that have been calculated along the batch axis.
        Shifting and normalizing a batch or dataset with common shift and width
        values maintains relative sizes of the values along that axis. For
        sequences the batch is masked.

        Args:
            shift:  A tensor of shift values calculated only along batch axis
            width:  A tensor of width values calculated only along batch axis

        Returns:
            shift:  A tensor of shift values calculated along multiple axes
            width:  A tensor of width values calculated along multiple axes

        Example:
            A sequence dataset tensor of shape (20, 7, 5) can be normalized
            using shift and width tensors of shape (7, 5), normalizing each
            entry along the sequence (1) axis and feature (2) axis
            independently. Normalizing with reduced shift and width tensors of
            shapes (5,) ensures that the relative sizes along the sequence axis
            are maintained.

        """
        # FIXME: Is this step still necessary?
        # TODO: use variance/M2 instead
        # TODO: Use stored values instead?
        shift = tf.convert_to_tensor(shift)
        width = tf.convert_to_tensor(width)
        shift = tf.cast(shift, self.dtype)
        width = tf.cast(width, self.dtype)
        if len(self.reduction_axes) > 1:
            nonbatch_axes = tuple(
                i - 1 for i in self.reduction_axes if i > self.batch_axis
            )
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(shift)
                # Calculate average shift value and variance
                shift, shift_var = tf.nn.weighted_moments(
                    shift,
                    axes=nonbatch_axes,
                    frequency_weights=sequence_mask,
                    keepdims=True,
                )
            else:
                # Calculate average shift value and calculate shift variance
                shift, shift_var = tf.nn.moments(
                    shift, axes=nonbatch_axes, keepdims=True
                )
            # Calculate squared width (variance)
            square_width = tf.square(width)
            # Add all variances together
            #  https://stats.stackexchange.com/questions/280667/higher-dimensional-version-of-variance
            square_width = (
                tf.reduce_sum(square_width, axis=nonbatch_axes, keepdims=True)
                + shift_var
            )
            width = tf.sqrt(square_width)
        return shift, width

    @variable_scoped_method
    def _reduce_online_minmax(self, mean, min, max):
        """Reduce mean, min, and max for axes other than the batch axis.

        This function reduces mean, min and max entries of arrays that have been
        calculated along the batch axis. Shifting and normalizing a batch or
        dataset with common mean and min/max values maintains relative sizes of
        the values along that axis. For sequences the batch is masked.

        Args:
            mean:  A tensor of mean values calculated only along the batch axis
            min:   A tensor of min values calculated only along the batch axis)
            min:   A tensor of max values calculated only along the batch axis)

        Returns:
            mean:  A tensor of mean values calculated along multiple axes
            min:   A tensor of min values calculated along multiple axes
            max:   A tensor of max values calculated along multiple axes

        Example:
            A sequence dataset tensor of shape (20, 7, 5) can be normalized
            using shift and width tensors of shape (7, 5), normalizing each
            entry along the sequence (1) axis and feature (2) axis
            independently. Normalizing with reduced mean and width tensors of
            shapes (5,) ensures that the relative sizes along the sequence axis
            are maintained.

        """
        # TODO: store reduced values instead?
        mean = tf.convert_to_tensor(mean)
        min = tf.convert_to_tensor(min)
        max = tf.convert_to_tensor(max)
        mean = tf.cast(mean, self.dtype)
        min = tf.cast(min, self.dtype)
        max = tf.cast(max, self.dtype)
        if len(self.reduction_axes) > 1:
            nonbatch_axes = tuple(
                i - 1 for i in self.reduction_axes if i > self.batch_axis
            )
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(mean)
                # Mask and reduce mean
                mean, _ = tf.nn.weighted_moments(
                    mean,
                    axes=nonbatch_axes,
                    frequency_weights=sequence_mask,
                    keepdims=True,
                )
                # Mask and reduce min, max with sequence mask
                min = tf.reduce_min(
                    tf.where(
                        sequence_mask, min, tf.fill(tf.shape(min), self.dtype.max)
                    ),
                    axis=nonbatch_axes,
                    keepdims=True,
                )
                max = tf.reduce_max(
                    tf.where(
                        sequence_mask, max, tf.fill(tf.shape(max), self.dtype.min)
                    ),
                    axis=nonbatch_axes,
                    keepdims=True,
                )
            else:
                # Calculate average mean, min and max value
                mean = tf.reduce_mean(mean, axis=nonbatch_axes, keepdims=True)
                min = tf.reduce_mean(min, axis=nonbatch_axes, keepdims=True)
                max = tf.reduce_mean(max, axis=nonbatch_axes, keepdims=True)
        return mean, min, max

    @variable_scoped_method
    def _update_online_stats(self, x, transform_fun=None):
        """Updates and returns stored shift and width values for normalization.

        Args:
            x:              batch tensor
            transform_fun:  optional function to transform x (after centering)

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if self.normalize_mode == "stddev":

            def zero_online_delta_moments():
                delta_count = tf.zeros([], dtype=tf.int32)
                delta_mean = tf.zeros(self.stat_shape, dtype=self.dtype)
                delta_m2 = tf.zeros(self.stat_shape, dtype=self.dtype)
                return delta_count, delta_mean, delta_m2

            delta_count, delta_mean, delta_m2 = tf.cond(
                self.freeze,
                zero_online_delta_moments,
                lambda: self._calc_online_moments(x),
            )
            # Transform function applied on centered data
            if transform_fun is not None:
                assert callable(transform_fun)
                x = transform_fun(x - self.mean - delta_mean)
                _, _, delta_m2 = tf.cond(
                    self.freeze,
                    zero_online_delta_moments,
                    lambda: self._calc_online_moments(x),
                )
            # Ensure that all deltas are calculated monolithically
            with tf.control_dependencies([delta_count, delta_mean, delta_m2]):
                pop_count, pop_mean, pop_m2, pop_std = self._assign_online_moments(
                    delta_count, delta_mean, delta_m2
                )
            # Ensure that all assignments are completed
            with tf.control_dependencies([pop_count, pop_mean, pop_m2, pop_std]):
                shift = pop_mean
                width = pop_std
        elif self.normalize_mode == "minmax":
            pop_count, pop_mean, pop_min, pop_max = tf.cond(
                self.freeze,
                self._get_online_minmax,
                lambda: self._calc_online_minmax(x),
            )
            # Transform function applied on centered data
            if transform_fun is not None:
                assert callable(transform_fun)
                x = transform_fun(x - pop_mean)
                _, _, pop_min, pop_max = tf.cond(
                    self.freeze,
                    self._get_online_minmax,
                    lambda: self._calc_online_minmax(x),
                )
            # Ensure that all calculations are performed monolithically
            with tf.control_dependencies([pop_count, pop_mean, pop_min, pop_max]):
                pop_count, pop_mean, pop_min, pop_max = self._assign_online_minmax(
                    pop_count, pop_mean, pop_min, pop_max
                )
            # Ensure that all assignments are completed
            with tf.control_dependencies([pop_count, pop_mean, pop_min, pop_max]):
                shift = pop_mean
                if transform_fun is None:
                    width = tf.maximum(tf.abs(pop_max - shift), tf.abs(pop_min - shift))
                else:
                    # width calculated on centered data if data is transformed
                    width = tf.maximum(tf.abs(pop_max), tf.abs(pop_min))
        else:
            raise ValueError("Invalid self.normalize_mode: stddev or minmax expected!")
        return shift, width

    @variable_scoped_method
    def _get_online_stats(self, x):
        """Returns stored shift and width values for normalization.

        Args:
            x:      batch or sample tensor

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        # Ensure all previous steps in graph are executed (assignments!)
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        with tf.control_dependencies([x]):
            if self.normalize_mode == "stddev":
                _, pop_mean, pop_std, _ = self._get_online_moments()
                shift = pop_mean
                width = pop_std
                # FIXME: No longer necessary
                # shift, width = self._reduce_online_moments(pop_mean, pop_std)
            elif self.normalize_mode == "minmax":
                _, pop_mean, pop_min, pop_max = self._get_online_minmax()
                # FIXME: No longer necessary
                # pop_mean, pop_min, pop_max = \
                #     self._reduce_online_minmax(pop_mean, pop_min, pop_max)
                shift = pop_mean
                width = tf.maximum(tf.abs(pop_max - shift), tf.abs(pop_min - shift))
            else:
                raise ValueError(
                    "Invalid self.normalize_mode: stddev or minmax expected!"
                )
            return shift, width

    @variable_scoped_method
    def online_normalize(
        self, x, center=True, scale=True, transform_fun=None, merge_features=None
    ):
        """Successively normalize batches using online statistics.

        Shift (e.g. Mean) and width (e.g. standard deviation) tensors are
        calculated using an online algorithm along the batch (0) axis. Then both
        tensors are further reduced along other axes"specified in
        self.reduction_axes. Finally, shift and normalization are calculated.
        Sequences are masked.

        Args:
            x:              sample or batch tensor
            transform_fun:  optional function to apply after centering

        Returns:
            x:      normalized sample or batch tensor

        """
        with tf.device("/device:cpu:0"):
            x = tf.convert_to_tensor(x)
            x = tf.cast(x, self.dtype)
            if len(x.shape) < len(self.data_format):
                x = tf.expand_dims(x, 0)
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(x)
            shift, width = self._update_online_stats(x, transform_fun=transform_fun)
            # Shift
            if center:
                x = x - shift
            # Transform
            if transform_fun is not None:
                assert callable(transform_fun)
                x = transform_fun(x)
            # Merge feature widths
            if merge_features:
                width_list = tf.unstack(width, axis=self.feature_axis)
                for merge_indices in merge_features:
                    merged_width = tf.reduce_max(
                        tf.gather(width, merge_indices, axis=self.feature_axis)
                    )
                    for i in merge_indices:
                        width_list[i] = merged_width
                width = tf.stack(width_list)
            # Scale
            if scale:
                x = x / tf.maximum(width, self.smoothing_const)
            # Mask
            if "sequence_mask" in locals():
                # Mask x to undo shift for empty frames
                x = tf.where(sequence_mask, x, tf.zeros_like(x))
            return x

    @variable_scoped_method
    def online_rescale(
        self, x, center=True, scale=True, transform_fun=None, merge_features=None
    ):
        """Rescales and unshifts a normalized batch with online statistics.

        Args:
            x:              normalized sample or batch
            transform_fun:  optional function to apply after centering
            merge_features: optional nested lists of feature indices merge together

        Returns:
            sample:  sample rescaled to original size

        """
        with tf.device("/device:cpu:0"):
            x = tf.convert_to_tensor(x)
            initial_dtype = x.dtype
            x = tf.cast(x, self.dtype)
            if len(x.shape) < len(self.data_format):
                x = tf.expand_dims(x, 0)
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(x)
            shift, width = self._get_online_stats(x)
            # Merge feature widths
            if merge_features:
                width_list = tf.unstack(width, axis=self.feature_axis)
                for merge_indices in merge_features:
                    merged_width = tf.reduce_max(
                        tf.gather(width, merge_indices, axis=self.feature_axis)
                    )
                    for i in merge_indices:
                        width_list[i] = merged_width
                width = tf.stack(width_list)
            # Scale
            if scale:
                x = x * tf.maximum(width, self.smoothing_const)
            # Transform
            if transform_fun is not None:
                assert callable(transform_fun)
                x = transform_fun(x)
            # Shift
            if center:
                x = x + shift
            # Mask
            if "sequence_mask" in locals():
                # Mask x to undo shift for empty frames
                x = tf.where(sequence_mask, x=x, y=tf.zeros_like(x))
            x = tf.cast(x, initial_dtype)
            return x

    @variable_scoped_method
    def _calc_batch_stats(self, x, reduction_axes=None):
        """Calculates and returns shift and width values for normalization.

        Args:
            x:      sample or batch tensor

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        reduction_axes = reduction_axes or self.reduction_axes
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if self.normalize_mode == "stddev":
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(x)
                # Mask moments with sequence mask
                shift, variance = tf.nn.weighted_moments(
                    x,
                    axes=reduction_axes,
                    frequency_weights=sequence_mask,
                    keep_dims=True,
                )
            else:
                # Calculate shift and variance
                shift, variance = tf.nn.moments(x, axes=reduction_axes, keep_dims=True)
            width = tf.sqrt(variance)
        elif self.normalize_mode == "minmax":
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(x)
                # Mask mean and reduce
                shift, _ = tf.nn.weighted_moments(
                    x,
                    axes=reduction_axes,
                    frequency_weights=sequence_mask,
                    keep_dims=True,
                )
                # Mask and reduce min and max with sequence mask
                min = tf.reduce_min(
                    tf.where(sequence_mask, x, tf.fill(tf.shape(x), self.dtype.max)),
                    axis=reduction_axes,
                    keepdims=True,
                )
                max = tf.reduce_max(
                    tf.where(sequence_mask, x, tf.fill(tf.shape(x), self.dtype.min)),
                    axis=reduction_axes,
                    keepdims=True,
                )
            else:
                # Calculate shift and variance
                shift = tf.reduce_mean(x, axis=reduction_axes, keepdims=True)
                min = tf.reduce_min(x, axis=reduction_axes, keepdims=True)
                max = tf.reduce_max(x, axis=reduction_axes, keepdims=True)
            width = tf.maximum(tf.abs(max - shift), tf.abs(min - shift))
        else:
            raise ValueError("Invalid self.normalize_mode: stddev or minmax expected!")
        return shift, width

    @variable_scoped_method
    def batch_pca_transform(self, x, num_components=None, whitening=True):
        """Calculates the principal component transformation of the batch.

        Args:
            x:                  batch tensor [batch_size, ..., num_features]
            num_components:     dimension of the output matrix (None defaults
                                to minimum of x.shape[-2:])
            whitening:          Scale down to (near) unit variance

        Returns:
            x:  transformed batch tensors [batch_size, ..., num_components]

        """
        # TODO: function should calculate mean after svd (but increased cost)
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        # Center data
        if self.sequence_axis is not None:
            # Calculate sequence mask
            sequence_mask = self.sequence_mask(x)
            # Center data
            self.pca_shift, _ = tf.nn.weighted_moments(
                x,
                axes=self.reduction_axes,
                frequency_weights=sequence_mask,
                keep_dims=True,
            )
            x -= self.pca_shift
            x = tf.where(sequence_mask, x, tf.zeros_like(x))
            # Reduce along batch axis
            reduced_sequence_axis = self.sequence_axis - 1
            reduced_sequence_mask = tf.reduce_any(
                sequence_mask, axis=self.batch_axis, keepdims=True
            )
            batch_mean, _ = tf.nn.weighted_moments(
                x, axes=self.batch_axis, frequency_weights=sequence_mask, keep_dims=True
            )
            batch_mean = tf.where(
                reduced_sequence_mask, batch_mean, tf.zeros_like(batch_mean)
            )
            cov = tf.matmul(
                tf.squeeze(batch_mean), tf.squeeze(batch_mean), transpose_a=True
            ) / tf.cast(tf.shape(batch_mean)[reduced_sequence_axis], dtype=self.dtype)
        else:
            self.pca_shift = tf.reduce_mean(x, axis=self.reduction_axes, keepdims=True)
            x -= self.pca_shift
            cov = tf.matmul(x, x, transpose_a=True) / tf.cast(
                tf.shape(x)[self.batch_axis], self.dtype
            )
        # Calculate singular values and vectors
        s, u, v = tf.svd(cov, compute_uv=True)
        # Calculate reduced transformation matrix
        s_reduced = s[:num_components]
        u_reduced = u[:, :num_components]
        v_reduced = v[:, :num_components]
        # Transform
        x = tf.tensordot(x, u_reduced, axes=[[self.feature_axis], [0]])
        if whitening:
            x /= tf.sqrt(s_reduced + tf.sqrt(self.smoothing_const))
            # Smoothing must be stronger here than in mean_normalization
        # Ensure padded values remain padded
        if "sequence_mask" in locals():
            # Mask x to undo rotations for empty frames
            multiples = len(x.shape) * [1]
            multiples[self.feature_axis] = num_components
            reduced_sequence_mask = tf.tile(
                tf.expand_dims(sequence_mask[:, :, self.feature_axis], axis=-1),
                multiples=multiples,
            )
            x = tf.where(reduced_sequence_mask, x, tf.zeros_like(x))
        return x, s, u, v, cov

    @variable_scoped_method
    def batch_normalize(self, x, reduction_axes=None):
        """Mean normalize a batch of samples with batch statistics.

        Shift (e.g. Mean) and width (e.g. standard deviation) tensors are
        calculated along multiple reduction axes. Finally, shift and
        normalization are calculated. Sequences are masked.

        Args:
            x:  batch tensor

        Returns:
            x:  normalized batch tensor

        """
        reduction_axes = reduction_axes or self.reduction_axes
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if self.sequence_axis is not None:
            sequence_mask = self.sequence_mask(x)
        shift, width = self._calc_batch_stats(x, reduction_axes=reduction_axes)
        # Perform shift and normalization
        x = (x - shift) / tf.maximum(
            width, self.smoothing_const
        )  # avoid division by zero
        # Ensure padded values remain padded
        if "sequence_mask" in locals():
            # Mask x to undo shift for empty frames
            x = tf.where(sequence_mask, x=x, y=tf.zeros_like(x))
        return x, shift, width

    @variable_scoped_method
    def batch_rescale(self, x, shift, width):
        """Rescales and unshifts a normalized batch with batch statistics.

        Args:
            x:  normalized x of samples
            shift:  (e.g. mean)
            width:  (e.g. standard deviation)

        Returns:
            x:  x of samples rescaled to original size

        """
        x = tf.convert_to_tensor(x)
        shift = tf.convert_to_tensor(shift)
        width = tf.convert_to_tensor(width)
        initial_dtype = x.dtype
        x = tf.cast(x, self.dtype)
        shift = tf.cast(shift, self.dtype)
        width = tf.cast(width, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if self.sequence_axis is not None:
            # Calculate sequence mask
            sequence_mask = self.sequence_mask(x)
        x = x * tf.maximum(width, self.smoothing_const) + shift
        if "sequence_mask" in locals():
            # Mask x to undo shift for empty frames
            x = tf.where(sequence_mask, x=x, y=tf.zeros_like(x))
        x = tf.cast(x, initial_dtype)
        return x

    @variable_scoped_method
    def split_features(self, xy, x_idx, y_idx):
        """Split tensor into inputs and targets along the feature axis.

        Args:
            xy:     Tensor
            x_idx:  Indices for the input tensor
            y_idx:  Indices for the target tensor

        Returns:
            x:      Input tensor
            y:      Output/target tensor

        """
        xy = tf.convert_to_tensor(xy)
        # Unstack along feature axis
        xy_list = tf.unstack(xy, axis=self.feature_axis)
        # Select inputs and targets
        x_list = [xy_list[i] for i in x_idx]
        y_list = [xy_list[i] for i in y_idx]
        # Stack inputs and targets together
        x = tf.stack(x_list, axis=self.feature_axis)
        y = tf.stack(y_list, axis=self.feature_axis)
        return x, y

    @variable_scoped_method
    def split_batch(self, x, ratios=(0.7, 0.15, 0.15)):
        """Split tensor along batch axis.

        Args:
            x:      Tensor
            ratios: tuple of ratios for train, valid and test sets

        Returns:
            x_train:    Training tensor
            x_valid:    Validation tensor
            x_test:     Test tensor
        """
        assert sum(ratios) == 1.0, "ratios must sum up to 1.0"
        x = tf.convert_to_tensor(x)
        ratios = tf.convert_to_tensor(ratios)
        size = tf.cast(tf.shape(x)[self.batch_axis], tf.float32)
        size_splits = tf.cast(tf.round(size * ratios), dtype=tf.int32)
        x_train, x_valid, x_test = tf.split(
            x, num_or_size_splits=size_splits, axis=self.batch_axis
        )
        return x_train, x_valid, x_test

    @variable_scoped_method
    def shuffle_batch(self, x):
        """Randomly shuffle tensor along batch axis.

        Args:
            x:  tensor

        Returns:
            x:  shuffled tensor

        """
        # FIXME: result has rounding errors
        x = tf.convert_to_tensor(x)
        x = tf.random_shuffle(x, seed=self.shuffle_seed)
        return x

    @variable_scoped_method
    def log_transform(self, x, base=2.0, invert=False):
        """Performs a signed log transformation on the input data.

        Args:
            x:      tensor
            base:   base for the transformation
            invert: perform the inverse operation instead

        Returns:
            x:      transformed tensor

        """
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        base = tf.convert_to_tensor(base)
        base = tf.cast(base, self.dtype)
        if not invert:
            x = tf.convert_to_tensor(x)
            x = tf.sign(x) * tf.log(tf.abs(x) + 1.0)
            x /= tf.log(base)
        else:
            x = tf.convert_to_tensor(x)
            x *= tf.log(base)
            x = tf.sign(x) * (tf.exp(tf.abs(x)) - 1.0)
        return x

    @variable_scoped_method
    def power_transform(self, x, power=2.0, mode="naive", invert=False):
        """Perform a power transform on the input.

        Args:
            x:          input tensor
            power:      the exponent used
            mode:       "naive", "signed-box-cox", or "yeo-johnson"
            invert:     perform the inverse operation instead

        Returns:
                x:      transformed tensor

        """
        x = tf.convert_to_tensor(x)
        x = tf.cast(x, self.dtype)
        if mode == "naive":
            if power == 0.0:
                if not invert:
                    return tf.log(x)
                else:
                    return tf.exp(x)
            else:
                power = tf.convert_to_tensor(power)
                power = tf.cast(power, self.dtype)
                if invert:
                    power = tf.reciprocal(power)
                return tf.sign(x) * tf.abs(x) ** power
        elif mode == "signed-box-cox":
            if power == 0.0:
                raise ValueError('Exponent cannot be zero for mode="signed-cox-box"')
            else:
                power = tf.convert_to_tensor(power)
                power = tf.cast(power, self.dtype)
                if invert:
                    power = tf.reciprocal(power)
                return (tf.sign(x) * tf.abs(x) ** power - 1.0) / power
        elif mode == "yeo-johnson":
            if invert:
                raise NotImplementedError
            neg_power = tf.constant(2.0, dtype=self.dtype) - power
            if power == 0.0:
                power = tf.convert_to_tensor(power)
                power = tf.cast(power, self.dtype)
                return tf.where(
                    x >= 0.0,
                    tf.sign(x) * tf.log(tf.abs(x) + 1),
                    (-((-x + 1.0) ** neg_power) - 1) / neg_power,
                )
            elif power == 2.0:
                power = tf.convert_to_tensor(power)
                power = tf.cast(power, self.dtype)
                return tf.where(
                    x >= 0.0,
                    ((x + 1.0) ** power - 1) / power,
                    tf.sign(x) * tf.log(tf.abs(x) + 1),
                )
            elif 0.0 < power < 2.0:
                power = tf.convert_to_tensor(power)
                power = tf.cast(power, self.dtype)
                return tf.where(
                    x >= 0.0,
                    ((x + 1.0) ** power - 1) / power,
                    (-((-x + 1.0) ** neg_power) - 1) / neg_power,
                )
            else:
                raise ValueError(
                    'Exponent must be smaller than 2.0 for mode="yeo-johnson"'
                )
        else:
            raise NotImplementedError

    @variable_scoped_method
    def gaussian_noise(self, x, noise_std=0.1, is_train=True):
        noise = tf.random.normal(tf.shape(x), stddev=noise_std, dtype=x.dtype)
        return tf.cond(is_train, lambda: x + noise, lambda: x)

# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
from pathlib import Path

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
os.environ["TF_FORCE_GPU_ALLOW_GROWTH"] = "true"

import cids

import numpy as np
import tensorflow as tf
import seaborn as sns

from tensorflow.keras import layers as klayers
from cids.tensorflow import layers as clayers
from cids.tensorflow.tuner import SearchResults
from cids.statistics import metrics
from kadi_ai import KadiAIProject
from matplotlib import pyplot as plt
from sortedcontainers import SortedDict
from kerastuner import HyperParameters
from collections import OrderedDict

# Preamble
plt.style.use("seaborn-paper")  # seaborn-talk, seaborn-poster, seaborn-paper
plt.rcParams.update(
    {
        "font.family": "sans-serif",
        "figure.dpi": 300,
        "savefig.format": "png",
    }
)


################################################################################
# Controls

CHECK = False
SEARCH = False
USE_BEST_SEARCH_CONFIG = False
TRAIN = True
EVAL = True
PLOT = True
ANALYZE = False

TRAIN_CONTINUE = False


num_check_samples = 100
num_plot_samples = 20
num_principal_components = 3


################################################################################
# Data paths

project_name = "ex-mnist"
project_dir = Path.cwd().parent / "DATA" / project_name
project = KadiAIProject(project_name, root=project_dir)

# Read paths
train_samples, valid_samples, test_samples = project.get_split_datasets(
    shuffle=True, valid_split=0.15, test_split=0.15
)


################################################################################
# Data definition

data_definition = project.data_definition
data_definition.input_features = ["label"]
data_definition.output_features = ["image"]


################################################################################
# Neural network

# Model
def model_function(hp, data_definition):

    # Hyper parameters
    num_kernels_adv = hp.Choice("num_kernels_adv", [32, 64, 128, 256, 512], default=64)
    num_kernels_gen = hp.Choice("num_kernels_gen", [128, 256, 512, 1024], default=256)
    dropout_adv = hp.Float("dropout_adv", 0.0, 0.7, default=0.3)

    # Generator
    layers = []
    layers.append(
        clayers.Sampling(
            add_sampling_loss=False,
            mode="concat",
            latent_dim=100,
            use_input_as_seed=False,
        )
    )
    layers.append(klayers.Dense(7 * 7 * num_kernels_gen, use_bias=False))
    layers.append(klayers.BatchNormalization())
    layers.append(klayers.LeakyReLU())
    layers.append(klayers.Reshape((7, 7, num_kernels_gen)))
    layers.append(
        klayers.Conv2DTranspose(
            128, (5, 5), strides=(1, 1), padding="same", use_bias=False
        )
    )
    layers.append(klayers.BatchNormalization())
    layers.append(klayers.LeakyReLU())
    layers.append(
        klayers.Conv2DTranspose(
            num_kernels_gen // 2, (5, 5), strides=(2, 2), padding="same", use_bias=False
        )
    )
    layers.append(klayers.BatchNormalization())
    layers.append(klayers.LeakyReLU())
    layers.append(
        klayers.Conv2DTranspose(
            1, (5, 5), strides=(2, 2), padding="same", use_bias=False, activation="tanh"
        )
    )
    generator = tf.keras.Sequential(layers)

    # Adversarial
    layers = []
    layers.append(
        klayers.Conv2D(num_kernels_adv, (5, 5), strides=(2, 2), padding="same")
    )
    layers.append(klayers.LeakyReLU())
    layers.append(klayers.Dropout(dropout_adv))
    layers.append(
        klayers.Conv2D(num_kernels_adv * 2, (5, 5), strides=(2, 2), padding="same")
    )
    layers.append(klayers.LeakyReLU())
    layers.append(klayers.Dropout(dropout_adv))
    layers.append(klayers.Flatten())
    layers.append(klayers.Dense(1))
    discriminator = tf.keras.Sequential(layers)

    # Gather models
    model_dict = OrderedDict()
    model_dict["generator"] = generator
    model_dict["conditional_adversarial"] = discriminator
    return model_dict


# Set a model name
model_name = "mnist-dcgan"
model_name += "--" + "--".join(
    [
        "+".join(list(data_definition.input_features)),
        "+".join(list(data_definition.output_features)),
    ]
)
model_name += "--onehot"


# Training schedule
def schedule_function(hp):
    learning_rate = hp.Choice(
        "learning_rate", [3e-3, 1e-3, 3e-4, 1e-4, 3e-5, 1e-5], default=1e-4
    )
    batch_size = 256  # divided by number of GPUs
    schedule = {
        "count": [1, 201],
        "learning_rate": learning_rate,
        "batch_size": batch_size,
    }
    return schedule


model = cids.CIDSModel.generative_adversarial(
    data_definition,
    model_function,
    name=model_name,
    identifier="",
    result_dir=project.result_dir,
)
model.num_classes = 10
model.encode_categorical = "inputs"
model.metrics = {"adversarial": ["accuracy"], "forward": ["mae"]}
model.monitor = "val_loss_sum"
model.online_normalize = False
model.data_reader.prefetch = "cache"
model.save_freq = 5


if CHECK:
    if PLOT:
        check_samples = model.read_tfrecords(
            train_samples[:num_check_samples], disable_feature_merge=True
        )
        project.log("Plotting feature distributions.")
        model.plot_feature_distribution(check_samples, project.input_dir)


if SEARCH:
    project.log(">> Hyperparameters: searching...")
    hp = model.search(
        train_samples,
        valid_samples,
        schedule=schedule_function,
        executions_per_trial=1,
        max_epochs=51,  # hyperband only
        overwrite=False,
        objective="val_mae",
        method="hyperband",
        # method="bayes",
        num_trials=200,
        # callbacks=[tf.keras.callbacks.EarlyStopping(patience=5)],
    )
    if PLOT:
        search_results = SearchResults(model)
        search_results.plot_hyperparameter_search()
    model.identifier = "best"
    project.log(">> Hyperparameters: search complete.")
elif USE_BEST_SEARCH_CONFIG:
    try:
        search_results = SearchResults(model)
        hps = search_results.get_best_hyperparameters(print="best")
        hp = hps[0]
        model.identifier = "best"
        project.log(">> Hyperparameters: loaded from previous search.")
    except (FileNotFoundError, PermissionError) as e:
        project.log(">> Hyperparameters: " + str(e))
        project.log(">> Hyperparameters: No search found. Defaults loaded.")
        hp = HyperParameters()
        model.identifier = "default"
else:
    project.log(">> Hyperparameters: Defaults loaded.")
    hp = HyperParameters()
    model.identifier = "default"


if TRAIN:
    project.log(">> Training...")
    if TRAIN_CONTINUE:
        project.log(">> ...continuing from last...")
        checkpoint = "last"
    else:
        project.log(">> ...starting new...")
        checkpoint = None
    model.VERBOSITY = 2
    model.train(
        train_samples,
        valid_samples,
        schedule=schedule_function,
        checkpoint=checkpoint,
        hp=hp,
    )
    project.log(">> Training complete.")

if EVAL:
    project.log(">> Evaluating...")
    project.log(">>> Metrics...")
    # Compute predictions
    # test_loss = model.eval_data(
    #     test_samples, batch_size=4, checkpoint="last", hp=hp, submodels="generator")
    X, Y, Y_ = model.infer_data(
        test_samples, batch_size=4, checkpoint="last", hp=hp, submodels="generator"
    )
    test_result_file = Path(model.base_model_dir, "test_results.npz")
    np.savez(test_result_file, X=X, Y=Y, Y_=Y_)
    # Evaluate metrics
    total_metrics = SortedDict()
    total_metrics["mae"] = metrics.mean_absolute_error(Y, Y_)
    total_metrics["mape"] = metrics.mean_absolute_percentage_error(Y, Y_)
    total_metrics["smape"] = metrics.symmetric_mean_absolute_percentage_error(Y, Y_)
    total_metrics["rmse"] = metrics.root_mean_square_error(Y, Y_)
    total_metrics["nrmse"] = metrics.normalized_root_mean_square_error(Y, Y_)
    project.log("Total metrics")
    for k, v in total_metrics.items():
        project.log(f"   {k}: {v}")
    project.log("")

    # Feature metrics
    feature_metrics = SortedDict()
    feature_metrics["mae"] = metrics.mean_absolute_error(
        Y, Y_, reduction_axes=(0, 1, 2)
    )
    feature_metrics["mape"] = metrics.mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(0, 1, 2)
    )
    feature_metrics["smape"] = metrics.symmetric_mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(0, 1, 2)
    )
    feature_metrics["rmse"] = metrics.root_mean_square_error(
        Y, Y_, reduction_axes=(0, 1, 2)
    )
    feature_metrics["nrmse"] = metrics.normalized_root_mean_square_error(
        Y, Y_, reduction_axes=(0, 1, 2)
    )
    project.log("Feature metrics")
    for k, v in feature_metrics.items():
        project.log(f"   {k}: {v}")
    project.log("")

    # Sample metrics
    sample_metrics = SortedDict()
    sample_metrics["mae"] = metrics.mean_absolute_error(Y, Y_, reduction_axes=(1, 2, 3))
    sample_metrics["mape"] = metrics.mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(1, 2, 3)
    )
    sample_metrics["smape"] = metrics.symmetric_mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(1, 2, 3)
    )
    sample_metrics["rmse"] = metrics.root_mean_square_error(
        Y, Y_, reduction_axes=(1, 2, 3)
    )
    sample_metrics["nrmse"] = metrics.normalized_root_mean_square_error(
        Y, Y_, reduction_axes=(1, 2, 3)
    )
    project.log("Sample metrics:")
    for k, v in sample_metrics.items():
        project.log(f"   {k}: {v}")
    project.log("")

    # Select worst, best, mean, median samples
    error_metric = "mae"
    sample_errors = sample_metrics[error_metric]
    total_error = total_metrics[error_metric]
    sorted_sample_ids = np.argsort(sample_errors)
    sorted_sample_errors = sample_errors[sorted_sample_ids]
    # nonconvergence_cutoff = 100000.0
    # converged_sorted_sample_errors = sorted_sample_errors[
    #     sorted_sample_errors < nonconvergence_cutoff
    # ]
    # converged_sorted_sample_ids = sorted_sample_ids[
    #     sorted_sample_errors < nonconvergence_cutoff
    # ]
    converged_sorted_sample_ids = sorted_sample_ids
    converged_sorted_sample_errors = sorted_sample_errors
    best_sample_id = converged_sorted_sample_ids[0]
    worst_sample_id = converged_sorted_sample_ids[-1]
    median_sample_id = converged_sorted_sample_ids[
        len(converged_sorted_sample_ids) // 2
    ]
    perc25_sample_id = converged_sorted_sample_ids[
        len(converged_sorted_sample_ids) * 1 // 4
    ]
    perc75_sample_id = converged_sorted_sample_ids[
        len(converged_sorted_sample_ids) * 3 // 4
    ]
    mean_sample_id = np.argmin(
        np.abs(sample_errors - np.mean(converged_sorted_sample_errors))
    )
    project.log(f"{project_name}: Sample quality: sample {error_metric}")
    project.log(
        f"{project_name}:\n"
        + f"    mean: {sample_errors[mean_sample_id]}"
        + f" ({test_samples[mean_sample_id]})\n"
        + f"    best: {sample_errors[best_sample_id]}"
        + f" ({test_samples[best_sample_id]})\n"
        + f"    worst: {sample_errors[worst_sample_id]}"
        + f" ({test_samples[worst_sample_id]})\n"
        + f"    median: {sample_errors[median_sample_id]}"
        + f" ({test_samples[median_sample_id]})\n"
        + f"    perc25: {sample_errors[perc25_sample_id]}"
        + f" ({test_samples[perc25_sample_id]})\n"
        + f"    perc75: {sample_errors[perc75_sample_id]}"
        + f" ({test_samples[perc75_sample_id]})\n"
    )

# Plot
if PLOT:
    project.log(">> Plotting results...")
    test_result_file = Path(model.base_model_dir) / "test_results.npz"
    test_results = np.load(test_result_file)
    X = test_results["X"][:num_plot_samples]
    Y = test_results["Y"][:num_plot_samples]
    Y_ = test_results["Y_"][:num_plot_samples]
    # Plot individual samples
    project.log(">>> Plotting individual samples")
    for i, (x, y, y_) in enumerate(zip(X, Y, Y_)):
        label = int(np.squeeze(x))
        image = np.squeeze(y)
        image_ = np.squeeze(y_)
        # Draw
        fig, axes = plt.subplots(1, 2, figsize=(7, 4))
        plt.sca(axes[0])
        plt.imshow(image, cmap=sns.color_palette("mako_r", as_cmap=True))
        plt.axis("off")
        plt.sca(axes[1])
        plt.imshow(image_, cmap=sns.color_palette("mako_r", as_cmap=True))
        plt.axis("off")
        fig.suptitle(f"label = {label:d}")
        fig.tight_layout()
        # Save and close
        plot_file = Path(model.plot_dir) / f"best_to_worst_{i:05d}_label={label:d}"
        plt.savefig(plot_file)
        plt.close()

    # # Plot distributions
    # project.log(">>> Plotting individual samples")
    # for ofeature in data_definition.output_features:
    #     for ifeature in data_definition.input_features:
    #         # Extract
    #         fi = data_definition.get_input_feature_indices(ifeature)
    #         fo = data_definition.get_output_feature_indices(ofeature)
    #         xfi_cols = [ifeature + f"{i:02d}" for i in range(len(fi))]
    #         yfo_cols = [ofeature + f"{i:02d}" for i in range(len(fo))]
    #         yfo_cols_ = [ofeature + f"{i:02d}" for i in range(len(fo))]
    #         xfi = pd.DataFrame(data=np.squeeze(X[..., fi]), columns=xfi_cols)
    #         yfo = pd.DataFrame(data=np.squeeze(Y[..., fo]), columns=yfo_cols)
    #         yfo_ = pd.DataFrame(data=np.squeeze(Y_[..., fo]), columns=yfo_cols_)
    #         # bin_up = yfo_ >= 0.8
    #         # bin_mid = np.logical_and(yfo_ < 0.8, yfo_ > 0.4)
    #         # bin_low = yfo_ <= 0.4
    #         # binned = np.copy(yfo_)
    #         # binned[bin_up] = 1.0
    #         # binned[bin_mid] = 0.6
    #         # binned[bin_low] = 0.2
    #         # true_binned_x = xfi[binned == yfo]
    #         # true_binned_y = binned[binned == yfo]
    #         # false_binned_x = xfi[binned != yfo]
    #         # false_binned_y = binned[binned != yfo]
    #         # Plot
    #         if xfi.ndim == 1:
    #             fig = plt.figure(figsize=(4, 3))
    #             plt.scatter(xfi, yfo, color="C1", marker="o", label="ref")
    #             plt.scatter(xfi, yfo_, color="C0", marker="s", alpha=0.3, label="nn")
    #             # plt.scatter(true_binned_x, true_binned_y, color="C0", marker="s",
    #             #             label="true binned nn")
    #             # plt.scatter(false_binned_x, false_binned_y, color="C0", marker="x",
    #             #             label="false binned nn")
    #             plt.xlabel(ifeature)
    #             plt.ylabel(ofeature)
    #             plt.legend(loc="upper center", ncol=2)
    #             plt.tight_layout()
    #             plt.draw()
    #         else:
    #             melted = yfo.melt()
    #             melted_ = yfo_.melt()
    #             melted.columns = [melted.columns[0], "true"]
    #             melted["predicted"] = melted_["value"]
    #             df = melted
    #             df["predicted"][df["predicted"] > 0.6] = 1.0
    #             df["predicted"][df["predicted"] <= 0.6] = 0.2
    #             g = sns.FacetGrid(
    #                 df,
    #                 col="variable",
    #                 col_wrap=5,
    #                 aspect=1,
    #                 height=2,
    #                 sharex=False,
    #                 sharey=False,
    #             )
    #             g.map(sns.scatterplot, "predicted", "true")
    #             g.set(xlim=(0.0, 1.0), ylim=(0.0, 1.0))
    #             plt.draw()
    #         # TODO: histograms? allow no accurate matching but better for
    #         #  comparing distributions
    #         # Save and close
    #         plot_file = Path(model.plot_dir) / f"feature_{ifeature:s}_{ofeature:s}"
    #         plt.savefig(plot_file)
    #         plt.close()
    #         # csv_file = Path(model.plot_dir) / f"hyst_{sample_name}_{si:06d}.csv"
    #         # np.savetxt(
    #         #     csv_file, np.concatenate([x, y, y_], axis=-1),
    #         #     header=",".join(
    #         #         data_definition.input_features
    #         #         + data_definition.output_features
    #         #         + [f + "_" for f in data_definition.output_features]),
    #         #     delimiter=",")

# Explain
if ANALYZE:
    analze_samples = test_samples[:10]
    X, Y = model.read_tfrecords(test_samples[:num_plot_samples])
    # # PCA analysis
    # Y_, C1, C2 = model.predict(X, return_states=True)
    # for si in range(num_plot_samples):
    #     # Compute PCA on single sample      # TODO: pca over multiple samples?
    #     cov = np.dot(C1[si, ...].T, C1[si, ...])
    #     u, s, v = np.linalg.svd(cov, compute_uv=True)
    #     pc_axes = np.dot(C1[si, ...], u[:, :num_principal_components])
    #     # Plot loading and relaxation
    #     fig, axes = plt.subplots(3, 1, sharex=True, figsize=(7, 6),
    #                              gridspec_kw={"height_ratios": [0.5, 1, 1]})
    #     plt.sca(axes[0])
    #     plt.plot(X[si, :, 0], "C0", label="strain")
    #     plt.ylabel("strain [-]")
    #     plt.sca(axes[1])
    #     plt.plot(Y[si, :, 1], "C2", label="strain_plastic_ref")
    #     plt.plot(Y_[si, :, 1], "C2", linestyle="dashed",
    #              label="strain_plastic")
    #     for pc in range(num_principal_components):
    #         plt.plot(pc_axes[:, pc], "C{:d}".format(3 + pc),
    #                  linestyle="dotted", label="state{:d}".format(pc))
    #     plt.ylabel("history [-]")
    #     plt.sca(axes[2])
    #     plt.plot(Y[si, :, 0], "C1", label="stress_ref")
    #     plt.plot(Y_[si, :, 0], "C1", linestyle="dashed", label="stress")
    #     plt.ylabel("stress [-]")
    #     plt.xlabel("increments [-]")
    #     fig.legend(loc="upper center", ncol=4)
    #     plt.tight_layout()
    #     plt.draw()
    #     plt.savefig(Path(model.plot_dir) / f"pca_{si:06d}")
    #     plt.close()
    # Local response propagation
    analysis = model.analyze(
        X[:num_plot_samples, ...],
        method="lrp.epsilon",
        neuron_selection_mode="all",
        # neuron_selection=0,
        plot=True,
        checkpoint="last",
    )
    project.log(">> Evaluation complete.")


################################################################################
# Finished

project.log("done")

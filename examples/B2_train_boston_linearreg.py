from pathlib import Path

import numpy as np
import sklearn as sk
from matplotlib import pyplot as plt
from sklearn import linear_model

import cids
from kadi_ai.projects import Project


# Preamble
plt.style.use("seaborn-paper")  # seaborn-talk, seaborn-poster, seaborn-paper
plt.rcParams.update(
    {
        "font.family": "sans-serif",
        "figure.dpi": 300,
        "savefig.format": "png",
    }
)


###############################################################################
# Controls

CHECK = True
SEARCH = False
USE_BEST_SEARCH_CONFIG = False
TRAIN = True
EVAL = True
PLOT = True
ANALYZE = False

TRAIN_CONTINUE = False
SAVE = True


num_check_samples = 100
num_plot_samples = 20


###############################################################################
# Data paths

project_name = "boston"
project_dir = Path.cwd().parent / "DATA" / project_name
project = Project(project_name, root=project_dir)

# Read paths
train_samples, valid_samples, test_samples = project.get_split_datasets(
    shuffle=True, valid_split=0.15, test_split=0.15
)


###############################################################################
# Data definition

data_definition = project.data_definition
data_definition.input_features = [
    "CRIM",
    "ZN",
    "INDUS",
    "CHAS",
    "NOX",
    "RM",
    "AGE",
    "DIS",
    "RAD",
    "TAX",
    "PTRATIO",
    "B",
    "LAST",
]
data_definition.output_features = ["MEDV"]


###############################################################################
# Model

# Set a model name
model_name = "boston"
model_name += "--" + "--".join(
    [
        "+".join(list(data_definition.input_features)),
        "+".join(list(data_definition.output_features)),
    ]
)
model_name += "--linear-regression"


def model_function(**kwargs):
    return linear_model.LinearRegression(**kwargs)


model = cids.CIDSModelSK.regression(
    data_definition,
    model_function,
    name=model_name,
    identifier="",
    result_dir=project.result_dir,
)

skkwargs = {}  # empty to use sklearn default hp settings

model.build(hp=skkwargs)


if CHECK:
    if PLOT:
        check_samples = model.read_tfrecords(
            train_samples[:num_check_samples], disable_feature_merge=True
        )
        project.log("Plotting feature distributions.")
        # model.check_feature_distributions(check_samples, project.input_dir)

if TRAIN:
    project.log(">> Training...")
    model.train(train_samples)  # , valid_samples)
    project.log(">> Training complete.")

if SAVE:
    project.log(">> Saving...")
    model.save()
    project.log(">> Saving complete.")

# only for testing: load saved models
# model.load()

if EVAL:
    project.log(">> Evaluating...")
    X, Y, Y_ = model.infer_data(test_samples, postprocess=False)
    # save test resuls
    test_result_file = Path(model.base_model_dir, "test_results.npz")
    np.savez(test_result_file, X=X, Y=Y, Y_=Y_)

    # Create dict with sk metrics and optional functional arguments
    # TODO: how to parse param. to mean_squared_error like "squared=False"?
    metric_dict = [sk.metrics.mean_squared_error, sk.metrics.r2_score]
    metric_dict = model.eval_data(Y, Y_, metrics=metric_dict)
    project.log("Metrics")
    for k, v in metric_dict.items():
        project.log(f"   {k}: {v}")
    project.log("")
    project.log("Evaluating complete.")

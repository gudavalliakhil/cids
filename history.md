# List of changes

## Version 3.0 (September 2022)

* Published as open-source software via Gitlab, Kadi, and Zenodo
* KadiAI as interface between CIDS and Kadi
  * KadiAIProject handles synchronization, linking, and organizing data locally
  * KadiAIProjects define the ML task to be solved by CIDSModels
  * KadiAIProjects implement an ML ontology in Kadi
  * Interactive Dashboards for Kadi Workflows
  * CIDS nodes for Kadi workflows
* Improved and streamlined CIDS' DataDefinition and preprocessing capabilities
* Experimental wrapper for SKLearn
* Providing CIDS' search, tuning, and optimization to SKLearn models and Kadi workflows

#!/usr/bin/env bash
if [ $# -eq 0 ]
then
    img="intelligent_analysis-gpu:latest"
else
    img=${1}
fi
docker run -v "${PWD}:/opt/intelligent_analysis" \
           -w "/opt/intelligent_analysis/fem/beam" \
           ${img} \
           python setup.py build_ext -if

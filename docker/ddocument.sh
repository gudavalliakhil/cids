#!/usr/bin/env bash

IMG="intelligent_analysis2:latest"

USERNAME="${USER}"
USERID="$(id -u)"

RESULTDIR=$1

echo "Building documentation.";

docker run \
    -v "${PWD}:/opt/intelligent_analysis" \
    -w "/opt/intelligent_analysis/docs" \
    -u "developer" \
    --name "${IMG%:*}.documentation" \
    --rm \
    ${IMG} \
    make clean

rm -rf docs/source/_autosummary/*


docker run \
    -v "${PWD}:/opt/intelligent_analysis" \
    -w "/opt/intelligent_analysis/docs" \
    -u "developer" \
    --name "${IMG%:*}.documentation" \
    --rm \
    ${IMG} \
    make html

#!/usr/bin/env bash
docker build \
    -t "$(basename $1 .dockerfile):latest" \
    -f ${1} \
    .

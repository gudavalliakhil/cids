#!/usr/bin/env bash

IMG="intelligent_analysis2:latest"

USERNAME="${USER}"
USERID="$(id -u)"

RESULTDIR=$1

echo "Running documentation and tensorboard.";

# Run tensorboard
echo "   Running tensorboard...";
echo "   |- Summary directory: ${RESULTDIR} " \
    "    (must be relative to and in ../DATA)";
docker run -d \
    -e "HOSTNAME=$(cat /etc/hostname)" \
    -e "DISPLAY=${DISPLAY}" \
    -p 6006:6006 \
    -v "/tmp/.X11-unix":"/tmp/.X11-unix" \
    -v "${PWD}:/opt/intelligent_analysis" \
    -v "${PWD}/../DATA:/opt/DATA" \
    -v "${PWD}/../demos:/opt/demos" \
    -w "/opt/intelligent_analysis" \
    -u "developer" \
    --name "${IMG%:*}.tensorboard" \
    --rm \
    ${IMG} \
    tensorboard --host=0.0.0.0 --logdir="/opt/DATA/${RESULTDIR}" >/dev/null;

sleep 5.0;
xdg-open "http://localhost:6006";

echo "   [ Waiting for interrupt... ]"

trap ctrl_c INT TERM

function ctrl_c() {
    echo "   [ Interrupted. ]"
    docker stop "${IMG%:*}.tensorboard" >/dev/null;
    echo "   Stopped tensorboard.";
#    docker stop "${IMG%:*}.documentation" >/dev/null;
#    echo "   Stopped documentation server.";
}

sleep infinity;

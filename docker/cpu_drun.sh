#!/usr/bin/env bash

IMG="intelligent_analysis2:latest"

USERNAME="${USER}"
USERID="$(id -u)"

SCRIPTNAME=$1
TIME=$(date -u +%Y-%m-%d_UTC%H-%M-%S)
LOGNAME="RESULTS/${TIME}_$(basename ${SCRIPTNAME} .py).log"

if [ $# -eq 1 ]
then
    export CPUS="8.0";
elif [ $# -eq 2 ]
then
    export CPUS="8.0";
elif [ $# -eq 3 ]
then
    export CPUS=$2;
elif [ $# -eq 4 ]
then
    export CPUS=$2;
    export IMG=$3":latest";
else
    echo $@
fi

echo "CPUS:"
echo "${CPUS}"

# TODO: remove when tensorflow is less verbose about not using a GPU implementation
echo -e "\e[34mThe following CUDA errors can be ignored. We are not using CUDA in this script.\e[39m"

# Run script
docker run \
    -e "HOSTNAME=$(cat /etc/hostname)" \
    -e "DISPLAY=${DISPLAY}" \
    -v "/tmp/.X11-unix":"/tmp/.X11-unix" \
    -v "${PWD}:/opt/intelligent_analysis" \
    -v "${PWD}/../DATA:/opt/DATA" \
    -v "${PWD}/../demos:/opt/demos" \
    -w "/opt/intelligent_analysis" \
    -u "developer" \
    --cpus="${CPUS}" \
    --name "${IMG%:*}.${TIME}.${SCRIPTNAME##*/}" \
    --rm \
    ${IMG} \
    python -u "${SCRIPTNAME}" |& tee -i ${LOGNAME}

# Set Permissions correctly for download
docker run \
    -v "${PWD}:/opt/intelligent_analysis" \
    -v "${PWD}/../DATA:/opt/DATA" \
    -w "/opt/intelligent_analysis" \
    -u "developer" \
    --rm \
    ${IMG} \
    chmod -R a+rw "RESULTS" >/dev/null 2>&1; \
    chown -R "${USERID}:${USERID}" "RESULTS" >/dev/null 2>&1; \
    chmod -R a+rw "/opt/DATA" >/dev/null 2>&1; \
    chown -R "${USERID}:${USERID}" "/opt/DATA" >/dev/null 2>&1;

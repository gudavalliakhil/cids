CIDS and KadiAI readme
===================

[![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![flake8 status](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/flake8/flake8-badge.svg?job=flake8-master)](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/flake8/flake8stats.txt)
[![pylint status](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/pylint/pylint-badge.svg?job=pylint-master)](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/pylint/pylint.log)
[![test](https://gitlab.com/intelligent-analysis/cids/badges/master/pipeline.svg?job=unittests&key_text=tests)](https://gitlab.com/intelligent-analysis/cids/-/commits/master)
[![docs](https://gitlab.com/intelligent-analysis/cids/badges/master/pipeline.svg?job=pages&key_text=docs)](https://intelligent-analysis.gitlab.io/cids/index.html)
[![pipeline](https://gitlab.com/intelligent-analysis/cids/badges/master/pipeline.svg)](https://gitlab.com/intelligent-analysis/cids/-/commits/master)
[![coverage](https://gitlab.com/intelligent-analysis/cids/badges/master/coverage.svg?job=unittests)](https://gitlab.com/intelligent-analysis/cids/-/commits/master)

*CIDS* is a framework for Artificial Intelligence (AI) and Machine Learning (ML) for
applications from engineering, materials, and natural sciences. It combines models,
functions, and pipelines from libraries such as tensorflow/keras, sklearn, scipy, and
pandas to build modular, flexible, and reproducible AI models.

The interface *KadiAI* integrates AI tools, such as CIDS, seamlessly into Kadi workflows
and interacts with Kadi's repositories and data management features.

**The full documentation is available at:**

[https://intelligent-analysis.gitlab.io/cids/](https://intelligent-analysis.gitlab.io/cids/)

**The CIDS and KadiAI source codes are available at:**

[https://gitlab.com/intelligent-analysis/cids](https://gitlab.com/intelligent-analysis/cids)

**Demo scripts of CIDS projects are available at:**

The invite-only community repository `demos` contains scripts for applications ranging
from motion analysis to hybrid finite elements in solid mechanics.

[https://gitlab.com/intelligent-analysis/demos](https://gitlab.com/intelligent-analysis/demos)

## Install

**The reference configuration is a Linux (Ubuntu 20.04) OS.
For any other configuration, the steps below are given as is, but not guaranteed
to work.**

1. Download and install your favorite Python IDE (e.g. PyCharm Professional, VS Code)
2. Download and install Git (Ubuntu: `sudo apt-get install git-all`)
    * Install your favorite Git Client (e.g. GitKraken)

There are two ways to install the required environments.
*Manual installation* with pip requires all operating system components and packages
(in particular Nvidia CUDA, CUDNN, and drivers) to be installed manually with compatible
versions.
*Docker* automatically installs the required packages and system components in a
container that is independent of the operating system.

### a) Manual install (pip, recommended)

3. Install a Python (>3.9) distribution (e.g. Anaconda): [https://www.tensorflow.org/install/pip](https://www.tensorflow.org/install/pip)
4. Set up GPU support and CUDA: [https://www.tensorflow.org/install/gpu](https://www.tensorflow.org/install/gpu)
    * Requires CUDA, CUDNN and Nvidia drivers with compatible versions (may clash
    with requirements by other programs on the host machine)
    * Compatible combinations: [https://www.tensorflow.org/install/source#gpu](https://www.tensorflow.org/install/source#gpu)
    * Anaconda offers a convenient way that sets up CUDA and CUDNN, if the right driver is available.
    Select the CUDA toolkit and CUDNN version compatible with your GPU:

    ```
    >>> conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0
    ```

5. Permanently add the CUDA library path to your environment, e.g., via conda activate:

    ```
    mkdir -p $CONDA_PREFIX/etc/conda/activate.d
    echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/' > $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
    ```

6. Go to the CIDS repository directory on your system.
7. Linux (Ubuntu)/Mac shell or Windows command prompt:

    ```
    >>> pip install -e .[dev]
    ```

8. Install git pre-commit hooks for development

    ```
    >>> pre-commit install
    ```

### b) Docker (automatic containerized deployment)

3. Download and install Docker
    * Linux (Ubuntu):
        1. Install: [https://docs.docker.com/install/linux/docker-ce/ubuntu/](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
        2. Configure: [https://docs.docker.com/install/linux/linux-postinstall/](https://docs.docker.com/install/linux/linux-postinstall/)
        3. (GPU support: [https://github.com/NVIDIA/nvidia-docker](https://github.com/NVIDIA/nvidia-docker); no longer necessary!)
    * Windows/Mac: [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)
        * Probably no GPU support
4. Clone the repository from Git using a Git client
5. Build the docker images
     * Linux (Ubuntu)/Mac shell:
        * CPU only:

            ```
            >>> ./docker/dbuild.sh docker/intelligent_analysis2.dockerfile
            ```

        * GPU:

            ```
            >>> ./docker/dbuild.sh docker/intelligent_analysis2-gpu.dockerfile
            ```

            * ensure that NVIDIA driver is compatible with CUDA 11.2
            (nvidia-driver-470)
     * Windows command prompt:
        * CPU only:

            ```
            >>> .\docker\dbuild.bat intelligent_analysis2
            ```

        * GPU (may not work):

            ```
            >>> .\docker\dbuild.bat intelligent_analysis2-gpu
            ```

            * ensure that Nvidia driver is compatible with cuda 11.2
6. Optional: configure run configuration e.g. in Pycharm Professional
    1. Add ProjectInterpreter "Docker", choosing one of the previously created
    docker images
    2. Create a run configuration for each script
        * Script name relative from project root
            * e.g. ```demos/00_examples/A1_convert_mnist.py```
        * Working directory ```/opt/intelligent_analysis/```
        * Docker container settings: volume bindings  (container : host)
            * ```/opt/intelligent_analysis``` : ```[project root]```
            * ```/opt/DATA``` : ```[data directory]```
    3. Ensure Pycharm's memory heap size is increased to a reasonable size
    ([https://www.jetbrains.com/help/pycharm/tuning-the-ide.html](https://www.jetbrains.com/help/pycharm/tuning-the-ide.html))

### Docker troubleshooting

* sudo chmod a+rwx /var/run/docker.sock and/or sudo chmod a+rwx /var/run/docker.pid may
be necessary.
* To access data on the host machine, ensure the path mappings are correct

## First steps

After installing and building the docker images, scripts can be executed from the
project root directory with the following bash scripts (linux only). The scripts under
```demos/00_examples``` can serve as templates.

Clone the repository [https://gitlab.com/intelligent-analysis/demos](https://gitlab.com/intelligent-analysis/demos) besides your `cids`
repository:

```bash
$ git clone git@gitlab.com:intelligent-analysis/demos.git
$ ls
cids/ demos/
```

### a) Python after manual install

```bash
>>> python -u [ path to file in demo folder ]
```

#### Examples

```
>>> python -u demos/00_examples/A1_convert_mnist.py
```

### b) Docker

Allows automatic development and installation with the same settings on all machines.

*Windows users:* replace ```./drun.sh``` with ```drun.bat``` in the following
commands.

**CPU only**

```bash
>>> ./docker/cpu_drun.sh   [ path ] "[ OPT.: CPUs  ]" "[ OPT.: docker image ]"
```

**GPU**

```bash
>>> ./docker/drun.sh [ path ] "[ OPT.: GPU index ]" "[ OPT.: CPUs ]" "[ OPT.: docker image ]"
```

#### Examples

```bash
>>> ./docker/cpu_drun.sh demos/00_examples/A1_convert_mnist.py
```

```bash
>>> ./docker/cpu_drun.sh demos/00_examples/A2_train_mnist_dcgan.py "16.0" "intelligent_analysis2"
```

```bash
>>> ./docker/drun.sh demos/00_examples/A2_train_mnist_dcgan.py "0" "16.0" "intelligent_analysis2-gpu"
```

```bash
>>> ./docker/drun.sh demos/00_examples/A2_train_mnist_dcgan.py
```

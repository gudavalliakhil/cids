# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

from .projects.project import KadiAIProject
from cids.tensorflow.model import CIDSModel
from cids.tensorflow.model_functions import dense_dropout_model_function


def _setup_project(**kwargs):
    # Read kwargs
    project_name = kwargs.get("project_name")
    project_dir = kwargs.get("project_dir")
    src_identifier = kwargs.get("src_identifier", None)
    tfrecord_identifier = kwargs.get("tfrecord_identifier", None)
    result_identifier = kwargs.get("result_identifier", None)

    # Setup project
    project = KadiAIProject(
        project_name,
        seed=project_name,
        root=project_dir,
        src_identifier=src_identifier,
        tfrecord_identifier=tfrecord_identifier,
        result_identifier=result_identifier,
    )

    # Kill other CIDS workflow nodes
    project.kill()

    if "detached" in kwargs:
        print(f"Detached process with pid: {os.getpid()}")
        project.write_pid()

    # Always write dashboard PID
    print(f"CIDS workflow node with pid: {os.getpid()}")
    project.write_pid()

    # Manually set all directories
    project.tfrecord_dir = project.input_dir / "tfrecord"
    project.src_dir = project.input_dir / "src"

    return project


def _setup_data_definition(project, **kwargs):
    # Read kwargs
    input_features = kwargs.get("input_features", None)
    output_features = kwargs.get("output_features", None)

    # Data definition
    data_definition = project.data_definition
    if input_features is None and not data_definition.input_features:
        data_definition.input_features = []
    else:
        input_features = [fi.strip() for fi in input_features.split(",")]
        data_definition.input_features = input_features
    if output_features is None and not data_definition.output_features:
        data_definition.output_features = []
    else:
        output_features = [fo.strip() for fo in output_features.split(",")]
        data_definition.output_features = output_features

    return data_definition


def _setup_model(project, data_definition, model_settings=None, **kwargs):

    # Read kwargs
    # TODO: select initial model and model function
    model = kwargs.get("model", dense_dropout_model_function)
    name = kwargs.get(
        "name",
        project.name + "--" + model.__name__.replace("_model_function", ""),
    )
    # TODO: Does the following implementation add any functionality? If not, remove
    #   This will help:
    #  https://stackoverflow.com/questions/547829/how-to-dynamically-load-a-python-class
    # if "name" in kwargs:
    #     model_function = kwargs.get("name")
    #     model_module = locate(
    #         "cids.tensorflow.model_functions." + model_function
    #     )
    #     if model_module:
    #         model = kwargs.get("model", model_module)
    #         name = kwargs.get(
    #             "name",
    #             project.name + "--" + model.__name__.replace("_model_function", ""),
    #         )
    #     else:
    #         model = kwargs.get("name", model_function)
    #         name = kwargs.get("name", project.name + "--" + model.name)
    # else:
    #     model = kwargs.get("model")
    #     name = kwargs.get(
    #         "name",
    #         project.name + "--" + model.__name__.replace("_model_function", ""),
    #     )
    # Instantiate model
    # TODO: loss, task, and constructors
    if model_settings:
        model_method = getattr(CIDSModel, model_settings)
        model = model_method(
            data_definition,
            model,
            name=name,
            identifier="",
            result_dir=project.result_dir,
            **kwargs,
        )
    else:
        model = CIDSModel(
            data_definition,
            model,
            name=name,
            identifier="",
            result_dir=project.result_dir,
        )
    return model

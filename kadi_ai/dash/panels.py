# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Dashboard elements and panels for KadiAI dashboards. Part of KadiAI."""
import base64
import importlib.util
import inspect
import os
import random
from copy import deepcopy
from enum import EnumMeta
from pathlib import Path
from typing import Iterable

import dash_bootstrap_components as dbc
import dash_editor_components as dec
from dash import callback_context
from dash import dcc
from dash import html
from dash.dependencies import ALL
from dash.dependencies import MATCH
from dash.exceptions import PreventUpdate
from dash_extensions.enrich import Input
from dash_extensions.enrich import Output
from dash_extensions.enrich import State
from flask import request

from ..utility import _setup_model
from .constants import WebAppIdentifiers as ID
from cids.data.offline_processing.content import read_file_hierarchy
from cids.data.preprocessor import Preprocessor
from cids.data.reader import DataReader
from cids.data.writer import DataWriter


def _function_label(function_name):
    return function_name.replace("_", " ").capitalize()


def find_functions(module, file=None):
    candidates = {
        name: fun
        for name, fun in module.__dict__.items()
        if callable(fun) and name.endswith("function") and not name.startswith("_")
    }
    labels = [
        {"label": k.replace("_", " ").capitalize(), "value": k}
        for k in sorted(candidates.keys())
    ]
    if file is None or not Path(file).exists():
        return candidates, labels
    file = Path(file)
    spec = importlib.util.spec_from_file_location("module.name", file)
    dummy_module = importlib.util.module_from_spec(spec)
    try:
        spec.loader.exec_module(dummy_module)  # TODO: this is quite unsafe
    except (ModuleNotFoundError, ImportError):
        return candidates, labels
    file_candidates = {
        "custom_" + name: fun
        for name, fun in dummy_module.__dict__.items()
        if callable(fun) and name.endswith("function") and not name.startswith("_")
    }
    file_labels = [
        {"label": _function_label(k), "value": k}
        for k in sorted(file_candidates.keys())
    ]
    candidates.update(file_candidates)
    labels = file_labels + labels
    return candidates, labels


def _find_file_extensions(path):
    path = Path(path)
    if not path.is_dir():
        return path.suffix
    extensions = set()
    for p in path.glob("*"):
        content_extensions = _find_file_extensions(p)
        if isinstance(content_extensions, str):
            extensions.add(content_extensions)
        else:
            extensions = extensions | content_extensions
    return extensions


def file_glob_selection_panel_builder(app, src_dir):
    file_extensions = sorted(_find_file_extensions(src_dir))
    initial_glob_options = [
        {"label": f"**/*{e}", "value": f"**/*{e}"} for e in file_extensions
    ]
    # Elements
    dd_branch = dcc.Dropdown(
        id={"type": ID.filter_dropdown_glob, "index": "branch"},
        multi=True,
        options=initial_glob_options,
        disabled=False,
    )
    dd_leaves = dcc.Dropdown(
        id={"type": ID.filter_dropdown_glob, "index": "leaves"},
        multi=True,
        options=initial_glob_options,
        disabled=False,
    )
    lock_button = html.Button(
        id=ID.button_glob_lock,
        children=[html.I(id=ID.label_glob_lock, className="fa fa-check")],
    )
    # Panel and layout
    panel = html.Div(
        className="row p-3 justify-content-center align-items-center",
        children=[
            html.Div(
                className="col-1",
                children=[html.Label("Branch files:")],
            ),
            html.Div(className="col-2", children=[dd_branch]),
            html.Span(className="col-1"),
            html.Div(
                className="col-1",
                children=[
                    html.Label(
                        "Leaf files:",
                        htmlFor="data-definition-dd-leaf-globs",
                    ),
                ],
            ),
            html.Div(
                className="col-5",
                children=[dd_leaves],
            ),
            html.Span(className="col-1"),
            html.Div(className="col-1", children=[lock_button]),
        ],
    )

    # Callbacks
    @app.callback(
        Output({"type": ID.filter_dropdown_glob, "index": MATCH}, "options"),
        Input({"type": ID.filter_dropdown_glob, "index": MATCH}, "search_value"),
        State({"type": ID.filter_dropdown_glob, "index": MATCH}, "value"),
        State({"type": ID.filter_dropdown_glob, "index": MATCH}, "options"),
    )
    def glob_dropdown_update_options(search_value, values, options):
        # Check if received search value
        if not search_value:
            raise PreventUpdate()
        # Check if alreay exists
        labels = [o["label"] for o in options]
        if search_value in labels:  # Ensure search value string not in list
            raise PreventUpdate()
        # Check if valid glob pattern
        try:
            found = src_dir.glob(search_value)
            next(found)
        except (ValueError, StopIteration) as e:
            raise PreventUpdate() from e
        # Rebuild options
        options = deepcopy(initial_glob_options)
        # Add already selected values to options
        if values:
            values = sorted(values)
            labels = [o["label"] for o in options]
            for value in values:
                if value not in labels:
                    options.append({"label": value, "value": value})
        # Add new option
        options = [{"label": search_value, "value": search_value}] + options
        return options

    @app.callback(
        Output(ID.label_glob_lock, "className"),
        Output({"type": ID.filter_dropdown_glob, "index": ALL}, "disabled"),
        Output(ID.button_preview, "disabled"),
        Output(ID.button_preprocess, "disabled"),
        Output(ID.table_before_datadefinition, "data"),
        Output(ID.table_after_datadefinition, "data"),
        Input(ID.button_glob_lock, "n_clicks"),
        State({"type": ID.filter_dropdown_glob, "index": ALL}, "id"),
        State({"type": ID.filter_dropdown_glob, "index": ALL}, "value"),
        State({"type": ID.filter_dropdown_glob, "index": ALL}, "disabled"),
        State({"type": ID.filter_dropdown_function_select, "index": ALL}, "value"),
        State(
            {
                "type": ID.filter_panel_param,
                "function": ALL,
                "index": ALL,
                "param": ALL,
            },
            "value",
        ),
        State(
            {
                "type": ID.filter_label_param,
                "function": ALL,
                "param": ALL,
                "index": ALL,
            },
            "id",
        ),
        prevent_initial_call=True,
    )
    def glob_dropdown_update_value(
        n_clicks,
        ids,
        values,
        disabled,
        preprocessing_functions,
        preprocessing_params,
        preprocessing_labels,
    ):
        if not any(values):
            raise PreventUpdate()
        # Execute preprocessing
        if not all(disabled):
            # Limit number of displayed files
            display_files = 5
            # Extract glob pattern lists
            indices = [id["index"] for id in ids]
            branch_globs = values[indices.index("branch")] or []
            leaf_globs = values[indices.index("leaves")] or []
            # Execute preprocessing
            preprocessing_steps = _parse_preprocessing_steps(
                preprocessing_functions, preprocessing_params, preprocessing_labels
            )
            (
                project,
                _,
                samples,
                raw_data_definition,
                data_definition,
            ) = _execute_preprocessing(
                app._project,
                branch_globs,
                leaf_globs,
                preprocessing_steps=preprocessing_steps,
                limit_files=display_files,
                dry=False,
            )
            # Store internally
            app._project = project
            app._samples = samples
            app._raw_data_definition = raw_data_definition
            app._data_definition = data_definition
        else:
            raw_data_definition = app._raw_data_definition
            data_definition = app._data_definition

        # Update buttons and input fields
        disabled_glob_fields = [not bool(d) for d in disabled]
        if not all(disabled_glob_fields):
            class_name = "fa fa-check"
            disabled_buttons = True
        else:
            class_name = "fa fa-edit"
            disabled_buttons = False
        return (
            class_name,
            disabled_glob_fields,
            disabled_buttons,
            disabled_buttons,
            raw_data_definition.to_table(),
            data_definition.to_table(),
        )

    return panel


def model_function_selection_panel_builder(
    app,
    project,
    model_function_choices,
    model_definition,
    training_function_choices,
    training_definition,
):
    # Elements
    dd_model_fun = dcc.Dropdown(
        id=ID.dropdown_model_fun_select,
        options=model_function_choices,
        value=model_definition.name,
    )
    dd_training_fun = dcc.Dropdown(
        id=ID.dropdown_training_fun_select,
        options=training_function_choices,
        value=training_definition.name,
    )
    # Panel and layout
    panel = html.Div(
        className="col p-3 border border-secondary rounded justify-content-between align-items-center",  # pylint:disable=line-too-long
        children=[
            html.H4("Model function"),
            dd_model_fun,
            html.Br(),
            html.H4("Training function"),
            dd_training_fun,
            html.Br(),
        ],
    )

    @app.callback(
        Output(ID.editor_model_function, "value"),
        Output(ID.panel_model_hyperparameters, "children"),
        Input(ID.dropdown_model_fun_select, "value"),
    )
    def update_model_function(select):
        app._selected_fun["model"] = select
        # TODO: model function should be saved by the model... but how to pass?
        pycode = app._hp_definitions["model"][select].to_py(path=project.result_dir)
        panel = app._hp_panels["model"][select]
        return pycode, panel

    @app.callback(
        Output(ID.editor_training_function, "value"),
        Output(ID.panel_training_hyperparameters, "children"),
        Output(ID.panel_progress_hyperparameters, "children"),
        Input(ID.dropdown_training_fun_select, "value"),
    )
    def update_training_function(select):
        app._selected_fun["training"] = select
        # TODO: training function should be saved by the model... but how to pass?
        pycode = app._hp_definitions["training"][select].to_py(path=project.result_dir)
        training_panel = app._hp_panels["training"][select]
        progress_panel = app._hp_panels["progress"][select]
        return pycode, training_panel, progress_panel

    return panel


def preprocessing_panel_builder(app, functions):
    # Initial layout
    children = [
        html.H3("Preprocessing steps"),
        html.Div(
            id=ID.panel_preprocessing,
            children=[_function_panel(app, functions, 0)],
        ),
    ]
    # Create panel
    panel = html.Div(children=children)

    # Callbacks
    @app.callback(
        Output(ID.panel_preprocessing, "children"),
        Input({"type": ID.filter_dropdown_function_select, "index": ALL}, "value"),
        State(ID.panel_preprocessing, "children"),
        prevent_initial_call=True,
    )
    def dropdown_function_select(all_values, pp_children):
        # Delete if all but one empty
        if all_values.count(None) > 1:
            del_index = all_values.index(None)
            del pp_children[del_index]
            return pp_children
        # Add new function select dropdown for next preprocessing step
        if all_values.count(None) < 1:
            new_index = len(pp_children)
            f_panel = _function_panel(app, functions, new_index)
            pp_children.append(f_panel)
        return pp_children

    @app.callback(
        Output({"type": ID.filter_panel_function_param, "index": MATCH}, "children"),
        Input({"type": ID.filter_dropdown_function_select, "index": MATCH}, "value"),
        State(ID.table_before_datadefinition, "data"),
        prevent_initial_call=True,
    )
    def dropdown_function_param_fields(value, raw_data_definition_list):
        # Get triggered child
        input_index = list(callback_context.triggered_prop_ids.values())[0]["index"]
        # Add new children
        if value is not None:
            # Get corresponding function from function label
            function = next(f for f in functions if f.__name__ == value)
            fpp_child, _ = _function_params(
                app, function, input_index, raw_data_definition_list
            )
            fpp_children = [fpp_child]
        else:
            fpp_children = []
        return fpp_children  # Return parent before child elements!

    @app.callback(
        Output(
            {
                "type": ID.filter_panel_param,
                "function": MATCH,
                "index": MATCH,
                "param": MATCH,
            },
            "value",
        ),
        Input(
            {
                "type": ID.filter_panel_param,
                "function": MATCH,
                "index": MATCH,
                "param": MATCH,
            },
            "value",
        ),
        State(
            {
                "type": ID.filter_panel_param,
                "function": MATCH,
                "index": MATCH,
                "param": MATCH,
            },
            "options",
        ),
        prevent_initial_call=True,
    )
    def update_feature_keyword_values(values, options):
        if not values:
            raise PreventUpdate()
        # Check if alreay exists
        if "ALL" in values:
            all_values = [o["value"] for o in options]
            nonkeyword_values = [v for v in values if v != "ALL"]
        elif "ALL_FEATURES" in values:
            all_values = list(app._data_definition.features.keys())
            nonkeyword_values = [v for v in values if v != "ALL_FEATURES"]
        else:
            raise PreventUpdate()
        additional_values = [v for v in all_values if v not in nonkeyword_values]
        values = nonkeyword_values + additional_values
        return values

    @app.callback(
        Output(
            {
                "type": ID.filter_panel_param,
                "function": MATCH,
                "index": MATCH,
                "param": MATCH,
            },
            "options",
        ),
        Input(
            {
                "type": ID.filter_panel_param,
                "function": MATCH,
                "index": MATCH,
                "param": MATCH,
            },
            "search_value",
        ),
        State(
            {
                "type": ID.filter_panel_param,
                "function": MATCH,
                "index": MATCH,
                "param": MATCH,
            },
            "value",
        ),
        State(
            {
                "type": ID.filter_panel_param,
                "function": MATCH,
                "index": MATCH,
                "param": MATCH,
            },
            "options",
        ),
        prevent_initial_call=True,
    )
    def update_feature_multi_options(search_value, values, options):
        if not search_value:
            raise PreventUpdate()
        # Check if alreay exists
        labels = [o["label"] for o in options]
        if search_value in labels:  # Ensure search value string not in list
            raise PreventUpdate()
        # Add already selected values to options
        if values:
            try:
                values = sorted(values)
            except TypeError as e:
                raise PreventUpdate() from e
            for value in values:
                if value not in labels:
                    options.append({"label": value, "value": value})
        # Add new option
        options = [{"label": search_value, "value": search_value}] + options
        return options

    return panel


def _function_panel(app, functions, index):
    # Function select
    fs_panel = _function_select(app, functions, index)
    fp_panel = html.Div(
        id={"type": ID.filter_panel_function_param, "index": index}, children=[]
    )
    # Panel and layout
    panel = html.Div(
        className="col p-3 border border-secondary rounded bg-secondary",
        children=[fs_panel, fp_panel],
    )
    return panel


def _function_select(app, functions, index):
    options = [{"label": f.__name__, "value": f.__name__} for f in functions]
    # Element
    elements = [
        dcc.Dropdown(
            id={"type": ID.filter_dropdown_function_select, "index": index},
            options=options,
            placeholder="Add preprocessing step...",
            multi=False,
            clearable=True,
        )
    ]
    panel = html.Div(className="row", children=elements)
    return panel


def _wrap_param_field(label_id, label_text, input_id, field, docstring):
    return html.Div(
        className="row",
        children=[
            html.Label(
                id=label_id,
                children=[label_text],
                className="col-4",
                htmlFor=input_id,
            ),
            html.Div(className="col-6", children=[field]),
            dbc.Tooltip(docstring, target=input_id, placement="bottom"),
        ],
    )


def _function_params(
    app,
    function,
    index,
    raw_data_definition_list,
    ignore_params=("self", "sample", "samples"),
):
    # Extract function information
    name = function.__name__
    docstring = inspect.getdoc(function)
    signature = inspect.signature(function)
    # Assemble Element an callbacks
    elements = []
    callbacks = []
    # Assemble special param fields
    special_params = []
    param = signature.parameters.get(
        "features", signature.parameters.get("feature_names", None)
    )
    if param:
        param_name = "features"
        input_id = {
            "type": ID.filter_panel_param,
            "function": name,
            "index": index,
            "param": param_name,
        }
        label_id = {
            "type": ID.filter_label_param,
            "function": name,
            "param": param_name,
            "index": index,
        }
        input_default = param.default
        if raw_data_definition_list is None:
            raw_data_definition_list = []
        initial_options = [
            {"label": f["name"], "value": f["name"]} for f in raw_data_definition_list
        ]
        field = dcc.Dropdown(
            options=initial_options,
            value=input_default,
            id=input_id,
            multi=True,
        )
        elements.append(
            _wrap_param_field(label_id, param_name, input_id, field, docstring)
        )
    special_params.extend(["feature_names", "features"])
    # Assemble all other fields
    for param_name, param in signature.parameters.items():
        if param_name in special_params + list(ignore_params):
            continue
        # Unique identifier
        input_id = {
            "type": ID.filter_panel_param,
            "function": name,
            "index": index,
            "param": param_name,
        }
        label_id = {
            "type": ID.filter_label_param,
            "function": name,
            "param": param_name,
            "index": index,
        }
        # Placeholder / default value
        input_default = param.default
        if input_default == inspect.Parameter.empty:
            input_default = None
        # Type of input field
        if param.annotation in (int, float):
            field = dcc.Input(id=input_id, type="number", value=input_default)
        elif param.annotation == bool:
            field = dcc.Dropdown(
                options=[
                    {"label": "True", "value": True},
                    {"label": "False", "value": False},
                ],
                id=input_id,
                multi=False,
                value=input_default,
            )
        elif isinstance(param.annotation, EnumMeta):
            # Options defined by Enum
            options = [
                {"label": str(choice.value), "value": choice.value}
                for choice in param.annotation
            ]
            field = dcc.Dropdown(
                options=options,
                id=input_id,
                multi=False,
                value=input_default.value,
            )
        elif param.annotation == str:
            field = dcc.Input(id=input_id, type="text", value=input_default)
        elif param.annotation in (list, tuple, Iterable[str]):
            field = dcc.Dropdown(
                options=[],
                value=input_default,
                id=input_id,
                multi=True,
            )

            @app.callback(
                Output(input_id, "options"),
                Input(input_id, "search_value"),
                State(input_id, "value"),
                prevent_initial_call=True,
            )
            def update_multi_options(search_value, value):
                if not search_value:
                    raise PreventUpdate
                candidates = value or []
                options = [{"label": v, "value": v} for v in candidates]
                options.append({"label": search_value, "value": search_value})
                return options

            callbacks.append(update_multi_options)
        else:
            field = dcc.Input(id=input_id, type="text", value=input_default)
        elements.append(
            _wrap_param_field(label_id, param_name, input_id, field, docstring)
        )
    panel = html.Div(className="px-3 pb-3 pt-1 row align-items-end", children=elements)
    return panel, callbacks


def _read_src(project, branch_globs, leaf_globs, limit_files=None):
    # Scan file tree
    file_hierarchy = project.scan_file_hierarchy(
        project.src_dir,
        branch_globs=branch_globs,
        leaf_globs=leaf_globs,
    )
    if not file_hierarchy:
        raise FileNotFoundError("No files found, check paths and path mappings!")
    # Limit number of found files
    if limit_files is not None:
        if not isinstance(limit_files, (list, tuple)):
            limit_files = min(limit_files, len(file_hierarchy))
            limit_files = random.sample(file_hierarchy.keys(), limit_files)
        file_hierarchy = {k: file_hierarchy[k] for k in limit_files}
    project.log("Raw data file hierachy scanned.")
    # Create empty data definition
    samples, data_definition = read_file_hierarchy(file_hierarchy, root=project.src_dir)
    # Exit
    project.log("Raw data read successfully.")
    return samples, data_definition


def _offline_preprocessing(samples, data_definition, preprocessing_steps, project):
    preprocessor = Preprocessor.from_json(data_definition, project.input_dir)
    preprocessor.preprocessing_steps = preprocessing_steps
    # Execute preprocessing
    samples, data_definition = preprocessor.preprocess(samples)
    # Serialize preprocessing steps
    preprocessor.to_json(path=project.input_dir)
    return samples, data_definition


def _write_samples(project, samples):
    # Process samples
    data_writer = DataWriter.from_json(project.data_definition)
    project.data_definition.samples = []
    data_writer.write_samples(samples, project.tfrecord_dir, basename=None)
    # Update data definition and project
    project.data_definition.to_json()
    project.to_json()
    project.log(f"Added {len(samples)} tfrecords to data definition.")
    return project


def _read_samples(project, limit_files=5):
    data_definition = project.data_definition
    tfrecords = data_definition.samples[:limit_files]
    if tfrecords:
        return DataReader(data_definition).read_tfrecords(tfrecords)
    return []


def _parse_preprocessing_steps(functions, params, labels):
    # TODO: function to unparse preprocessing steps to load previous state
    if not functions or not labels:
        return []
    functions = [f for f in functions if f is not None]
    # Group args and kwargs by index, maintain order and associating with function
    args_list = []
    kwargs_list = []
    args = []
    kwargs = {}
    index = labels[0]["index"]
    function_order = list(functions)
    current_function = function_order.pop(0)
    for param, label in zip(params, labels):
        if index != label["index"]:
            # index changed: add args and kwargs to list
            args_list.append(args)
            kwargs_list.append(kwargs)
            # Next function
            current_function = function_order.pop(0)
            args = []
            kwargs = {}
            index = label["index"]
        # Validate correct function
        if not current_function == label["function"]:
            raise IndexError("Parameter associated with wrong function!")
        # Add param to list
        kwargs[label["param"]] = param
    args_list.append(args)
    kwargs_list.append(kwargs)
    return list(zip(functions, args_list, kwargs_list))


def _execute_preprocessing(
    project,
    branch_globs,
    leaf_globs,
    preprocessing_steps=(),
    overwrite_data_definition=None,
    limit_files=5,
    dry=True,
):
    # Read raw data
    # TODO: raw or normal data definition has no samples set!!!
    # TODO: delete old tfrecords before for clean dataset
    raw_samples, raw_data_definition = _read_src(
        project, branch_globs, leaf_globs, limit_files=limit_files
    )
    if overwrite_data_definition is not None:
        raw_data_definition = overwrite_data_definition
    # Preprocess
    project.log("Executing offline preprocessing.")
    samples, data_definition = _offline_preprocessing(
        raw_samples, raw_data_definition, preprocessing_steps, project
    )
    if limit_files:
        samples = samples[:limit_files]
    project.log("Finished offline preprocessing.")
    # Write to tfrecords
    if not dry:
        project.data_definition = data_definition
        project = _write_samples(project, samples)
        samples = _read_samples(project, limit_files=limit_files)
    return project, raw_samples, samples, raw_data_definition, data_definition


def execute_panel_builder(app, project):
    # Elements
    button_preview = html.Button(
        className="button-primary",
        id=ID.button_preview,
        disabled=True,
        children=[
            html.I(className="fa fa-lg fa-magnifying-glass-chart"),
            html.Span(className="space-between-icon-and-label"),
            html.Span("Preview"),
        ],
    )
    button_preprocess = html.Button(
        className="button-primary",
        id=ID.button_preprocess,
        disabled=True,
        children=[
            html.I(id=ID.button_preprocess_icon, className="fa fa-lg fa-gear"),
            html.Span(className="space-between-icon-and-label"),
            html.Span("Preprocess"),
        ],
    )
    progress_bar = html.Div(
        className="w-50",
        children=[
            dcc.Interval(
                id=ID.interval_update_progress,
                n_intervals=0,
                interval=1000,
            ),
            dbc.Progress(
                id=ID.progress_bar,
                striped=True,
                animated=True,
                color="primary",
                value=25,
            ),
        ],
    )
    # Panel and layout
    panel = html.Div(
        className="d-flex justify-content-between align-items-center",
        children=[button_preview, button_preprocess, progress_bar],
    )
    # Workaround for global parameters (Not threadsafe but okay for single user)
    app._project = project

    # Callback
    @app.callback(
        Output(ID.table_before_datadefinition, "data"),
        Output(ID.table_after_datadefinition, "data"),
        Output(ID.table_after_datadefinition, "selected_rows"),
        Output(ID.button_push, "disabled"),
        Input(ID.button_preview, "n_clicks"),
        Input(ID.button_preprocess, "n_clicks"),
        State({"type": ID.filter_dropdown_glob, "index": "branch"}, "value"),
        State({"type": ID.filter_dropdown_glob, "index": "leaves"}, "value"),
        State({"type": ID.filter_dropdown_function_select, "index": ALL}, "value"),
        State(
            {
                "type": ID.filter_panel_param,
                "function": ALL,
                "index": ALL,
                "param": ALL,
            },
            "value",
        ),
        State(
            {
                "type": ID.filter_label_param,
                "function": ALL,
                "param": ALL,
                "index": ALL,
            },
            "id",
        ),
        prevent_initial_call=True,
    )
    def callback_execute_preprocessing(
        n_clicks_preview,
        n_clicks_preprocess,
        branch_globs,
        leaf_globs,
        preprocessing_functions,
        preprocessing_params,
        preprocessing_labels,
    ):
        if not branch_globs:
            raise PreventUpdate()
        if leaf_globs is None:
            leaf_globs = []
        # Check if preview or preprocess
        triggered = list(callback_context.triggered_prop_ids.values())[0]
        if triggered == ID.button_preprocess:
            limit_files = None
            disable_button_push = False
            dry = False  # TODO: it might be good if this works as dry run too
        else:
            limit_files = 5
            disable_button_push = True
            dry = False
        # Parse and execute preprocessing steps
        preprocessing_steps = _parse_preprocessing_steps(
            preprocessing_functions, preprocessing_params, preprocessing_labels
        )
        (
            app._project,
            _,
            samples,
            raw_data_definition,
            data_definition,
        ) = _execute_preprocessing(
            app._project,
            branch_globs,
            leaf_globs,
            preprocessing_steps,
            limit_files=limit_files,
            dry=dry,
        )
        raw_data_definition_dict = raw_data_definition.to_table()
        data_definition_dict = data_definition.to_table()
        # Update app memory
        app._samples = samples
        selected_rows = []
        return (
            raw_data_definition_dict,
            data_definition_dict,
            selected_rows,
            disable_button_push,
        )

    return panel


def titlebar_builder(title):
    logo1 = os.fspath(
        Path(__file__).parent.parent.parent
        / "docs"
        / "source"
        / "logo"
        / "kadi_ai_dark.png"
    )
    logo2 = os.fspath(
        Path(__file__).parent.parent.parent
        / "docs"
        / "source"
        / "logo"
        / "cids_dark.png"
    )
    with open(logo1, "rb") as f:
        encoded_logo1 = base64.b64encode(f.read()).decode("ascii")
    with open(logo2, "rb") as f:
        encoded_logo2 = base64.b64encode(f.read()).decode("ascii")
    return html.Div(
        [
            html.Div(
                [
                    html.Img(
                        src=f"data:image/png;base64,{encoded_logo1}",
                        height="50px",
                    )
                ],
                className="col-2 p-3 text-start",
            ),
            html.Div(
                [html.H2(title)],
                className="col-8 text-center",
            ),
            html.Div(
                [
                    html.Img(
                        src=f"data:image/png;base64,{encoded_logo2}",
                        height="50px",
                    )
                ],
                className="col-2 p-3 text-end",
            ),
        ],
        className="row px-3 align-items-center",
    )


def hyperparameter_panel_builder(app):
    # Elements
    model_hyperparameter_container = html.Div(id=ID.panel_model_hyperparameters)
    training_hyperparameter_container = html.Div(id=ID.panel_training_hyperparameters)
    progress_hyperparameter_container = html.Div(id=ID.panel_progress_hyperparameters)
    # Panel and layout
    panel = html.Div(
        className="col p-3 border border-secondary rounded",
        children=[
            html.H3("Model"),
            model_hyperparameter_container,
            html.H3("Training"),
            training_hyperparameter_container,
            html.H3("Training phases"),
            progress_hyperparameter_container,
        ],
    )
    return panel


def _is_valid_feature_slice(search, data_definition):
    features = data_definition.features.keys()
    if search is not None and search.partition("[")[0] in features:
        feature_name = search.partition("[")[0]
        if search.endswith("]"):
            slice_string = search[len(feature_name) :]
            feature = data_definition.features[feature_name]
            try:
                feature.slice(slice_string)
                return True
            except ValueError:
                return False
    return False


def _contains_label(label, option_list):
    return any(d["label"] in label for d in option_list)


def feature_select_panel_builder(app, model_definition):
    options = deepcopy(model_definition.feature_options)
    # Elements
    dd_input_features = dcc.Dropdown(
        id=ID.dropdown_input_features, multi=True, options=options
    )
    dd_output_features = dcc.Dropdown(
        id=ID.dropdown_output_features, multi=True, options=options
    )
    # Panel and layout
    panel = html.Div(
        className="col p-3 border border-secondary rounded",
        children=[
            html.H3("Input and output features"),
            html.Label("Inputs"),
            dd_input_features,
            html.Br(),
            html.Label("Outputs"),
            dd_output_features,
            html.Br(),
            html.Label(
                className="fst-italic",
                children="Select feature dimensions by indexing: e.g. [1] or [0,1]",
            ),
        ],
    )

    # Callbacks
    @app.callback(
        Output(ID.dropdown_input_features, "options"),
        Output(ID.dropdown_output_features, "options"),
        Input(ID.dropdown_input_features, "search_value"),
        Input(ID.dropdown_output_features, "search_value"),
    )
    def update_feature_options(input_search_value, output_search_value):
        search_value = input_search_value or output_search_value
        if not search_value:
            raise PreventUpdate()
        model_definition = app._hp_definitions[app._selected_fun["model"]]
        data_definition = model_definition.data_definition
        feature_opt = model_definition.feature_options
        if not _contains_label(search_value, feature_opt):
            raise PreventUpdate()
        if not _is_valid_feature_slice(search_value, data_definition):
            raise PreventUpdate()
        model_definition.add_feature(search_value)
        sorted_options = sorted(
            model_definition.feature_options, key=lambda k: k["label"].upper()
        )
        return sorted_options, sorted_options

    return panel


def editor_panel_builder(app, type="model"):
    if type == "training":
        id_editor = ID.editor_training_function
        id_button_lock = ID.button_editor_training_lock
        id_label_lock = ID.label_editor_training_lock
    else:
        id_editor = ID.editor_model_function
        id_button_lock = ID.button_editor_model_lock
        id_label_lock = ID.label_editor_model_lock
    # Elements
    editor = dec.PythonEditor(
        className="col-12 bg-light border border-secondary rounded",
        id=id_editor,
        placeholder="",
        autoFocus=True,
        readOnly=True,
    )
    lock_button = html.Button(
        id=id_button_lock,
        className="position-absolute top-0 start-100 bg-white",
        children=[html.I(id=id_label_lock, className="fa fa-lock")],
        # Overlap button onto editor field
        style={
            "z-index": "999",
            "transform": "translate(-110%, 10%)",
            "-ms-transform": "translate(-110%, 10%)",
            "width": "5%",
        },
    )
    # Panel and layout
    panel = html.Div(
        className="col position-relative",
        children=[
            lock_button,
            editor,
        ],
    )

    # Callbacks
    @app.callback(
        Output(id_editor, "readOnly"),
        Output(id_label_lock, "className"),
        Output(id_editor, "className"),
        Input(id_button_lock, "n_clicks"),
        State(id_editor, "className"),
        prevent_initial_call=True,
    )
    def unlock_edit_mode(n_clicks, editor_class_name):
        if n_clicks is None or n_clicks % 2 == 0:
            new_editor_class_name = editor_class_name.replace(
                "bg-white", "bg-light"
            ).replace("border-primary", "border-secondary")
            return True, "fa fa-lock", new_editor_class_name
        new_editor_class_name = editor_class_name.replace(
            "bg-light", "bg-white"
        ).replace("border-secondary", "border-primary")
        return False, "fa fa-lock-open", new_editor_class_name

    return panel


def model_figure_panel_builder(app, project):
    # Elements
    image = html.Img(
        id=ID.figure_model,
        className="w-100",
        src="",
        alt="Select input / output features to plot model.",
    )
    # Panel and layout
    panel = html.Div(
        className="col p-3 border border-primary rounded", children=[image]
    )

    # Callback
    @app.callback(
        Output(ID.figure_model, "src"),
        Input(ID.dropdown_input_features, "value"),
        Input(ID.dropdown_output_features, "value"),
        Input(ID.dropdown_model_fun_select, "value"),
        prevent_initial_call=True,
    )
    def update_image_src(input_features, output_features, select):
        if not input_features or not output_features:
            raise PreventUpdate()
        # Assign input and output features
        cids_model = _setup_model(
            project,
            project.data_definition,
            model=app._hp_definitions["model"][select].function,
        )
        cids_model.data_definition.input_features = input_features
        cids_model.data_definition.output_features = output_features
        # Update cids_model function
        hp = app._hp_definitions["model"][select].hp
        # IMPORTANT!!! activate default values!
        # TODO(Arnd): There must be a more elegant way
        hp.values = {k: v[0]._default for k, v in hp._hps.items() if v}
        cids_model.build(hp=hp)
        # Plot model to file(s)
        cids_model.plot_models()
        plotted_models = list(Path(cids_model.plot_dir).glob("*subcore*.png"))
        plotted_models = plotted_models or [Path(cids_model.plot_dir) / "model.png"]
        # Clean-up
        cids_model.clear()
        # TODO: multi-models
        with plotted_models[0].open("rb") as f:
            bytestring = base64.b64encode(f.read()).decode("utf8")
        image_source = f"data:image/png;base64,{bytestring}"
        return image_source

    return panel


def _shutdown():
    # TODO: fix deprecated warning
    #   https://werkzeug.palletsprojects.com/en/2.0.x/serving/#shutting-down-the-server
    func = request.environ.get("werkzeug.server.shutdown")
    if func is None:
        raise RuntimeError("Not running with the Werkzeug Server")
    func()


def exit_button_builder(app, project):
    # Elements
    dialog_button = dcc.ConfirmDialogProvider(
        children=html.Button(
            id=ID.button_exit,
            className="button-exit",
            children=[
                html.I(className="fa fa-lg fa-arrow-right-from-bracket"),
                html.Span(className="space-between-icon-and-label"),
                html.Span("Exit"),
            ],
        ),
        id=ID.button_exit_dialog,
        message="Are you sure you want to close the dashboard?",
    )
    # Panel and layout
    panel = html.Div(children=[dialog_button])

    # Callbacks
    @app.callback(
        Output(ID.button_exit, "n_clicks"),
        Input(ID.button_exit_dialog, "submit_n_clicks"),
    )
    def shutdown_server(submit_n_clicks):
        if submit_n_clicks != 0 and submit_n_clicks is not None:
            _shutdown()
            project.delete_pid()
        return 0

    return panel


def push_button_builder(app, project, content=None):
    content = content or []
    # Elements
    push_button = html.Button(
        id=ID.button_push,
        className="button-primary",
        disabled=True,
        n_clicks=0,
        children=[
            html.I(className="fa fa-lg fa-cloud-arrow-up"),
            html.Span(className="space-between-icon-and-label"),
            html.Span("Push to Kadi"),
        ],
    )
    # Panel and layout
    panel = html.Div(children=[push_button])

    # Callbacks
    @app.callback(
        Output(ID.button_push, "class_name"),
        Input(ID.button_push, "n_clicks"),
        State(ID.button_push, "class_name"),
        prevent_initial_call=True,
    )
    def push_to_kadi(n_clicks, class_name):
        if not n_clicks:
            raise PreventUpdate()
        kwargs = {c: True for c in content}
        project.push(**kwargs, force="update")
        return class_name

    return panel


def pull_button_builder(app, project, content=None):
    content = content or []
    # Elements
    pull_button = html.Button(
        id=ID.button_pull,
        className="button-primary",
        disabled=True,
        n_clicks=0,
        children=[
            html.I(className="fa fa-lg fa-cloud-arrow-down"),
            html.Span(className="space-between-icon-and-label"),
            html.Span("Pull from Kadi"),
        ],
    )
    # Panel and layout
    panel = html.Div(children=[pull_button])

    # Callbacks
    @app.callback(
        Output(ID.button_pull, "class_name"),
        Input(ID.button_pull, "n_clicks"),
        State(ID.button_pull, "class_name"),
        prevent_initial_call=True,
    )
    def pull_from_kadi(n_clicks, class_name):
        if not n_clicks:
            raise PreventUpdate()
        kwargs = {c: True for c in content}
        project.pull(**kwargs, force="update")
        return class_name

    return panel

# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Hyperparameter for model and training definitions. Part of KadiAI."""
import inspect
import re
from copy import deepcopy
from pathlib import Path

from black import FileMode
from black import format_str
from dash import callback_context
from dash import dcc
from dash import html
from dash.dependencies import ALL
from dash.exceptions import PreventUpdate
from dash_extensions.enrich import Input
from dash_extensions.enrich import Output
from dash_extensions.enrich import State
from kerastuner import HyperParameters
from kerastuner.engine.hyperparameters import Choice
from kerastuner.engine.hyperparameters import Fixed
from kerastuner.engine.hyperparameters import Float
from kerastuner.engine.hyperparameters import Int

from .constants import WebAppIdentifiers as ID


class HyperparameterDefinition:
    def __init__(self, function, name, code_editor, *args):
        """An algorithmic hyperparameter container.

        Args:
            function:       function that uses hyperparameters
            name:           name of the hyperparameter definition
            code_editor:    identifier of a dash element with a value
        """
        self.name = name
        self.function = function
        self.code_editor = code_editor
        self.collection = ""
        self.source_code = f"{self.imports}\n\n{inspect.getsource(self.function)}"
        # Get hyperparameters and results of default hyperparameters
        self.hp = HyperParameters()
        self.default_results = self.function(self.hp, *args)

    @property
    def imports(self):
        module_file = Path(inspect.getsourcefile(self.function))
        # Find all parents
        parents = []
        for p in module_file.parents:
            parents.append(p.stem)
            if p.stem in ["cids", "cidsflow"]:  # TODO: what about other module names?
                break
        # Read source file
        with module_file.open("r", encoding="utf8") as f:
            imports = f.read()
        # Maybe make imports absolute
        lines = []
        for line in imports.split("\n"):
            if line.startswith("import ") or line.startswith("from "):
                if "cids" in parents or "cidsflow" in parents:
                    for i in reversed(range(len(parents))):
                        line = line.replace(
                            " ." + (i * "."),
                            " " + ".".join(reversed(parents[i:])) + ".",
                        )
                lines.append(line)
        imports = "\n".join(lines)

        return imports

    def to_py(self, path=None):
        """Export HyperparameterDefinition.function to a python string.

        Args:
            file (os.Pathlike): a file to save the python code in

        Returns:
            pycode (str):  the function serialized as python code string
        """
        pycode = self.source_code
        if path is not None:
            path = Path(path)
            if path.is_dir():
                file = path / f"{self.collection}_definition.py"
            else:
                file = path
            file.parent.mkdir(parents=True, exist_ok=True)
            with file.open("w", encoding="utf8") as f:
                f.write(pycode)
        return pycode

    def hyperparameters_to_html(self, app):
        """Convert hyperparameters to editable html elements.

        Args:
            app (dash.App): a plotly dash application

        Returns:
            children: the html elements to pass as children into a dash html element
            callbacks: the callbacks that trigger this definition to be updated.
        """
        children = []
        for name, hyperparameter in self.hp._hps.items():
            h = hyperparameter[0]
            if isinstance(h, Int):
                child = self._hyperparameter_range(
                    name,
                    int(h.min_value),
                    int(h.max_value),
                    step=h.step,
                    default=int(h.default),
                    parent=self.name,
                    target=self.code_editor,
                )
                children.append(child)
            if isinstance(h, Float):
                child = self._hyperparameter_range(
                    name,
                    float(h.min_value),
                    float(h.max_value),
                    step=h.step,
                    default=float(h.default),
                    parent=self.name,
                    target=self.code_editor,
                )
                children.append(child)
            elif isinstance(h, Choice):
                child = self._hyperparameter_dropdown(
                    name,
                    h.values,
                    default=h.default,
                    parent=self.name,
                    target=self.code_editor,
                )
                children.append(child)
            elif isinstance(h, Fixed):
                child = self._hyperparameter_slider(
                    name, h.value, parent=self.name, target=self.code_editor
                )
                children.append(child)

        # Callbacks
        @app.callback(
            Output(
                {"type": ID.filter_hp_slider, "collection": ALL, "index": ALL}, "min"
            ),
            Output(
                {"type": ID.filter_hp_slider, "collection": ALL, "index": ALL}, "max"
            ),
            Output(
                {"type": ID.filter_hp_slider, "collection": ALL, "index": ALL},
                "marks",
            ),
            Output(self.code_editor, "value"),
            Input(
                {"type": ID.filter_hp_slider, "collection": ALL, "index": ALL},
                "value",
            ),
            State({"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "min"),
            State({"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "max"),
            State(
                {"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "marks"
            ),
            State({"type": ID.filter_hp_slider, "collection": ALL, "index": ALL}, "id"),
            prevent_initial_call=True,
        )
        def upate_hp_slider(values, min_values, max_values, marks_values, value_ids):
            # Get triggered child and associated indices
            input_collection = list(callback_context.triggered_prop_ids.values())[0][
                "collection"
            ]
            if input_collection != self.collection:
                raise PreventUpdate()
            input_index = list(callback_context.triggered_prop_ids.values())[0]["index"]
            index = [i for i, v in enumerate(value_ids) if v["index"] == input_index][0]
            # Update relevant values
            value = values[index]
            border, _ = HyperparameterDefinition._compute_border_and_step(value=value)
            # Update hyperparameters
            selected_fun = app._selected_fun[self.collection]
            hp_name = input_index.replace(selected_fun + "-", "")
            try:
                current_hps = self.hp._hps
                hp = current_hps[hp_name][0]
            except (KeyError, IndexError):
                raise PreventUpdate()  # pylint:disable=raise-missing-from
            hp.value = value
            # Update source code
            source_code = self.update_source_code()
            # Compute visualization values
            min_value = value - border
            max_value = value + border
            marks = {value - border: "-", value: str(value), value + border: "+"}
            # Apply to correct value in values list
            min_values = deepcopy(min_values)
            max_values = deepcopy(max_values)
            marks_values = deepcopy(marks_values)
            min_values[index] = min_value
            max_values[index] = max_value
            marks_values[index] = marks
            return min_values, max_values, marks_values, source_code

        @app.callback(
            # The MATCH keyword does not work here, due to the MultiplexerTransform
            # Thus, need to filter by hand
            Output(
                {"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "min"
            ),
            Output(
                {"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "max"
            ),
            Output(
                {"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "marks"
            ),
            Output(self.code_editor, "value"),
            Input(
                {"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "value"
            ),
            State({"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "min"),
            State({"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "max"),
            State(
                {"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "marks"
            ),
            State({"type": ID.filter_hp_range, "collection": ALL, "index": ALL}, "id"),
            prevent_initial_call=True,
        )
        def update_hp_range(values, min_values, max_values, marks_values, value_ids):
            # Get triggered child and associated indices
            input_collection = list(callback_context.triggered_prop_ids.values())[0][
                "collection"
            ]
            if input_collection != self.collection:
                raise PreventUpdate()
            input_index = list(callback_context.triggered_prop_ids.values())[0]["index"]
            index = [i for i, v in enumerate(value_ids) if v["index"] == input_index][0]
            # Modify current hp range
            value = values[index]
            value = sorted(value)
            default = value[len(value) // 2]
            min_value = value[0]
            max_value = value[-1]
            border, _ = HyperparameterDefinition._compute_border_and_step(
                value=default, min_value=min_value, max_value=max_value
            )
            marks = self._range_slider_marks(min_value, max_value, default, border)
            # Update hyperparameters
            selected_fun = app._selected_fun[self.collection]
            hp_name = input_index.replace(selected_fun + "-", "")
            try:
                current_hps = self.hp._hps
                hp = current_hps[hp_name][0]
            except (KeyError, IndexError):
                raise PreventUpdate()  # pylint:disable=raise-missing-from
            hp.min_value = min_value
            hp.max_value = max_value
            hp._default = default
            # Update source code
            source_code = self.update_source_code()
            # Compute visualization values
            min_value = min_value - border
            max_value = max_value + border
            # Apply to correct value in values list
            min_values = deepcopy(min_values)
            max_values = deepcopy(max_values)
            marks_values = deepcopy(marks_values)
            min_values[index] = min_value
            max_values[index] = max_value
            marks_values[index] = marks
            return min_values, max_values, marks_values, source_code

        @app.callback(
            # The MATCH keyword does not work here, due to the MultiplexerTransform
            # Thus, need to filter by hand
            Output(
                {"type": ID.filter_hp_dropdown, "collection": ALL, "index": ALL},
                "options",
            ),
            Output(
                {"type": ID.filter_hp_default, "collection": ALL, "index": ALL},
                "options",
            ),
            Input(
                {"type": ID.filter_hp_dropdown, "collection": ALL, "index": ALL},
                "search_value",
            ),
            State(
                {"type": ID.filter_hp_dropdown, "collection": ALL, "index": ALL},
                "value",
            ),
            State(
                {"type": ID.filter_hp_dropdown, "collection": ALL, "index": ALL},
                "options",
            ),
            State(
                {"type": ID.filter_hp_default, "collection": ALL, "index": ALL},
                "options",
            ),
            State(
                {"type": ID.filter_hp_dropdown, "collection": ALL, "index": ALL}, "id"
            ),
            State(
                {"type": ID.filter_hp_default, "collection": ALL, "index": ALL}, "id"
            ),
            prevent_initial_call=True,
        )
        def update_hp_dropdown_options(
            search_values, values, dd_options, default_options, value_ids, default_ids
        ):
            if not any(search_values):
                raise PreventUpdate()
            # Get triggered child and associated indices
            input_collection = list(callback_context.triggered_prop_ids.values())[0][
                "collection"
            ]
            if input_collection != self.collection:
                raise PreventUpdate()
            input_index = list(callback_context.triggered_prop_ids.values())[0]["index"]
            dd_index = [
                i for i, v in enumerate(value_ids) if v["index"] == input_index
            ][0]
            default_index = [
                i for i, v in enumerate(default_ids) if v["index"] == input_index
            ][0]
            search_values[dd_index] = search_values[dd_index].replace(",", "").strip()
            current_values = values[dd_index]
            labels = [o["label"] for o in dd_options[dd_index]]
            if search_values[dd_index] in labels:  # Ensure search value not in list
                raise PreventUpdate()
            try:
                numeric_type = type(current_values[0])
                search_values[dd_index] = numeric_type(search_values[dd_index])
            except ValueError as e:
                raise PreventUpdate() from e
            if search_values[dd_index] not in current_values:
                # Search value as numeric type is not a value
                current_values.append(search_values[dd_index])
            current_values = sorted(current_values)
            current_options = self._values_to_options(current_values)
            dd_options = deepcopy(dd_options)
            default_options = deepcopy(default_options)
            dd_options[dd_index] = current_options
            default_options[default_index] = current_options
            return dd_options, default_options

        @app.callback(
            # The MATCH keyword does not work here, due to the MultiplexerTransform
            # Thus, need to filter by hand
            Output(self.code_editor, "value"),
            Input(
                {"type": ID.filter_hp_dropdown, "collection": ALL, "index": ALL},
                "value",
            ),
            Input(
                {"type": ID.filter_hp_default, "collection": ALL, "index": ALL}, "value"
            ),
            State(
                {"type": ID.filter_hp_dropdown, "collection": ALL, "index": ALL}, "id"
            ),
            State(
                {"type": ID.filter_hp_default, "collection": ALL, "index": ALL}, "id"
            ),
            prevent_initial_call=True,
        )
        def update_hp_choice(values, defaults, value_ids, default_ids):
            # Get triggered child and associated indices
            input_collection = list(callback_context.triggered_prop_ids.values())[0][
                "collection"
            ]
            if input_collection != self.collection:
                raise PreventUpdate()
            input_index = list(callback_context.triggered_prop_ids.values())[0]["index"]
            dd_index = [
                i for i, v in enumerate(value_ids) if v["index"] == input_index
            ][0]
            default_index = [
                i for i, v in enumerate(default_ids) if v["index"] == input_index
            ][0]
            # Update hyperparameters
            selected_fun = app._selected_fun[self.collection]
            hp_name = input_index.replace(selected_fun + "-", "")
            try:
                current_hps = self.hp._hps
                hp = current_hps[hp_name][0]
            except (KeyError, IndexError):
                raise PreventUpdate()  # pylint:disable=raise-missing-from
            hp.values = values[dd_index]
            if defaults[default_index] in values[dd_index]:
                hp._default = defaults[default_index]
            # Update source code
            source_code = self.update_source_code()
            return source_code

        return children

    def _hyperparameter_slider(self, name, value, step=None, parent="", target=None):
        """Create a hyperparameter value slider."""
        border, step = self._compute_border_and_step(value=value, step=step)
        label = name.replace("_", " ").capitalize()
        if parent:
            name = parent + "-" + name
        # Elements
        label_hp = html.Label(
            children=label,
        )
        slider = dcc.Slider(
            id={
                "type": ID.filter_hp_slider,
                "collection": self.collection,
                "index": name,
            },
            value=value,
            min=value - border,
            max=value + border,
            step=step,
            updatemode="drag",
        )
        # Panel and layout
        panel = html.Div(
            className="row p-2 justify-content-between, align-items-start",
            children=[
                html.Div(className="col-2", children=[label_hp]),
                html.Div(className="col-10", children=[slider]),
            ],
        )
        return panel

    @staticmethod
    def _range_slider_marks(min_value, max_value, default, border):
        if isinstance(min_value, float):
            return {
                min_value - border: "-",
                min_value: f"{min_value:g}",
                default: f"{default:g}",
                max_value: f"{max_value:g}",
                max_value + border: "+",
            }
        return {
            min_value - border: "-",
            min_value: f"{min_value}",
            default: f"{default}",
            max_value: f"{max_value}",
            max_value + border: "+",
        }

    @staticmethod
    def _compute_border_and_step(value=None, max_value=None, min_value=None, step=None):
        if max_value is not None and min_value is not None:
            value = (max_value - min_value) / 2
        if value:
            if isinstance(value, int):
                border = step or abs(value) // 10
                step = step or 1
            else:
                border = abs(value) / 10.0
                step = step or border / 5.0
        else:
            border = 1.0
            step = 1.0
        return border, step

    def _hyperparameter_range(
        self,
        name,
        min_value,
        max_value,
        step=None,
        default=None,
        parent="",
        target=None,
    ):
        """Create a hyperparameter range slider."""
        if default is None:
            default = type(min_value)((max_value + min_value) / 2)
        border, step = self._compute_border_and_step(
            min_value=min_value, max_value=max_value, value=default, step=step
        )
        values = [min_value, default, max_value]
        label = name.replace("_", " ").capitalize()
        if parent:
            name = parent + "-" + name
        marks = self._range_slider_marks(min_value, max_value, default, border)
        # Elements
        label_hp = html.Label(
            children=[label],
        )
        range_slider = dcc.RangeSlider(
            id={
                "type": ID.filter_hp_range,
                "collection": self.collection,
                "index": name,
            },
            value=values,
            min=min_value - border,
            max=max_value + border,
            step=step,
            marks=marks,
            allowCross=False,
            updatemode="drag",
        )
        # Panel and layout
        panel = html.Div(
            className="row p-2 justify-content-between, align-items-start",
            children=[
                html.Div(className="col-2", children=[label_hp]),
                html.Div(className="col-10", children=[range_slider]),
            ],
        )
        return panel

    @staticmethod
    def _values_to_options(values):
        options = []
        for v in values:
            if isinstance(v, int):
                options.append({"label": f"{v:d}", "value": v})
            elif isinstance(v, float):
                options.append(
                    {
                        "label": f"{v:.3g}".replace("e-0", "e-").replace("e+0", "e"),
                        "value": v,
                    }
                )
            elif isinstance(v, str):
                # Assume first value defines other numeric type of all string values
                numeric_type = type(values[0])
                options.append({"label": f"{v}", "value": numeric_type(v)})
        return options

    def _hyperparameter_dropdown(
        self, name, values, default=None, parent="", target=None
    ):
        """Create a hyperparmeter choice dropdown menu."""
        default = default or values[len(values) // 2]
        options = self._values_to_options(values)
        label = name.replace("_", " ").capitalize()
        if parent:
            name = parent + "-" + name
        # Elements
        label_hp = html.Label(
            children=[label],
        )
        label_default = html.Label("(Default)")
        dd_choices = dcc.Dropdown(
            id={
                "type": ID.filter_hp_dropdown,
                "collection": self.collection,
                "index": name,
            },
            options=options,
            value=values,
            multi=True,
        )
        dd_default = dcc.Dropdown(
            id={
                "type": ID.filter_hp_default,
                "collection": self.collection,
                "index": name,
            },
            options=options,
            value=default,
            multi=False,
        )
        # Panel and layout
        panel = html.Div(
            className="row p-2 justify-content-between, align-items-start",
            children=[
                html.Div(className="col-2", children=[label_hp]),
                html.Div(className="col-7", children=[dd_choices]),
                html.Div(className="col-1", children=[label_default]),
                html.Div(className="col-2", children=[dd_default]),
            ],
        )
        return panel

    @staticmethod
    def _find_matching_parentheses(code_fragment, open_par="(", close_par=")"):
        toret = {}
        pstack = []
        for i, c in enumerate(code_fragment):
            if c == open_par:
                pstack.append(i)
            elif c == close_par:
                try:
                    toret[pstack.pop()] = i
                except IndexError as e:
                    raise IndexError("No matching closing parens at: " + str(i)) from e
        if len(pstack) > 0:
            raise IndexError("No matching opening parens at: " + str(pstack.pop()))
        return toret

    @staticmethod
    def _extract_hyperparameter_strings(code):
        hp_indices = [
            m.start()
            for m in re.finditer(
                r"(?:hp\.Choice|hp\.Float|hp\.Int|hp\.Fixed|\"count\"\:)", code
            )
        ]
        hp_indices.append(len(code))
        hp_strings = []
        for i, start in enumerate(hp_indices[:-1]):
            # Get find start and end indices of hp string
            next_start = hp_indices[i + 1]
            fragment = code[start:next_start]
            if fragment.startswith('"count":'):
                open_par = "["
                close_par = "]"
            else:
                open_par = "("
                close_par = ")"
            matching_brackets = HyperparameterDefinition._find_matching_parentheses(
                fragment, open_par=open_par, close_par=close_par
            )
            end = start + matching_brackets[min(matching_brackets.keys())]
            # Extract and sanitize hyperparameter string
            hp_string = code[start : end + 1].replace("\n", "").replace(" ", "")
            # Assume substring between first quotes pair in hp_string is hp name
            all_quoted = re.search(r"(?:\"|')([A-Za-z0-9_\./\\-]*)(?:\"|')", hp_string)
            hp_name = all_quoted.group(0)[1:-1]
            # Add to dictionary
            hp_strings.append(
                {"name": hp_name, "indices": (start, end), "string": hp_string}
            )
        return hp_strings

    def update_source_code(self, hp=None):
        """Update the function source code with the current or given hyperparameters.

        Args:
            hp (HyperParameters, optional): a kerastuner HyperParameters instance.
                Defaults to instance hyperparameters.
        """
        hp = hp or self.hp
        orig_source_code = self.source_code
        new_source_code = ""
        hp_strings = self._extract_hyperparameter_strings(orig_source_code)
        parsed = 0
        for d in hp_strings:
            # Get start and end indices of strings to insert
            start, end = d["indices"]
            # Treat count / phases / epochs separately
            if d["name"] == "count":
                try:
                    new_hp_string = f'"count": {str(self.phases)}'
                except AttributeError:
                    pass  # Not a training definition
            else:
                # Serialize hyperparameter to string
                new_hp = hp._hps[d["name"]][0]
                new_hp_string = str(new_hp)
                new_hp_string = "hp." + new_hp_string.replace(": ", "=")
                # Workaround for minor naming bug in kerastuner Fixed class
                if "Fixed" in new_hp_string:
                    new_hp_string = new_hp_string.replace("name=", 'name="')
                    ei = new_hp_string.index(",")
                    new_hp_string = new_hp_string[:ei] + '"' + new_hp_string[ei:]
                # Yet another workaround
                if "Choice" in new_hp_string and isinstance(new_hp.default, str):
                    new_hp_string = new_hp_string.replace("default=", 'default="')
                    si = new_hp_string.index("default=")
                    try:
                        ei = new_hp_string[si:].index(",")
                    except ValueError:
                        ei = new_hp_string[si:].index(")")
                    new_hp_string = (
                        new_hp_string[: si + ei] + '"' + new_hp_string[si + ei :]
                    )
            # Assemble new source code
            new_source_code += orig_source_code[parsed:start] + new_hp_string
            parsed = end + 1
        # Append last portion
        new_source_code += orig_source_code[parsed:]
        # Reformat source code with black
        formatted_source_code = format_str(new_source_code, mode=FileMode())
        # Update instance source code
        self.source_code = formatted_source_code
        return self.source_code


class TrainingDefinition(HyperparameterDefinition):
    def __init__(self, function, name, code_editor):
        super().__init__(function, name, code_editor)
        self._phases = self.default_results["count"]
        self.collection = "training"
        # TODO: update dict

    @property
    def phases(self):
        return self._phases

    @phases.setter
    def phases(self, iterable):
        if len(iterable) < max(iterable):  # Need more epochs than phases
            phases = sorted(set(iterable))
            # phases[-1] = self.num_epochs  # Ensure number of epochs remain constant
            self._phases = phases

    @property
    def num_epochs(self):
        return max(self.phases)

    @num_epochs.setter
    def num_epochs(self, value):
        if value > self.num_phases:  # Need more epochs than phases
            # Remove invalidated phases and adjust final value
            phases = deepcopy(self.phases)
            phases[-1] = value
            phases = [p for p in phases if p <= value]
            self.phases = phases
        else:
            raise ValueError(
                "Number of epochs must be larger than"
                + f" number of phases ({self.num_phases:d})"
                + f". Received value: {value:d}"
            )

    @property
    def num_phases(self):
        return len(self.phases)

    @num_phases.setter
    def num_phases(self, value):
        # TODO: max value (delta becomes zero eventually)
        if 0 < value < self.num_epochs:
            phases = deepcopy(self.phases)
            added_phases = value - len(phases)
            if added_phases > 0:
                # Add equidistant phases between last and second to last phase
                x1 = phases[-1]
                try:
                    x0 = phases[-2]
                except IndexError:
                    x0 = 0
                delta = (x1 - x0) // (added_phases + 1) + 1
                phases = phases[:-1] + list(range(x0 + delta, x1, delta)) + phases[-1:]
            elif added_phases < 0:
                # Remove phases between last and second to last phase
                phases = phases[: (added_phases - 1)] + phases[-1:]
            self.phases = phases
        else:
            raise ValueError(
                "Number of phases must be between 0"
                + f" and number of epochs ({self.num_epochs:d})"
                + f". Received value: {value:d}"
            )

    def phases_to_html(self, app):
        # Elements
        range_phases = dcc.RangeSlider(
            id=ID.range_td_phases,
            value=[0] + self.phases,
            min=0,
            max=self.num_epochs,
            allowCross=False,
            updatemode="drag",
            marks=None,
            step=1,
            tooltip={"always_visible": True, "placement": "top"},
        )
        input_num_epochs = dcc.Input(
            id=ID.input_td_num_epochs,
            type="number",
            min=1,
            step=1,
            value=self.num_epochs,
            required=True,
        )
        label_num_epochs = html.Label("Number of epochs")
        input_num_phases = dcc.Input(
            id=ID.input_td_num_phases,
            type="number",
            min=1,
            step=1,
            value=self.num_phases,
            required=True,
        )
        label_num_phases = html.Label("Number of phases")
        # Panel and layout
        panel = html.Div(
            className="col-12 p-3 justify-content-between, align-items-start",
            children=[
                html.Div(className="col-12", children=[range_phases]),
                html.Div(
                    className="row justify-content-between, align-items-center",
                    children=[
                        html.Div(className="col-2", children=[label_num_phases]),
                        html.Div(className="col-3", children=[input_num_phases]),
                        html.Span(className="col-2"),
                        html.Div(className="col-2", children=[label_num_epochs]),
                        html.Div(className="col-3", children=[input_num_epochs]),
                    ],
                ),
            ],
        )

        # Callbacks
        @app.callback(
            Output(ID.range_td_phases, "value"),
            Output(ID.range_td_phases, "max"),
            Output(ID.input_td_num_phases, "value"),
            Output(ID.input_td_num_epochs, "value"),
            Output(self.code_editor, "value"),
            Input(ID.range_td_phases, "value"),
            Input(ID.input_td_num_phases, "value"),
            Input(ID.input_td_num_epochs, "value"),
            prevent_initial_call=True,
        )
        def update_phases(phases, num_phases, num_epochs):

            triggered = list(callback_context.triggered_prop_ids.values())[0]

            # Reapply triggered change due to interdependence (last property dominates)
            if triggered == ID.range_td_phases:
                self.phases = phases[1:]

            if triggered == ID.input_td_num_phases:
                self.num_phases = num_phases

            if triggered == ID.input_td_num_epochs:
                self.num_epochs = num_epochs

            source_code = self.update_source_code()

            return (
                [0] + self.phases,
                self.num_epochs,
                self.num_phases,
                self.num_epochs,
                source_code,
            )

        return panel


class ModelDefinition(HyperparameterDefinition):
    def __init__(self, function, name, code_editor, data_definition):
        super().__init__(function, name, code_editor, data_definition)
        self.collection = "model"
        self.data_definition = data_definition
        self._temporary_feature_options = []

    @property
    def input_features(self):
        return self.data_definition.input_features

    @property
    def output_features(self):
        return self.data_definition.output_features

    @property
    def persistent_feature_options(self):
        return [{"label": k, "value": k} for k in self.data_definition.features.keys()]

    @property
    def feature_options(self):
        return self.persistent_feature_options + self._temporary_feature_options

    def add_feature(self, name):
        self._temporary_feature_options.append({"label": name, "value": name})

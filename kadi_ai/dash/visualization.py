# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Visualization elements, graphs, and plots for KadiAI dashboards. Part of KadiAI."""
import json
from pathlib import Path

import hiplot
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objs as go
import plotly.io as pio
from dash import dash_table
from dash import dcc
from dash import html
from dash.exceptions import PreventUpdate
from dash_extensions.enrich import Input
from dash_extensions.enrich import Output
from plotly.subplots import make_subplots

from .constants import DataDefinitinTooltips as DDT
from .constants import DataDefinitionColumns as DDC
from .constants import DataDefinitionFormats as DDF
from .constants import WebAppIdentifiers as ID
from cids.data.definition import DataDefinition


pio.templates.default = "simple_white"


def _matplotlib_to_plotly(cmap, pl_entries):
    h = 1.0 / (pl_entries - 1)
    pl_colorscale = []
    for k in range(pl_entries):
        C = list(map(np.uint8, np.array(cmap(k * h)[:3]) * 255))
        pl_colorscale.append([k * h, "rgb" + str((C[0], C[1], C[2]))])
    return pl_colorscale


# COLORMAP = _matplotlib_to_plotly(sns.color_palette("mako", as_cmap=True), 255)
# COLORMAP = "dense_r"
COLORMAP = "Plasma"


def _wrap_datatable(datatable):
    return html.Div(children=[datatable])


def before_datatable_panel_builder(app):
    # Extract data
    empty_data_definition = DataDefinition()
    empty_data_definition.samples = []
    data_definition_df = empty_data_definition.to_dataframe()
    data_definition_df_columns = [
        {"name": i, "id": i} for i in data_definition_df.columns
    ]
    # Elements and panel
    element = dash_table.DataTable(
        id=ID.table_before_datadefinition,
        columns=data_definition_df_columns,
        data=None,
        editable=False,
        row_deletable=False,
        sort_action="native",
        filter_action="native",
        cell_selectable=False,
        tooltip_header=DDT.tooltips,
        style_as_list_view=True,
        # style_data_conditional=validate.validate(data_definition_df),
        style_data_conditional=[
            {
                "if": {"column_editable": True},
                "backgroundColor": "#0FA0CE",
            }
        ],
        style_header_conditional=[
            {
                "if": {"column_editable": True},
                "backgroundColor": "#0FA0CE",
            }
        ],
    )
    panel = _wrap_datatable(element)

    return panel


def after_datatable_panel_builder(app):
    # Extract data
    empty_data_definition = DataDefinition()
    empty_data_definition.samples = []
    data_definition_df = empty_data_definition.to_dataframe()
    data_definition_df_columns = [
        {"name": i, "id": i} for i in data_definition_df.columns
    ]
    # Elements and panel
    element = dash_table.DataTable(
        id=ID.table_after_datadefinition,
        columns=data_definition_df_columns,
        data=None,
        editable=False,
        row_deletable=False,
        sort_action="native",
        filter_action="native",
        cell_selectable=False,
        row_selectable="multi",
        selected_rows=[],
        # style_data_conditional=validate.validate(data_definition_df),
        tooltip_header=DDT.tooltips,
        style_as_list_view=True,
    )
    panel = _wrap_datatable(element)
    return panel


def visualization_panel_builder(app):
    empty_data_definition = DataDefinition()
    empty_data_definition.samples = []
    data_definition_df = empty_data_definition.to_dataframe()
    # Panel
    # TODO: Reactivate when issue is fixed:
    #   https://github.com/plotly/dash-core-components/issues/922
    # elements = [
    #     dcc.Tabs(
    #         [
    #           dcc.Tab(label="Statistics", children=feature_statistics(df, samples)),
    #           dcc.Tab(label="Visualize", children=feature_visualization(df, samples)),
    #         ]
    #     )
    # ]
    panel = html.Div(id=ID.panel_after_visualization, children=[])

    # Callbacks
    @app.callback(
        Output(ID.panel_after_visualization, "children"),
        Input(ID.table_after_datadefinition, "derived_virtual_data"),
        Input(ID.table_after_datadefinition, "derived_virtual_selected_rows"),
    )
    def update_visualized_features(rows, derived_virtual_selected_rows):
        # Title
        children = [html.H3("Selected feature visualization and statistics")]

        # When the table is first rendered, `derived_virtual_data` and
        # `derived_virtual_selected_rows` will be `None`. This is due to an
        # idiosyncrasy in Dash (unsupplied properties are always None and Dash
        # calls the dependent callbacks when the component is first rendered).
        # So, if `rows` is `None`, then the component was just rendered
        # and its value will be the same as the component's dataframe.
        # Instead of setting `None` in here, you could also set
        # `derived_virtual_data=df.to_rows('dict')` when you initialize
        # the component.
        if not derived_virtual_selected_rows:
            return children

        # Get current selected features
        if rows is None:
            current_data_definition_df = data_definition_df
        else:
            current_data_definition_df = pd.DataFrame(rows)
        selected_data_definition_df = current_data_definition_df.iloc[
            derived_virtual_selected_rows
        ]
        # Plot visualization
        children.extend(
            feature_visualization(selected_data_definition_df, app._samples)
        )
        return children

    return panel


def statistics_violin(name, data, data_format, max_points=10000):
    data = np.asarray(data)
    F = data.shape[data_format.index("F")]
    colors = list(
        reversed(px.colors.sample_colorscale(COLORMAP, np.linspace(0.0, 1.0, F)))
    )
    # Figure
    fig = go.Figure()
    for f in range(F):
        data_points = np.ravel(data[..., f])
        if len(data_points) > max_points:
            data_points = np.random.choice(data_points, max_points, replace=False)
        plot = go.Violin(
            x=data_points,
            name=f"{name}_F{f:d}",
            line_color=colors[f],
        )
        fig.add_trace(plot)
    fig.update_traces(
        orientation="h",
        side="positive",
        width=1.0,
        box_visible=True,
        meanline_visible=True,
        points="all",
        pointpos=-0.2,
        jitter=0.05,
    )
    fig.update_layout(violingap=1, violingroupgap=1)
    fig.update_xaxes(title_text=name)
    # Layout
    fig.update_layout(
        margin=dict(l=20, r=20, t=30, b=20),
        height=300,
    )
    return fig


def visualize_nxyf(name, data, data_format):
    data = np.asarray(data)
    N = data.shape[data_format.index("N")]
    F = data.shape[data_format.index("F")]
    if F == 1:
        color_continuous_scale = COLORMAP
        binary_string = False
    else:
        color_continuous_scale = None
        binary_string = True
    # Figure
    fig = px.imshow(
        np.squeeze(
            np.transpose(
                data,
                (
                    data_format.index("N"),
                    data_format.index("Y"),
                    data_format.index("X"),
                    data_format.index("F"),
                ),
            )
        ),
        facet_col=0,
        binary_string=binary_string,
        labels=dict(facet_col="sample"),
        color_continuous_scale=color_continuous_scale,
        height=300,
    )
    # Titles, labels, and annotations
    for i in range(N):
        fig.layout.annotations[i]["text"] = f"sample {i:d}"
    fig.update_xaxes(title_text="X")
    fig.update_yaxes(title_text="Y")
    # Layout
    fig.update_layout(
        margin=dict(l=20, r=20, t=30, b=20),
        height=300,
        coloraxis_colorbar=dict(title=name),
        # title={"yref": "paper", "y": 1, "yanchor": "bottom"},
        # annotations={"yref": "paper", "y": 0.95, "yanchor": "bottom"},
    )
    return fig


def visualize_nf(name, data):
    elements = [
        html.Div(
            className="row",
            children=[
                html.Label(f"sample {i:d}:", className="col-2"),
                html.Label(str(datum), className="col-10"),
            ],
        )
        for i, datum in enumerate(data)
    ]
    return html.Div(
        className="row",
        children=[
            html.Div(className="col-2", children=[html.H5(name)]),
            html.Div(className="col-10", children=elements),
        ],
    )


def _get_row_col(i, max=False):
    num = i + 1
    max_col = 5
    row = int(np.ceil(num / max_col))
    col = int(num - (row - 1) * max_col)
    if max and row > 1:
        return row, max_col
    return row, col


def visualize_nsf(name, data, data_format):
    data = np.asarray(data)
    N = data.shape[data_format.index("N")]
    # Figure
    rows, cols = _get_row_col(N - 1, max=True)
    subplot_titles = [f"sample {n:d}" for n in range(N)]
    fig = make_subplots(
        rows=rows,
        cols=cols,
        row_heights=[200] * rows,
        subplot_titles=subplot_titles,
        # shared_xaxes=True,
        # shared_yaxes=True,
        x_title="S",
        y_title=name,
    )
    for n, sample_data in enumerate(data):
        # Subfigure
        subfig = go.Scatter(
            y=np.squeeze(sample_data), mode="lines", name=f"sample {n:d}"
        )
        # Add to figure
        r, c = _get_row_col(n)
        fig.add_trace(subfig, row=r, col=c)
    # Layout
    fig.update_layout(
        margin=dict(l=20, r=20, t=30, b=20),
        height=300,
        # annotations={"yref": "paper", "y": 0.95, "yanchor": "bottom"},
    )
    return fig


def wrap_feature_figure(figs, name):
    c = [html.H5(name)] + [dcc.Graph(figure=f) for f in figs] + [html.Hr()]
    panel = html.Div(children=c)
    return panel


def feature_visualization(data_definition_df, samples):
    elements = []
    for _, feature_row in data_definition_df.iterrows():
        feature_type = feature_row[DDC.type]
        feature_decode_type = feature_row[DDC.decode_type]
        feature_format = feature_row[DDC.format]
        feature_name = feature_row[DDC.name]
        feature_data = samples[feature_name]
        if feature_decode_type != "None":
            # Numerical data
            figs = []
            # 1D Sequence
            if feature_format == DDF.NSF:
                figs.append(visualize_nsf(feature_name, feature_data, feature_format))
            # 2D image
            if feature_format == DDF.NXYF:  # 2D, Images
                figs.append(visualize_nxyf(feature_name, feature_data, feature_format))
            # # Scalar datapoint
            # if feature_format == DDF.NF:
            #     return html.Div([html.H2("NF")])
            # # 2D, video
            # if feature_format == DDF.NSXYF:
            #     return html.Div([html.H2("NSXYF")])
            # # Frequenzbereich
            # if feature_format == DDF.NXF:
            #     return html.Div([html.H2("NXF")])
            # # 3D
            # if feature_format == DDF.NXYZF:
            #     return html.Div([html.H2("NXYZF")])
            # Violin plot
            figs.append(statistics_violin(feature_name, feature_data, feature_format))
            # Append elements
            elements.append(wrap_feature_figure(figs, feature_name))
        else:
            if feature_type == "string":
                element = visualize_nf(feature_name, samples[feature_name])
            # Append elemnents
            elements.append(element)
    return elements


def _build_trial_dictionary(trial_json_to_build, hyperparameters):
    # Filter hyperparameters
    if hyperparameters:
        trial_dict = {
            k: v
            for k, v in trial_json_to_build["hyperparameters"]["values"].items()
            if k in hyperparameters
        }
    else:
        trial_dict = {
            k: v
            for k, v in trial_json_to_build["hyperparameters"]["values"].items()
            if "tuner/" not in k
        }
    # Add id and score
    trial_dict["trial_id"] = trial_json_to_build["trial_id"]
    trial_dict["score"] = trial_json_to_build["score"]
    return trial_dict


def _parse_search_data(search_dir, search_parameters=""):
    search_dir = Path(search_dir)
    # Search for trials
    trial_path_list = list(search_dir.glob("**/trial.json"))
    # Read trial.json files
    trial_list = []
    for trial in trial_path_list:
        with open(trial, encoding="utf8") as trial_json:
            trial_list.append(json.load(trial_json))
    # Extract relevant search parameters from trials
    search_parameters = [s.strip() for s in search_parameters.split(",")]
    # Gather search parameters
    trials = [_build_trial_dictionary(trial, search_parameters) for trial in trial_list]
    # Provide empty placeholder
    if not trials:
        trials = [{"uid": 0, "score": 0.0}]
    return trials


def _update_hiplot_html(search_df):
    search_results = search_df.to_dict(orient="records")
    hiplot_experiment = hiplot.Experiment.from_iterable(search_results)
    return hiplot_experiment.to_html()


def hiplot_panel_builder(app, project, search_df, **kwargs):
    # Elements
    hiplot_html = _update_hiplot_html(search_df)
    iframe = html.Iframe(
        id=ID.iframe_hiplot,
        srcDoc=hiplot_html,
        style={"height": "80vh", "width": "100%"},
    )
    interval_hiplot_refresh = dcc.Interval(
        id=ID.interval_refresh_hiplot,
        interval=60000,  # in milliseconds
        n_intervals=0,
    )
    checklist_refresh = dcc.Checklist(
        id=ID.checklist_hiplot_refresh,
        options=[
            {
                "label": (
                    "Automatic refresh HiPlot (Warning! "
                    + "Refresh deletes interaction)"
                ),
                "value": "refresh",
            }
        ],
    )
    # Panel and layout
    panel = html.Div(
        className="col",
        children=[
            iframe,
            html.Div(
                className="row", children=[interval_hiplot_refresh, checklist_refresh]
            ),
        ],
    )

    # Callbacks
    @app.callback(
        Output(ID.iframe_hiplot, "srcDoc"),
        Input(ID.interval_refresh_hiplot, "n_intervals"),
        Input(ID.checklist_hiplot_refresh, "value"),
    )
    def refresh_hiplot(n_intervals, refresh_value):
        if not refresh_value:
            raise PreventUpdate()
        search_results = _parse_search_data(
            project.result_dir,
            search_parameters=kwargs.get("search_parameters", ""),
        )
        search_df = pd.DataFrame(search_results)
        return _update_hiplot_html(search_df)

    return panel


def _extract_plot_dimensions(search_df):
    dimensions_list = []
    for column in search_df:
        if search_df[column].dtype == "object":
            categorized_string_values = search_df[column].astype("category")
            encoded_string_values = categorized_string_values.cat.codes
            sorted_codes = list(set(encoded_string_values))
            sorted_codes.sort()
            sorted_list = list(set(search_df[column].values))
            sorted_list.sort()
            dimensions_list.append(
                {
                    "tickvals": sorted_codes,
                    "label": column,
                    "values": encoded_string_values,
                    "ticktext": sorted_list,
                }
            )
        else:
            dimensions_list.append({"label": column, "values": search_df[column]})
    return dimensions_list


def _update_dashplot_parallelplot(search_df):
    dimensions = _extract_plot_dimensions(search_df)
    fig = go.Figure(
        data=go.Parcoords(
            line=dict(
                color=search_df["score"],
                colorscale=[
                    [0, "blue"],
                    [0.25, "purple"],
                    [0.5, "red"],
                    [0.75, "orange"],
                    [1, "gold"],
                ],
            ),
            dimensions=dimensions,
        )
    )
    fig.update_layout(plot_bgcolor="white", paper_bgcolor="white")
    return fig


def dashplot_panel_builder(app, project, search_df, **kwargs):
    # Elements
    fig = _update_dashplot_parallelplot(search_df)
    graph = dcc.Graph(
        id=ID.graph_dashplot_parallelplot,
        figure=fig,
        style={"height": "50vh", "width": "100%"},
    )
    data_table = dash_table.DataTable(
        id=ID.table_dashplot_configurations,
        columns=[{"name": i, "id": i} for i in search_df.columns],
        data=search_df.to_dict("records"),
        filter_action="native",
        sort_action="native",
        sort_mode="multi",
        page_action="native",
        page_current=0,
        page_size=10,
    )
    interval_dashplot_refresh = dcc.Interval(
        id=ID.interval_refresh_dashplot,
        interval=60000,  # in milliseconds
        n_intervals=0,
    )
    checklist_refresh = dcc.Checklist(
        id=ID.checklist_dashplot_refresh,
        options=[{"label": "Automatic refresh Plotly", "value": "refresh"}],
    )
    # Panel and layout
    panel = html.Div(
        id=ID.panel_dashplot,
        className="col",
        children=[
            graph,
            data_table,
            html.Div(
                className="row", children=[interval_dashplot_refresh, checklist_refresh]
            ),
        ],
    )

    @app.callback(
        Output(ID.table_dashplot_configurations, "data"),
        Input(ID.interval_refresh_dashplot, "n_intervals"),
        Input(ID.checklist_dashplot_refresh, "value"),
    )
    def refresh_plotly(n_intervals, value):
        if not value:
            raise PreventUpdate()
        new_search_results = _parse_search_data(
            project.result_dir,
            search_parameters=kwargs.get("search_parameters", ""),
        )
        new_search_df = pd.DataFrame(new_search_results)
        return new_search_df.to_dict(orient="records")

    @app.callback(
        Output(ID.graph_dashplot_parallelplot, "figure"),
        Input(ID.table_dashplot_configurations, "derived_virtual_data"),
    )
    def filter_parallelcoords(rows):
        filtered_results = (
            search_df.to_dict(orient="records") if rows is None else pd.DataFrame(rows)
        )
        fig = _update_dashplot_parallelplot(filtered_results)
        fig.update_layout(uirevision="constant")
        return fig

    return panel


def tensorboard_panel_builder():
    # Elements
    iframe = html.Iframe(
        id=ID.iframe_tensorboard,
        src="http://127.0.0.1:3334/",
        style={"height": "80vh", "width": "100%"},
    )
    # Panel and layout
    panel = html.Div(children=[iframe])
    return panel

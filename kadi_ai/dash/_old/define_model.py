import base64
from copy import deepcopy
from pathlib import Path

import dash
import dash_editor_components as dec
from dash import dcc
from dash import html
from dash.dependencies import State
from dash.exceptions import PreventUpdate
from dash_extensions.enrich import DashProxy as Dash
from dash_extensions.enrich import Input
from dash_extensions.enrich import MultiplexerTransform
from dash_extensions.enrich import Output

from ..utility import _setup_model
from ..utility import _setup_project
from .dash_definitions import local_annotation
from .dash_definitions import ModelDefinition
from .dash_functions import contains_label
from .dash_functions import find_functions
from .dash_functions import is_valid_feature_slice
from .dash_functions import shutdown
from .dash_functions import titlebar
from cids.tensorflow import model_functions
from cids.tensorflow.model import CIDSModel

try:
    from staticfg import CFGBuilder
except ImportError as e:
    print(
        "WARNING: staticfg is not available, therefore there will be no AST-Visualisation."
    )
    print(e)


def layout(model_definition, model_function_choices, code_editor_identifier, **kwargs):
    # Assemble
    currentHashOfPythonCode: int

    astDisabled = False
    try:
        CFGBuilder
    except:
        astDisabled = True

    elements = [
        titlebar("Model Definition"),
        html.Div(
            [
                # First column
                html.Div(
                    [
                        # First column: user selection panel
                        html.Div(
                            [
                                html.Label(
                                    "Input features", style={"display": "inline-block"}
                                ),
                                html.Label(
                                    "*",
                                    style={
                                        "font-style": "italic",
                                        "font-size": "small",
                                        "display": "inline-block",
                                    },
                                ),
                                html.Br(),
                                dcc.Dropdown(
                                    id="model-definition-input-features",
                                    multi=True,
                                    options=deepcopy(model_definition.feature_options),
                                ),
                                html.Br(),
                                html.Label(
                                    "Output features", style={"display": "inline-block"}
                                ),
                                html.Label(
                                    "*",
                                    style={
                                        "font-style": "italic",
                                        "font-size": "small",
                                        "display": "inline-block",
                                    },
                                ),
                                dcc.Dropdown(
                                    id="model-definition-output-features",
                                    multi=True,
                                    options=deepcopy(model_definition.feature_options),
                                ),
                                html.Br(),
                                html.Label(
                                    "*Select feature dimensions by indexing: "
                                    + "e.g. [1] or [0,1] ",
                                    style={
                                        "font-style": "italic",
                                        "font-size": "small",
                                    },
                                ),
                            ],
                            style={
                                "border": "solid",
                                "border-width": "thin",
                                "border-color": "#D3D3D3",
                                "border-radius": "5px",
                                "padding": "10px",
                            },
                        ),
                        html.Br(),
                        # First column: hyperparameter panel
                        html.Div(
                            [
                                html.H3("Hyperparameters"),
                                html.Div(id="model-definition-hyperparameters"),
                            ],
                            style={
                                "border": "solid",
                                "border-width": "thin",
                                "border-color": "#D3D3D3",
                                "border-radius": "5px",
                                "padding": "10px",
                            },
                        ),
                        html.Br(),
                        # First column: model methods selection
                        html.Div(
                            [
                                html.Label(
                                    "Model methods", style={"display": "inline-block"}
                                ),
                                html.Br(),
                                dcc.Dropdown(
                                    id="model-definition-model-methods",
                                    options=deepcopy(model_definition.model_settings),
                                ),
                            ],
                            style={
                                "border": "solid",
                                "border-width": "thin",
                                "border-color": "#D3D3D3",
                                "border-radius": "5px",
                                "padding": "10px",
                            },
                        ),
                        html.Br(),
                        html.Div(
                            [
                                html.Label(
                                    "Model method parameters",
                                    style={"display": "inline-block"},
                                ),
                                html.Div(id="model-method-parameters"),
                            ],
                            style={
                                "border": "solid",
                                "border-width": "thin",
                                "border-color": "#D3D3D3",
                                "border-radius": "5px",
                                "padding": "10px",
                            },
                        ),
                        html.Br(),
                    ],
                    className="four columns",
                ),
                # Second column
                html.Div(
                    [
                        html.Div(
                            [
                                dcc.Dropdown(
                                    id="model-definition-select",
                                    options=model_function_choices,
                                    value=model_definition.name,
                                    className="nine columns",
                                ),
                                html.Button(
                                    "Save & Exit",
                                    id="model-definition-save",
                                    n_clicks=0,
                                    className="three columns",
                                ),
                            ],
                            className="twelve columns",
                        ),
                        html.Div([html.Br()], className="twelve columns"),
                        html.Div(
                            [
                                dcc.Tabs(
                                    id="model-tabs-code-or-ast",
                                    value="tab-1-code",
                                    children=[
                                        dcc.Tab(label="Code", value="tab-1-code"),
                                        dcc.Tab(
                                            label="AST-Visualisation",
                                            value="tab-2-ast",
                                            disabled=astDisabled,
                                        ),
                                    ],
                                ),
                            ],
                            className="twelve columns",
                        ),
                        html.Div(
                            [
                                html.A(
                                    [
                                        html.Img(
                                            id="model-ast-figure",
                                            alt="Loading...",
                                            style={
                                                "width": "100%",
                                                "border": "solid",
                                                "border-width": "thin",
                                                "border-color": "#D3D3D3",
                                                "box-sizing": "border-box",
                                                "cursor": "zoom-in",
                                            },
                                        )
                                    ],
                                    id="model-ast-figure-link",
                                    target="_blank",
                                ),
                            ],
                            style={
                                "display": "none",
                            },
                            id="model-ast-tab",
                            className="twelve columns",
                        ),
                        html.Div(
                            [
                                html.Div(
                                    [
                                        dec.PythonEditor(
                                            id=code_editor_identifier,
                                            placeholder="",
                                            autoFocus=True,
                                            readOnly=True,
                                            style={
                                                "border": "solid",
                                                "border-width": "thin",
                                                "border-color": "#D3D3D3",
                                            },
                                        ),
                                        html.I(
                                            id="model-definition-edit",
                                            className="fa fa-lock fa-lg",
                                            style={
                                                "color": "gray",
                                                "position": "absolute",
                                                "top": "5px",
                                                "right": "5px",
                                                "padding": "5px",
                                            },
                                        ),
                                    ],
                                    style={"position": "relative"},
                                ),
                            ],
                            id="model-code-editor-tab",
                            className="twelve columns",
                        ),
                    ],
                    className="six columns",
                ),
                # Third column
                html.Div(
                    [
                        html.Div(
                            [
                                html.A(
                                    [
                                        html.Button(
                                            [
                                                html.I(className="fa fa-download"),
                                                html.Span(
                                                    style={
                                                        "display": "inline-block",
                                                        "width": "3px",
                                                    }
                                                ),
                                                html.Span("Save image as..."),
                                            ],
                                        )
                                    ],
                                    id="model-definition-download",
                                    download="plotted_model.png",
                                    style={"text-decoration": "none"},
                                ),
                            ],
                            style={"text-align": "right"},
                            className="twelve columns",
                        ),
                        html.Div([html.Br()], className="twelve columns"),
                        html.Div(
                            [
                                html.Img(
                                    id="model-definition-figure",
                                    alt="Select input / output features to plot model.",
                                    style={
                                        "display": "block",
                                        "max-width": "100%",
                                        "width": "100%",
                                        "border": "solid",
                                        "border-width": "thin",
                                        "border-color": "#D3D3D3",
                                        "padding": "10px",
                                    },
                                ),
                            ],
                            className="twelve columns",
                        ),
                    ],
                    className="two columns",
                ),
            ],
            className="twelve columns",
        ),
    ]
    return html.Div(elements)


def setup_dash_define_model(**kwargs):
    # Setup project
    project = _setup_project(**kwargs)

    # Pull from Kadi
    project.pull(tfrecords=True, data_definition=True)

    # Get data definition and setup model
    data_definition = project.data_definition
    # # TODO: Something about features???
    feature_keys = data_definition.features.keys()
    if not data_definition.input_features:
        data_definition.input_features = list(feature_keys)[:-1]
    if not data_definition.output_features:
        data_definition.output_features = list(feature_keys)[-1:]

    # TODO: select CIDSModel classmethods
    cids_model = _setup_model(project, data_definition, model_settings=None, **kwargs)

    # Get model functions
    model_functions, model_function_choices = find_functions(
        predefined_model_functions,
        file=Path(cids_model.base_model_dir) / "model_function.py",
    )

    # Create app
    external_stylesheets = [
        "https://codepen.io/chriddyp/pen/bWLwgP.css",
        "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",  # pylint: disable=line-too-long
    ]
    app = Dash(
        __name__,
        title="Model Definition",
        external_stylesheets=external_stylesheets,
        transforms=[MultiplexerTransform()],
    )
    code_editor_identifier = "model-definition-code-editor"

    # Gather callbacks for all possible elements to the app
    app_model_definitions = {}
    app_callbacks = {}
    app_panels = {}
    app_method_parameters = {}
    app_parameter_identifiers = {}
    for mfc in model_function_choices:
        name = mfc["value"]
        model_definition = ModelDefinition(
            model_functions[name],
            name,
            code_editor_identifier,
            data_definition,
        )
        hp_panel, hp_callbacks = model_definition.hyperparameters_to_html(app)
        app_model_definitions[name] = model_definition
        app_callbacks[name] = hp_callbacks
        app_panels[name] = hp_panel

    # Select first model function and model method
    name = model_function_choices[0]["value"]
    method_name = model_definition.model_settings[0]["value"]
    model_definition = ModelDefinition(
        model_functions[name],
        name,
        code_editor_identifier,
        data_definition,
    )

    model_methods = model_definition.model_settings
    parameter_target_identifier = "model-method-parameters"
    for method_name in model_methods:
        method = getattr(CIDSModel, method_name["value"])
        method_vars = local_annotation(method)
        if method_vars:
            (
                parameter_panel,
                identifiers,
                param_callbacks,
            ) = model_definition.method_parameters_to_html(
                app, method_vars, parameter_target_identifier
            )
            app_callbacks[method_name["value"]] = param_callbacks
        else:
            parameter_panel = html_element = html.Div(
                [
                    dcc.Textarea(
                        id="No_method_parameter",
                        value="No input parameters for this method",
                        style={"width": "100%", "height": 70},
                    ),
                    html.Div(id="No-input-parameter", style={"whiteSpace": "pre-line"}),
                ]
            )
            identifiers = "method-parameters-noinput"
        app_method_parameters[method_name["value"]] = parameter_panel
        app_parameter_identifiers[method_name["value"]] = identifiers

    # Ensure nothing gets deleted
    app._my_selected = name
    app._my_app_definitions = app_model_definitions
    app._my_app_callbacks = app_callbacks
    app._my_app_panels = app_panels
    app._my_methods_selected = method_name
    app._my_app_parameters_panels = app_method_parameters
    app._my_parameter_identifiers = app_parameter_identifiers
    # Create layout
    app.layout = layout(
        model_definition, model_function_choices, code_editor_identifier, **kwargs
    )

    app.currentHashOfPythonCode = -1

    @app.callback(
        Input("model-tabs-code-or-ast", "value"),
        Input("model-definition-select", "value"),
        Input("model-ast-figure", "src"),
        Output("model-code-editor-tab", "style"),
        Output("model-ast-tab", "style"),
        Output("model-ast-figure", "src"),
        Output("model-tabs-code-or-ast", "value"),
        Output("model-ast-figure-link", "href"),
    )
    def select_tab(tab, select, astImage):
        ctx = dash.callback_context
        triggered = ctx.triggered[0]["prop_id"].split(".")[0]
        image_source = astImage
        tabValue = tab

        if tab == "tab-1-code" or triggered == "model-definition-select":
            tabValue = "tab-1-code"
            editor_style = {
                "display": "block",
            }
            ast_style = {"display": "none"}

        elif tab == "tab-2-ast":
            project.to_json()
            pyCode = app._my_app_definitions[select].to_py()

            if app.currentHashOfPythonCode != hash(pyCode):
                app.currentHashOfPythonCode = hash(pyCode)
                cfg = CFGBuilder().build_from_src("model_function", pyCode)
                cfg.build_visual(
                    Path(cids_model.plot_dir) / "model_image",
                    format="png",
                    calls=True,
                    show=False,
                )

                with open(Path(cids_model.plot_dir) / "model_image.png", "rb") as f:
                    bytestring = base64.b64encode(f.read()).decode("utf8")
                image_source = f"data:image/png;base64,{bytestring}"

            editor_style = {
                "display": "none",
            }
            ast_style = {
                "display": "block",
            }

        return (editor_style, ast_style, image_source, tabValue, image_source)

    @app.callback(
        Output(code_editor_identifier, "value"),
        Output("model-definition-hyperparameters", "children"),
        Input("model-definition-select", "value"),
        Input("model-definition-save", "n_clicks"),
    )
    def update_model_definition_function(select, save):
        ctx = dash.callback_context
        triggered = ctx.triggered[0]["prop_id"].split(".")[0]

        if triggered == "model-definition-select":
            app._my_selected = select

        if triggered == "model-definition-save":
            project.to_json()
            app._my_app_definitions[select].to_py(
                file=Path(cids_model.base_model_dir) / "model_function.py"
            )
            # TODO: push project to Kadi
            shutdown()

        return (
            app._my_app_definitions[select].to_py(),
            app._my_app_panels[select],
        )

    @app.callback(
        Output("model-method-parameters", "children"),
        Input("model-definition-model-methods", "value"),
    )
    def update_model_parameter_panel(methods):
        if methods:
            ctx = dash.callback_context
            triggered = ctx.triggered[0]["prop_id"].split(".")[0]

            if triggered == "model-definition-model-methods":
                app._my_methods_selected = methods

            return (app._my_app_parameters_panels[methods],)

    @app.callback(
        Output("model-definition-figure", "src"),
        Output("model-definition-download", "href"),
        Input("model-definition-input-features", "value"),
        Input("model-definition-output-features", "value"),
        Input("model-definition-model-methods", "value"),
        Input("model-definition-select", "value"),
        Input("model-method-parameters", "value"),
    )
    def update_image_src(
        input_features, output_features, model_methods, select, method_parameter
    ):
        parameter_dict = {}
        image_source = ""
        if method_parameter is not None:
            parameter_name = app._my_parameter_identifiers[
                app._my_methods_selected
            ].split("-")[-1]
            parameter_dict = {parameter_name: method_parameter}
        if input_features is not None and output_features is not None:

            # Assign input and output features

            cids_model = _setup_model(
                project,
                data_definition,
                model_methods,
                model=app._my_app_definitions[select].function,
                **parameter_dict,
            )
            cids_model.data_definition.input_features = input_features
            cids_model.data_definition.output_features = output_features
            # Update cids_model function
            hp = app._my_app_definitions[select].hp
            # IMPORTANT!!! activate default values!
            # TODO(Arnd): There must be a more elegant way
            hp.values = {k: v[0]._default for k, v in hp._hps.items()}

            cids_model.build(hp=hp)
            json_dict = project.to_json(
                run_params={
                    "model_method": model_methods,
                    "method_parameter": parameter_dict,
                },
                write_data_definition=True,
            )
            # Plot model to file(s)
            cids_model.plot_models()

            plotted_models = list(Path(cids_model.plot_dir).glob("*subcore*.png"))
            plotted_models = plotted_models or [Path(cids_model.plot_dir) / "model.png"]

            # Clean-up
            cids_model.clear()

            # TODO: multi-models
            with plotted_models[0].open("rb") as f:
                bytestring = base64.b64encode(f.read()).decode("utf8")
            image_source = f"data:image/png;base64,{bytestring}"
        return image_source, image_source

    @app.callback(
        Output(code_editor_identifier, "readOnly"),
        Output("model-definition-edit", "className"),
        Output(code_editor_identifier, "style"),
        Input("model-definition-edit", "n_clicks"),
    )
    def update_edit_mode(n_clicks):
        editor_edit_mode_style = {
            "border": "solid",
            "border-width": "thin",
            "border-color": "#C3C3D3",
            "background-color": "#FFFFFF",
        }
        editor_lock_mode_style = {
            "border": "solid",
            "border-width": "thin",
            "border-color": "#D3D3D3",
            "background-color": "#F5F5F5",
        }
        if n_clicks is None or n_clicks % 2 == 0:
            return True, "fa fa-lock fa-lg", editor_lock_mode_style
        return False, "fa fa-code fa-lg", editor_edit_mode_style

    @app.callback(
        Output("model-definition-input-features", "options"),
        Output("model-definition-output-features", "options"),
        Input("model-definition-input-features", "search_value"),
        Input("model-definition-output-features", "search_value"),
    )
    def update_feature_options(input_search_value, output_search_value):
        search_value = input_search_value or output_search_value
        if not search_value:
            raise PreventUpdate()
        model_definition = app._my_app_definitions[app._my_selected]
        data_definition = model_definition.data_definition
        feature_opt = model_definition.feature_options
        if not contains_label(search_value, feature_opt):
            raise PreventUpdate()
        if not is_valid_feature_slice(search_value, data_definition):
            raise PreventUpdate()
        model_definition.add_feature(search_value)
        sorted_options = sorted(
            model_definition.feature_options, key=lambda k: k["label"].upper()
        )
        return sorted_options, sorted_options

    return app

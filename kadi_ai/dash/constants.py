# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Constants for the Dashboard elements and callbacks. Part of KadiAI."""
from enum import Enum


class WebAppIdentifiers(str, Enum):
    """HTML component identifiers for eventhandling"""

    button_hiplot_close = "button-hiplot-close"
    button_editor_model_lock = "button-editor-model-lock"
    button_editor_training_lock = "button-editor-training-lock"
    button_exit = "button-exit"
    button_exit_dialog = "button-exit-dialog"
    button_glob_lock = "button-glob-lock"
    button_preprocess = "button-preprocess"
    button_preprocess_icon = "button-preprocess-icon"
    button_preview = "button-preview"
    button_pull = "button-pull"
    button_push = "button-push"
    checklist_dashplot_refresh = "cecklist-dashplot-refresh"
    checklist_hiplot_refresh = "cecklist-hiplot-refresh"
    dropdown_input_features = "dropdown-input-features"
    dropdown_model_fun_select = "dropdown-model-fun-select"
    dropdown_output_features = "dropdown-output-features"
    dropdown_training_fun_select = "dropdown-training-fun-select"
    editor_model_function = "model-code-editor"
    editor_training_function = "training-code-editor"
    figure_model = "figure-model"
    filter_dropdown_function_select = "filter-dropdown-function-select"
    filter_dropdown_glob = "filter-dropdown-glob"
    filter_hp_default = "filter-hp-default"
    filter_hp_dropdown = "filter-hp-dropdown"
    filter_hp_range = "filter-hp-range"
    filter_hp_slider = "filter-hp-slider"
    filter_label_param = "filter-label-param"
    filter_panel_function_param = "filter-panel-function-param"
    filter_panel_param = "filter-panel-param"
    graph_dashplot_parallelplot = "graph-dashplot-parallelplot"
    iframe_hiplot = "iframe-hiplot"
    iframe_tensorboard = "iframe-tensorboard"
    input_td_num_epochs = "input-td-num-epochs"
    input_td_num_phases = "input-td-num-phases"
    interval_refresh_dashplot = "interval-refresh-dashplot"
    interval_refresh_hiplot = "interval-refresh-hiplot"
    interval_update_progress = "interval-update-progress"
    label_editor_model_lock = "label-editor-model-lock"
    label_editor_training_lock = "label-editor-training-lock"
    label_glob_lock = "label-glob-lock"
    panel_after_visualization = "after-visualization-panel"
    panel_dashplot = "panel-dashplot"
    panel_model_hyperparameters = "panel-model-hyperparameters"
    panel_preprocessing = "panel-preprocessing-panel"
    panel_progress_hyperparameters = "panel-progress-hyperparameters"
    panel_training_hyperparameters = "panel-training-hyperparameters"
    progress_bar = "progress-bar"
    range_td_phases = "range-td-phases"
    table_after_datadefinition = "table-after-data-definition"
    table_before_datadefinition = "table-before-data-definition"
    table_dashplot_configurations = "tabe-dashplot-configurations"


class DataDefinitionColumns(str, Enum):
    """Column names for data definition table"""

    decode_type = "decode_str_to"
    format = "data_format"
    name = "name"
    shape = "data_shape"
    type = "dtype"


class DataDefinitionFormats(str, Enum):
    """Data formats for data definition table"""

    NF = "NF"
    NSF = "NSF"
    NSXYF = "NSXYF"
    NXF = "NXF"
    NXYF = "NXYF"
    NXYZF = "NXYZF"


class DataDefinitinTooltips(str, Enum):
    """Tooltips for"""

    tooltips = {
        "name": {"type": "markdown", "value": "Name of the feature"},
        "data_format": {
            "type": "markdown",
            "value": """
                data format string that defines order of the axes: \n
                    N: batch axis. Rule: Data format should always start with N
                    F: Feature axis. Rule: Data format should always end with F
                    S: Sequence axis
                    X,Y,Z: spatial axis
            """,
        },
        "data_shape": {
            "value": """
                The shape of the feature tensor. \n\n
                Use "None" for undefined (typically batch axis).
            """,
            "type": "markdown",
        },
    }


class Messages(str, Enum):
    """User text message"""

    data_definition = "Data definition and offline preprocessing"
    model_definition = "Model definition and hyperparameters"
    training_definition = "Training definition and hyperparameters"
    control_center = "Training and tuning control center"


class ExternalLinks(str, Enum):
    """External links"""

    font_awesome = (
        "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css",
    )

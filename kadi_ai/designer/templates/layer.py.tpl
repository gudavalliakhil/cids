import os
from xmlhelpy import command, argument, option, output

with open(os.path.join(
        os.path.dirname(__file__), '..', '..', 'cids', '_version.py')) as f:
    exec(f.read())

@command('{{ cmd_name|e }}',
         version=__version__,
         description='{{ cmd_description|e }}')
{%- for parameter_name, parameter_desc, parameter_default, parameter_required, parameter_is_flag in cmd_parameters %}
@option('{{ parameter_name|e }}',
        description='{{ parameter_desc|e }}',
        default={{ parameter_default }},
        required={{ parameter_required }},
        is_flag={{ parameter_is_flag }})
{%- endfor %}
{%- for output_name, output_desc, output_default, output_required in cmd_outputs %}
@output('{{ output_name|e }}',
        description='{{ output_desc|e }}',
        default={{ output_default }},
        required={{ output_required }})
{%- endfor %}
def execute():
    # Dummy execute (just a placeholder to assemble the workflow)
    pass


if __name__ == '__main__':
    execute()

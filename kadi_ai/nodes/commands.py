# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Executables for KadiAI and CIDS workflow nodes. Part of KadiAI."""
import os

import click_completion
from kadi_apy.lib.exceptions import KadiAPYInputError
from xmlhelpy import Bool
from xmlhelpy import Choice
from xmlhelpy import Float
from xmlhelpy import group
from xmlhelpy import Integer
from xmlhelpy import option
from xmlhelpy import TokenList

from kadi_ai.projects.project import KadiAIProject
from kadi_ai.repo.record import KadiAIRecord

click_completion.init()

__version__ = "0.0"
with open(
    os.path.join(os.path.dirname(__file__), "..", "..", "cids", "_version.py"),
    encoding="utf8",
) as f:
    exec(f.read())  # pylint:disable=exec-used


def _get_host_port(url):
    try:
        host, port = url.split(":")
    except ValueError as e:
        raise ValueError('Url must be formatted like: "host:port".') from e
    return host, port


@group(version=__version__)
def cids_flow():
    """Main executable of CIDSflow."""


@cids_flow.group()
def interactive():
    """Interactive tools of CIDSflow."""


@interactive.command(
    "define_data",
    version=__version__,
    description="Define data for machine learning in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option(
    "src_identifier",
    "Identifier of a Kadi Data Record to extract source data from.",
    required=False,
    char="s",
)
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    char="d",
    default="./PROJECT",
)
@option(
    "url",
    "A URL to be displayed in a WebView or local browser.",
    required=False,
    default="127.0.0.1:3333",
    char="u",
)
def define_data(*args, **kwargs):
    from ..dash import dashboard_data

    host, port = _get_host_port(kwargs["url"])
    app = dashboard_data.setup_dash(**kwargs)
    app.run_server(debug=False, host=host, port=port)


@interactive.command(
    "define_model",
    version=__version__,
    description="Define neural network model in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option("model_name", "Name of the neural network model.", required=True, char="n")
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    char="d",
    default="./PROJECT",
)
@option(
    "tfrecord_identifier",
    "Identifier of a Kadi Data Record with tfrecords and a data definition.",
    required=False,
    char="t",
)
@option(
    "url",
    "A URL to be displayed in a WebView or local browser.",
    required=False,
    default="127.0.0.1:3333",
    char="u",
)
def define_model(*args, **kwargs):
    from ..dash import dashboard_model

    host, port = _get_host_port(kwargs["url"])
    app = dashboard_model.setup_dash(**kwargs)
    app.run_server(debug=False, host=host, port=port)


@interactive.command(
    "control_center",
    version=__version__,
    description="Select neural network in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    char="d",
    default="./PROJECT",
)
@option(
    "result_identifier",
    "Identifier of a Kadi Data Record with CIDS results.",
    required=False,
    char="r",
)
@option(
    "url",
    "A URL to be displayed in a WebView or local browser.",
    required=False,
    default="127.0.0.1:3333",
    char="u",
)
def control_center(*args, **kwargs):
    from ..dash import dashboard_control_center

    host, port = _get_host_port(kwargs["url"])
    app = dashboard_control_center.setup_dash(**kwargs)
    app.run_server(debug=False, host=host, port=port)


@interactive.command(
    "stop",
    version=__version__,
    description="Train neural network in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option("project_dir", "Path to the project directory", required=True, char="d")
def stop(*args, **kwargs):
    from ..utility import _setup_project

    project = _setup_project(**kwargs)
    project.log("Stopped previous CIDSflow nodes.")


@interactive.command(
    "bayesian_oracle",
    version=__version__,
    description="Find the next most best configuration with Bayesian Optimization.",
    example="Enter example here",
)
@option(  # optional, displaied on both --xmlhelp and --help
    "property_definition_id",
    description="(Umbrella) Record id holding the parameter space.",
    # example="momaf-foams-optimization-bayes"
    required=True,
    char="d",
)
@option(
    "objective",
    description="Objective function(/parameter?) to be optimized.",
    default="score",
    # example="Half Life Time",
    required=False,
    char="f",
)
@option(
    "exploration_factor",
    description="Exploration over exploitation percentage.",
    default=2.6,  # keras tuner default
    required=False,
    char="e",
    param_type=Float,
)
@option(
    "max_index",
    description="Maximum number of iterations.",
    default=100,
    required=False,
    char="m",
    param_type=Integer,
)
@option(
    "random_samples",
    description="Number of starting random samples.",
    default=None,  # keras tuner: 3*hyp space dim
    required=False,
    char="r",
    param_type=Integer,
)
@option(
    "plot",
    description="Plot the optimization process evolution.",
    default=False,
    required=False,
    char="p",
    param_type=Bool,
)
def oracle(*args, **kwargs):
    """Bayesian Optimization oracle node.

    Args:
        property_definition_id: (Umbrella) Record id holding parameter space for
            Bayesian Optimization.
        objective: Objective function(/parameter?) to be optimized.
        exploration_factor: Exploration over exploitation percentage. # TODO
    """
    # Imports (Inside, to not slow xmlhelp down)
    from .intelligent import bayesian_oracle

    bayesian_oracle(**kwargs)


# scorer
@interactive.command(
    "scorer",
    version=__version__,
    description="Compute score.",
    example="Enter example here",
)
@option(
    "trial_id",
    description="ID of the trial to be scored.",
    required=False,
    default="COMPLETED",
    char="i",
)
@option(
    "metric",
    description="Compute score with the given metric.",
    default="MSE",
    required=True,
    char="m",
)
@option(
    "break_threshold",
    description="Threshold for break condition.",
    default=1e-5,
    required=False,
    char="t",
)
def scorer(*args, **kwargs):
    """Compute the score of one (already executed) Trial.

    Args:
        trial_id: ID of the trial to be scored. Default: all the generated trial with
            status COMPLETED.
        metric: metric to be used for the evaluation.
        break_threshold:
    """
    # Imports (Inside, to not slow xmlhelp down)
    from .intelligent import compute_score

    compute_score(**kwargs)


# runner
@interactive.command(
    "runner",
    version=__version__,
    description="Fake sim./exp. with the given configuration setting.",
    example="Enter example here",
)
@option(
    "trial_id",
    description="ID of the trial containing the configuration setting to be used.",
    required=False,
    default="last/all the generated trial with status IDLE.",  # (?)
    char="i",
)
def runner(*args, **kwargs):
    """Node faking the execution of an experiment/simulation with a given configuration.

    Args:
        trial_id: id of the trial to be run.
    """
    # Imports (Inside, to not slow xmlhelp down)
    from .intelligent import runner

    runner(**kwargs)


@cids_flow.group()
def noninteractive():
    """Noninteractive tools of CIDSflow."""


@noninteractive.command(
    "convert_and_preprocess",
    version=__version__,
    description="Convert data for machine learning in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    default="./PROJECT",
    char="d",
)
@option(
    "src_identifier",
    "Identifier of a Kadi Data Record to extract source data from.",
    required=False,
    char="s",
)
@option(
    "tfrecord_identifier",
    "Identifier of a Kadi Data Record to upload tfrecords and data definition.",
    required=False,
    char="t",
)
def convert_and_preprocess(**kwargs):

    from ..utility import _setup_project
    from ..utility import _setup_data_definition

    # Setup project
    project = _setup_project(**kwargs)

    # Download source dataset
    project.pull(src=True, force="update")

    # TODO: Convert instead of pull existing
    raise NotImplementedError
    project.pull(tfrecords=True, force="update")  # pylint: disable=unreachable
    data_definition = _setup_data_definition(project, **kwargs)
    project.data_definition = data_definition

    project.to_json(write_data_definition=True)

    # Upload converted dataset
    project.push(tfrecords=True, data_definition=True, force="update")


@noninteractive.command(
    "train",
    version=__version__,
    description="Train neural network in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    default="./PROJECT",
    char="d",
)
@option("name", "Name of the model", required=True, char="n")
# @option(
#     "input_features",
#     "List of input features for the neural network.",
#     required=True,
#     char="i",
# )
# @option(
#     "output_features",
#     "List of output features for the neural network.",
#     required=True,
#     char="o",
# )
# @option("loss",
#         "Optimization mode of the neural network.",
#         required=False, char="l", default="mse")
@option(
    "metrics",
    "List of metrics to monitor during training.",
    required=False,
    char="m",
    default="",
)
@option(
    "checkpoint",
    "The checkpoint to start from.",
    required=False,
    char="c",
    default="None",
)
@option(
    "url",
    "A URL to be displayed in a WebView or local browser.",
    required=False,
    char="u",
    default="127.0.0.1:3333",
)
@option(
    "results_identifier",
    "Identifier of a Kadi Data Record to store training results.",
    required=False,
    char="r",
)
def train(*args, **kwargs):
    # Read kwargs
    if kwargs.get("metrics"):
        metrics = [m.strip for m in kwargs.get("metrics").split(",")]
    else:
        metrics = []
    checkpoint = kwargs.get("checkpoint")
    host, port = _get_host_port(kwargs["url"])

    # Imports (Inside, to not slow xmlhelp down)
    from tensorflow.keras.callbacks import EarlyStopping
    from kerastuner import HyperParameters
    from cids.tensorflow.training_functions import basic_training_function
    from ..utility import _setup_project
    from ..utility import _setup_data_definition
    from ..utility import _setup_model

    # Setup project
    project = _setup_project(**kwargs)

    # Load data definition and model from project
    data_definition = project.data_definition
    model = _setup_model(project, data_definition, **kwargs)  # TODO: model from def
    project.to_json(write_data_definition=False)

    # Get data
    train_samples, valid_samples, _ = project.get_split_datasets(
        valid_split=0.15, test_split=0.15
    )

    # TODO: load training function
    schedule_function = basic_training_function

    # Set metrics
    model.metrics.extend(metrics)

    # TODO: Select training hyperparameters
    hp = HyperParameters()  # defaults

    # Start tensorboard
    project.start_tensorboard_daemon(host=host, port=port)

    # Training
    project.log(">> Training...")
    model.train(
        train_samples,
        valid_samples,
        schedule=schedule_function(hp),
        checkpoint=checkpoint,
        callbacks=[EarlyStopping(patience=10)],
    )
    project.log(">> Training complete.")
    project.push(results=True, tfrecords=False, src=False)


@noninteractive.command(
    "infer",
    version=__version__,
    description="Predict with a neural network in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    default="./PROJECT",
    char="d",
)
@option(
    "dataset",
    'The dataset to infer ("test", "valid", "test" or directory).',
    required=False,
    char="f",
    default="test",
)
@option(
    "checkpoint",
    "The checkpoint to start from.",
    required=False,
    char="c",
    default="last",
)
@option(
    "results_identifier",
    "Identifier of a Kadi Data Record to store inferred results.",
    required=False,
    char="r",
)
def infer(*args, **kwargs):
    # Read kwargs
    dataset = kwargs.get("dataset")

    # Import (Inside, to not slow xmlhelp down)
    import glob
    import numpy as np
    from pathlib import Path
    from ..utility import _setup_project
    from ..utility import _setup_data_definition
    from ..utility import _setup_model

    # Setup project
    project = _setup_project(**kwargs)
    data_definition = _setup_data_definition(project, **kwargs)
    model = _setup_model(project, data_definition, **kwargs)
    project.to_json(write_data_definition=False)

    # Get data
    # TODO: manually select additional data
    if dataset in ["test", "valid", "train"]:
        train_samples, valid_samples, test_samples = project.get_split_datasets(
            valid_split=0.15, test_split=0.15
        )
        if dataset == "test":
            infer_samples = test_samples
        elif dataset == " valid":
            infer_samples = valid_samples
        elif dataset == "train":
            infer_samples = train_samples
        else:
            raise NotImplementedError  # Impossible
    else:
        infer_samples = glob.glob(os.path.join(dataset, "*.tfrecord"))

    # Infer
    project.log(">> Inference...")
    X, Y, Y_ = model.infer_data(infer_samples, batch_size=1, checkpoint="last")
    project.log(">> Inference complete.")

    test_result_file = Path(model.base_model_dir, "test_results.npz")
    np.savez(test_result_file, X=X, Y=Y, Y_=Y_)
    # TODO: do more useful stuff here
    # TODO: adjust name?


@noninteractive.command(
    "eval",
    version=__version__,
    description="Evaluate neural network performance in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    default="./PROJECT",
    char="d",
)
# @option(
#     "input_features",
#     "List of input features for the neural network.",
#     required=True,
#     char="i",
# )
# @option(
#     "output_features",
#     "List of output features for the neural network.",
#     required=True,
#     char="o",
# )
@option(
    "results_identifier",
    "Identifier of a Kadi Data Record to store evaluation results.",
    required=False,
    char="r",
)
def eval(*args, **kwargs):
    pass


@noninteractive.command(
    "search",
    version=__version__,
    description="Search hyperparameters in the CIDS framework.",
    example="Enter example here",
)
@option("project_name", "Name of the project", required=True, char="p")
@option(
    "project_dir",
    "Path to the project directory",
    required=False,
    default="./PROJECT",
    char="d",
)
# @option(
#     "input_features",
#     "List of input features for the neural network.",
#     required=True,
#     char="i",
# )
# @option(
#     "output_features",
#     "List of output features for the neural network.",
#     required=True,
#     char="o",
# )
# @option("loss",
#         "Optimization mode of the neural network.",
#         required=False, char="l", default="mse")
@option(
    "metrics",
    "List of metrics to monitor during training.",
    required=False,
    char="m",
    default="",
)
@option(
    "checkpoint",
    "The checkpoint to start from.",
    required=False,
    char="c",
    default="None",
)
@option(
    "url",
    "A URL to be displayed in a WebView or local browser.",
    required=False,
    char="u",
    default="127.0.0.1:3333",
)
@option(
    "results_identifier",
    "Identifier of a Kadi Data Record to store search results.",
    required=False,
    char="r",
)
def search(*args, **kwargs):
    # Read kwargs
    metrics = kwargs.get("metrics")
    if metrics:
        assert isinstance(metrics, str)
        metrics = [m.strip() for m in metrics.split(",") if m.strip()]
    else:
        metrics = []
    # checkpoint = kwargs.get("checkpoint")
    host, port = _get_host_port(kwargs["url"])

    # Imports (Inside, to not slow xmlhelp down)
    from tensorflow.keras.callbacks import EarlyStopping
    from cids.tensorflow.training_functions import basic_training_function
    from ..utility import _setup_project
    from ..utility import _setup_data_definition
    from ..utility import _setup_model

    # Setup project
    project = _setup_project(**kwargs)
    data_definition = _setup_data_definition(project, **kwargs)
    model = _setup_model(project, data_definition, **kwargs)
    project.to_json(write_data_definition=False)

    # Get data
    train_samples, valid_samples, _ = project.get_split_datasets(
        valid_split=0.15, test_split=0.15
    )

    # TODO: load training function
    schedule_function = basic_training_function

    # Set metrics
    model.metrics.extend(metrics)

    # Start tensorboard
    project.start_tensorboard_daemon(host=host, port=port)

    # Searching
    project.log(">> Searching...")
    model.search(
        train_samples,
        valid_samples,
        schedule_function,
        executions_per_trial=1,
        max_epochs=21,  # hyperband only
        overwrite=False,
        method="hyperband",
        callbacks=[EarlyStopping(patience=3)],
    )
    project.log(">> Search complete.")


@noninteractive.command(
    "push",
    version=__version__,
    description="Upload CIDS project to Kadi.",
    example="Enter example here",
)
@option(
    "project_dir",
    "Path to the project directory",
    required=True,
    char="d",
)
@option("project_name", "Name of the project", required=False, char="n")
@option(
    "src",
    "Whether source data should be uploaded",
    required=False,
    default=False,
    char="s",
    param_type=Bool,
)
@option(
    "tfrecords",
    "Whether tfrecord files should be uploaded",
    required=False,
    default=False,
    char="t",
    param_type=Bool,
)
@option(
    "results",
    "Whether results should be uploaded",
    required=False,
    default=False,
    char="r",
    param_type=Bool,
)
@option(
    "data_definition",
    "Whether data-definition should be uploaded",
    required=False,
    default=True,
    char="D",
    param_type=Bool,
)
@option(
    "project",
    "Whether project *.json file should be uploaded",
    required=False,
    default=False,
    char="p",
    param_type=Bool,
)
@option(
    "link_collection",
    "Whether records should be linked to collection",
    required=False,
    default=True,
    char="C",
    param_type=Bool,
)
@option(
    "link_records",
    "Whether records should be linked to each other",
    required=False,
    default=True,
    char="R",
    param_type=Bool,
)
@option(
    "force",
    "Whether to overwrite existing files in record",
    required=False,
    default="no",
    char="f",
    param_type=Choice(["no", "all", "update"], case_sensitive=False),
)
@option(
    "create",
    "Whether new collection should be created",
    required=False,
    default=True,
    char="c",
    param_type=Bool,
)
@option(
    "exclude",
    "Regular expressions in filenames to exclude these files",
    required=False,
    default=(".*", "_*"),
    char="e",
    param_type=TokenList(","),
)
def push(*args, **kwargs):
    from ..utility import _setup_project

    # Setup project
    if kwargs["project_name"] is None:
        project_name = kwargs["project_dir"].split("/")[-1]
        kwargs.pop("project_name")
    else:
        project_name = kwargs["project_name"]
        kwargs.pop("project_name")
    # Check if project.json exists
    for file in os.listdir(kwargs["project_dir"]):
        if file.endswith(".json") and "project" in file:
            jsonfile = file
            break
    # Setup project
    if jsonfile in locals():
        project = KadiAIProject.from_json(jsonfile, log_file=None)
    else:
        project = _setup_project(project_name=project_name, **kwargs)
    kwargs.pop("project_dir")
    project.to_json(write_data_definition=False)
    # Push project
    if kwargs["force"] == "no":
        kwargs["force"] = False
    project.push(**kwargs)


@noninteractive.command(
    "pull",
    version=__version__,
    description="Download project from Kadi.",
    example="Enter example here",
)
@option(
    "collection_identifier",
    "Identifier of a Kadi collection.",
    required=False,
    char="c",
)
@option("project_name", "Name of the project", required=False, char="n")
@option(
    "project_dir",
    "Target directory for project download",
    required=False,
    default="/tmp/KadiAI",
    char="d",
)
@option(
    "src",
    "Whether source data should be downloaded",
    required=False,
    default=False,
    char="s",
    param_type=Bool,
)
@option(
    "tfrecords",
    "Whether tfrecord files should be downloaded",
    required=False,
    default=False,
    char="t",
    param_type=Bool,
)
@option(
    "results",
    "Whether results should be downloaded",
    required=False,
    default=False,
    char="r",
    param_type=Bool,
)
@option(
    "data_definition",
    "Whether data-definition should be uploaded",
    required=False,
    default=True,
    char="D",
    param_type=Bool,
)
@option(
    "force",
    "Whether to overwrite existing files in target directory",
    required=False,
    default="no",
    char="f",
    param_type=Choice(["no", "all", "update"], case_sensitive=False),
)
def pull(*args, **kwargs):
    from ..utility import _setup_project

    # Setup project
    if kwargs.get("project_name"):
        pass
    elif kwargs.get("collection_identifier"):
        try:
            project_record = KadiAIRecord.use_config(
                kwargs["collection_identifier"] + "-project", create=False
            )
            kwargs["project_name"] = project_record.get_metadatum(
                ["CIDS:Project", "init_params", "name"]
            )["value"]
        except KadiAPYInputError as e:
            raise ValueError(
                "Project record for this project doesn't exist. Cannot pull project."
            ) from e
    else:
        raise ValueError("Please provide either collection_identifier or project_name.")
    kwargs.pop("collection_identifier")
    project = _setup_project(**kwargs)
    kwargs.pop("project_name")
    kwargs.pop("project_dir")
    # Pull project
    if kwargs["force"] == "no":
        kwargs["force"] = False
    project.pull(**kwargs)

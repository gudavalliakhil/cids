# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# import json
"""Bayesian optimization functionality for KadiAI in Kadi Workflows. Part of KadiAI."""
import os
from pathlib import Path

import click
import keras_tuner
import matplotlib.pyplot as plt
import numpy as np
import xmlhelpy
from click.core import Context
from kadi_apy import KadiManager
from kadi_apy.lib.exceptions import KadiAPYInputError
from keras_tuner import HyperParameters
from keras_tuner.engine.trial import Trial
from keras_tuner.engine.trial import TrialStatus
from keras_tuner.tuners import bayesian as bo_module
from xmlhelpy.properties import read_kadi_properties  # pylint:disable=import-error
from xmlhelpy.properties import read_xml  # pylint:disable=import-error
from xmlhelpy.properties import set_kadi_property  # pylint:disable=import-error
from xmlhelpy.properties import write_kadi_properties  # pylint:disable=import-error

from ..repo.record import KadiAIRecord

# TODO: remove pylint import error masking


class TrialStatusExtended(TrialStatus):
    # IDLE = "IDLE"             # config. setting just created
    # RUNNING = "RUNNING"       # sim./exp. running with this config.
    EXECUTED = "EXECUTED"  # results available ready to be scored
    # COMPLETED = "COMPLETED"   # results scored, everything done
    # INVALID = "INVALID"
    # STOPPED = "STOPPED"


meta_status = [
    {
        "key": "status",
        "type": "str",
        "validation": {
            "options": [
                "IDLE",
                "RUNNING",
                "EXECUTED",
                "COMPLETED",
                "STOPPED",
                "INVALID",
            ]
        },
        "value": "",
    }
]
meta_score = [
    {
        "key": "score",
        "type": "float",
        "value": 0.0,
    }
]


def black_box_function1D(x, noise=0.1):
    """Function with unknown internals we wish to maximize.

    This is just serving as an example, for all intents and
    purposes think of the internals of this function, i.e.: the process
    which generates its output values, as unknown.
    """
    from math import sin, pi

    # noise = np.random.normal(loc=0, scale=noise)
    return 1.0 - (x**2.0 * sin(5.0 * pi * x) ** 6.0)


def bayesian_plot(n_iter):
    for _ in range(n_iter):
        # TODO
        print("")


def bayesian_oracle(
    property_definition_id,
    objective,
    exploration_factor=2.6,
    max_index=100,
    random_samples=None,
    plot=False,
):
    """Tool of CIDSflow that creates a Trial with the setting of the next
    most informative datapoint based on Bayesian Optimization.

    It uses keras tuner Bayesian optimization with a underlying Gaussian process model.
    The acquisition function used is upper confidence bound (UCB)
    Args:
        property_definition_id: Umbrella Record identifier holding parameter space for
            Bayesian Optimization.
        objective: A string, `keras_tuner.Objective` instance, or a list of
            `keras_tuner.Objective`s and strings. If a string, the direction of
            the optimization (min or max) will be inferred. If a list of
            `keras_tuner.Objective`, we will minimize the sum of all the
            objectives to minimize subtracting the sum of all the objectives to
            maximize. The `objective` argument is optional when
            `Tuner.run_trial()` or `HyperModel.fit()` returns a single float as
            the objective to minimize.
        exploration_factor (beta): Float, the balancing factor of exploration and
            exploitation. The larger it is, the more explorative it is. Defaults to 2.6.
    """

    # Query kadi and collect all the Records already available
    manager = KadiManager(instance=None, host=None, token=None)
    umbrella = KadiAIRecord(manager, identifier=property_definition_id)
    linked_ids = umbrella.auto_get_link_ids(direction="in", filter="trial for")
    linked_records = [KadiAIRecord(manager, id=i) for i in set(linked_ids.values())]
    linked_trial_records = [r for r in linked_records if r.meta["type"] == "trial"]

    # TODO: KadiFS: mount instead of download all
    project_dir = Path.home() / "workspace" / "bayesian-opt" / "oracle_test"
    project_name = "project_test"
    result_dir = project_dir / project_name
    for linked_record in set(linked_trial_records):
        linked_trial_id = linked_record.get_metadatum("trial_id")[
            "value"
        ]  # FIXME(Giovanna): naming convention!
        current_dir = result_dir / f"{linked_trial_id}"
        if not current_dir.exists():
            linked_record.download_files(current_dir)

    umbrella_hyp = HyperParameters()

    extras = umbrella.meta["extras"]
    for m in extras:
        key = m["key"]
        if m["type"] == "str":
            # Qualitative key
            validation = umbrella.get_metadatum(key)["validation"]
            valid_choices = validation["options"]
            new_hp = umbrella_hyp.Choice(key, valid_choices)
        elif m["type"] == "dict":
            # Quantitative key
            min_value = umbrella.get_metadatum([key, "min"])["value"]
            max_value = umbrella.get_metadatum([key, "max"])["value"]
            try:
                step_value = umbrella.get_metadatum([key, "step"])["value"]
            except KadiAPYInputError:
                step_value = None
            dtype = umbrella.get_metadatum([key, "min"])["type"]
            try:
                default_value = umbrella.get_metadatum([key, "default"])["value"]
            except KadiAPYInputError:
                if step_value is None:
                    default_value = (max_value - min_value) / 2
                else:
                    possible_values = np.arange(min_value, max_value, step_value)
                    default_value = possible_values[len(possible_values) // 2]
            if dtype == "float":
                new_hp = umbrella_hyp.Float(
                    key,
                    min_value=min_value,
                    max_value=max_value,
                    step=step_value,
                    default=float(default_value),
                )
            elif dtype == "int":
                new_hp = umbrella_hyp.Int(
                    key,
                    min_value=min_value,
                    max_value=max_value,
                    step=step_value,
                    default=int(default_value),
                )
            else:
                raise ValueError("Invalid data type for quantitative parameter.")
        print(new_hp)

    # TODO: (for foam project specific)
    # get metadata (settings) from Trial record files
    # - read excel and extract info
    # - read trial.json

    # Create a guided Randomsearch trial
    # oracle_test = RandomSearchOracle(
    #     objective=keras_tuner.Objective(objective, "max"),
    #     #max_trials=10, # default value
    #     hyperparameters=umbrella_hyp,
    # )

    oracle_test = bo_module.BayesianOptimizationOracle(
        # objective=keras_tuner.Objective(objective, direction="min"),
        objective=keras_tuner.Objective("score", direction="min"),
        max_trials=max_index
        + 1,  # default value     # FIXME: get from index somehow???
        num_initial_points=random_samples,  # default = 3*hyp space dim
        hyperparameters=umbrella_hyp,
        beta=exploration_factor,
    )

    # set project directory and reload
    oracle_test._set_project_dir(project_dir, project_name)

    # Create a Trial instance with the information from the oracle
    trial = oracle_test.create_trial("tuner1")

    # dictionary with keys values (mapping of parameter names to suggested values) and
    #   status
    # oracle_out = oracle_test.populate_space(trial.trial_id)
    # trial.hyperparameters.values = oracle_out["values"]
    # oracle_test.update_trial(trial.trial_id, {"score": 1.0}) # TODO: put in scorer
    # oracle.end_trial(trial.trial_id, "COMPLETED") # TODO: put in scorer

    # debug - 1D case
    if objective == "black_box_function1D":

        # Compute metrics and update trial
        res = black_box_function1D(trial.hyperparameters.values["x"])
        print(f"{res}")
        oracle_test.update_trial(trial.trial_id, {"score": res})
        print(f"{res}")

        # save data for plot
        file = Path("data_points.txt")
        with file.open(mode="a", encoding="utf-8") as f:
            x_val = trial.hyperparameters.values["x"]
            f.write(f"{x_val},{res}")
            f.write("\n")

        # Plot bayesian optimization process
        if plot:
            # (this works only if trial_id is, as now, an integer 0,1,2,...)
            if int(trial.trial_id) < (oracle_test.num_initial_points):
                # Random generated points
                print(f"Plot {oracle_test.num_initial_points} starting random points")
            else:
                # Bayesian Process
                x, _ = oracle_test._vectorize_trials()
                # try:
                #     oracle_test.gpr.fit(x, y)
                # except ValueError as e:
                #     if "array must not contain infs or NaNs" in str(e):
                #         return oracle_test._random_populate_space()
                #     raise e

                def predict_confidence_values(x):
                    print("predict_confidence_values(x)")
                    if oracle_test.gpr.can_predict():
                        x = x.reshape(1, -1)
                        mu, sigma = oracle_test.gpr.predict(x)
                        ucb = mu - oracle_test.beta * sigma
                        return mu, sigma, ucb
                    raise KeyError("Missing arguments of the Gaussian Process.")

                # Get boundaries
                bounds = oracle_test._get_hp_bounds()
                # Get (100) equidistant points between boundaries
                x_obj = np.linspace(bounds[:, 0][0], bounds[:, 0][0], num=100)
                # or (50) random points
                # x_seeds = oracle_test._random_state.uniform(
                #     bounds[:, 0], bounds[:, 1], size=(50, bounds.shape[0])
                # )# this is better for more than one param-problems
                for x in x_obj:
                    mu, sigma, ucb = predict_confidence_values(x)
                    print(mu, sigma, ucb)

                # confidence intervals
                # plt.fill_between(x_seeds,
                #     mu +  exploration_factor * sigma,
                #     mu - exploration_factor * sigma,
                #     alpha=0.1)

                # Noise-free objective
                y_obj = [black_box_function1D(x) for x in x_obj]
                plt.plot(x_obj, y_obj, "y--", lw=1, label="Noise-free objective")
                plt.savefig("bayesian_plot.png", format="png")  # eps

                # Plot y as function of x[..., i]  ????
                # Seaborn
                # project.plot_dir

                # TODO: move to external function
                # bayesian_plot(oracle_test, n_iter)

    # End trial
    oracle_test.end_trial(trial.trial_id, "COMPLETED")

    # trial.status = oracle_out["status"] # updates status to "RUNNING"

    # Save trial(s)
    trial_dir = result_dir / f"{trial.trial_id}"  # FIXME(Giovanna): naming convention
    trial_dir.mkdir()
    trial.save(trial_dir / "trial.json")

    # Save oracle
    oracle_test.save()

    # Use kadi_properties to save it as a json and xml file
    set_kadi_property("trial_id", trial.trial_id, overwrite=True)
    for k in trial.hyperparameters.values.keys():
        val = trial.hyperparameters[k]
        if isinstance(val, np.number):
            val = val.item()
        # write xml file
        set_kadi_property(k, val, overwrite=True)
        # TODO: write json file
        # xml_to_json("kadi_properties.xml")

    # Create a Kadi record with identifier = umbrella identifier + trial id
    kadi_identifier = f"trial-{trial.trial_id}"  # FIXME(Giovanna): naming convention
    new_record = manager.record(
        # identifier=f"{property_definition_id}-{trial.trial_id}",
        identifier=kadi_identifier,
        title=f"Trial {trial.trial_id}",  # FIXME: naming
        create=True,
    )

    # FOR DEBUG: save trial ids into text file to make kadi cleaning easier
    file = Path("list_identifiers.txt")
    with file.open(mode="a", encoding="utf-8") as f:
        # f.write(kadi_identifier) # saves the identifiers of the records
        f.write(f"{new_record.id}")  # saves the IDs of the records
        f.write("\n")

    # Upload file trial.json into the Record as file
    new_record.upload_file(
        file_path=trial_dir / "trial.json",
        file_name="trial.json",
    )

    # Status as metadata (it should mirror the status in trial.json).
    #   The class TrialStatus of Keras Tuner contains:
    #   IDLE, RUNNING, COMPLETED, INVALID, STOPPED
    #   but I will need one more to distinguish the following phases of the process:
    #       - setting configuaration created but not yet used
    #       - the setting conf. has been taken care of and the related
    #           simulation/experiment is running
    #       - the sim/exp is completed and the results available (uploaded in kadi?)
    #           but they haven't been scored yet
    #       - completed = scored

    meta_status[0]["value"] = "IDLE"
    new_record.add_metadata(meta_status)
    new_record.add_metadatum(
        {"key": "trial_id", "type": "str", "value": trial.trial_id}
    )
    new_record.set_attribute(attribute="type", value="trial")
    new_record.set_attribute(
        attribute="description", value="Trial test for Bayesian Optimization."
    )

    # Link it to the umbrella record
    new_record.link_record(record_to=umbrella.id, name="trial for")

    # TODO: second link (maybe collection link?) to mark this trial as part of
    #   ExperimentX (to allow the possibility of using a specific set of the created
    #   trials for each bayesian-opt. project)


@xmlhelpy.command()
@xmlhelpy.option(
    "monitored",
    description="Parameter to be read from kadi_properties file.",
    default="score",
    # example="Liquid",
    required=False,
)
# Expect some key values and read them from kadi_properties file
def extract_value(param, file="kadi_properties.xml"):
    """Read values of a given property from kadi_properties file."""
    for child in read_xml(file):
        for grandchild in child:
            for grandgrandchild in grandchild:
                if grandgrandchild.attrib["name"] in param:
                    return grandgrandchild.text


def debug_random_value():
    return np.abs(np.random.normal(loc=0.5))


def read_value_from_results(file, search_str):
    file = Path(file)
    with file.open(file, encoding="utf-8") as f:
        lines = f.readlines()
        for line in lines:
            if search_str in line:
                line_split = line.split()
                val = line_split[-1]
                return float(val)
        raise ValueError(f"{search_str} not found.")


def compute_score(trial_id, metric, break_threshold):
    """Compute the score of one (executed) Trial and update the Kadi Record."""

    # TODO: insert option look for local files
    # ...

    if trial_id is None:
        kadi_prop_file = Path.cwd() / "kadi_properties.xml"
        workflow_element = read_kadi_properties(kadi_prop_file)
        node_element = workflow_element[-1]
        program_element = node_element[0]
        found = 0
        for p in program_element.findall("param"):
            if p.get("name") == "trial_id":
                trial_id = p.text
                found = 1
        if not found:
            raise KeyError("Trial id not found.")

    # Check which trials have status "EXECUTED" and therefore must be scored
    manager = KadiManager(instance=None, host=None, token=None)
    current_record = KadiAIRecord(manager, identifier=trial_id)
    to_be_scored = False
    extras = current_record.meta["extras"]
    for m in extras:
        if m["key"] == "status":
            if m["value"] == "EXECUTED":
                to_be_scored = True
                break

    if to_be_scored:
        # Compute and save score
        trial_json = Path.cwd() / f"{trial_id}" / "trial.json"
        if metric == "debug_random_value":
            score = debug_random_value()
        elif metric == "read_value_from_results":
            results_file = Path.cwd() / f"{trial_id}" / "results.txt"
            score = read_value_from_results(results_file, "HLT")
        else:
            raise ValueError(f"Unknown metric: {metric}")

        hyperparameters = HyperParameters()
        trial = Trial(hyperparameters)
        trial.reload(trial_json)
        trial.score = score
        trial.status = "COMPLETED"

        # save score into the current trial json file
        trial.save(trial_json)
        # Upload the updated trial.json into kadi
        current_record.upload_file(
            file_path=Path.cwd() / f"{trial.trial_id}" / "trial.json",
            file_name="trial.json",
            force=True,
        )

        # save score into the kadi_properties file
        if isinstance(score, np.number):
            score = score.item()
        set_kadi_property("score", score, overwrite=True)

        # Change record status into "COMPLETED"
        meta_status[0]["value"] = "COMPLETED"
        current_record.add_metadata(meta_status, force=True)
        # add the score as record metadata
        meta_score[0]["value"] = score
        current_record.add_metadata(meta_score, force=True)

    else:
        raise ValueError("No executed trial to be scored.")


def runner(trial_id):
    """Execute the next simulation / experiment given the setting configuration."""

    if trial_id is None:
        kadi_prop_file = Path.cwd() / "kadi_properties.xml"
        workflow_element = read_kadi_properties(kadi_prop_file)
        node_element = workflow_element[-1]
        program_element = node_element[0]
        found = 0
        for p in program_element.findall("param"):
            if p.get("name") == "trial_id":
                trial_id = p.text
                found = 1
        if not found:
            raise KeyError("Trial id not found.")

    # Check which trials have status "IDLE" and therefore must be run
    manager = KadiManager(instance=None, host=None, token=None)
    current_record = KadiAIRecord(manager, identifier=trial_id)
    to_be_run = False
    extras = current_record.meta["extras"]
    for m in extras:
        if m["key"] == "status":
            if m["value"] == "IDLE":
                to_be_run = True

    if to_be_run:
        # Change record status into "RUNNING", to clarify that
        # the execution of this trial has been taken care of.
        meta_status[0]["value"] = "RUNNING"
        current_record.add_metadata(meta_status, force=True)

        trial_json = Path.cwd() / f"{trial_id}" / "trial.json"
        if os.path.exists(trial_json):
            # reload trial
            hyperparameters = HyperParameters()
            trial = Trial(hyperparameters)
            trial.reload(trial_json)
            # Change trial status into "RUNNING"
            trial.status = "RUNNING"
            trial.save(trial_json)

            # Upload the updated trial.json into kadi
            current_record.upload_file(
                file_path=Path.cwd() / f"{trial.trial_id}" / "trial.json",
                file_name="trial.json",
                force=True,
            )

            # Real runner should take care of execute the sim/experiment
            # DUMMY FOR DEBUG: text file containing some easily readable results
            file = Path.cwd() / f"{trial.trial_id}" / "results.txt"
            with file.open(mode="a", encoding="utf-8") as f:
                volume = trial.hyperparameters["Volume"]
                concentration = trial.hyperparameters["Concentration"]
                random_result = volume * concentration
                f.write(f"HLT: {random_result}")
                f.write("\n")
            current_record.upload_file(
                file_path=Path.cwd() / f"{trial.trial_id}" / "results.txt",
                file_name="results.txt",
                force=True,
            )

            # Change record status into "EXECUTED"
            meta_status[0]["value"] = "EXECUTED"
            current_record.add_metadata(meta_status, force=True)

            # Change trial status into "EXECUTED"
            trial.status = "EXECUTED"
            trial.save(trial_json)
            # Upload the updated trial.json into kadi
            current_record.upload_file(
                file_path=Path.cwd() / f"{trial.trial_id}" / "trial.json",
                file_name="trial.json",
                force=True,
            )
        else:
            raise ValueError(f"File {trial_json} not found.")
    else:
        raise ValueError("No new trial to be run.")


if __name__ == "__main__":

    max_steps = 10
    monitored = "Liquid"

    ctx = Context(command=bayesian_oracle)
    ctx.params["property_definition_id"] = "momaf-foams"
    # ctx.params["max_steps"] = max_steps

    # Objective: value to give to keras-tuner to be maximized. Could be the same as
    # metric, or I could define a different metric on this value to score the results.
    ctx.params["objective"] = "halflifetime"
    ctx.params["exploration_factor"] = 0.4

    ctx2 = Context(command=compute_score)
    ctx2.params["metric"] = "debug_random_value"
    ctx2.params["break_threshold"] = 1e-5

    # clean kadi_properties.xml file
    kadi_prop_file = Path.cwd() / "kadi_properties.xml"
    if os.path.exists(kadi_prop_file):
        os.remove(kadi_prop_file)

    # bayesian loop
    click.echo(f"Performing {max_steps} iterations of Bayesian Optimization.")
    for i in range(max_steps):
        # ctx.params["node_id"] = f"{i+1:d}"
        ctx.params["node_id"] = "dummy"
        # find the next most informative configuration through bayesian oracle node,
        # write the information on kadi_properties.xml and trial_id/trial.json
        ctx = write_kadi_properties(ctx)  # dafault file is already kadi_properties.xml
        bayesian_oracle.callback(**ctx.params)

        # Execute the last created trial.

        # extract the information about the last trial from kadi_properties file
        workflow_element = read_kadi_properties(kadi_prop_file)
        node_element = workflow_element[-1]
        program_element = node_element[0]
        found = 0
        for p in program_element.findall("param"):
            if p.get("name") == "trial_id":
                trial_id = p.text
                ctx2.params["trial_id"] = trial_id
                # run the simulation/experiment given the setting configuration
                runner.callback(trial_id)
                # compute (and save) score
                compute_score.callback(**ctx2.params)
                found = 1
        if not found:
            raise KeyError("Trial id not found.")

    extract_value.callback(
        monitored,
    )
    # TODO: compute (smarter) score
    # TODO: check status of Trials and select the completed ones

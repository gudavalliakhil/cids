# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Base project functionality for AI projects. Part of KadiAI.

    Classes:
        BaseProject:    Project with a specific data structure on the local filesystem.

"""
import datetime
import json
import os
import signal
import subprocess
import time
from pathlib import Path

import psutil
import tensorboard as tb
from sklearn import model_selection as skms

from .utility import get_env
from .utility import set_project_seeds
from cids.base.path_handler import BasePathHandler
from cids.data.definition import DataDefinition
from cids.data.offline_processing.files import split_samples
from cids.data.offline_processing.files import split_samples_by_directory
from cids.data.offline_processing.files import split_samples_by_subjects
from cids.data.offline_processing.files import split_samples_stratified_group


class BaseProject(BasePathHandler):

    VERBOSITY = 1

    def __init__(  # pylint: disable=super-init-not-called
        self,
        name,
        seed="name",
        log_file="auto",
        root=None,
        gitcommit=None,
        _params=None,
    ):
        """BaseProject class that holds all information of the current project.

        A project has a project name, has folders for results, inputs, and
        logs, has seeds and has a logger. There is only one project and one
        logger at the same time.

        Args:
            name:       name of the project
            seed:       a seed to set the random number generators
                            ("name" to use project name, "time" to use time,
                            None to disable)
            log_file:    path to the log file
                            ("auto" to use name and timestamp, None to disable)
            root:       project root directory
            gitcommit:  assumed git commit of the project
        """
        environment, runscript = get_env()
        # Set root directory and runscript name
        if root is None:
            if environment == "colab":
                root = "/content/"
            else:
                root = "."
        # Strictness: allows legacy behavior
        self.strict_directory_structure = False
        # Name
        self.name = name
        # Directories and files
        self._root_dir = Path(root)
        self._input_dir = self.root_dir / "INPUTS"
        self.src_dir = self.input_dir / "src"
        self._result_dir = self.root_dir / "RESULTS"
        self.plot_dir = self.root_dir / "PLOTS"
        self._log_dir = self.root_dir / "LOGS"
        project_file = self.root_dir / f"project_{name}.json"
        if project_file.exists():
            self.project_file = project_file
        else:
            # Legacy: old-style project_json
            legacy_project_file = self.root_dir / "project.json"
            if legacy_project_file.exists():
                self.project_file = legacy_project_file
            else:
                self.project_file = project_file  # Will create new project file
        self._tfrecord_dir = None  # Set by find
        self._datasets = None  # Set by split
        self.dataset_params = None  # Set by split
        self._data_definition = None  # Set by discover in property
        # Logging
        timestamp = str(datetime.datetime.now()).split(".", maxsplit=1)[0]
        timestamp = timestamp.replace(":", "-").replace(" ", "_")
        self._timestamp = timestamp
        self._runscript = runscript or name
        self.log_file = log_file
        if log_file:
            # Initialize Mixin (DO NOT REMOVE!)
            self._start_logging(set_root=True)
            self.log(f"Logging to {os.fspath(self.log_file):s}")
        # Seeding
        if seed:
            if seed == "name":
                seed = name
            elif seed == "time":
                seed = datetime.datetime.now()
            hashed_seed = self.set_seeds(seed)
            self.seed = hashed_seed
        else:
            self.seed = seed
        # Overwrite params
        _params = _params or {}
        if not _params:
            # Check if project has been created
            if self.project_file.exists() and self.project_file.stat().st_size:
                self.log(f"Loading from {os.fspath(self.project_file)}")
                with self.project_file.open("r", encoding="utf8") as f:
                    json_dict = json.load(f)
                if "run_params" in json_dict.keys():
                    _params = json_dict["run_params"]
                    self.strict_directory_structure = False
        # Directories before other params since other run params need dirs
        if _params:
            self._update_params(_params)
        # Git commit and versioning
        current_gitcommit = self._get_gitcommit()
        if gitcommit is not None and current_gitcommit != gitcommit:
            self.warn(
                f"Git commit changed from {gitcommit}" + f" to {current_gitcommit}"
            )
        self.gitcommit = current_gitcommit
        self.log(f"Git commit: {self.gitcommit}")

    @property
    def root_dir(self):
        return self._root_dir

    @root_dir.setter
    def root_dir(self, newpath):
        self._root_dir = Path(newpath)

    @property
    def src_dir(self):
        return self._src_dir

    @src_dir.setter
    def src_dir(self, newpath):
        newpath = Path(newpath)
        if not self._check_if_child_of(newpath, self.input_dir):
            if newpath.is_absolute():
                self.warn(
                    f"Path src_dir ({os.fspath(newpath)}) is not in input_dir "
                    + f"({os.fspath(self.root_dir)}). "
                    + "External source directories are discouraged for data provenance."
                )
            else:
                newpath = self.root_dir / newpath
        self._src_dir = newpath

    @property
    def input_dir(self):
        return self._input_dir

    @input_dir.setter
    def input_dir(self, newpath):
        if self.strict_directory_structure:
            raise AttributeError(
                "Attribute input_dir is private. To switch datasets, set tfrecord_dir."
            )
        if newpath is None:
            return
        newpath = Path(newpath)
        if not self._check_if_child_of(newpath, self.root_dir):
            if newpath.is_absolute():
                raise ValueError(
                    f"Path input_dir ({os.fspath(newpath)}) is not in project_dir "
                    + f"({os.fspath(self.root_dir)})"
                )
            newpath = self.root_dir / newpath
        self._input_dir = newpath

    @property
    def tfrecord_dir(self):
        if self._tfrecord_dir is not None:
            # Directory has alreay been set by the user or previous search
            return self._tfrecord_dir
        try:
            # Find tfrecords in input directory
            _, directories = self._data_definition.find_samples(
                self.input_dir, pattern="**/*.tfrecord", return_directories=True
            )
            if len(directories) == 1:
                # All tfrecords in a single directory
                self._tfrecord_dir = directories[0]
                return self._tfrecord_dir
            if len(directories) == 0:
                # The tfrecords are directly in the input directory
                # (shouldn"t happen, should return input directory automatically)
                self._tfrecord_dir = self.input_dir
                return self._tfrecord_dir
            # TODO: Manage pre-split dataset (training, validation, testing)
            # The tfrecords are split in different directories
            self.warn("Multiple tfrecord directories found: " + str(directories))
            return directories
        except (FileNotFoundError, AttributeError):
            self._tfrecord_dir = self.input_dir / "tfrecord"
            self._tfrecord_dir.mkdir(parents=True, exist_ok=True)
            return self._tfrecord_dir

    @tfrecord_dir.setter
    def tfrecord_dir(self, newpath):
        if newpath is None:
            self._tfrecord_dir = None
        else:
            newpath = Path(newpath)
            if not self._check_if_child_of(newpath, self.input_dir):
                if newpath.is_absolute():
                    raise ValueError(
                        f"Path tfrecord_dir ({os.fspath(newpath)}) is not in input_dir "
                        + f"({os.fspath(self.input_dir)})"
                    )
                newpath = self.root_dir / newpath
            self._tfrecord_dir = newpath

    @property
    def result_dir(self):
        return self._result_dir

    @result_dir.setter
    def result_dir(self, newpath):
        if self.strict_directory_structure:
            raise AttributeError(
                "Attribute result_dir is private. To distinguish models, change the "
                + "model_name in CIDSModel."
            )
        newpath = Path(newpath)
        if not self._check_if_child_of(newpath, self.root_dir):
            if newpath.is_absolute():
                raise ValueError(
                    f"Path result_dir ({os.fspath(newpath)}) is not in project_dir "
                    + f"({os.fspath(self.root_dir)})"
                )
            newpath = self.root_dir / newpath
        self._result_dir = newpath

    @property
    def plot_dir(self):
        return self._plot_dir

    @plot_dir.setter
    def plot_dir(self, newpath):
        newpath = Path(newpath)
        if not self._check_if_child_of(newpath, self.root_dir):
            if newpath.is_absolute():
                raise ValueError(
                    f"Path plot_dir ({os.fspath(newpath)}) is not in project_dir "
                    + f"({os.fspath(self.root_dir)})"
                )
            newpath = self.root_dir / newpath
        self._plot_dir = newpath

    @property
    def log_dir(self):
        return self._log_dir

    @log_dir.setter
    def log_dir(self, newpath):
        if self.strict_directory_structure:
            raise AttributeError("Attribute log_dir is private.")
        newpath = Path(newpath)
        if not self._check_if_child_of(newpath, self.root_dir):
            if newpath.is_absolute():
                raise ValueError(
                    f"Path log_dir ({os.fspath(newpath)}) is not in project_dir "
                    + f"({os.fspath(self.root_dir)})"
                )
            newpath = self.root_dir / newpath
        self._log_dir = newpath

    @property
    def project_file(self):
        return self._project_file

    @project_file.setter
    def project_file(self, newpath):
        newpath = Path(newpath)
        if not self._check_if_child_of(newpath, self.root_dir):
            if newpath.is_absolute():
                raise ValueError(
                    f"Path project_file ({os.fspath(newpath)}) is not in project "
                    + f" directory ({os.fspath(self.root_dir)})"
                )
            newpath = self.root_dir / newpath
        self._project_file = newpath

    @property
    def data_definition(self):
        if self._data_definition is None:
            # Discover data definition file in project
            try:
                # Projects may have multiple. Find will always first find and
                #     potentially overwrite previous data definitions unless manually
                #     changed. Otherwise, so will any static definitions. This way,
                #     the users can decide where to store their data definitions, e.g.
                #     with their tfrecords or in the project folder
                search_dir = self._tfrecord_dir or self._input_dir or self.root_dir
                try:
                    data_definition_file = self._find_data_definition_file(
                        search_dir=search_dir
                    )
                except FileNotFoundError:
                    search_dir = self.root_dir
                    data_definition_file = self._find_data_definition_file(
                        search_dir=search_dir
                    )
                self._data_definition = DataDefinition.from_json(data_definition_file)
            except FileNotFoundError:
                self.warn(
                    "No data definition file found. Creating empty data definition"
                )
                self._data_definition = DataDefinition()
                self._data_definition.file = self.tfrecord_dir / "data_definition.json"
        # Otherwise return previously set data definition
        return self._data_definition

    @data_definition.setter
    def data_definition(self, new_data_definition):
        if isinstance(new_data_definition, DataDefinition):
            self._data_definition = new_data_definition
        # elif isinstance(new_data_definition, os.PathLike):
        #     path = Path(new_data_definition)
        #     self._data_definition = DataDefinition.from_json(path)
        # elif isinstance(new_data_definition, dict):
        #     self._data_definition = DataDefinition.from_json(new_data_definition)
        else:
            raise ValueError(
                "Project.data_definition must be an instance of DataDefinition."
            )
        if self._data_definition.file is None:
            self._data_definition.file = self.tfrecord_dir / "data_definition.json"

    @property
    def data_definition_file(self):
        if self._data_definition is not None:
            return self._data_definition.file
        return None

    @data_definition_file.setter
    def data_definition_file(self, newpath):
        newpath = Path(newpath)
        if not self._check_if_child_of(newpath, self.root_dir):
            if newpath.is_absolute():
                raise ValueError(
                    f"Path data_definition_file ({os.fspath(newpath)}) is not in "
                    + f"project directory ({os.fspath(self.root_dir)})"
                )
            newpath = self.root_dir / newpath
        if self._data_definition is None:
            if newpath.is_file():
                self._data_definition = DataDefinition.from_json(newpath)
            else:
                raise ValueError(
                    "Cannot set non-existent data definition file "
                    + "before project.data_definition"
                )
        else:
            self._data_definition.file = newpath

    @property
    def datasets(self):
        return self._datasets

    @datasets.setter
    def datasets(self, new_dict):
        new_dict = {
            k: self._to_absolute_paths(v, root_dir=self.tfrecord_dir)
            for k, v in new_dict.items()
        }
        self._datasets = new_dict

    @property
    def samples(self):
        return self.data_definition.samples

    @property
    def log_file(self):
        if self._log_file is not None:
            return Path(self._log_file)
        return None

    @log_file.setter
    def log_file(self, value):
        if value == "auto":
            filename = self._timestamp + "_" + self._runscript + ".log"
            value = self.log_dir / filename
        if value:
            self._log_file = Path(value)
        else:
            self._log_file = None

    def _find_data_definition_file(
        self, name="**/data_definition.json", search_dir=None
    ):
        """Find the file containing a DataDefinition in the input directory."""
        search_dir = search_dir or self.root_dir
        files = self._find_paths(search_dir, name)
        if len(files) > 1:
            raise ValueError(
                f"Multiple files named {name} found in {search_dir}: {str(files)}"
            )
        return files[0]

    def set_seeds(self, seed):
        """Sets the seeds for the random, numpy, and tensorflow random functions."""
        self.log(f"Setting seeds to: {seed}")
        return set_project_seeds(seed)

    def get_split_datasets(
        self,
        samples=None,
        shuffle=True,
        valid_split=None,
        test_split=None,
        split_mode="random",
        enforce_new_split=False,
        split_kwargs=None,
    ):
        """Shuffle and split all samples into three datasets."""
        assert (
            isinstance(split_mode, skms.BaseCrossValidator)
            or split_mode in ["random", "directory", "autogroup", "subject"]
            or "cv" in split_mode
        )
        if split_kwargs is None:
            split_kwargs = {}
        # Maybe retrieve list of samples from data definition
        if samples is None:
            samples = self.samples
        # Ensure paths are absolute and strings only
        samples = self._to_absolute_paths(samples, root_dir=self.tfrecord_dir)
        samples = self._to_filesystem_paths(samples)
        # Record dataset parameters
        dataset_params = {
            "shuffle": shuffle,
            "test_split": test_split,
            "valid_split": valid_split,
            "split_mode": str(split_mode),
        }
        dataset_params.update(
            {k: v if not callable(v) else v.__name__ for k, v in split_kwargs.items()}
        )
        if self.dataset_params is not None:
            # Compare dataset split to previous split
            if dataset_params != self.dataset_params:
                enforce_new_split = True
                self.warn(
                    "Changed dataset split parameters. Enforcing new dataset split."
                )
        self.dataset_params = dataset_params
        # Split samples into datasets
        if self.datasets is None or enforce_new_split:
            self.warn(
                "Enforcing a new dataset split. Ensure that you change "
                + 'the randomization seed given in Project(..., seed="name") '
                + "(defaults to project name) or change split parameters, "
                + "otherwise seed will reproduce the same random split as before."
            )
            # Split
            if split_mode == "directory":
                (
                    train_samples,
                    valid_samples,
                    test_samples,
                ) = split_samples_by_directory(samples, shuffle=shuffle)
            elif (
                valid_split is not None
                or test_split is not None
                or isinstance(
                    split_mode, (skms.BaseCrossValidator, skms.BaseShuffleSplit)
                )
            ):
                if split_mode == "random":
                    train_samples, valid_samples, test_samples = split_samples(
                        samples, split_off=[valid_split, test_split], shuffle=shuffle
                    )
                elif split_mode == "autogroup":
                    if not shuffle:
                        raise ValueError(
                            "split_mode='autogroup' requires shuffle=True."
                        )
                    (
                        train_samples,
                        valid_samples,
                        test_samples,
                    ) = split_samples_stratified_group(
                        samples, split_off=[valid_split, test_split], **split_kwargs
                    )
                elif split_mode == "subject":
                    (
                        train_samples,
                        valid_samples,
                        test_samples,
                    ) = split_samples_by_subjects(
                        samples,
                        valid_split=valid_split,
                        test_split=test_split,
                        **split_kwargs,
                    )
                elif (
                    isinstance(
                        split_mode, (skms.BaseCrossValidator, skms.BaseShuffleSplit)
                    )
                    or "cv" in split_mode
                ):
                    if test_split:
                        develop_samples, test_samples = split_samples(
                            samples, split_off=[test_split], **split_kwargs
                        )
                    else:
                        develop_samples = samples
                        test_samples = []

                    if isinstance(
                        split_mode, (skms.BaseCrossValidator, skms.BaseShuffleSplit)
                    ):
                        cv = split_mode
                    elif "leave-one-out" in split_mode:
                        cv = skms.LeaveOneOut(**split_kwargs)
                    elif "leave-p-out" in split_mode:
                        cv = skms.LeavePOut(**split_kwargs)
                    elif "shuffle-split" in split_mode:
                        cv = skms.ShuffleSplit(**split_kwargs, test_size=valid_split)
                    elif "stratified-shuffle-split" in split_mode:
                        cv = skms.StratifiedShuffleSplit(
                            **split_kwargs, test_size=valid_split
                        )
                    elif "kfold" in split_mode:
                        n_splits = int(1.0 / valid_split)
                        if abs(1.0 / float(n_splits) - valid_split) > 1e-3:
                            self.warn(
                                "Cross validation number of folds deviates from "
                                + "desired validation split."
                            )
                        cv = skms.KFold(**split_kwargs, n_splits=n_splits)
                    else:
                        raise ValueError(
                            f"Unknown cross validation split: {split_mode}."
                        )
                    folds = cv.split(develop_samples)
                    train_idx, valid_idx = list(zip(*folds))
                    train_samples = [
                        [develop_samples[i] for i in idx] for idx in train_idx
                    ]
                    valid_samples = [
                        [develop_samples[i] for i in idx] for idx in valid_idx
                    ]
                else:
                    raise ValueError(f"Unknown split mode: {split_mode:s}.")
            else:
                self.warn(
                    "Dataset should be split to check for generalization "
                    + "during training."
                )
                train_samples = samples
                valid_samples = []
                test_samples = []

        elif isinstance(self.datasets, dict):
            # Dataset has already been shuffled and split. Do not change
            self.log(
                "Reusing dataset split from previous run. "
                + "Change project name or set enforce_new_split=True for new split."
            )
            # Read paths from dictionary
            train_samples = self.datasets["train"]
            valid_samples = self.datasets["valid"]
            test_samples = self.datasets["test"]
        else:
            raise NotImplementedError(
                f"Project.dataset of {str(type(self.dataset))} not supported."
            )
        # Ensure paths are absolute and passed on as strings
        train_samples = self._to_absolute_paths(
            train_samples, root_dir=self.tfrecord_dir
        )
        valid_samples = self._to_absolute_paths(
            valid_samples, root_dir=self.tfrecord_dir
        )
        test_samples = self._to_absolute_paths(test_samples, root_dir=self.tfrecord_dir)
        train_samples = self._to_filesystem_paths(train_samples)
        valid_samples = self._to_filesystem_paths(valid_samples)
        test_samples = self._to_filesystem_paths(test_samples)
        # Save in dictionary
        self.datasets = {
            "train": train_samples,
            "valid": valid_samples,
            "test": test_samples,
        }
        # Serialize to file for persistent split across architectures
        self.to_json()
        return train_samples, valid_samples, test_samples

    def start_tensorboard_daemon(self, host="127.0.0.1", port="3333"):
        """Start detached tensorboard instance on the result directory."""
        tensorboard = tb.program.TensorBoard()
        tensorboard.configure(
            argv=[
                None,
                "--logdir",
                os.fspath(self.result_dir),
                "--port",
                str(port),
                "--host",
                str(host),
            ]
        )
        return tensorboard.launch()

    def write_pid(self):
        pid = os.getpid()
        path = Path(self.root_dir) / f"{pid:d}.pid"
        path.touch()

    def delete_pid(self, pid=None):
        if pid is None:
            pid = os.getpid()
        path = self.root_dir / f"{pid:d}.pid"
        path.unlink()

    def kill(self, pid=None, sig=None):
        if sig is None:
            sig = [signal.SIGINT, signal.SIGTERM]
        if pid is None:
            candidates = self.root_dir.glob("*.pid")
            for c in candidates:
                pid = int(c.stem)
                self._kill_process(pid, sig)
                c.unlink()
        else:
            self._kill_process(pid, sig)

    def _kill_process(self, pid, sigs, delay=3.0):
        pid = int(pid)
        if not isinstance(sigs, (list, tuple)):
            sigs = [sigs]
        for sig in sigs:
            try:
                os.kill(pid, sig)
            except (ProcessLookupError, OSError):
                pass
            time.sleep(delay)
            if not psutil.pid_exists(pid):
                self.log(f"Interrupted detached process: {pid}.")
                break

    def to_json(
        self, file=None, init_params=None, run_params=None, write_data_definition=True
    ):
        """Serialize project to a json file.

        Args:
            file:                   a json file to serialize Features into
            init_params:            dictionary of entries to add to init parameters
            run_params:             dictionary of entries to add to runtime parameters
            write_data_definition:  whether to also serialize the data definition
        """
        if file != "dict":
            if file is not None:
                file = Path(file)
            else:
                file = self.project_file
        else:
            # Do not write data definition if requested
            write_data_definition = False
        # Initialization parameters
        if init_params is None:
            init_params = {}
        init_params["name"] = self.name
        init_params["seed"] = self.seed
        if self.log_file:
            init_params["log_file"] = self._to_generic_paths(
                self._to_relative_paths(self.log_file, root_dir=self.root_dir)
            )
        else:
            init_params["log_file"] = None
        init_params["root"] = self._to_generic_paths(self.root_dir)
        init_params["runscript"] = self._runscript
        init_params["gitcommit"] = self.gitcommit
        # Run parameters
        if run_params is None:
            run_params = {}
        run_params["input_dir"] = self._to_generic_paths(
            self._to_relative_paths(self.input_dir, root_dir=self.root_dir)
        )
        run_params["src_dir"] = self._to_generic_paths(
            self._to_relative_paths(self.src_dir, root_dir=self.root_dir)
        )
        if self._tfrecord_dir is not None:
            run_params["tfrecord_dir"] = self._to_generic_paths(
                self._to_relative_paths(self._tfrecord_dir, root_dir=self.root_dir)
            )
        else:
            run_params["tfrecord_dir"] = None
        run_params["result_dir"] = self._to_generic_paths(
            self._to_relative_paths(self.result_dir, root_dir=self.root_dir)
        )
        run_params["log_dir"] = self._to_generic_paths(
            self._to_relative_paths(self.log_dir, root_dir=self.root_dir)
        )
        run_params["plot_dir"] = self._to_generic_paths(
            self._to_relative_paths(self.plot_dir, root_dir=self.root_dir)
        )
        # Serialize data definition
        if self._data_definition is not None:
            if write_data_definition:
                self._data_definition.to_json()
            run_params["data_definition_file"] = self._to_generic_paths(
                self._to_relative_paths(
                    self._data_definition.file, root_dir=self.root_dir
                )
            )
            run_params[
                "data_definition.input_features"
            ] = self._data_definition.input_features
            run_params[
                "data_definition.output_features"
            ] = self._data_definition.output_features
            run_params[
                "data_definition.merge_strategy"
            ] = self._data_definition.merge_strategy
        # Serialize dataset split
        if self.datasets:
            run_params["datasets"] = {
                k: self._to_generic_paths(
                    self._to_relative_paths(v, root_dir=self.tfrecord_dir)
                )
                for k, v in self.datasets.items()
            }
            run_params["dataset_params"] = self.dataset_params
        # Create json file
        json_dict = {"init_params": init_params, "run_params": run_params}
        if file != "dict":
            file.parent.mkdir(parents=True, exist_ok=True)
            with file.open("w", encoding="utf8") as f:
                f.write(
                    json.dumps(
                        json_dict, sort_keys=True, indent=4, separators=(",", ": ")
                    )
                )
        return json_dict

    @classmethod
    def from_json(cls, file, log_file="auto"):
        """Instantiates Project from json file.

        Contains a list of features and defines how they are merged together
        into inputs and outputs for CIDSModels. Provides access to shared axes
        and tfrecord feature instructions for the reading of tfrecord files.
        Can be serialized to and instantiated from a json file.

        Args:
            file:               a json file containing a serialized Project
            log_file:            new log file
        """
        file = Path(file)
        with file.open("r", encoding="utf8") as ofile:
            json_dict = json.load(ofile)
        # Legacy hacks
        try:
            json_dict["init_params"]["log_file"] = json_dict["init_params"]["logfile"]
        except KeyError:
            pass
        # Overwrite log file
        if log_file is not None and "log_file" in json_dict["init_params"].keys():
            json_dict["init_params"]["log_file"] = log_file
        # Instantiate class
        inst = cls(**json_dict["init_params"], _params=json_dict["run_params"])
        return inst

    def _get_parent_and_attribute(self, key):
        if "." in key:
            obj, _ = self._get_parent_and_attribute(key[: key.rindex(".")])
            return obj, key[key.rindex(".") + 1 :]
        return self, key

    def _overwrite_run_parameters(self, run_params):
        for key, value in run_params.items():
            if key == "data_definition":
                self.warn(
                    "Data definition is no longer stored directly in projects. "
                    + "Ignoring stored data definition to discover anew in project "
                    + "directory."
                )
                continue
            obj, attr = self._get_parent_and_attribute(key)
            setattr(obj, attr, value)

    def _update_params(self, _params):
        # Directories before other params since other run params need
        self._overwrite_run_parameters(
            {
                k: self._to_pathlib_paths(v)
                for k, v in _params.items()
                if k.endswith("_dir")
            }
        )
        self.data_definition.input_features = _params.get(
            "data_definition.input_features", []
        )
        self.data_definition.output_features = _params.get(
            "data_definition.output_features", []
        )
        self._overwrite_run_parameters(
            {
                k: self._to_pathlib_paths(v) if k.endswith("_file") else v
                for k, v in _params.items()
                if not k.endswith("_dir")
            }
        )

    def _get_gitcommit(self):
        try:
            return (
                subprocess.check_output(
                    ["git", "describe", "--always"], cwd=os.path.dirname(__file__)
                )
                .strip()
                .decode()
            )
        except (subprocess.CalledProcessError, FileNotFoundError):
            return "[ could not be identified... git installed? ]"

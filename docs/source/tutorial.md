Tutorial and examples
=====================

``` {literalinclude} ../../examples/A1_convert_mnist.py
    :caption: Convert data to tfrecords
    :language: python
    :linenos:
```

``` {literalinclude} ../../examples/A2_train_mnist_dcgan.py
    :caption: Train on tfrecords
    :language: python
    :linenos:
```

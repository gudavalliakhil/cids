CIDS overview
====================

```{eval-rst}
.. toctree::
    :maxdepth: 1

    readme_link.md
    tutorial.md
    api.rst
    license_link.md
    appendix.md
```

```{include} readme_link.md
```

```{include} tutorial.md
```

```{include} license_link.md
```

```{include} appendix.md
```

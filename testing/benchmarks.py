# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Benchmarks for the CIDSModelTF. Part of the CIDS toolbox."""
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import numpy as np
import tensorflow as tf
import shutil
import inspect

# from pathlib import Path
from tensorflow.python.platform import test
from cids.tensorflow.model import CIDSModel
from cids.tensorflow.model import create_legacy_data_definition
from cids.data import DataDefinition, Feature
from testing.src.create_test_dataset import (
    # _NF_dataset,
    # _NSF_dataset,
    # _NSF_binary_class_dataset,
    # _NSF_categorical_class_dataset,
    # _NXYF_binary_class_dataset,
    # _NXYF_categorical_class_dataset,
    _benchmark_dataset,
    # _normalize_test_dataset,
)


DEBUG = True


class Benchmarks(test.TestCase):
    """Tester for the neural_network module."""

    # TODO: Assertions

    def _core_model_NF(self, num_outputs):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Dense(5, activation="relu"))
        model.add(tf.keras.layers.Dense(10, activation="relu"))
        model.add(tf.keras.layers.Dense(num_outputs, activation=None))
        return model

    def _core_model_NF_classification(self, num_outputs, final_activiation="sigmoid"):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Dense(5, activation="relu"))
        model.add(tf.keras.layers.Dense(10, activation="relu"))
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activiation))
        return model

    def _core_model_NSF(self, num_outputs):
        model = tf.keras.models.Sequential()
        model.add(
            tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(5, activation="relu"))
        )
        model.add(
            tf.keras.layers.TimeDistributed(
                tf.keras.layers.Dense(10, activation="relu")
            )
        )
        model.add(tf.keras.layers.LSTM(num_outputs, return_sequences=True))
        model.add(
            tf.keras.layers.TimeDistributed(
                tf.keras.layers.Dense(num_outputs, activation=None)
            )
        )
        return model

    def _core_model_NSF_classification(self, num_outputs, final_activation="sigmoid"):
        model = tf.keras.models.Sequential()
        model.add(
            tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(5, activation="relu"))
        )
        model.add(tf.keras.layers.LSTM(10, return_sequences=False))
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activation))
        return model

    def _core_model_NXYF_classification(self, num_outputs, final_activation="sigmoid"):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Conv2D(10, 3, padding="same", activation=tf.nn.relu))
        model.add(tf.keras.layers.Conv2D(10, 3, padding="same", activation=tf.nn.relu))
        model.add(tf.keras.layers.Conv2D(10, 3, padding="same", activation=tf.nn.relu))
        model.add(tf.keras.layers.MaxPool2D(2))
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activation))
        return model

    def _core_model_mnist_NXYF_classification(
        self, num_outputs, final_activation="sigmoid"
    ):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(128, activation="relu"))
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activation))
        return model

    # TODO: real use cases

    def benchmark_regression_NF(self):
        """Test on the benchmark regression problem."""
        # TODO: use project
        name = inspect.currentframe().f_code.co_name
        result_dir = os.path.join("RESULTS", "test", name)
        shutil.rmtree(result_dir, ignore_errors=True)
        # Data
        dataset, data_shape, data_format = _benchmark_dataset()
        input_idx = [0]
        output_idx = [1, 2]
        train_data = dataset
        valid_data = dataset
        # Training
        schedule = {}
        schedule["batch_size"] = [2, 4, 32]
        schedule["count"] = [2, 4, 10]
        schedule["learning_rate"] = [1e-2, 1e-3, 1e-4]
        # Architecture
        model = self._core_model_NF(len(output_idx))
        data_definition = create_legacy_data_definition(
            data_shape, data_format, input_idx, output_idx
        )
        cids_nn = CIDSModel(
            data_definition,
            model,
            report_freq=1,
            save_freq=10,
            result_dir=result_dir,
            name=name,
        )
        cids_nn.data_reader.src_type = "placeholder"
        cids_nn.data_reader.slice_tensors = True
        history = cids_nn.train(train_data, valid_data, schedule)
        print(history)
        print("done")

    def benchmark_mnist_categorical_classification_train_NXYF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        result_dir = os.path.join("RESULTS", "test")
        shutil.rmtree(result_dir, ignore_errors=True)
        # Data
        (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
        # Add feature axis
        dataset = (x_train[..., None], y_train[..., None])
        dataset_test = (x_test[..., None], y_test[..., None])
        num_classes = int(np.max(y_train)) + 1
        train_data = dataset
        valid_data = dataset_test
        # Architecture
        model = self._core_model_mnist_NXYF_classification(
            num_classes, final_activation=tf.nn.softmax
        )
        data_shapes = [[None] + list(d.shape[1:]) for d in dataset]
        data_definition = DataDefinition(
            Feature("input", data_shapes[0], "NXYF", dtype=tf.uint8),
            Feature("output", data_shapes[1], "NF", dtype=tf.uint8),
            input_features=["input"],
            output_features=["output"],
        )
        cids_nn = CIDSModel.categorical_classification(
            num_classes,
            data_definition,
            model,
            report_freq=1,
            save_freq=1,
            result_dir=result_dir,
            name=name,
        )
        cids_nn.metrics.append("accuracy")
        cids_nn.data_reader.src_type = "placeholder"
        cids_nn.data_reader.split_batch = False
        cids_nn.data_reader.slice_tensors = True
        # cids_nn.count_mode = "steps"
        schedule = {"batch_size": 32, "learning_rate": 3e-3, "count": [1, 4]}
        history = cids_nn.train(train_data, valid_data, schedule)
        assert history["accuracy"][-1] > history["accuracy"][0]
        print("done")


if __name__ == "__main__":
    test.main()

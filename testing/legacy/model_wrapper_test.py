# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""CIDS computational intelligence library"""
import os
import shutil

import numpy as np
import tensorflow as tf
from tensorflow.python.platform import test

from cids.legacy.nn.fully_connected import NeuralNetwork
from cids.legacy.nn.model_wrapper import ModelWrapper
from cids.legacy.nn.recurrent import RecNet
from testing.src.create_test_dataset import _NF_dataset
from testing.src.create_test_dataset import _NSF_dataset


DEBUG = True


class ModelWrapperTest(test.TestCase):
    """Tester for the neural_network module."""

    def test_train_fully_connected(self):
        """Tester for NeuralNetwork."""
        # Clean up
        NN = NeuralNetwork
        name = "TestModelWrapper" + NN.__name__
        result_dir = "RESULTS/test"
        shutil.rmtree(os.path.join(result_dir, name), ignore_errors=True)
        # Data
        train_paths = [
            "tests/src/tfrecord/2D_dataset.tfrecord",
            "tests/src/tfrecord/2D_dataset.tfrecord",
        ]
        valid_paths = ["tests/src/tfrecord/2D_dataset.tfrecord"]
        dataset, data_shape, data_format = _NF_dataset()
        input_idx = [0, 1]
        output_idx = [2, 3, 4]
        # Architecture
        layers = [("fc_relu", 5), ("fc_relu", 10), ("fc_out", -1)]
        # Settings
        batch_size_schedule = [2, 4, 4]
        num_steps_schedule = [100] * len(batch_size_schedule)
        dr_kwargs = dict()
        dr_kwargs["slice_tensors"] = True
        nn_kwargs = dict()
        report_steps = 10
        # Train
        with ModelWrapper(
            NN,
            layers,
            data_shape,
            data_format,
            input_idx,
            output_idx,
            nn_kwargs=nn_kwargs,
            dr_kwargs=dr_kwargs,
            report_steps=report_steps,
            save_steps=report_steps,
            name=name,
        ) as model:
            e, ev, ev_min, = model.training_schedule(
                train_paths,
                valid_paths,
                batch_size_schedule=batch_size_schedule,
                num_steps_schedule=num_steps_schedule,
            )

    def test_train_classificator(self):
        # Clean up
        NN = NeuralNetwork
        name = "TestClassificator" + NN.__name__
        result_dir = "RESULTS/test"
        shutil.rmtree(os.path.join(result_dir, name), ignore_errors=True)
        # Data
        data = tf.keras.datasets.mnist
        (train_images, train_labels), (test_images, test_labels) = data.load_data()
        # Preprocess
        train_images = train_images.astype("float64") / 255.0
        train_labels = train_labels.astype("float64")
        train_data = np.concatenate(
            [
                np.reshape(train_images, [train_images.shape[0], -1]),
                np.reshape(train_labels, [train_images.shape[0], -1]),
            ]
        )
        test_images = test_images.astype("float64") / 255.0
        test_labels = test_labels.astype("float64")
        test_data = np.concatenate(
            [
                np.reshape(test_images, [test_images.shape[0], -1]),
                np.reshape(test_labels, [test_images.shape[0], -1]),
            ]
        )
        input_idx = list(range(0, 28 * 28))
        output_idx = [28 * 28]
        # Architecture
        layers = [
            tf.keras.layers.Dense(128, activation=tf.nn.relu),
            tf.keras.layers.Dense(10, activation=tf.nn.softmax),
        ]
        # Settings
        data_shape = [None] + train_data.shape[1:]
        data_format = "NF"
        batch_size_schedule = [64, 64]
        num_steps_schedule = [1000, 10000]
        dr_kwargs = dict()
        dr_kwargs["src_type"] = "placeholder"
        dr_kwargs["slice_tensors"] = True
        nn_kwargs = dict()
        report_steps = 10
        # Train
        with ModelWrapper(
            NN,
            layers,
            data_shape,
            data_format,
            input_idx,
            output_idx,
            nn_kwargs=nn_kwargs,
            dr_kwargs=dr_kwargs,
            report_steps=report_steps,
            save_steps=report_steps,
            name=name,
        ) as model:
            e, ev, ev_min, = model.training_schedule(
                train_data,
                test_data,
                batch_size_schedule=batch_size_schedule,
                num_steps_schedule=num_steps_schedule,
            )

    def test_infer(self):
        """Tester for NeuralNetwork."""
        # Clean up
        NN = NeuralNetwork
        name = "TestModelWrapper" + NN.__name__
        result_dir = "RESULTS/test"
        shutil.rmtree(os.path.join(result_dir, name), ignore_errors=True)
        # Data
        paths = ["tests/src/tfrecord/2D_dataset.tfrecord"]
        dataset, data_shape, data_format = _NF_dataset()
        input_idx = [0, 1]
        output_idx = [2, 3]
        # Architecture
        layers = [("fc_out", -1)]
        # Settings
        dr_kwargs = dict()
        dr_kwargs["src_type"] = "file"
        dr_kwargs["slice_tensors"] = True
        nn_kwargs = dict()
        nn_kwargs["result_dir"] = result_dir
        batch_size_schedule = [2, 2]
        num_steps_schedule = [100] * len(batch_size_schedule)
        # Train to get fixed checkpoint
        with ModelWrapper(
            NN,
            layers,
            data_shape,
            data_format,
            input_idx,
            output_idx,
            nn_kwargs=nn_kwargs,
            dr_kwargs=dr_kwargs,
            report_steps=10,
            save_steps=num_steps_schedule[0],
            name=name,
        ) as model:
            model.training_schedule(
                paths,
                paths,
                batch_size_schedule=batch_size_schedule,
                num_steps_schedule=num_steps_schedule,
            )
            X0, Y0, Y0_ = model.infer(
                test_src=paths, test_batch_size=data_shape[0], use_feed=False
            )
        # Infer from file after restart
        with ModelWrapper(
            NN,
            layers,
            data_shape,
            data_format,
            input_idx,
            output_idx,
            nn_kwargs=nn_kwargs,
            dr_kwargs=dr_kwargs,
            report_steps=10,
            save_steps=num_steps_schedule[0],
            name=name,
        ) as model:
            X1, Y1, Y1_ = model.infer(
                test_src=paths, test_batch_size=data_shape[0], use_feed=False
            )
        # Infer from placeholder
        dr_kwargs["src_type"] = "placeholder"
        with ModelWrapper(
            NN,
            layers,
            data_shape,
            data_format,
            input_idx,
            output_idx,
            nn_kwargs=nn_kwargs,
            dr_kwargs=dr_kwargs,
            report_steps=10,
            save_steps=num_steps_schedule[0],
            name=name,
        ) as model:
            X2, Y2, Y2_ = model.infer(test_src=dataset, use_feed=False)
            X3, Y3, Y3_ = model.infer(test_src=dataset, use_feed=True)
            X4, Y4, Y4_ = model.infer(test_src=dataset, use_feed=True)
        # Asserts
        # np.testing.assert_array_almost_equal(X0, X1, decimal=5)
        # np.testing.assert_array_almost_equal(Y0, Y1, decimal=5)
        # np.testing.assert_array_almost_equal(Y0_, Y1_, decimal=5)
        np.testing.assert_array_almost_equal(X1, X2, decimal=4)
        np.testing.assert_array_almost_equal(Y1, Y2, decimal=4)
        np.testing.assert_array_almost_equal(Y1_, Y2_, decimal=4)
        np.testing.assert_array_almost_equal(X1, X3, decimal=4)
        np.testing.assert_array_almost_equal(Y1, Y3, decimal=4)
        np.testing.assert_array_almost_equal(Y1_, Y3_, decimal=4)
        np.testing.assert_allclose(X3, X4, rtol=1e-6, atol=1e-6)
        np.testing.assert_allclose(Y3, Y4, rtol=1e-6, atol=1e-6)
        np.testing.assert_allclose(Y3_, Y4_, rtol=1e-6, atol=1e-6)

    def test_infer_3D(self):
        """Tester for NeuralNetwork."""
        # Clean up
        NN = RecNet
        name = "TestModelWrapper" + NN.__name__
        result_dir = "RESULTS/test"
        shutil.rmtree(os.path.join(result_dir, name), ignore_errors=True)
        # Data
        train_paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = [
            "tests/src/tfrecord/3D_dataset_sample_00.tfrecord",
            "tests/src/tfrecord/3D_dataset_sample_01.tfrecord",
        ]
        dataset, data_shape, data_format = _NSF_dataset()
        input_idx = [0, 1]
        output_idx = [2, 3]
        # Architecture
        layers = [
            tf.keras.layers.LSTM(10, return_sequences=True),
            tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(2)),
        ]
        # Settings
        dr_kwargs = dict()
        dr_kwargs["src_type"] = "file"
        dr_kwargs["slice_tensors"] = False
        nn_kwargs = dict()
        nn_kwargs["result_dir"] = result_dir
        batch_size_schedule = [2, 2]
        num_steps_schedule = [100] * len(batch_size_schedule)
        # # Train to get fixed checkpoint
        # with ModelWrapper(NN, layers, data_shape, data_format, input_idx,
        #                   output_idx, nn_kwargs=nn_kwargs, dr_kwargs=dr_kwargs,
        #                   report_steps=10, save_steps=num_steps_schedule[0],
        #                   name=name) as model:
        #     model.training_schedule(
        #         train_paths, valid_paths,
        #         batch_size_schedule=batch_size_schedule,
        #         num_steps_schedule=num_steps_schedule)
        #     X0, Y0, Y0_ = model.infer(
        #         test_src=valid_paths, test_batch_size=data_shape[0],
        #         use_feed=False)
        # # Infer from file after restart
        # with ModelWrapper(NN, layers, data_shape, data_format, input_idx,
        #                   output_idx, nn_kwargs=nn_kwargs, dr_kwargs=dr_kwargs,
        #                   report_steps=10, save_steps=num_steps_schedule[0],
        #                   name=name) as model:
        #     X1, Y1, Y1_ = model.infer(
        #         test_src=valid_paths, test_batch_size=data_shape[0],
        #         use_feed=False)
        # Infer from placeholder
        dr_kwargs["slice_tensors"] = True
        dr_kwargs["src_type"] = "placeholder"
        with ModelWrapper(
            NN,
            layers,
            data_shape,
            data_format,
            input_idx,
            output_idx,
            nn_kwargs=nn_kwargs,
            dr_kwargs=dr_kwargs,
            report_steps=10,
            save_steps=num_steps_schedule[0],
            name=name,
        ) as model:
            X2, Y2, Y2_ = model.infer(test_src=dataset, use_feed=False)
            X3, Y3, Y3_ = model.infer(test_src=dataset, use_feed=True)
            X4, Y4, Y4_ = model.infer(test_src=dataset, use_feed=True)
        # Asserts
        # np.testing.assert_array_almost_equal(X0, X1, decimal=5)
        # np.testing.assert_array_almost_equal(Y0, Y1, decimal=5)
        # np.testing.assert_array_almost_equal(Y0_, Y1_, decimal=5)
        np.testing.assert_array_almost_equal(X1, X2, decimal=4)
        np.testing.assert_array_almost_equal(Y1, Y2, decimal=4)
        np.testing.assert_array_almost_equal(Y1_, Y2_, decimal=4)
        np.testing.assert_array_almost_equal(X1, X3, decimal=4)
        np.testing.assert_array_almost_equal(Y1, Y3, decimal=4)
        np.testing.assert_array_almost_equal(Y1_, Y3_, decimal=4)
        np.testing.assert_allclose(X3, X4, rtol=1e-6, atol=1e-6)
        np.testing.assert_allclose(Y3, Y4, rtol=1e-6, atol=1e-6)
        np.testing.assert_allclose(Y3_, Y4_, rtol=1e-6, atol=1e-6)


if __name__ == "__main__":
    test.main()

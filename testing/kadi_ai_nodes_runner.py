# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Runner to manually test KadiAI / CIDS nodes for Kadi's workflow engine.
Part of the CIDS toolbox and KadiAI.
"""
from tempfile import TemporaryDirectory

from kadi_ai.nodes.commands import control_center  # pylint: disable=unused-import
from kadi_ai.nodes.commands import define_data  # pylint: disable=unused-import
from kadi_ai.nodes.commands import define_model  # pylint: disable=unused-import

if __name__ == "__main__":

    with TemporaryDirectory(prefix="cids-workflow-") as tmpdir:

        print("Temporary project directory:", tmpdir)

        define_data.callback(
            project_name="FatigAI",
            project_dir="./PROJECT/",
            src_identifier="fatigue-data-ldpe",
            url="127.0.0.1:3333",
        )

        # define_model.callback(
        #     project_name="fatigAI",
        #     project_dir=tmpdir,
        #     tfrecord_identifier="cids-fatigAI-tfrecords",
        #     url="127.0.0.1:3333",
        # )

        # control_center.callback(
        #     project_name="fatigAI",
        #     project_dir=tmpdir,
        #     tfrecord_identifier="cids-fatigAI-results",
        #     url="127.0.0.1:3333",
        # )

# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tester for the data module. Part of the CIDS toolbox and KadiAI."""
from pathlib import Path
from tempfile import TemporaryDirectory

import numpy as np
import tensorflow as tf
from tensorflow.python.platform import test

from cids.data.definition import DataDefinition
from cids.data.definition import Feature
from cids.data.preprocessor import OptionsAxis
from cids.data.preprocessor import Preprocessor
from cids.data.reader import DataReader
from cids.data.writer import DataWriter
from testing.src.create_test_dataset import _NF_dataset


DEBUG = True


class DataTest(test.TestCase):
    """Tester for the datareader module."""

    def test_data_definition(self):
        # Instantiate data definition
        data_definition = DataDefinition(
            Feature("test_input1", [None, 2]),
            Feature("test_input2", [None, 3]),
            Feature("test_target", [None, 1]),
            input_features=["test_input1", "test_input2"],
            output_features=["test_target"],
            merge_strategy="all",
        )
        print(data_definition.data_shape)
        print(data_definition.data_format)

        data_definition2 = DataDefinition(
            Feature("test_input1", [None, 2]),
            Feature("test_input2", [None, 3]),
            Feature("test_target", [None, 1]),
            input_features=["test_input1", "test_input2"],
            output_features=["test_target"],
            merge_strategy="format",
        )
        print(data_definition2.data_shape)
        print(data_definition2.data_format)

        assert data_definition == data_definition2

        data_definition3 = DataDefinition(
            Feature("test_input1", [None, 2], "NF"),
            Feature("test_input2", [None, 3], "NF"),
            Feature("test_input3", [None, 100, 3], "NSF"),
            Feature("test_target", [None, 1]),
            input_features=["test_input1", "test_input2", "test_input3"],
            output_features=["test_target"],
            merge_strategy="format",
        )
        print(data_definition3.data_shape)
        print(data_definition3.data_format)

        assert data_definition != data_definition3

    def test_data_writer_data_reader(self):
        dataset, _, data_format = _NF_dataset(write=False)
        # Gather key wise
        samples = [
            {"inputs1": d[..., :1], "inputs2": d[..., 1:3], "targets": d[..., 3:]}
            for d in dataset
        ]
        # Create data definition
        data_definition = DataDefinition()
        for k, v in samples[0].items():
            data_definition.add_feature(
                Feature(k, [None, v.shape[-1]], data_format=data_format)
            )
        # Write to temporary directory
        with TemporaryDirectory(prefix="cids-test-") as tmpdir:
            tmpdir = Path(tmpdir)
            data_definition_file = tmpdir / "data_definition.json"
            data_writer = DataWriter(data_definition)
            data_writer.write_samples(samples, tmpdir)
            data_writer.write_data_definition(data_definition_file)

            # Assert written files
            assert len(list(tmpdir.glob("*.tfrecord"))) == len(samples)
            assert len(list(tmpdir.glob("*.json"))) == 1

            # Read from directory
            data_definition_new = DataDefinition.from_json(data_definition_file)
            data_reader = DataReader(data_definition_new, cast_dtype=tf.float64)
            tfrecords = sorted(list(tmpdir.glob("*.tfrecord")))
            samples = data_reader.read_tfrecords(tfrecords)

            # Assert read data
            assert np.array_equal(samples["inputs1"], dataset[..., :1])
            assert np.array_equal(samples["inputs2"], dataset[..., 1:3])
            assert np.array_equal(samples["targets"], dataset[..., 3:])

    def test_data_processor_recording(self):
        dataset, _, data_format = _NF_dataset(write=False)
        # Gather key wise
        samples = [
            {"inputs1": d[..., :1], "inputs2": d[..., 1:3], "targets": d[..., 3:]}
            for d in dataset
        ]
        # Create data definition
        data_definition = DataDefinition()
        for k, v in samples[0].items():
            data_definition.add_feature(
                Feature(k, [None, v.shape[-1]], data_format=data_format)
            )
        # Preprocess
        preprocessor = Preprocessor(data_definition)
        samples = [preprocessor.expand_sample_arrays(sample) for sample in samples]
        samples, data_definition = preprocessor.split_features(
            samples, features=("inputs2",), axis=OptionsAxis.F
        )
        samples, data_definition = preprocessor.drop_features(
            samples, features=("inputs2",)
        )
        samples, data_definition = preprocessor.rename_features(
            samples,
            features=("inputs2_F0", "inputs2_F1"),
            new_features=("inputs2.1", "inputs2.2"),
        )
        # Assert changes to data definition
        assert "inputs2.1" in data_definition.features
        assert "inputs2.2" in data_definition.features
        assert "inputs2" not in data_definition.features
        # Assert changes to samples
        for ns in samples:
            assert "inputs2.1" in ns
            assert "inputs2.2" in ns
            assert "inputs2" not in ns
        # Assert data consistency
        rows = [
            np.concatenate(
                (ns["inputs1"], ns["inputs2.1"], ns["inputs2.2"], ns["targets"]),
                axis=-1,
            )
            for ns in samples
        ]
        new_dataset = np.concatenate(rows, axis=0)
        assert np.array_equal(dataset, new_dataset)

    def test_data_processor_replay(self):
        dataset, _, data_format = _NF_dataset(write=False)
        # Gather key wise
        samples = [
            {"inputs1": d[..., :1], "inputs2": d[..., 1:3], "targets": d[..., 3:]}
            for d in dataset
        ]
        # Create data definition
        data_definition = DataDefinition()
        for k, v in samples[0].items():
            data_definition.add_feature(
                Feature(k, [None, v.shape[-1]], data_format=data_format)
            )
        # Preprocess
        preprocessor = Preprocessor(data_definition)
        preprocessor.add_preprocessing_step(
            ("split_features", (), {"features": ("inputs2",), "axis": "F"})
        )
        preprocessor.add_preprocessing_step(
            ("drop_features", (), {"features": ("inputs2",)})
        )
        preprocessor.add_preprocessing_step(
            (
                "rename_features",
                (),
                {
                    "features": ("inputs2_F0", "inputs2_F1"),
                    "new_features": ("inputs2.1", "inputs2.2"),
                },
            )
        )
        samples, data_definition = preprocessor.preprocess(samples)
        # Assert changes to data definition
        assert "inputs2.1" in data_definition.features
        assert "inputs2.2" in data_definition.features
        assert "inputs2" not in data_definition.features
        # Assert changes to samples
        for ns in samples:
            assert "inputs2.1" in ns
            assert "inputs2.2" in ns
            assert "inputs2" not in ns
        # Assert data consistency
        rows = [
            np.concatenate(
                (ns["inputs1"], ns["inputs2.1"], ns["inputs2.2"], ns["targets"]),
                axis=-1,
            )
            for ns in samples
        ]
        new_dataset = np.concatenate(rows, axis=0)
        assert np.array_equal(dataset, new_dataset)


if __name__ == "__main__":
    test.main()

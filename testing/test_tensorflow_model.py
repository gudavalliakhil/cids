# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tester for the tensorflow layers module. Part of the CIDS toolbox."""
import inspect
import os
from pathlib import Path
from tempfile import TemporaryDirectory

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import numpy as np
import tensorflow as tf
from kerastuner import HyperParameters
from tensorflow.python.platform import test

from cids.data.definition import DataDefinition
from cids.data.definition import Feature
from cids.tensorflow.model import CIDSModelTF
from cids.tensorflow.model import create_legacy_data_definition
from testing.src.create_test_dataset import _NF_dataset
from testing.src.create_test_dataset import _normalize_test_dataset
from testing.src.create_test_dataset import _NSF_binary_class_dataset
from testing.src.create_test_dataset import _NSF_categorical_class_dataset
from testing.src.create_test_dataset import _NSF_dataset
from testing.src.create_test_dataset import _NXYF_binary_class_dataset
from testing.src.create_test_dataset import _NXYF_categorical_class_dataset


DEBUG = True


class CIDSModelTFTest(test.TestCase):
    """Tester for the neural_network module."""

    # TODO: Assertions

    def _core_model_NF(self, num_outputs, max_width=10):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Dense(5, activation="relu"))
        model.add(tf.keras.layers.Dense(max_width, activation="relu"))
        model.add(tf.keras.layers.Dense(num_outputs, activation=None))
        return model

    def _core_model_NF_classification(self, num_outputs, final_activiation="sigmoid"):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Dense(5, activation="relu"))
        model.add(tf.keras.layers.Dense(10, activation="relu"))
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activiation))
        return model

    def _core_model_NSF(self, num_outputs):
        model = tf.keras.models.Sequential()
        model.add(
            tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(5, activation="relu"))
        )
        model.add(
            tf.keras.layers.TimeDistributed(
                tf.keras.layers.Dense(10, activation="relu")
            )
        )
        model.add(tf.keras.layers.LSTM(num_outputs, return_sequences=True))
        model.add(
            tf.keras.layers.TimeDistributed(
                tf.keras.layers.Dense(num_outputs, activation=None)
            )
        )
        return model

    def _core_model_NSF_classification(self, num_outputs, final_activation="sigmoid"):
        model = tf.keras.models.Sequential()
        model.add(
            tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(5, activation="relu"))
        )
        model.add(tf.keras.layers.LSTM(10, return_sequences=False))
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activation))
        return model

    def _core_model_NXYF_classification(self, num_outputs, final_activation="sigmoid"):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Conv2D(10, 3, padding="same", activation=tf.nn.relu))
        model.add(tf.keras.layers.Conv2D(10, 3, padding="same", activation=tf.nn.relu))
        model.add(tf.keras.layers.Conv2D(10, 3, padding="same", activation=tf.nn.relu))
        model.add(tf.keras.layers.MaxPool2D(2))
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activation))
        return model

    def _core_model_mnist_NXYF_classification(
        self, num_outputs, final_activation="sigmoid"
    ):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(128, activation="relu"))
        model.add(tf.keras.layers.Dense(num_outputs, activation=final_activation))
        return model

    def test_regression_train_NF_files(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name

        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            input_dir = Path(__file__).parent.joinpath("src", "tfrecord")
            dataset_path = os.fspath(input_dir.joinpath("2D_dataset.tfrecord"))
            train_data = [dataset_path] * 10
            valid_data = [dataset_path] * 3
            _, data_shape, data_format = _NF_dataset()
            input_idx = [0, 1]
            output_idx = [2, 3, 4]
            # Architecture
            model = self._core_model_NF(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.metrics.append("mae")
            cids_model.data_reader.slice_tensors = True
            schedule = {"count": [1, 21], "batch_size": 2, "learning_rate": 1e-3}
            # Train
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            # Infer results
            X1, Y1, Y1_ = cids_model.infer_data(train_data, batch_size=len(train_data))
            # Clean up
            cids_model.clear()
            del cids_model
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.data_reader.slice_tensors = True
            # Infer results
            X2, Y2, Y2_ = cids_model.infer_data(train_data, batch_size=len(train_data))
            # Asserts
            np.testing.assert_array_equal(X1, X2)
            np.testing.assert_array_equal(Y1, Y2)
            np.testing.assert_array_equal(Y1_, Y2_)
            print("done")

    def test_regression_train_NSF_files(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            input_dir = Path(__file__).parent.joinpath("src", "tfrecord")
            train_data = [
                os.fspath(f) for f in input_dir.glob("3D_dataset_sample_*.tfrecord")
            ]
            train_data = sorted(train_data)
            valid_data = train_data[:3]
            _, data_shape, data_format = _NSF_dataset()
            input_idx = [0, 1]
            output_idx = [2, 3, 4]
            # Architecture
            model = self._core_model_NSF(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.src_type = "file"
            cids_model.slice_tensors = True
            schedule = {"count": [1, 21], "batch_size": 2}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_regression_train_NF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            dataset, data_shape, data_format = _NF_dataset()
            input_idx = [0, 1]
            output_idx = [2, 3, 4]
            train_data = dataset
            valid_data = dataset
            # Architecture
            model = self._core_model_NF(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            schedule = {"count": [1, 21], "batch_size": 2}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_regression_train_NSF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            dataset, data_shape, data_format = _NSF_dataset()
            input_idx = [0, 1]
            output_idx = [2, 3, 4]
            train_data = dataset
            valid_data = dataset
            # Architecture
            model = self._core_model_NSF(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            schedule = {"count": [1, 21], "batch_size": 2}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_regression_infer_NF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            dataset, data_shape, data_format = _NF_dataset()
            input_idx = [0, 1]
            output_idx = [2, 3, 4]
            # Architecture
            model = self._core_model_NF(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                name=name,
                result_dir=result_dir,
            )
            # Run
            prediction = cids_model.predict(dataset[..., input_idx], checkpoint=None)
            print(prediction)
            print("done")

    def test_binary_classification_train_NF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            dataset, data_shape, data_format = _NF_dataset()
            input_idx = [0, 1]
            output_idx = [2]
            pseudo_classes = dataset[..., 0:1]
            pseudo_classes = np.asarray(
                pseudo_classes > np.mean(pseudo_classes), dtype=np.float64
            )
            dataset = np.concatenate([dataset[..., input_idx], pseudo_classes], axis=-1)
            dataset = dataset.astype("float64")
            data_shape = dataset.shape
            train_data = dataset
            valid_data = dataset
            # Architecture
            model = self._core_model_NF_classification(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF.binary_classification(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.metrics.append("accuracy")
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            cids_model.collapse_repeated_features = "outputs"
            schedule = {"count": [1, 21], "batch_size": 2}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_categorical_classification_train_NF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            num_classes = 3
            dataset, data_shape, data_format = _NF_dataset()
            input_idx = [0, 1]
            output_idx = [2]
            pseudo_classes = np.zeros_like(dataset[..., 0:1])
            boundaries = [
                np.float64(i / num_classes) * np.max(dataset[..., 0:1])
                for i in range(1, num_classes)
            ]
            for i, b in enumerate(boundaries):
                pseudo_classes[dataset[..., 0:1] > b] = np.float64(i + 1)
            dataset = np.asarray(
                np.concatenate([dataset[..., input_idx], pseudo_classes], axis=-1),
                dtype=np.float64,
            )
            dataset = dataset.astype("float64")
            data_shape = dataset.shape
            train_data = dataset
            valid_data = dataset
            # Architecture
            model = self._core_model_NF_classification(
                num_classes, final_activiation=tf.nn.softmax
            )
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF.categorical_classification(
                num_classes,
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.metrics.append("accuracy")
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            cids_model.collapse_repeated_features = "outputs"
            schedule = {"count": [1, 21], "batch_size": 2}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_binary_classification_train_NSF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            dataset, data_shape, data_format = _NSF_binary_class_dataset()
            train_data = dataset
            valid_data = dataset
            input_idx = [0, 1]
            output_idx = [2]
            # Architecture
            model = self._core_model_NSF_classification(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF.binary_classification(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.metrics.append("accuracy")
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            cids_model.collapse_repeated_features = "outputs"
            schedule = {"count": [1, 21], "batch_size": 2, "learning_rate": 3e-3}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_categorical_classification_train_NSF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            input_idx = [0, 1]
            output_idx = [2]
            num_classes = 3
            dataset, data_shape, data_format = _NSF_categorical_class_dataset(
                num_classes
            )
            train_data = dataset
            valid_data = dataset
            # Architecture
            model = self._core_model_NSF_classification(
                num_classes, final_activation=tf.nn.softmax
            )
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF.categorical_classification(
                num_classes,
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.metrics.append("accuracy")
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            cids_model.collapse_repeated_features = "outputs"
            schedule = {"count": [1, 21], "batch_size": 2}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_binary_classification_train_NXYF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            dataset, data_shape, data_format = _NXYF_binary_class_dataset()
            train_data = dataset
            valid_data = dataset
            input_idx = [0, 1]
            output_idx = [2]
            # Architecture
            model = self._core_model_NXYF_classification(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF.binary_classification(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.metrics.append("accuracy")
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            cids_model.collapse_repeated_features = "outputs"
            schedule = {"count": [1, 21], "batch_size": 2, "learning_rate": 3e-3}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_categorical_classification_train_NXYF_array(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            num_classes = 3
            dataset, data_shape, data_format = _NXYF_categorical_class_dataset(
                num_classes
            )
            train_data = dataset
            valid_data = dataset
            input_idx = [0, 1]
            output_idx = [2]
            # Architecture
            model = self._core_model_NXYF_classification(
                num_classes, final_activation=tf.nn.softmax
            )
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF.categorical_classification(
                num_classes,
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.metrics.append("accuracy")
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            cids_model.collapse_repeated_features = "outputs"
            schedule = {"count": [1, 21], "batch_size": 2, "learning_rate": 3e-3}
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            print("done")

    def test_hyperparameters(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name

        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            input_dir = Path(__file__).parent.joinpath("src", "tfrecord")
            dataset_path = os.fspath(input_dir.joinpath("2D_dataset.tfrecord"))
            _, data_shape, data_format = _NF_dataset()
            data_shape = list(data_shape)
            data_shape[data_format.index("N")] = None
            data_definition = DataDefinition(
                Feature("data", data_shape=data_shape, data_format=data_format),
                input_features=["data[0,1]"],
                output_features=["data[2,3,4]"],
            )
            train_data = [dataset_path] * 10
            valid_data = [dataset_path] * 3

            # Architecture
            def model_fun(hp: HyperParameters, data_definition: DataDefinition):
                max_width = hp.Int("max_width", 9, 11, default=10)
                return self._core_model_NF(
                    data_definition.num_output_features, max_width=max_width
                )

            # Create model
            cids_model = CIDSModelTF(
                data_definition,
                model_fun,
                report_freq=1,
                save_freq=1,
                result_dir=result_dir,
                name=name,
            )
            cids_model.data_reader.slice_tensors = True
            schedule = {"count": [1, 21], "batch_size": 2, "learning_rate": 1e-3}
            # Train
            history = cids_model.train(train_data, valid_data, schedule)
            assert history["loss"][-1] < history["loss"][0]
            # Infer results
            X1, Y1, Y1_ = cids_model.infer_data(train_data, batch_size=len(train_data))
            # Clean up
            cids_model.clear()
            del cids_model
            # Create model
            cids_model = CIDSModelTF(
                data_definition,
                model_fun,
                report_freq=1,
                save_freq=1,
                result_dir=result_dir,
                name=name,
            )
            cids_model.data_reader.slice_tensors = True
            # Infer results
            X2, Y2, Y2_ = cids_model.infer_data(train_data, batch_size=len(train_data))
            # Asserts
            np.testing.assert_array_equal(X1, X2)
            np.testing.assert_array_equal(Y1, Y2)
            np.testing.assert_array_equal(Y1_, Y2_)
            # Clean up
            cids_model.clear()
            del cids_model
            # Create model
            hp = HyperParameters()
            core_model = model_fun(hp, data_definition)
            cids_model = CIDSModelTF(
                data_definition,
                core_model,
                report_freq=1,
                save_freq=1,
                result_dir=result_dir,
                name=name,
            )
            cids_model.data_reader.slice_tensors = True
            # Infer results
            X3, Y3, Y3_ = cids_model.infer_data(train_data, batch_size=len(train_data))
            # Asserts
            np.testing.assert_array_equal(X1, X3)
            np.testing.assert_array_equal(Y1, Y3)
            np.testing.assert_array_equal(Y1_, Y3_)
            print("done")

    def test_save_load(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            input_dir = Path(__file__).parent.joinpath("src", "tfrecord")
            train_data = [
                os.fspath(f) for f in input_dir.glob("3D_dataset_sample_*.tfrecord")
            ]
            train_data = sorted(train_data)
            valid_data = train_data[:3]
            dataset, data_shape, data_format = _NSF_dataset()
            input_idx = [0, 1]
            output_idx = [2, 3, 4]
            # Architecture
            model = self._core_model_NSF(len(output_idx))

            print("Initial training")
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                save_freq=10,
                report_freq=1,
                result_dir=result_dir,
                name=name,
            )
            cids_model.src_type = "file"
            cids_model.save_best_only = False
            cids_model.train_phase(
                train_data, valid_data, 10, batch_size=2, checkpoint=None
            )
            weights_initial1 = cids_model.core_model.get_weights()

            print("Manual save and reload")
            cids_model.save("tmp")
            cids_model.load("tmp")
            weights_initial1_reloaded = cids_model.core_model.get_weights()
            for w1, w2 in zip(weights_initial1, weights_initial1_reloaded):
                np.testing.assert_array_equal(w1, w2)

            print("Continue training")
            cids_model.train_phase(
                train_data, valid_data, 20, batch_size=2, checkpoint=None
            )
            weights_phase2 = cids_model.core_model.get_weights()

            print("Check manual save overwrite")
            cids_model.load("tmp")
            weights_initial1_reloaded = cids_model.core_model.get_weights()
            for w1, w2, _ in zip(
                weights_initial1, weights_initial1_reloaded, weights_phase2
            ):
                np.testing.assert_array_equal(w1, w2)

            print("Check auto checkpoint loading after reset")
            cids_model.clear()
            cids_model.train_phase(
                train_data, valid_data, 30, batch_size=2, checkpoint="last"
            )
            weights_train1 = cids_model.core_model.get_weights()
            cids_model.clear()
            prediction1 = cids_model.predict(dataset[..., input_idx], checkpoint="last")
            weights_train1_reloaded = cids_model.core_model.get_weights()
            for w1, w2 in zip(weights_train1[1:], weights_train1_reloaded[1:]):
                np.testing.assert_array_equal(w1, w2)

            print("Clean infer after reset")
            cids_model.clear()
            strategy = cids_model.strategy
            del cids_model
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                save_freq=10,
                report_freq=1,
                result_dir=result_dir,
                name=name,
            )
            cids_model.strategy = strategy
            cids_model.src_type = "file"
            cids_model.save_best_only = False
            prediction2 = cids_model.predict(dataset[..., input_idx], checkpoint="last")
            weights_clean_infer1 = cids_model.core_model.get_weights()
            # Weights must be identical
            for w1, w2 in zip(weights_clean_infer1[1:], weights_train1_reloaded[1:]):
                np.testing.assert_array_equal(w1, w2)
            # Predictions should be equal
            np.testing.assert_array_equal(prediction1, prediction2)

            print("Clean training after reset")
            cids_model.clear()
            strategy = cids_model.strategy
            del cids_model
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                save_freq=10,
                report_freq=1,
                result_dir=result_dir,
                name=name,
            )
            cids_model.strategy = strategy
            cids_model.train_phase(
                train_data, valid_data, 10, batch_size=2, checkpoint=None
            )
            weights_clean_train1 = cids_model.core_model.get_weights()
            # Weights must be changed
            for w1, w2 in zip(weights_clean_train1[1:], weights_train1_reloaded[1:]):
                with np.testing.assert_raises(AssertionError):
                    np.testing.assert_array_equal(w1, w2)

            print("done")

    def test_freeze_control(self):
        """Tester for NeuralNetwork."""
        tf.keras.backend.clear_session()
        name = inspect.currentframe().f_code.co_name
        with TemporaryDirectory(prefix="cids-test-") as result_dir:
            # Data
            dataset, data_shape, data_format = _normalize_test_dataset()
            input_idx = [0, 1]
            output_idx = [2]
            train_data = dataset
            valid_data = 1000.0 * dataset
            # Architecture
            model = self._core_model_NF(len(output_idx))
            data_definition = create_legacy_data_definition(
                data_shape, data_format, input_idx, output_idx
            )
            cids_model = CIDSModelTF(
                data_definition,
                model,
                report_freq=1,
                save_freq=10,
                result_dir=result_dir,
                name=name,
            )
            cids_model.data_reader.src_type = "placeholder"
            cids_model.data_reader.slice_tensors = True
            history = cids_model.train_phase(
                train_data, valid_data, 10, batch_size=2, checkpoint=None
            )
            print(history)
            weights = {
                n.name: w
                for n, w in zip(
                    cids_model.input_preprocess_model.weights,
                    cids_model.input_preprocess_model.get_weights(),
                )
            }
            np.testing.assert_array_less(weights["InputOnlineNormalize/mean:0"], 1.0)
            np.testing.assert_array_less(weights["InputOnlineNormalize/std:0"], 0.5)
            print("done")


if __name__ == "__main__":
    test.main()
